﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel;


    public class PNRCancellation
    {
        public List<ObjCancelPNR> PnrCan { get; set; }
        public ListOfLists objLists { get; set; }
        public ObjCancelPNR objPnr { get; set; }
        public string OrderID { get; set; }
        public string RejectRemarks { get; set; }
        public string TktNo { get; set; }
        public string PId { get; set; }
        public string TripForQueue { get; set; }
        public string DEK { get; set; }
        #region Cancellation
        public string RefNo { get; set; }
        #endregion
    }

    public class CommonObj
    {
        public string OrderID { get; set; }
        /*public string TktNo { get; set; }*/
    }

    
    public class ListOfLists
    {
        
        public List<Corporate> ObjCorp { get; set; }
        
        public List<Corporate> ObjEntity { get; set; }
        
        public string CancellationChg { get; set; }
        
        public string ServiceChg { get; set; }
        
        public string RefundAmt { get; set; }
        
        public string Remarks { get; set; }
        
        public string PaymentType { get; set; }
        
        public string FopCode { get; set; }
        
        public string CorpEntityID { get; set; }
        
        public string CardNo { get; set; }
        
        public string CardType { get; set; }
        
        public string GdsPnr { get; set; }
        
        public string Counter { get; set; }
        
        public string OrderID { get; set; }
        
        public string IPAdd { get; set; }
        
        public string CorpType { get; set; }
        
        public string TktNo { get; set; }
        
        public string ExecID { get; set; }
        
        public string PaxID { get; set; }
        
        public string TotalChg { get; set; }
        
        public string BookingDate { get; set; }
        
        public string TDS { get; set; }
        
        public string FAREAFTERDIS { get; set; }
        
        public string TktAirline { get; set; }
        
        public string CardHldrName { get; set; }
        
        public string CCExpDate { get; set; }
        
        public List<ObjCancelPNR> PnrCan { get; set; }
        
        public List<clsMasters1> lstFOP { get; set; }
        
        public List<FopCancel> lstFopNew { get; set; }
        
        public string CreditNoteNo { get; set; }
        
        public string EntityID { get; set; }
        
        public string EntityName { get; set; }
        
        public string CCHolderCode { get; set; }
        
        public string BookingPaymentType { get; set; }
        #region Cancellation
        
        public string RefNo { get; set; }
        
        public string ADMINMRK { get; set; }
        #endregion
    }

   
    public class clsMasters1
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string DisplayName { get; set; }
    }

    public class FopCancel
    {
        
        public string OrderID { get; set; }
        
        public string CardNo { get; set; }
        
        public string CardType { get; set; }
        
        public string CreatedDate { get; set; }
        
        public string ExpDate { get; set; }
        
        public string GdsEntry { get; set; }
        
        public string ID { get; set; }
        
        public string Module { get; set; }
        
        public string PayType { get; set; }
        
        public string CVV { get; set; }
        
        public string CardHldrName { get; set; }
    }


    
    public class Corporate
    {
        
        public string GROUPCODE { get; set; }
        
        public string GROUPNAME { get; set; }
        
        public string Address1 { get; set; }
        
        public string Address { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Country { get; set; }
        
        public string ZipCode { get; set; }
        
        public string TDSGroup { get; set; }
        
        public string TFGroup { get; set; }
        
        public string STGroup { get; set; }
        
        public string DisGroup { get; set; }
        
        public string ENAME { get; set; }
        
        public string ECODE { get; set; }
        
        public string CRELIMIT { get; set; }
        
        public string ISENTITY { get; set; }
        
        public string CorpType { get; set; }
    }
    
    public class Segments
    {
        
        public string Sector { get; set; }
        
        public string FltId { get; set; }
        
        public bool SelectedSector { get; set; }
    }
   
    public class FOPDetailsCan
    {
        
        public string CorpID { get; set; }
        
        public string SentFOPCode { get; set; }
        
        public string FopModeFetched { get; set; }
        
        public string CardNo { get; set; }
        
        public string CardType { get; set; }
        
        public string ChargeType { get; set; }
        
        public string ChargeAmt { get; set; }
        
        public string HolderName { get; set; }
        
        public string CVV { get; set; }
        
        public string CardExpDate { get; set; }
        
        public string PTypeCode { get; set; }
        
        public string FopType { get; set; }
        
        public string GDSType { get; set; }
        
        public string IsCCCharge { get; set; }
        
        public string DisplayName { get; set; }
        
        public string CardID { get; set; }
        
        public string IsActive { get; set; }
        
        public string CardName { get; set; }
    }
    #region Cancellation
    
    public class SegmentDetail
    {
        
        public string PaxId { get; set; }
        
        public string PaxTitle { get; set; }
        
        public string PaxFName { get; set; }
        
        public string PaxLName { get; set; }
        
        public string Sector { get; set; }
        
        public string FltId { get; set; }
        
        public string Segment { get; set; }

        
        public string TicketNumber { get; set; }
        
        public string Class { get; set; }
        
        public string DeptDate { get; set; }
        
        public string DeptTime { get; set; }
        
        public string ArrDate { get; set; }
        
        public string ArrTime { get; set; }
        
        public string VC { get; set; }
        
        public string CancelStatus { get; set; }
        
        public string RefNo { get; set; }

        
        public string DepAirCode { get; set; }
        
        public string ArrAirCode { get; set; }
        
        public string FltNo { get; set; }
        
        public string SegmentStatus { get; set; }
        
        public string ReissueStatus { get; set; }
        
        public string root { get; set; }
        
        public string Error { get; set; }
        
        public string PaxType { get; set; }

        //Start Change by birendra for cancellation
        
        public string Provider { get; set; }
        
        public string GDSPNR { get; set; }
        
        public string AIRLINEPNR { get; set; }
        
        public string Trip { get; set; }
        
        public string CorpId { get; set; }
        
        public string Key { get; set; }
        
        public string ADTFareBasis { get; set; }

        //End Change by birendra for cancellation
    }

    
    public class PaxSegments
    {
        
        public string Error { get; set; }
        
        public string PaxId { get; set; }
        
        public string Sector { get; set; }
        
        public List<SegmentDetail> lstPaxSeg { get; set; }
    }
    
    public class PaxList
    {
        
        public string CCHolderCode { get; set; }
        
        public string BookingPaymentType { get; set; }
        
        public string Segment { get; set; }
        
        public string Segments { get; set; }
        
        public List<Segments> lstSeg { get; set; }
        
        public List<PaxList> lstPaxList { get; set; }
        
        public string OrderID { get; set; }
        
        public string PAXID { get; set; }
        
        public string FNAME { get; set; }
        
        public string LNAME { get; set; }
        
        public string TICKETNUMBER { get; set; }
        
        public string PAXTYPE { get; set; }
        
        public string FAREAFTERDIS { get; set; }
        
        public string CANCELORNOT { get; set; }
        
        public bool Select { get; set; }
        
        public string ExecID { get; set; }
        
        public string AirLinePNR { get; set; }
        
        public string ArrDate { get; set; }
        
        public string ArrTime { get; set; }
        
        public string Base_Fare { get; set; }
        
        public string CorpID { get; set; }
        
        public string CorpName { get; set; }
        
        public string CorpType { get; set; }
        
        public string DepTime { get; set; }
        
        public string Destination { get; set; }
        
        public string EntityID { get; set; }
        
        public string EntityName { get; set; }
        
        public string FlightNo { get; set; }
        
        public string Sector { get; set; }
        
        public string Title { get; set; }
        
        public string Trip { get; set; }
        
        public string TktAirline { get; set; }
        
        public string YQ { get; set; }
        
        public string TotalFare { get; set; }
        
        public string BookingDate { get; set; }
        
        public string RequestedDate { get; set; }
        
        public string GdsPnr { get; set; }
        
        public string Counter { get; set; }
        
        public string TDS { get; set; }
        
        public string FromDate { get; set; }
        
        public string ToDate { get; set; }
        
        public string Status { get; set; }
        
        public string BookingDT { get; set; }
        
        public string CancellationChg { get; set; }
        
        public string CreatedDate { get; set; }
        
        public string Departure { get; set; }
        
        public string DepartureDate { get; set; }
        
        public string UpdatedDate { get; set; }
        
        public string UpdatedRem { get; set; }
        
        public string RefundFare { get; set; }
        
        public string ServiceTax { get; set; }
        
        public string ServiceChg { get; set; }
        
        public string TicketNo { get; set; }
        
        public string CmdType { get; set; }
        
        public string ReqRemarks { get; set; }
        
        public string IsGDS { get; set; }
        
        public string CheckCount { get; set; }
        
        public string AllCount { get; set; }
        
        public string Provider { get; set; }
        
        public string IsPvdr1G { get; set; }
        
        public string CreditNoteNo { get; set; }
        
        public string FareType { get; set; }
        
        public string Class { get; set; }
        
        public string VC { get; set; }
        
        public string FltID { get; set; }
        
        public string ReissueStatus { get; set; }
    }
    
    public class ObjCancelPNR
    {
        
        public string CCHolderCode { get; set; }
        
        public string BookingPaymentType { get; set; }

        
        public string Segments { get; set; }
        
        public List<Segments> lstSeg { get; set; }
        
        public List<ObjCancelPNR> pnrList { get; set; }
        
        public string OrderID { get; set; }
        
        public string PAXID { get; set; }
        
        public string FNAME { get; set; }
        
        public string LNAME { get; set; }
        
        public string TICKETNUMBER { get; set; }
        
        public string PAXTYPE { get; set; }
        
        public string FAREAFTERDIS { get; set; }
        
        public string CANCELORNOT { get; set; }
        
        public bool Select { get; set; }
        
        public string ExecID { get; set; }
        
        public string AirLinePNR { get; set; }
        
        public string ArrTime { get; set; }
        
        public string Base_Fare { get; set; }
        
        public string CorpID { get; set; }
        
        public string CorpName { get; set; }
        
        public string CorpType { get; set; }
        
        public string DepTime { get; set; }
        
        public string Destination { get; set; }
        
        public string EntityID { get; set; }
        
        public string EntityName { get; set; }
        
        public string FlightNo { get; set; }
        
        public string Sector { get; set; }
        
        public string Title { get; set; }
        
        public string Trip { get; set; }
        
        public string TktAirline { get; set; }
        
        public string YQ { get; set; }
        
        public string TotalFare { get; set; }
        
        public string BookingDate { get; set; }
        
        public string RequestedDate { get; set; }
        
        public string GdsPnr { get; set; }
        
        public string Counter { get; set; }
        
        public string TDS { get; set; }
        
        public string FromDate { get; set; }
        
        public string ToDate { get; set; }
        
        public string Status { get; set; }
        
        public string BookingDT { get; set; }
        
        public string CancellationChg { get; set; }
        
        public string CreatedDate { get; set; }
        
        public string Departure { get; set; }
        
        public string DepartureDate { get; set; }
        
        public string UpdatedDate { get; set; }
        
        public string UpdatedRem { get; set; }
        
        public string RefundFare { get; set; }
        
        public string ServiceTax { get; set; }
        
        public string ServiceChg { get; set; }
        
        public string TicketNo { get; set; }
        
        public string CmdType { get; set; }
        
        public string ReqRemarks { get; set; }
        
        public string IsGDS { get; set; }
        
        public string CheckCount { get; set; }
        
        public string AllCount { get; set; }
        
        public string Provider { get; set; }
        
        public string IsPvdr1G { get; set; }
        
        public string CreditNoteNo { get; set; }
        
        public string FareType { get; set; }
        #region Cancellation
        
        public string RefNo { get; set; }
        
        public System.Data.DataTable PAXSEG { get; set; }
        
        public string IPAdd { get; set; }
        
        public string CancelStatus { get; set; }
        
        public string OnlineRefundAmount { get; set; }
        
        public string ADMINMRK { get; set; }
        
        public bool MngFee { get; set; }
        #endregion


        
        public System.Data.DataSet ExcelDS { get; set; }
        
        public string ExportCookieVal { get; set; }
        #region SBT
        
        public string UserType { get; set; }
        #endregion
    }
    public class SegTotalCost
    {
        
        public string Segment { get; set; }
        
        public string Departure { get; set; }
        
        public string Arrival { get; set; }
        
        public string TotalCost { get; set; }
        
        [DefaultValue(0)]
        public float AdtFSur { get; set; }
        
        [DefaultValue(0)]
        public float AdtWO { get; set; }
        
        [DefaultValue(0)]
        public float AdtIN { get; set; }
        
        [DefaultValue(0)]
        public float AdtJN { get; set; }
        
        [DefaultValue(0)]
        public float AdtYR { get; set; }
        
        [DefaultValue(0)]
        public float AdtBfare { get; set; }
        
        [DefaultValue(0)]
        public float AdtOT { get; set; }
        
        [DefaultValue(0)]
        public float AdtFare { get; set; }
    }
    public class MBSPrice
    {
        
        public string Type { get; set; }
        
        public string Segment { get; set; }
        
        public string Price { get; set; }

    }
#endregion
public class VersionUAPI
{
    public static string APIVersion { get { return "v45_0"; } }
    public static string APIVersion_terminal { get { return "v33_0"; } }
    public static string APIVersion_Hotel { get { return "v34_0"; } }
    public static string APIVersion_terminal_Hotel { get { return "v33_0"; } }
}

//public class CredentialList
//{
//    public string TAID { get; set; }
//    public string TAUSERID { get; set; }
//    public string TAPASSWORD { get; set; }
//    public string LOGINID { get; set; }
//    public string PASSWORD { get; set; }
//    public string URL { get; set; }
//    public string PCatagoryCode { get; set; }
//    public string SUPPLIERCode { get; set; }
//    public string AirCode { get; set; }
//    public string SupplierType { get; set; }
//    public string Exprs1 { get; set; }
//    public string Exprs2 { get; set; }
//    public string Exprs3 { get; set; }
//    public string Exprs4 { get; set; }
//    public string Exprs5 { get; set; }
//    public string Exprs6 { get; set; }
//    public bool IsOwnPcc { get; set; }
//    public string IdType { get; set; }
//}

