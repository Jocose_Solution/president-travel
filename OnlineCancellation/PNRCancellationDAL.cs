﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


  public  class PNRCancellationDAL
    {
        private SqlDatabase DBHelper;
        public PNRCancellationDAL(string connStr)
        {
            DBHelper = new SqlDatabase(connStr);
        }

        public List<SegmentDetail> GetCancellationSegmentDetails(string OrderID, string PaxID, string Action)
        {
            List<SegmentDetail> ObjS = new List<SegmentDetail>();
            try
            {
                SqlCommand cmd = new SqlCommand("USP_GetCancellationSegment");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", OrderID.Trim());
                cmd.Parameters.AddWithValue("@PaxId", PaxID.Trim());
                cmd.Parameters.AddWithValue("@Operation", Action.Trim());
                var ds = DBHelper.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            for (int s = 0; s < ds.Tables[0].Rows.Count; s++)
                            {
                                ObjS.Add(new SegmentDetail
                                {
                                    PaxId = Convert.ToString(ds.Tables[0].Rows[s]["PAXPAXID"]),
                                    PaxTitle = Convert.ToString(ds.Tables[0].Rows[s]["TITLE"]),
                                    PaxFName = Convert.ToString(ds.Tables[0].Rows[s]["FNAME"]),
                                    PaxLName = Convert.ToString(ds.Tables[0].Rows[s]["LNAME"]),
                                    FltId = Convert.ToString(ds.Tables[0].Rows[s]["FLTID"]),
                                    Sector = Convert.ToString(ds.Tables[0].Rows[s]["SECTOR"]),
                                    Segment = Convert.ToString(ds.Tables[0].Rows[s]["SEGMENT"]),
                                    TicketNumber = Convert.ToString(ds.Tables[0].Rows[s]["TICKETNUMBER"]),
                                    DeptDate = Convert.ToString(ds.Tables[0].Rows[s]["DEPDATE"]),
                                    DeptTime = Convert.ToString(ds.Tables[0].Rows[s]["DEPTIME"]),
                                    ArrDate = Convert.ToString(ds.Tables[0].Rows[s]["ARRDATE"]),
                                    ArrTime = Convert.ToString(ds.Tables[0].Rows[s]["ARRTIME"]),
                                    Class = Convert.ToString(ds.Tables[0].Rows[s]["CLASS"]),
                                    VC = Convert.ToString(ds.Tables[0].Rows[s]["VC"]),
                                    CancelStatus = Convert.ToString(ds.Tables[0].Rows[s]["CANCELSTATUS"]),
                                    RefNo = Convert.ToString(ds.Tables[0].Rows[s]["CANCELID"]),
                                    DepAirCode = Convert.ToString(ds.Tables[0].Rows[s]["DEPCODE"]),
                                    ArrAirCode = Convert.ToString(ds.Tables[0].Rows[s]["ARRCODE"]),
                                    FltNo = Convert.ToString(ds.Tables[0].Rows[s]["FLTNUMBER"]),
                                    ReissueStatus = Convert.ToString(ds.Tables[0].Rows[s]["REISSUESTATUS"]),
                                    PaxType = Convert.ToString(ds.Tables[0].Rows[s]["PAXTYPE"]),
                                    //Start Change by birendra for cancellation
                                    Provider = Convert.ToString(ds.Tables[0].Rows[s]["Provider"]),
                                    GDSPNR = Convert.ToString(ds.Tables[0].Rows[s]["GDSPNR"]),
                                    AIRLINEPNR = Convert.ToString(ds.Tables[0].Rows[s]["AIRLINEPNR"]),
                                    CorpId = Convert.ToString(ds.Tables[0].Rows[s]["CORPID"]),
                                    Trip = Convert.ToString(ds.Tables[0].Rows[s]["TRIP"]),
                                    ADTFareBasis = Convert.ToString(ds.Tables[0].Rows[s]["ADTFAREBASIS"]),
                                    //END Change by birendra for cancellation
                                });
                            }
                        }
                    }
                }


                ds.Dispose();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
            }
            return ObjS;
        }


    public DataSet GetHdrDetails(string trackid)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd;
        try
        {
            cmd = new SqlCommand("GetFltHdr");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TrackId",trackid.Trim());
             ds = DBHelper.ExecuteDataSet(cmd);


            ds.Dispose();
            cmd.Dispose();
        }
        catch (Exception EX)
        { }

        finally
        {
          
        }
        return ds;


    }

}


