﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data;
using BS_BAL;
using System.Xml;
using BS_SHARED;
using Newtonsoft.Json.Linq;

namespace BS_BAL
{
   public  class EtsService
    {
        string EtsUserId = "";//ConfigurationManager.AppSettings["EtsUserId"].ToString();
        string EtsPassword = "";//ConfigurationManager.AppSettings["EtsPassword"].ToString();
        string EtsHostUrl = "";//ConfigurationManager.AppSettings["EtsHostUrl"].ToString();
        private string DigestAuthentication(string url, string username, string password, string postType, string requestBody)
        {
            string pageContent = "";
            try
            {
                Uri myUri = new Uri(url);
                if (postType == "GET")
                {
                    WebRequest myWebRequest = HttpWebRequest.Create(myUri);
                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)myWebRequest;
                    NetworkCredential myNetworkCredential = new NetworkCredential(username, password);
                    CredentialCache myCredentialCache = new CredentialCache();
                    myCredentialCache.Add(myUri, "Digest", myNetworkCredential);
                    myHttpWebRequest.PreAuthenticate = true;
                    myHttpWebRequest.Method = postType;
                    myHttpWebRequest.Credentials = myCredentialCache;
                    myHttpWebRequest.ContentType = "application/json";
                    WebResponse myWebResponse = myWebRequest.GetResponse();
                    Stream responseStream = myWebResponse.GetResponseStream();
                    StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                    pageContent = myStreamReader.ReadToEnd();
                    responseStream.Close();
                    myWebResponse.Close();
                }
                else
                {
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(myUri);
                    NetworkCredential myNetworkCredential = new NetworkCredential(username, password);
                    CredentialCache myCredentialCache = new CredentialCache();
                    myCredentialCache.Add(myUri, "Digest", myNetworkCredential);
                    httpWebRequest.PreAuthenticate = true;
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = postType;
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = requestBody;
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                        httpWebRequest.Credentials = myCredentialCache;
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            pageContent = streamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                //reading the custom messages sent by the server
                using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    pageContent = "Error";
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_DigestAuthentication_" + postType);
            }
            return pageContent;
        }
        public string GetEtsResponse(string methodName, string postTypes, string requestBody)
        {
            string strURL = "";
            string EtsResponse = "";
            try
            {
                strURL = EtsHostUrl + methodName;
                EtsResponse = DigestAuthentication(strURL, EtsUserId, EtsPassword, postTypes, requestBody);
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_GetEtsResponse_" + methodName);
            }
            return EtsResponse;
        }
        public List<BS_SHARED.SHARED> GetAllEtsSource()
        {
            List<BS_SHARED.SHARED> EtsList = new List<BS_SHARED.SHARED>();
            try
            {
                DataSet ds = new DataSet();
                string stSer = GetEtsResponse("getStations", "GET", "");
                if (stSer.Trim() != "Error")
                {
                    ds = convertJsonStringToDataSet(stSer);
                    if (ds.Tables["apiStatus"].Rows[0]["success"].ToString() == "true")
                        EtsList = ConvertDatasetToList(ds, "", "", "");
                }
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_GetAllEtsSource");
            }
            return EtsList;
        }
        public List<BS_SHARED.SHARED> GetAllEtsDest(string srcName, string srcId)
        {
            List<BS_SHARED.SHARED> EtsDestList = new List<BS_SHARED.SHARED>();
            try
            {
                DataSet ds = new DataSet();
                string stSer = GetEtsResponse("getToStations?sourceStation=" + srcName + "", "GET", "");
                if (stSer.Trim() != "Error" && stSer.ToString().IndexOf("Error") == -1)
                {
                    ds = convertJsonStringToDataSet(stSer);
                    if (ds.Tables["apiStatus"].Rows[0]["success"].ToString() == "true")
                        EtsDestList = ConvertDatasetToList(ds, srcId, srcName, "ES");
                }
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_GetAllEtsDest");
            }
            return EtsDestList;
        }
        public List<BS_SHARED.SHARED> GetAllAvlBus(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> ListAvlEsBus = new List<BS_SHARED.SHARED>();
            List<BS_SHARED.SHARED> farelist; ;
            SharedBAL ESsharedBal = new SharedBAL();

            string ResGetAllAvlBus = "";
            try
            {
                ResGetAllAvlBus = GetEtsResponse("getAvailableBuses?sourceCity=" + shared.src + "&destinationCity=" + shared.dest + "&doj=" + shared.journeyDate + "", "GET", "");

                if (ResGetAllAvlBus != "" && ResGetAllAvlBus != "Error")
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    JObject array = JObject.Parse(ResGetAllAvlBus);
                    RootObject2 objESRootObject = serializer.Deserialize<RootObject2>(array.ToString());
                   // RootObject1 d=serializer.Deserialize<RootObject1>(array.ToString());
                    try
                    {
                        for (int i = 0; i < objESRootObject.apiAvailableBuses.Count; i++)
                        {
                            farelist = new List<BS_SHARED.SHARED>();
                            shared.fare = objESRootObject.apiAvailableBuses[i].fare;
                            shared.traveler = objESRootObject.apiAvailableBuses[i].operatorName;
                            farelist = ESsharedBal.getCommissionList(shared);
                            shared.seat_farewithMarkp = new string[farelist.Count];
                            shared.seat_Originalfare = new string[farelist.Count];
                            shared.Arr_adcomm = new decimal[farelist.Count];
                            shared.Arr_taTds = new decimal[farelist.Count];
                            shared.Arr_serviceChrg = new decimal[farelist.Count];
                            shared.Arr_taNetFare = new decimal[farelist.Count];
                            shared.Arr_taTotFare = new decimal[farelist.Count];
                            shared.Arr_totFare = new decimal[farelist.Count];
                            for (int b = 0; b < farelist.Count; b++)
                            {
                                shared.seat_Originalfare[b] = objESRootObject.apiAvailableBuses[i].fare.Split(',')[b];
                                shared.seat_farewithMarkp[b] = Convert.ToString(Math.Round(farelist[b].taNetFare, 0));
                                shared.Arr_adcomm[b] = farelist[b].adcomm;
                                shared.Arr_taTds[b] = farelist[b].taTds;
                                shared.Arr_serviceChrg[b] = farelist[b].serviceChrg;
                                shared.Arr_taNetFare[b] = farelist[b].taNetFare;
                                shared.Arr_taTotFare[b] = farelist[b].taTotFare;
                                shared.Arr_totFare[b] = farelist[b].totFare;
                            }
                            shared.bdPoint = array["apiAvailableBuses"][i]["boardingPoints"].Cast<JObject>().Select(x => x.ToString()).ToArray();                                                                         ///
                            shared.drPoint = array["apiAvailableBuses"][i]["droppingPoints"].Cast<JObject>().Select(x => x.ToString()).ToArray();  //((IEnumerable)objESRootObject.apiAvailableBuses[i].droppingPoints).Cast<object>().Select(x => x.ToString()).ToArray();
                            if (objESRootObject.apiAvailableBuses[i].droppingPoints == null || objESRootObject.apiAvailableBuses[i].droppingPoints.Count == 0)
                            {
                                shared.drPoint = new string[1];
                                shared.drPoint[0] = "{\"location\":\"" + shared.dest + " \",\"id\":\"" + shared.destID + "\",\"time\":\"" + objESRootObject.apiAvailableBuses[i].arrivalTime + "\"}";
                            }
                            shared.canPolicy_RB = array["apiAvailableBuses"][i]["cancellationPolicy"].ToString();
                            string svcInvRutOp = objESRootObject.apiAvailableBuses[i].serviceId + "," + objESRootObject.apiAvailableBuses[i].inventoryType + "," + objESRootObject.apiAvailableBuses[i].routeScheduleId + "," + objESRootObject.apiAvailableBuses[i].operatorId;
                            if (objESRootObject.apiAvailableBuses[i].boardingPoints == null || objESRootObject.apiAvailableBuses[i].boardingPoints.Count == 0)
                            {
                                shared.bdPoint = new string[1];
                                shared.bdPoint[0] = "{\"location\":\"" + shared.src + " \",\"id\":\"" + shared.srcID + "\",\"time\":\"" + objESRootObject.apiAvailableBuses[i].departureTime + "\"}";
                            }
                            ListAvlEsBus.Add(new BS_SHARED.SHARED { src = shared.src, dest = shared.dest, srcID = shared.srcID, destID = shared.destID, serviceID = svcInvRutOp, traveler = objESRootObject.apiAvailableBuses[i].operatorName, serviceType = objESRootObject.apiAvailableBuses[i].busType, seat_farewithMarkp = shared.seat_farewithMarkp, seat_Originalfare = shared.seat_Originalfare, remainingSeat = Convert.ToInt32(objESRootObject.apiAvailableBuses[i].availableSeats), provider_name = "ES", partialCanAllowed = Convert.ToString(objESRootObject.apiAvailableBuses[i].partialCancellationAllowed), idproofReq = Convert.ToString(objESRootObject.apiAvailableBuses[i].idProofRequired), arrTime = objESRootObject.apiAvailableBuses[i].arrivalTime, departTime = objESRootObject.apiAvailableBuses[i].departureTime, Dur_Time = "00:00", Arr_adcomm = shared.Arr_adcomm, Arr_taTds = shared.Arr_taTds, Arr_serviceChrg = shared.Arr_serviceChrg, Arr_taNetFare = shared.Arr_taNetFare, Arr_taTotFare = shared.Arr_taTotFare, Arr_totFare = shared.Arr_totFare, bdPoint = shared.bdPoint, drPoint = shared.drPoint, canPolicy_RB = shared.canPolicy_RB });

                        }
                    }
                    catch (Exception ex) { }
                }
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_GetAllAvlBus");
            }
            return ListAvlEsBus;
        }
        public string ESSeatLayout(BS_SHARED.SHARED shared)
        {
            string tripDetails = ""; List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            SharedBAL sharedbal = new SharedBAL(); bool UP_LPlength = false;
            string layout_lower = "<table cellpadding='0' cellspacing='0'  id='seatTbllower_RB' class='tbl' border='0px'>";
            string layout_upper = "<table cellpadding='0' cellspacing='0'  id='seatTblupper_RB' class='tbl' border='0px'>";
            string seatLayout = "<table width='100%' cellpadding='0' cellspacing='0'>";
            seatLayout += "<tr>";
            seatLayout += "<td class='seatLayoutheaderSelect'>Select Your Seat:</td>";
            seatLayout += "</tr>";
            seatLayout += "<tr>";
            DateTime date = Convert.ToDateTime(shared.journeyDate.Trim().Replace("-", "/"));
            seatLayout += "<td class='seatLayoutheader'>" + shared.src.Trim() + " To " + shared.dest.Trim() + " on, " + date.DayOfWeek + " " + date.Day + " " + date.ToString("MMM") + " " + date.Year + " (" + shared.traveler + ")</td>";
            seatLayout += "</tr>";
            try
            {
                int inventryId = Convert.ToInt32(shared.serviceID.ToString().Split(',')[1]); string RouteSedId = Convert.ToString(shared.serviceID.ToString().Split(',')[2]);
                tripDetails = GetEtsResponse("getBusLayout?sourceCity=" + shared.src + "&destinationCity=" + shared.dest + "&doj=" + shared.journeyDate + "&inventoryType=" + inventryId + "&routeScheduleId=" + RouteSedId + "", "GET", "");
                if (tripDetails.Trim() != "Error")
                {
                    JObject arrayS = JObject.Parse(tripDetails);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RootObject2 objRootObject = new RootObject2();
                    RootObject2 LIRootObject = new RootObject2();
                    RootObject2 LIURootObject = new RootObject2();
                    RootObject2 collection = serializer.Deserialize<RootObject2>(tripDetails.ToString());
                    objRootObject.seats = collection.seats;
                    string[] maxRowcol = getMaxRowCol(objRootObject);
                    int lr = 0; int Ur = 0;
                    for (int R = 0; R <= Convert.ToInt32(maxRowcol[0]); R++)
                    {
                        LIRootObject.seats = objRootObject.seats.OrderBy(x => Convert.ToInt32(x.column)).Where(x => x.row.ToString() == Convert.ToString(R)).ToList().Where(u => u.zIndex.ToString() == "0").ToList();
                        LIURootObject.seats = objRootObject.seats.OrderBy(u => Convert.ToInt32(u.column)).Where(u => u.row.ToString() == Convert.ToString(R)).ToList().Where(u => u.zIndex.ToString() == "1").ToList();
                        if (LIRootObject.seats.Count != 0)
                        {
                            lr++;
                            layout_lower += SET_UPPER_LOWER(LIRootObject, Convert.ToInt32(maxRowcol[1]), list, shared);
                        }
                        if (LIURootObject.seats.Count != 0)
                        {
                            Ur++;
                            UP_LPlength = true;
                            layout_upper += SET_UPPER_LOWER(LIURootObject, Convert.ToInt32(maxRowcol[1]), list, shared);
                        }
                        if (lr == 2)
                        {
                            if (LIRootObject.seats.Count < 1)
                                layout_lower += "<tr><td colspan='" + Convert.ToInt32(Convert.ToInt32(maxRowcol[1]) * 2) + 1 + "'><div  style='height:30px;width:30px;'>&nbsp;</div></td></tr>";
                        }
                        if (Ur == 2)
                        {
                            if (LIURootObject.seats.Count < 1)
                                layout_upper += "<tr><td colspan='" + Convert.ToInt32(Convert.ToInt32(maxRowcol[1]) * 2) + 1 + "'><div  style='height:30px;width:30px;'>&nbsp;</div></td></tr>";

                        }

                    }
                    layout_lower += "</table>";
                    layout_upper += "</table>";
                    if (UP_LPlength == true)
                    {
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'>Upper</span>";
                        seatLayout += layout_upper;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'>Lower</span>";
                        seatLayout += layout_lower;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "</table>";
                    }
                    else
                    {
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'></span>";
                        seatLayout += layout_lower;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "</table>";
                    }
                    if (arrayS["inventoryType"].ToString() == "2" || arrayS["inventoryType"].ToString() == "4")
                    {
                        string strBoardPoints = "";
                        strBoardPoints += "<div style='display:none;' class='ESboardPoint' id='ESboardPoint'>";
                        strBoardPoints += "<select name='board' id='board' style='width:150px;' class='drpBox'>";
                        for (int bd = 0; bd < Convert.ToInt32(arrayS["boardingPoints"].ToArray().Length); bd++)
                        {
                            strBoardPoints += "<option value='" + arrayS["boardingPoints"][bd]["id"].ToString() + "'>" + arrayS["boardingPoints"][bd]["location"].ToString() + "(" + arrayS["boardingPoints"][bd]["time"].ToString() + ")" + "</option>";
                        }
                        strBoardPoints += "</select>";
                        strBoardPoints += "</div>";
                        seatLayout += strBoardPoints;
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_ESSeatLayout");
                seatLayout = "Error";
            }
            return seatLayout;
        }
        public SHARED GetESblockKey(SHARED shared)
        {
            string key = ""; string list = ""; string[] name = null;
            string LastName = "";
            try
            {
                RootObject1 objBlocktkeyReq = new RootObject1();
                BoardingPoint boardingPoint = new BoardingPoint();
                List<BlockSeatPaxDetail> blockSeatPaxDetails = new List<BlockSeatPaxDetail>();
                objBlocktkeyReq.sourceCity = shared.src.Trim();
                objBlocktkeyReq.destinationCity = shared.dest.Trim();
                objBlocktkeyReq.doj = shared.journeyDate.Trim();
                objBlocktkeyReq.routeScheduleId = shared.serviceID.Split(',')[2];
                boardingPoint.id = shared.boardpointid.Trim();
                boardingPoint.location = shared.boardpoint;
                boardingPoint.time = shared.boardpoint.Substring(shared.boardpoint.LastIndexOf("(")).Replace("(", "").Replace(")", "");
                objBlocktkeyReq.boardingPoint = boardingPoint;
                objBlocktkeyReq.customerName = shared.paxname[0].Split(',')[0].Trim();
                if (shared.paxname[0].Split(',')[0].IndexOf(' ') == -1)
                    objBlocktkeyReq.customerLastName = "";
                else
                    objBlocktkeyReq.customerLastName = shared.paxname[0].Split(',')[0].Substring(shared.paxname[0].Split(',')[0].LastIndexOf(' '));
                objBlocktkeyReq.customerEmail = shared.paxemail;
                objBlocktkeyReq.customerPhone = shared.paxmob;
                objBlocktkeyReq.emergencyPhNumber = shared.paxmob;
                objBlocktkeyReq.customerAddress = shared.paxaddress;
                for (int a = 0; a <= shared.paxseat.Count - 1; a++)
                {
                    name = shared.paxname[a].ToString().Split(',');
                    if (name[0].IndexOf(' ') == -1)
                        LastName = "";
                    else
                        LastName = name[0].Substring(name[0].LastIndexOf(' '));
                    blockSeatPaxDetails.Add(new BlockSeatPaxDetail { age = shared.paxage[a], name = shared.paxname[a].ToString().Split(',')[0], seatNbr = shared.paxseat[a].Trim(), sex = shared.gender[a].Trim().Substring(0, 1), fare = Convert.ToDecimal(shared.perOriginalFare[a]), totalFareWithTaxes = Convert.ToDecimal(shared.totWithTaxes.Split('*')[a]), ladiesSeat = Convert.ToBoolean(shared.ladiesSeat.Trim().Split(',')[a]), lastName = LastName, mobile = shared.paxmob, title = shared.title[a].Trim(), email = shared.paxemail.Trim(), idType = shared.idtype, idNumber = shared.idnumber, nameOnId = shared.paxname[a].ToString().Split(',')[0], primary = Convert.ToBoolean(shared.Isprimary), ac = Convert.ToBoolean(shared.AC_NONAC.Split('*')[a]), sleeper = Convert.ToBoolean(shared.SEAT_TYPE.Split('*')[a]) });
                }
                objBlocktkeyReq.blockSeatPaxDetails = blockSeatPaxDetails;
                objBlocktkeyReq.inventoryType = Convert.ToInt32(shared.serviceID.Split(',')[1].Trim());
                var json = new JavaScriptSerializer().Serialize(objBlocktkeyReq);
                list = json;
                key = GetEtsResponse("blockTicket", "POST", json);

                JObject array = JObject.Parse(key);
                shared.blockKey = array["blockTicketKey"].ToString();
                shared.bookreq = json;
                shared.bookres = key;
                string Status = array["apiStatus"]["success"].ToString();
                if (Status == "False" || key == "" && key.ToString().Contains("Error") == true && key.ToString().Contains("error") == true)
                {
                    if (key == "" || key == "Error")
                        key = "Error";
                    else
                        key = "Error_" + array["apiStatus"]["message"].ToString();
                }
                else
                    key = array["blockTicketKey"].ToString();
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_GetESblockKey_" + shared.orderID);
                shared.blockKey = key;
                shared.bookreq = list;
                shared.bookres = key;
            }

            return shared;
        }
        //public SHARED GetESblockKey(SHARED shared)
        //{
        //    string key = ""; string list = ""; string[] name = null;
        //    string LastName = "";
        //    try
        //    {
        //        RootObject1 objBlocktkeyReq = new RootObject1();
        //        BoardingPoint boardingPoint = new BoardingPoint();
        //        List<BlockSeatPaxDetail> blockSeatPaxDetails = new List<BlockSeatPaxDetail>();
        //        objBlocktkeyReq.sourceCity = shared.src.Trim();
        //        objBlocktkeyReq.destinationCity = shared.dest.Trim();
        //        objBlocktkeyReq.doj = shared.journeyDate.Trim();
        //        objBlocktkeyReq.routeScheduleId = shared.serviceID.Split(',')[2];
        //        boardingPoint.id = shared.boardpointid.Trim();
        //        boardingPoint.location = shared.boardpoint;
        //        boardingPoint.time = shared.boardpoint.Substring(shared.boardpoint.LastIndexOf("(")).Replace("(", "").Replace(")", "");
        //        objBlocktkeyReq.boardingPoint = boardingPoint;
        //        objBlocktkeyReq.customerName = shared.paxname[0].Split(',')[0].Trim();
        //        if (shared.paxname[0].Split(',')[0].IndexOf(' ') == -1)
        //            objBlocktkeyReq.customerLastName = "";
        //        else
        //            objBlocktkeyReq.customerLastName = shared.paxname[0].Split(',')[0].Substring(shared.paxname[0].Split(',')[0].LastIndexOf(' '));
        //        objBlocktkeyReq.customerEmail = shared.paxemail;
        //        objBlocktkeyReq.customerPhone = shared.paxmob;
        //        objBlocktkeyReq.emergencyPhNumber = shared.paxmob;
        //        objBlocktkeyReq.customerAddress = shared.paxaddress;
        //        for (int a = 0; a <= shared.paxseat.Count - 1; a++)
        //        {
        //            name = shared.paxname[a].ToString().Split(',');
        //            if (name[0].IndexOf(' ') == -1)
        //                LastName = "";
        //            else
        //                LastName = name[0].Substring(name[0].LastIndexOf(' '));
        //            //blockSeatPaxDetails.Add(new BlockSeatPaxDetail { age = shared.paxage[a], name = shared.paxname[a].ToString().Split(',')[0], seatNbr = shared.paxseat[a].Trim(), sex = shared.gender[a].Trim().Substring(0, 1), fare = Convert.ToInt32(shared.perOriginalFare[a].ToString()), ladiesSeat = Convert.ToBoolean(shared.ladiesSeat.Trim().Split(',')[a]), lastName = LastName, mobile = shared.paxmob, title = shared.title[a].Trim(), email = shared.paxemail.Trim(), idType = shared.idtype, idNumber = shared.idnumber, nameOnId = shared.paxname[a].ToString().Split(',')[0], primary = Convert.ToBoolean(shared.Isprimary), ac = Convert.ToBoolean(shared.serviceID.Split(',')[4].Split('*')[a]), sleeper = Convert.ToBoolean(shared.serviceID.Split(',')[5].Split('*')[a]) });
        //            blockSeatPaxDetails.Add(new BlockSeatPaxDetail { age = shared.paxage[a], name = shared.paxname[a].ToString().Split(',')[0], seatNbr = shared.paxseat[a].Trim(), sex = shared.gender[a].Trim().Substring(0, 1), fare = Convert.ToDecimal(shared.perOriginalFare[a]), ladiesSeat = Convert.ToBoolean(shared.ladiesSeat.Trim().Split(',')[a]), lastName = LastName, mobile = shared.paxmob, title = shared.title[a].Trim(), email = shared.paxemail.Trim(), idType = shared.idtype, idNumber = shared.idnumber, nameOnId = shared.paxname[a].ToString().Split(',')[0], primary = Convert.ToBoolean(shared.Isprimary), ac = Convert.ToBoolean(shared.serviceID.Split(',')[4].Split('*')[a]), sleeper = Convert.ToBoolean(shared.serviceID.Split(',')[5].Split('*')[a]) });
        //        }
        //        objBlocktkeyReq.blockSeatPaxDetails = blockSeatPaxDetails;
        //        objBlocktkeyReq.inventoryType = Convert.ToInt32(shared.serviceID.Split(',')[1].Trim());
        //        var json = new JavaScriptSerializer().Serialize(objBlocktkeyReq);
        //        list = json;
        //        key = GetEtsResponse("blockTicket", "POST", json);

        //        JObject array = JObject.Parse(key);
        //        shared.blockKey = array["blockTicketKey"].ToString();
        //        shared.bookreq = json;
        //        shared.bookres = key;
        //        string Status = array["apiStatus"]["success"].ToString();
        //        if (Status == "False" || key == "" && key.ToString().Contains("Error") == true && key.ToString().Contains("error") == true)
        //        {
        //            if (key == "" || key == "Error")
        //                key = "Error";
        //            else
        //                key = "Error_" + array["apiStatus"]["message"].ToString();
        //        }
        //        else
        //            key = array["blockTicketKey"].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
        //        erlog.writeErrorLog(ex, "EtsService_GetESblockKey_" + shared.orderID);
        //        shared.blockKey = key;
        //        shared.bookreq = list;
        //        shared.bookres = key;
        //    }

        //    return shared;
        //}
        public List<BS_SHARED.SHARED> GetFinalBookingES(SHARED shared)
        {
            string BookResponse = "";
            BS_DAL.SharedDAL shareddal = new BS_DAL.SharedDAL();
            List<BS_SHARED.SHARED> list = new List<SHARED>();
            try
            {
                BookResponse = GetEtsResponse("seatBooking?blockTicketKey=" + "ABCDEFGHIJKLMNOP", "GET", "");
                //BookResponse = GetEtsResponse("seatBooking?blockTicketKey=" + shared.blockKey, "GET", "");
                JObject array = JObject.Parse(BookResponse);
                JObject Rarray = JObject.Parse(BookResponse);
                string ticketDTLS = "";
                ticketDTLS = getETSTicketdetails(array["etstnumber"].ToString());
                string Rstatus = Rarray["apiStatus"]["success"].ToString();
                string Status = array["apiStatus"]["success"].ToString();
                if (Status == "False" || BookResponse == "" && BookResponse.ToString().Contains("Error") == true && BookResponse.ToString().Contains("error") == true)
                {
                    if (BookResponse == "" || BookResponse == "Error")
                        BookResponse = "Error";
                    else
                        BookResponse = "Error_" + array["apiStatus"]["message"].ToString();
                    list.Add(new BS_SHARED.SHARED { bookreq = "seatBooking?blockTicketKey=â+" + shared.blockKey, bookres = BookResponse, status = "Fail" });
                    shared.bookreq = "seatBooking?blockTicketKey=" + shared.blockKey;
                    shared.bookres = BookResponse;
                    shareddal.updateBookResponse(shared);
                }
                else
                {
                    shareddal.UPDATEBOOKING_STATUS(shared.orderID, "Booked", array["opPNR"].ToString());
                    if (Rstatus == "True")
                        list.Add(new BS_SHARED.SHARED { bookreq = "seatBooking?blockTicketKey=â+" + shared.blockKey, bookres = ticketDTLS, blockKey = shared.blockKey.Trim(), tin = array["etstnumber"].ToString(), pnr = array["opPNR"].ToString(), partialCancel = shared.partialCanAllowed, status = "success" });
                    else
                    list.Add(new BS_SHARED.SHARED { bookreq = "seatBooking?blockTicketKey=â+" + shared.blockKey, bookres = BookResponse, blockKey = shared.blockKey.Trim(), tin = array["etstnumber"].ToString(), pnr = array["opPNR"].ToString(), partialCancel = shared.partialCanAllowed, status = "success" });
                }
            }
            catch (Exception ex)
            {
                shared.bookreq = "seatBooking?blockTicketKey=" + shared.blockKey;
                shared.bookres = BookResponse+"_Error_"+ex.Message;
                shareddal.updateBookResponse(shared);
                list.Add(new BS_SHARED.SHARED { bookres = BookResponse, status = "Fail" });
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_GetFinalBookingES_" + shared.orderID);
            }
            return list;
        }
        public string getETSTicketdetails(string ETSticketNo)
        {
            string strticketDetalis = "";
            try
            {
                strticketDetalis = GetEtsResponse("getTicketByETSTNumber?ETSTNumber=" + ETSticketNo, "GET", "");
            }
            catch (Exception ex)
            {
            }
            return strticketDetalis;
        }



        public string[] ESGetCancelTicket(SHARED shared)
        {
            List<SHARED> ESlist = new List<SHARED>();
            BS_DAL.SharedDAL shareddal = new BS_DAL.SharedDAL();
            string[] s; string ss = "";
            string CanReq = ""; string CanRes = ""; string[] canstr = new string[4]; double busfare = 0;
            double canCharge = 0;
            double RefCharge = 0;
            try
            {
                s = shared.seat.Split(',');
                for (int y = 0; y < s.Length; y++)
                {
                    if (s[y] != "")
                        ss += "\"" + s[y].Trim() + "\"" + ",";
                }
                ss = ss.Remove(ss.LastIndexOf(","));
                CanReq = "{\"etsTicketNo\":\"" + shared.tin + "\",\"seatNbrsToCancel\":[" + ss.Trim() + "]}";
                CanRes = GetEtsResponse("cancelTicket", "POST", CanReq);
                shared.canrequest = CanReq;
                shared.canresponse = CanRes;
                shareddal.InsertUpdateCanREQ(shared, "Insert");
                if (CanRes != "" && CanRes.Contains("Error") == false)
                {
                    #region[save cancel request into db]
                    shareddal.InsertUpdateCanREQ(shared, "Update");
                    #endregion
                    JObject array = JObject.Parse(CanRes);
                    if (array["apiStatus"]["success"].ToString() == "False")
                    {
                        shared.canresponse = CanRes;
                        shareddal.InsertUpdateCanREQ(shared, "Update");
                        canstr[0] = "Fail, This Ticket is not cancellable:Invalid Ticket";
                        canstr[1] = CanRes;
                    }
                    else
                    {
                        shared.canresponse = CanRes;
                        shareddal.InsertUpdateCanREQ(shared, "Update");
                        string[] spltfare = shared.fare.Split(',');
                        string[] splitnetfare = shared.netfare.Split(',');
                        for (int b = 0; b < spltfare.Length; b++)
                        {
                            if (spltfare[b] != "")
                            {
                                busfare = Convert.ToDouble((Convert.ToDouble(spltfare[b]) * Convert.ToDouble(array["cancelChargesPercentage"].ToString().Replace("%", ""))) / 100);
                                shared.cancelRecharge += Convert.ToString(busfare) + ",";
                                shared.refundAmt += Convert.ToString(Convert.ToDouble(splitnetfare[b]) - busfare) + ",";
                                canCharge += busfare;
                                RefCharge += Convert.ToDouble(splitnetfare[b]) - busfare;
                            }
                        }
                        //-------------------------------insert into database----------------------------------//
                        shareddal.updateCanchrgandRefund(shared);
                        shared.refundAmt = shared.refundAmt.Remove(shared.refundAmt.LastIndexOf(","));
                        string[] addfare = shared.refundAmt.Split(',');
                        for (int x = 0; x < addfare.Length; x++)
                        {
                            if (addfare[x] != "")
                                shared.addAmt += Convert.ToDecimal(addfare[x].Trim());
                        }
                        shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                        shareddal.insertLedgerDetails(shared, "Add");
                        canstr[0] = "Success";
                        canstr[1] = Convert.ToString(canCharge);
                        canstr[2] = Convert.ToString(RefCharge);
                        canstr[3] = shared.tin;
                    }
                }
                else
                {
                    #region[save cancel request into db]
                    shared.canresponse = CanRes;
                    shareddal.InsertUpdateCanREQ(shared, "Update");
                    canstr[0] = "Fail, This Ticket is not cancellable:Invalid Ticket";
                    canstr[1] = CanRes;
                    #endregion
                }
            }
            catch (Exception ex)
            {
            }
            return canstr;
        }
        private string[] getMaxRowCol(RootObject2 listObj)
        {
            string[] row = new string[listObj.seats.Count];
            string[] col = new string[listObj.seats.Count];
            string[] max = new string[2];
            try
            {
                var a = from s in listObj.seats
                        select new { row = int.Parse(s.row.ToString()), col = int.Parse(s.column.ToString()) };
                max[0] = Convert.ToString(a.Select(x => x.row).Max());
                max[1] = Convert.ToString(a.Select(x => x.col).Max());
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_getMaxRowCol");
            }
            return max;
        }
        public string SET_UPPER_LOWER(RootObject2 LIRootObjects, int maxrow, List<BS_SHARED.SHARED> list, BS_SHARED.SHARED shared)
        {
            SharedBAL sharedbal = new SharedBAL();
            string birthLayout = "";
            birthLayout += "<tr>";
            int s = 0;

            for (int C = 0; C <= maxrow; C++)
            {
                if (C == Convert.ToInt32(LIRootObjects.seats[s].column.ToString().Trim()))
                {
                    string CheckConditions = ""; CheckConditions = LIRootObjects.seats[s].zIndex.ToString() + "," + LIRootObjects.seats[s].length.ToString() + "," + LIRootObjects.seats[s].width.ToString();
                    string ladiesSeat = ""; ladiesSeat = LIRootObjects.seats[s].ladiesSeat.ToString().ToLower();
                    string available = ""; available = LIRootObjects.seats[s].available.ToString().ToLower();
                    string divShow = "";
                    string svctype = ""; string Seattype = "";
                    if (LIRootObjects.seats[s].fare != null)
                    {
                        shared.fare = LIRootObjects.seats[s].fare.ToString().Trim();
                        list = sharedbal.getCommissionList(shared);
                        if (LIRootObjects.seats[s].ac.ToString() == "False") svctype = "NON AC"; else svctype = "AC";
                        if (LIRootObjects.seats[s].sleeper.ToString() == "False") Seattype = "SEATER"; else Seattype = "SELEEPER";
                        list[0].taTotFare += Convert.ToDecimal(LIRootObjects.seats[s].serviceTaxAmount);
                        divShow += "<div style='line-height:30px;'><div>Seat No:" + LIRootObjects.seats[s].id.ToString() + "</div><div>Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "</div>";
                        divShow += "<div>Service Type:" + svctype + "</div><div>Seat Type:" + Seattype + "</div><div>" + LIRootObjects.seats[s].serviceTaxAmount + "</div><div>" + LIRootObjects.seats[s].serviceTaxPer + "</div><div>" + LIRootObjects.seats[s].totalFareWithTaxes + "</div><div>" + LIRootObjects.seats[s].serviceTaxApplicable + "</div></div>";
                        birthLayout += "<td class='ES'><div class='" + SetClassName(CheckConditions, ladiesSeat, available) + "'  title='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "' rel='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + LIRootObjects.seats[s].fare.ToString() + "| Provider:" + shared.provider_name + "'></div><br /><div id='" + LIRootObjects.seats[s].id.Replace(" ", "").Replace("(", "").Replace(")", "") + "' class='divSeatshow'>" + divShow + "</div></td>";
                    }
                    else
                        birthLayout += "<td class='ES'><div class='" + SetClassName(CheckConditions, ladiesSeat, available) + "'  title='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + 0 + "' rel='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + 0 + "| Provider:" + shared.provider_name + "'></div><br /><div id='" + LIRootObjects.seats[s].id.Replace(" ", "").Replace("(", "").Replace(")", "") + "' class='divSeatshow'>Booked Seat</div></td>";
                    if (C < Convert.ToInt32(LIRootObjects.seats[Convert.ToInt32(LIRootObjects.seats.Count - 1)].column.ToString().Trim()))
                        s = s + 1;
                }
                else
                {
                    birthLayout += "<td><div>&nbsp;</div></td>";
                }
            }
            birthLayout += "</tr>";


            return birthLayout;
        }

        //public string SET_UPPER_LOWER(RootObject2 LIRootObjects, int maxrow, List<BS_SHARED.SHARED> list, BS_SHARED.SHARED shared)
        //{
        //    SharedBAL sharedbal = new SharedBAL();
        //    string birthLayout = "";
        //    birthLayout += "<tr>";
        //    int s = 0;

        //    for (int C = 0; C <= maxrow; C++)
        //    {
        //        if (C == Convert.ToInt32(LIRootObjects.seats[s].column.ToString().Trim()))
        //        {
        //            string CheckConditions = ""; CheckConditions = LIRootObjects.seats[s].zIndex.ToString() + "," + LIRootObjects.seats[s].length.ToString() + "," + LIRootObjects.seats[s].width.ToString();
        //            string ladiesSeat = ""; ladiesSeat = LIRootObjects.seats[s].ladiesSeat.ToString().ToLower();
        //            string available = ""; available = LIRootObjects.seats[s].available.ToString().ToLower();
        //            string divShow = "";
        //            string svctype = ""; string Seattype = "";
        //            if (LIRootObjects.seats[s].fare != null)
        //            {
        //                shared.fare = LIRootObjects.seats[s].fare.ToString().Trim();
        //                list = sharedbal.getCommissionList(shared);
        //                if (LIRootObjects.seats[s].ac.ToString() == "False") svctype = "NON AC"; else svctype = "AC";
        //                if (LIRootObjects.seats[s].sleeper.ToString() == "False") Seattype = "SEATER"; else Seattype = "SELEEPER";

        //                divShow += "<div style='line-height:30px;'><div>Seat No:" + LIRootObjects.seats[s].id.ToString() + "</div><div>Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "</div>";
        //                divShow += "<div>Service Type:" + svctype + "</div><div>Seat Type:" + Seattype + "</div></div>";
        //                birthLayout += "<td class='ES'><div class='" + SetClassName(CheckConditions, ladiesSeat, available) + "'  title='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "' rel='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + LIRootObjects.seats[s].fare.ToString() + "| Provider:" + shared.provider_name + "'></div><br /><div id='" + LIRootObjects.seats[s].id.Replace(" ", "").Replace("(", "").Replace(")", "") + "' class='divSeatshow'>" + divShow + "</div></td>";
        //            }
        //            else
        //                birthLayout += "<td class='ES'><div class='" + SetClassName(CheckConditions, ladiesSeat, available) + "'  title='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + 0 + "' rel='Seat No:" + LIRootObjects.seats[s].id.ToString() + " | Fare:" + 0 + "| Provider:" + shared.provider_name + "'></div><br /><div id='" + LIRootObjects.seats[s].id.Replace(" ", "").Replace("(", "").Replace(")", "") + "' class='divSeatshow'>Booked Seat</div></td>";
        //            if (C < Convert.ToInt32(LIRootObjects.seats[Convert.ToInt32(LIRootObjects.seats.Count - 1)].column.ToString().Trim()))
        //                s = s + 1;
        //        }
        //        else
        //        {
        //            birthLayout += "<td><div>&nbsp;</div></td>";
        //        }
        //    }
        //    birthLayout += "</tr>";


        //    return birthLayout;
        //}
        public string SetClassName(string CheckConditions, string ladiesSeat, string available)
        {
            string divclass = "";
            if (ladiesSeat == "true")    // check for Female
            {
                if (available == "true")
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divLadies";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperLadies";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperLadies";
                }
                else
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divBlockladies";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperBlockladies";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperBlockladies";
                }
            }
            else
            {
                if (available == "true")
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divAval";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperAval";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperAval";
                }
                else
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divBlock";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperBlock";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperBlock";
                }
            }
            return divclass;
        }
        private List<BS_SHARED.SHARED> ConvertDatasetToList(DataSet ds, string srcid, string src, string providername)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            try
            {
                if (srcid.Trim() == "")
                {
                    for (int t = 0; t <= ds.Tables[0].Rows.Count - 1; t++)
                    {
                        list.Add(new BS_SHARED.SHARED { src = ds.Tables[0].Rows[t]["stationName"].ToString(), srcID = ds.Tables[0].Rows[t]["stationId"].ToString(), provider_name = "ETS" });
                    }
                }
                else
                {
                    for (int t = 0; t <= ds.Tables[0].Rows.Count - 1; t++)
                    {
                        list.Add(new BS_SHARED.SHARED { src = src, dest = ds.Tables[0].Rows[t]["stationName"].ToString(), srcID = srcid, destID = ds.Tables[0].Rows[t]["stationId"].ToString(), provider_name = providername });
                    }
                }
            }
            catch (Exception ex)
            {
                EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "EtsService_ConvertDatasetToList");
            }
            return list;
        }
        public DataSet convertJsonStringToDataSet(string jsonString)
        {
            XmlDocument xd = new XmlDocument();
            jsonString = "{ \"rootNode\": {" + jsonString.Trim().TrimStart('{').TrimEnd('}') + "} }";
            xd = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonString);
            DataSet ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(xd));
            return ds;
        }
    }
}
