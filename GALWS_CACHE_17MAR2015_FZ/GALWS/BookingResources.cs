﻿using STD.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL
{
    public class BookingResources
    {
        public string sAddName { get; set; }
        public string sCity { get; set; }
        public string sCountry { get; set; }
        public string sAddLine1 { get; set; }
        public string sAddLine2 { get; set; }
        public string sState { get; set; }
        public string sZip { get; set; }
        public string sEmailId { get; set; }
        public string sAgencyPhn { get; set; }
        public string sAgencyMob { get; set; }
        public string sComments { get; set; }
        public string pay_type { get; set; }
        public string sFax { get; set; }
        public string sCurrency { get; set; }
        public string cityCode { get; set; }

        public BookingResources(string TCCode)
        {
            FlightCommonBAL obj = new FlightCommonBAL();
            BookingResource objData = new BookingResource();
            objData = obj.getAddress(TCCode);
            sAddName = objData.sAddName;
            sCity = objData.sCity;
            sCountry = objData.sCountry;
            sAddLine1 = objData.sAddLine1;
            sAddLine2 = objData.sAddLine2;
            sState = objData.sState;
            sZip = objData.sZip;
            sAgencyPhn = objData.sAgencyPhn;
            sComments = objData.sComments;
            pay_type = objData.pay_type;
            sFax = objData.sFax;
            sCurrency = objData.sCurrency;
            cityCode = objData.cityCode;
            sAgencyMob = objData.Mobile;
            sEmailId = objData.sEmailId;
            //sAddName = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sAddName");
            //sCity = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sCity");
            //sCountry = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sCountry");
            //sAddLine1 = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sAddLine1");
            //sAddLine2 = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sAddLine2");
            //sState = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sState");
            //sZip = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sZip");
            //sEmailId = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sEmailId");
            //sAgencyPhn = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sAgencyPhn");
            //sComments = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sComments");
            //pay_type = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("pay_type");
            //sFax = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sFax");
            //sCurrency = Flight.SGAnd6E.Booking_Resource.ResourceManager.GetString("sCurrency");
        }

    }

    public class BookingResource
    {
        public string sAddName { get; set; }
        public string sCity { get; set; }
        public string sCountry { get; set; }
        public string sAddLine1 { get; set; }
        public string sAddLine2 { get; set; }
        public string sState { get; set; }
        public string sZip { get; set; }
        public string sEmailId { get; set; }
        public string sAgencyPhn { get; set; }
        public string sComments { get; set; }
        public string pay_type { get; set; }
        public string sFax { get; set; }
        public string sCurrency { get; set; }
        public string cityCode { get; set; }
        public string Mobile { get; set; }
        public string GSTNumber { get; set; }
    }
}
