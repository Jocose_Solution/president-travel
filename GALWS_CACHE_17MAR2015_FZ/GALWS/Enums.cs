﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.Shared
{
    public enum SellStatus
    {
        None = 0,
        OK = 1,
        UNS = 2,
        Failed = 3,
    }

    public enum PnrOptionCode
    {
        No_special = 0,
        End_transact_ET = 10,
        End_transact_with_retrieve_ER = 11,
        End_transact_and_change_advice_codes_ETK = 12,
        End_transact_with_retrieve_and_change_advice_codes_ERK = 13,
        End_transact_split_PNR_EF_ = 14,
        Cancel_the_itinerary_for_all_PNRs_connected_by_the_AXR_and_end_transact_ETX = 15,
        Cancel_the_itinerary_for_all_PNRs_connected_by_the_AXR_and_end_transact_with_retrieve_ERX = 16,
        Ignore_IG = 20,
        Ignore_and_retrieve_IR = 21,
        Stop_EOT_if_segment_sell_error = 267,
        Show_warnings_at_first_EOT = 30,
        Reply_with_short_message = 50

    }

    public enum PnrStages
    {
        Mandatory = 0,
        FE = 1,
        PnrSave = 2,
        Optional = 3
    }

    public enum TransactionName
    {
        None = 0,
        MasterPricerSearch = 1,
        SellSegments = 2,
        PnrAddMultipleElement = 3,
        PnrPricing = 4,
        CreateTST = 5,
        SignIn = 6,
        SignOut = 7,
        SavePnrMendatory = 8,
        IgnoreTransaction = 9,
        PaymentDone = 10,
        PaymentFailure = 11,
        PnrRetrieve = 13,
        TicketingDocIssuance = 12,
        DisplayTST = 14,
        AddOptionalPnrElement = 15,
        SavePnrAfterOptionalElement = 16,
        PnrRetrieveFinal = 17,
        Sellability = 18
    }

    public enum BookingStatus
    {
        OnRequest = 0,
        Success = 1,
        Failure = 2,
        Error = 3
    }

    public enum PNRStatus
    {
        Request = 0,
        Pending = 1,
        InProcess = 2,
        Ticketed = 3,
        Rejected = 4,
        Cancelled = 5,
        Hold = 6
        //None = 0,
        //TICKETED = 1,
        //HOLD = 2,
        //CANCELLED = 3,
        //REJECTED = 4
    }

    public enum RequestTypeCode
    {
        GEN = 0,
        LTC = 1,
        MIL = 2,
        SUD = 3
    }

    public enum PassengerCode
    {
        ADT,
        CHD,
        INF
    }

    public enum JourneyType
    {
        D = 0,
        I = 1
        //Domestic = 0,
        //International = 1,
        //D = 2,
        //I = 3
    }

    public enum Trip
    {
        D,
        I
    }

    public enum TripType
    {
        //None = 0,
        //OneWay = 1,
        //RoundTrip = 2,
        //MultiCity = 3,

        //N = 4,
        O,
        R,
        //M = 7

        None,
        OneWay,
        RoundTrip,
        MultiCity

    }
    public enum TripType1
    {
        OneWay = 0,
        RoundTrip = 1
    }
    public enum TripTypeCode
    {
        None = 0,
        O = 1,
        R = 2,
        M = 3
    }

    public enum EmailTypes
    {
        None = 0,
        SuccessConfirmationMailToCustomer = 1,
        FailtureConfirmationMailToCustomer = 2,
        ErrorMailToAdmin = 3,
    }

    public enum HdfcPGResult
    {
        None = 0,
        CAPTURED = 1,
        NOT_CAPTURED = 2,
        DENIED_BY_RISK = 3,
        CANCELED = 4,
        MISMATCH = 5,
        IPMISMATCH = 6
    }
    public enum TicketStatus
    {
        Pending,
        InProcess,
        Ticketed,
        Rejected,
        Cancelled,
        Reissue,
        Confirmed,
        REQUEST,
        CONFIRM,
        HOLD
    }

    //Hotel Status
    public enum HotelStatus
    {
        PENDING,
        INPROCESS,
        REJECTED,
        CANCELLED,
        REQUEST,
        CONFIRM,
        HOLD,
        HotelBooking
    }
    public enum PGTranStatus
    {
        None = 0,
        InitialRequest = 1,
        PaymentIDReceived = 2,
        ResponsePending = 3,
        ResponseReceived = 4,
        ReadyToBook = 5,
        Error = 6
    }

    public enum ValidationStatus
    {
        Failed = 0,
        Success = 1
    }

    public enum ToValidate
    {
        State = 0,
        City = 1,
        AirportCity = 2
    }


}
