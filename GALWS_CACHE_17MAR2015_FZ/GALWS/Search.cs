﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.Shared
{
    public class Search
    {
       
        // For City and Airport Search
        private string m_CityName;
        private string m_AirportCode;
        private string m_CountryCode;
        private string m_ALName;
        private string m_ALCode;

        public string AirportCode
        {
            get { return m_AirportCode; }
            set { m_AirportCode = value; }
        }   
        public string CountryCode
        {
            get { return m_CountryCode; }
            set { m_CountryCode = value; }
        }
        public string ALName
        {
            get { return m_ALName; }
            set { m_ALName = value; }
        }       
        public string ALCode
        {
            get { return m_ALCode; }
            set { m_ALCode = value; }
        }      

        // For Hotel Search
        private string m_HName;

        public string HName
        {
            get { return m_HName; }
            set { m_HName = value; }
        }

        //For Agency
        private string m_AgencyName;
        private string m_User_Id;
        private int m_ID;

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Agency_Name
        {
            get { return m_AgencyName; }
            set { m_AgencyName = value; }
        }
        public string User_Id
        {
            get { return m_User_Id; }
            set { m_User_Id = value; }
        }
        public string CityName
        {
            get { return m_CityName; }
            set { m_CityName = value; }
        }
    }
}
