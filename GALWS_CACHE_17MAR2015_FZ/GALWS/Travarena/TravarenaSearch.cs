﻿using STD.BAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace GALWS.Travarena
{
    public class TravarenaSearch
    {
        public string LoginID { get; set; }
        public string LoginPass { get; set; }
        public string UserID { get; set; }
        public string UserPass { get; set; }
        public string SecurityGUID { get; set; }
        public string IP { get; set; }
        public string ServiceUrl { get; set; }
        public string TargetCode { get; set; }
        public TravarenaSearch(string loginId, string loginPass, string userId, string userPass, string ip, string serviceUrl, string targetCode)
        {
            LoginID = loginId;
            LoginPass = loginPass;
            UserID = userId;
            UserPass = userPass;
            IP = ip;
            ServiceUrl = serviceUrl;
            TargetCode = targetCode;
        }
        public ArrayList GetFlightAvailability(FlightSearch f, bool isSplFare, string fareType, bool isLCC, List<FlightCityList> CityList, List<AirlineList> AirLineList, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string TTrip, string TTripType, bool RTFSF, string constr, List<MISCCharges> MiscList, HttpContext context)
        {
            ArrayList resultList = new ArrayList();
            string exep = "";
            try
            {
                SecurityGUID = "Riya-" + f.SessionId;
                TravarenaRequest objFareQ = new TravarenaRequest(LoginID, LoginPass, ServiceUrl, TargetCode, SecurityGUID);
                resultList = ParseFareQoute(objFareQ.GetLowFareSearch(f, ref exep, TTrip, TTripType, RTFSF), f, CityList, AirLineList, SrvchargeList, MarkupDs, RTFSF, constr, MiscList,  isSplFare,  fareType,  isLCC,  context);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }
        private ArrayList ParseFareQoute(string responseXml, FlightSearch searchInput, List<FlightCityList> CityList, List<AirlineList> AirLineList, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, bool RTFS, string constr, List<MISCCharges> MiscList, bool isSplFare, string fareType, bool isLCC,  HttpContext context)
        {
            ArrayList FinalList = new ArrayList();
            try
            {
                int ADTTC = 0; int CHDTC = 0; int INFTC = 0;

                List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
                List<FlightSearchResults> fsrListR = new List<FlightSearchResults>();
                FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
                float srvChargeAdt = 0;
                float srvChargeChd = 0;
                float srvCharge = 0;
                DataTable CommDt = new DataTable();
                Hashtable STTFTDS = new Hashtable();
                int Leg = 0; int Flight = 0; int LineNo = 1; string TripCount = ""; int Stops = 0;
                if (responseXml.Contains("<Error>") == false)
                {
                    int adt = searchInput.Adult, chd = searchInput.Child, inf = searchInput.Infant; string Trip = searchInput.Trip.ToString();
                    XNamespace soap = "http://www.w3.org/2003/05/soap-envelope";
                    XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                    XNamespace xmlns = "http://apiapps.riyawings.com/";
                    string xmlr = "";

                    xmlr = responseXml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty);
                    xmlr = responseXml.Replace("xmlns=\"http://apiapps.riyawings.com/\"", string.Empty);

                    //  xmlr = responseXml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty);
                    XDocument xd = XDocument.Parse(xmlr);
                    var flts = xd.Descendants(soap + "Body").Descendants("FlightAvailabilityResult").ToArray();
                    string xmlrs = flts[0].ToString().Replace("&lt;", "<")
                                                       .Replace("&amp;", "&")
                                                       .Replace("&gt;", ">")
                                                       .Replace("&quot;", "\"")
                                                       .Replace("&apos;", "'");
                    xmlrs = xmlrs.Replace("<?xml", "<xml");
                    xmlrs = xmlrs.Replace("?>", "/>");
                    XDocument xd2 = XDocument.Parse(xmlrs);
                    Flight = 1;
                    foreach (var AvlR in xd2.Descendants("AvailabilityResponse").Descendants("ItinearyDetails"))
                    {
                        LineNo = 1;
                        foreach (var Items in AvlR.Descendants("Items"))
                        {
                            var FlightDetails = Items.Descendants("FlightDetails");
                            var FareDescription = Items.Descendants("FareDescription");
                            Leg = 1;
                            foreach (var fltdtls in FlightDetails)
                            {

                                FlightSearchResults objFS = new FlightSearchResults();
                                #region ADT
                                var ADTFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "ADT");
                                objFS.AdtBreakPoint = "";
                                objFS.AdtCabin = fltdtls.Element("ClassCode").Value;
                                objFS.AdtRbd = fltdtls.Element("ClassCode").Value;
                                objFS.RBD = fltdtls.Element("ClassCode").Value;
                                objFS.AdtFare = float.Parse(ADTFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                                objFS.AdtBfare = float.Parse(ADTFARELIST.First().Element("BasicAmount").Value);
                                var TaxBreakUp = from tx in ADTFARELIST.First().Element("OtherInfo").Elements("Item")
                                                 select new
                                                 {
                                                     Category = tx.Element("TaxCode").Value,
                                                     Amount = tx.Element("Amount").Value,
                                                 };
                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, fltdtls.Element("CarrierCode").Value, "NRM", Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion
                                objFS.AdtTax = TaxBreakUp.Where(y => y.Category != "YQ").Sum(x => float.Parse(x.Amount));
                                objFS.fareBasis = fltdtls.Element("FareBasisCode").Value;
                                objFS.AdtFareType = fltdtls.Element("ClassCodeDesc").Value;
                                objFS.HegFareType = fltdtls.Element("FareType").Value;
                                objFS.AdtFSur = TaxBreakUp.Where(y => y.Category == "YQ").Sum(x => float.Parse(x.Amount));
                                objFS.AdtOT = float.Parse((objFS.AdtTax - (objFS.AdtFSur + objFS.AdtJN + objFS.AdtYR + objFS.AdtWO + objFS.AdtIN)).ToString());
                                if (fltdtls.Element("FareType").Value == "N")
                                {
                                    objFS.AdtFar = "NRM";
                                }
                                else if (fltdtls.Element("FareType").Value == "Y")
                                {
                                    objFS.AdtFar = "RTSF";
                                }
                                else if (fltdtls.Element("FareType").Value == "C")
                                {
                                    objFS.AdtFar = "CRP";
                                }
                                else if (fltdtls.Element("FareType").Value == "R")
                                {
                                    objFS.AdtFar = "NRM";
                                }
                                else
                                {
                                    objFS.AdtFar = "NRM";
                                }
                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt;
                                //SMS charge add end
                                objFS.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fltdtls.Element("CarrierCode").Value, objFS.AdtFare, searchInput.Trip.ToString());
                                objFS.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fltdtls.Element("CarrierCode").Value, objFS.AdtFare, searchInput.Trip.ToString());
                                CommDt.Clear();
                                CommDt = objFltComm.GetFltComm_WithouDB(searchInput.AgentType, fltdtls.Element("CarrierCode").Value, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin, objFS.DepartureDate, searchInput.HidTxtDepCity.Split(',')[0].ToString().Trim() + "-" + searchInput.HidTxtArrCity.Split(',')[0].ToString().Trim(), "", objFS.fareBasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, "NRM", "1", context);
                                objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                STTFTDS.Clear();
                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fltdtls.Element("CarrierCode").Value, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInput.TDS);
                                objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                objFS.AdtEduCess = 0;
                                objFS.AdtHighEduCess = 0;
                                objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                #endregion
                                #region CHD
                                var CHDFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "CHD");
                                if (CHDFARELIST.Count() != 0)
                                {
                                    objFS.ChdBreakPoint = "";
                                    objFS.ChdCabin = fltdtls.Element("ClassCode").Value;
                                    objFS.ChdRbd = fltdtls.Element("FareType").Value;
                                    objFS.RBD = fltdtls.Element("FareType").Value;
                                    objFS.ChdFare = float.Parse(CHDFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                                    objFS.ChdBFare = float.Parse(CHDFARELIST.First().Element("BasicAmount").Value);
                                    var ChdTaxBreakUp = from tx in CHDFARELIST.First().Element("OtherInfo").Elements("Item")
                                                     select new
                                                     {
                                                         Category = tx.Element("TaxCode").Value,
                                                         Amount = tx.Element("Amount").Value,
                                                     };
                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, fltdtls.Element("CarrierCode").Value, "NRM", Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeChd = 0; }
                                    #endregion
                                    objFS.ChdTax = ChdTaxBreakUp.Where(y => y.Category != "YQ").Sum(x => float.Parse(x.Amount));
                                    objFS.fareBasis = fltdtls.Element("FareBasisCode").Value;
                                    objFS.ChdfareType = fltdtls.Element("FareType").Value;
                                    objFS.ChdFSur = ChdTaxBreakUp.Where(y => y.Category == "YQ").Sum(x => float.Parse(x.Amount));
                                    objFS.ChdOT = float.Parse((objFS.ChdTax - (objFS.ChdFSur + objFS.ChdJN + objFS.ChdYR + objFS.ChdWO + objFS.ChdIN)).ToString());
                                    objFS.ChdFar = "NRM";
                                    //SMS charge add 
                                    objFS.ChdOT = objFS.ChdOT + srvChargeChd;
                                    objFS.ChdTax = objFS.ChdTax + srvChargeChd;
                                    objFS.ChdFare = objFS.ChdFare + srvChargeChd;
                                    //SMS charge add end
                                    objFS.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fltdtls.Element("CarrierCode").Value, objFS.ChdFare, searchInput.Trip.ToString());
                                    objFS.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fltdtls.Element("CarrierCode").Value, objFS.ChdFare, searchInput.Trip.ToString());
                                    CommDt.Clear();
                                    CommDt = objFltComm.GetFltComm_WithouDB(searchInput.AgentType, fltdtls.Element("CarrierCode").Value, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin, objFS.DepartureDate, searchInput.HidTxtDepCity.Split(',')[0].ToString().Trim() + "-" + searchInput.HidTxtArrCity.Split(',')[0].ToString().Trim(), "", objFS.fareBasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, "NRM", "1", context);
                                    objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-ChdComm  
                                    objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS.Clear();
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fltdtls.Element("CarrierCode").Value, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInput.TDS);
                                    objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                    objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                    objFS.ChdEduCess = 0;
                                    objFS.ChdHighEduCess = 0;
                                    objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());

                                }
                                #endregion
                                #region INF
                                 var INFFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "INF");
                                 if (INFFARELIST.Count() != 0)
                                 {
                                     objFS.InfBreakPoint = "";
                                     objFS.InfCabin = fltdtls.Element("ClassCode").Value;
                                     objFS.InfRbd = fltdtls.Element("FareType").Value;
                                     objFS.RBD = fltdtls.Element("FareType").Value;
                                     objFS.InfFare = float.Parse(INFFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                                     objFS.InfBfare = float.Parse(INFFARELIST.First().Element("BasicAmount").Value);
                                     var InfTaxBreakUp = from tx in INFFARELIST.First().Element("OtherInfo").Elements("Item")
                                                      select new
                                                      {
                                                          Category = tx.Element("TaxCode").Value,
                                                          Amount = tx.Element("Amount").Value,
                                                      };
                                     
                                     objFS.InfTax = InfTaxBreakUp.Where(y => y.Category != "YQ").Sum(x => float.Parse(x.Amount));
                                     objFS.fareBasis = fltdtls.Element("FareBasisCode").Value;
                                     objFS.InfFareType = fltdtls.Element("FareType").Value;
                                     objFS.InfFSur = InfTaxBreakUp.Where(y => y.Category == "YQ").Sum(x => float.Parse(x.Amount));
                                     objFS.InfOT = float.Parse((objFS.InfTax - (objFS.InfFSur + objFS.InfJN + objFS.InfYR + objFS.InfWO + objFS.InfIN)).ToString());
                                     objFS.InfFar = "NRM";
                                     //SMS charge add 
                                     objFS.InfOT = objFS.InfOT ;
                                     objFS.InfTax = objFS.InfTax ;
                                     objFS.InfFare = objFS.InfFare ;
                                     //SMS charge add end
                                     objFS.InfAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fltdtls.Element("CarrierCode").Value, objFS.InfFare, searchInput.Trip.ToString());
                                     objFS.InfAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fltdtls.Element("CarrierCode").Value, objFS.InfFare, searchInput.Trip.ToString());
                                     CommDt.Clear();
                                     CommDt = objFltComm.GetFltComm_WithouDB(searchInput.AgentType, fltdtls.Element("CarrierCode").Value, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), 1, objFS.InfRbd, objFS.InfCabin, objFS.DepartureDate, searchInput.HidTxtDepCity.Split(',')[0].ToString().Trim() + "-" + searchInput.HidTxtArrCity.Split(',')[0].ToString().Trim(), "", objFS.fareBasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, "NRM", "1", context);
                                     objFS.InfDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-InfComm  
                                     objFS.InfCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                     STTFTDS.Clear();
                                     STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fltdtls.Element("CarrierCode").Value, objFS.InfDiscount1, objFS.InfBfare, objFS.InfFSur, searchInput.TDS);
                                     objFS.InfSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                     objFS.InfDiscount = objFS.InfDiscount1 - objFS.InfSrvTax1;
                                     objFS.InfEduCess = 0;
                                     objFS.InfHighEduCess = 0;
                                     objFS.InfTF = float.Parse(STTFTDS["TFee"].ToString());
                                     objFS.InfTds = float.Parse(STTFTDS["Tds"].ToString());
                                 }
                                #endregion

                                objFS.OrgDestFrom = TravarenaUtility.Left(searchInput.HidTxtDepCity, 3);// p.OrgDestFrom;
                                objFS.OrgDestTo = TravarenaUtility.Left(searchInput.HidTxtArrCity, 3);//.OrgDestTo;
                                objFS.DepartureLocation = fltdtls.Element("Origin").Value.Trim();
                                objFS.DepartureCityName = getCityName(fltdtls.Element("Origin").Value, CityList);
                                objFS.DepAirportCode = fltdtls.Element("Origin").Value;
                                objFS.DepartureAirportName = getCityName(objFS.DepartureLocation, CityList);
                                objFS.DepartureTerminal = fltdtls.Element("OrgTerminal").Value;
                                objFS.ArrivalLocation = fltdtls.Element("Destination").Value.Trim();
                                objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                                objFS.ArrAirportCode = fltdtls.Element("Destination").Value.Trim();
                                objFS.ArrivalAirportName = getCityName(objFS.ArrivalLocation, CityList);
                                objFS.ArrivalTerminal = fltdtls.Element("Destination").Value.Trim();
                                DateTime DepartureTime = Convert.ToDateTime(fltdtls.Element("DepartureDateTime").Value.Split(' ')[0].Split('/')[2] + "/" + fltdtls.Element("DepartureDateTime").Value.Split(' ')[0].Split('/')[1] + "/" + fltdtls.Element("DepartureDateTime").Value.Split(' ')[0].Split('/')[0] + " " + fltdtls.Element("DepartureDateTime").Value.Split(' ')[1]);
                                objFS.DepartureDate = DepartureTime.Date.ToString("ddMMyy");
                                objFS.Departure_Date = DepartureTime.Date.ToString("dd MMM");
                                objFS.DepartureTime = DepartureTime.ToString("HHmm");
                                objFS.depdatelcc = fltdtls.Element("DepartureDateTime").Value;
                                DateTime ArrivalTime = Convert.ToDateTime(fltdtls.Element("ArrivalDateTime").Value.Split(' ')[0].Split('/')[2] + "/" + fltdtls.Element("ArrivalDateTime").Value.Split(' ')[0].Split('/')[1] + "/" + fltdtls.Element("ArrivalDateTime").Value.Split(' ')[0].Split('/')[0] + " " + fltdtls.Element("ArrivalDateTime").Value.Split(' ')[1]);
                                objFS.ArrivalDate = ArrivalTime.Date.ToString("ddMMyy");
                                objFS.Arrival_Date = ArrivalTime.Date.ToString("dd MMM");
                                objFS.ArrivalTime = ArrivalTime.ToString("HHmm");
                                objFS.arrdatelcc = fltdtls.Element("ArrivalDateTime").Value;
                                objFS.TotDur = fltdtls.Element("Duration").Value.Replace("Hrs", ":").Replace("Mins", "");
                                objFS.Adult = adt;
                                objFS.Child = chd;
                                objFS.Infant = inf;
                                objFS.TotPax = adt + chd + inf;
                                objFS.MarketingCarrier = fltdtls.Element("CarrierCode").Value;
                                objFS.OperatingCarrier = fltdtls.Element("CarrierCode").Value;
                                objFS.ValiDatingCarrier = fltdtls.Element("ValidatingCarrier").Value;
                                objFS.AirLineName = getAirlineName(objFS.MarketingCarrier, AirLineList);
                                objFS.FlightIdentification = fltdtls.Element("FlightID").Value;
                                objFS.ElectronicTicketing = "True";
                                objFS.AvailableSeats = fltdtls.Element("Seats").Value;

                                objFS.sno = fltdtls.Element("FareType").Value + ":" + Flight + ":" + isLCC + ":" + objFS.DepAirportCode + "-" + objFS.ArrAirportCode;
                               // objFS.sno = "Key::" + xd2.Descendants("AvailabilityResponse").First().Element("TrackId").Value + "~Group::" + Flight + "~CONX::" + (FlightDetails.Count() - 1);
                                objFS.Searchvalue = xd2.Descendants("AvailabilityResponse").First().Element("TrackId").Value;
                                objFS.getVia = fltdtls.Element("Via").Value;
                                objFS.Leg = Leg;
                                objFS.Flight = Flight.ToString();
                                objFS.LineNumber = LineNo;
                                objFS.EQ = fltdtls.Element("FlightID").Value;
                                objFS.Stops = fltdtls.Element("NumberofStops").Value + "-Stop";
                                if (xd2.Descendants("AvailabilityResponse").Descendants("ItinearyDetails").Count() == 1 && Flight == 1)
                                { objFS.TripType = "O"; }
                                else if (xd2.Descendants("AvailabilityResponse").Descendants("ItinearyDetails").Count() == 2 && Flight == 1)
                                { objFS.TripType = "O"; }
                                else if (xd2.Descendants("AvailabilityResponse").Descendants("ItinearyDetails").Count() == 2 && Flight == 2)
                                { objFS.TripType = "R"; }
                                else { objFS.TripType = "M"; }
                                objFS.Trip = searchInput.Trip.ToString();//From Search Input
                                objFS.Sector = "";//Need to update
                                if (searchInput.TripType != TripType.MultiCity)
                                {
                                    if (searchInput.TripType == TripType.RoundTrip)
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                                    else
                                        objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                }
                                else
                                { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                                objFS.IATAComm = 1;//Need to update    
                                objFS.Provider = "RI";
                                #region Other
                                objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                                objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                                objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                                objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child) + (objFS.InfFSur * objFS.Infant);
                                objFS.OriginalTF = float.Parse(((objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant)).ToString());
                                srvCharge = srvChargeAdt + srvChargeChd;
                                objFS.OriginalTT = srvCharge;
                                objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                                objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                                objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child) + (objFS.InfDiscount * objFS.Infant);
                                objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child) + (objFS.InfCB * objFS.Infant);
                                objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child) + (objFS.InfTds * objFS.Infant);
                                objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child) + (objFS.InfAdminMrk * objFS.Infant) + (objFS.InfAgentMrk * objFS.Infant) + ADTTC + CHDTC;
                                objFS.TotMgtFee = 0;//Need to update
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee;
                                objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child) + (objFS.InfAgentMrk * objFS.Infant));//Need to update
                                objFS.SearchId = SecurityGUID;
                                #endregion
                                if (searchInput.TripType == TripType.RoundTrip && searchInput.Trip == STD.Shared.Trip.D && RTFS == true)
                                {
                                    if (Convert.ToInt16(objFS.Flight) == 1)
                                    {
                                        fsrList.Add(objFS);
                                    }
                                    else
                                    {
                                        fsrListR.Add(objFS);
                                    }
                                }
                                else if (searchInput.TripType == TripType.RoundTrip && searchInput.Trip == STD.Shared.Trip.I && RTFS == false)
                                {
                                    fsrList.Add(objFS);
                                }
                                else
                                {
                                    fsrList.Add(objFS);
                                }
                                Leg = Leg + 1;
                            }
                            LineNo = LineNo + 1;
                        }
                    }
                    Flight = Flight + 1;
                }
                FinalList.Add(fsrList);
                if (fsrListR.Count != 0)
                {
                    FinalList.Add(fsrListR);
                }
                if (RTFS == true)  //RTF True
                {
                    FinalList = RoundTripFare(fsrList, fsrListR, srvCharge);
                }
            }
            catch (Exception ex)
            {

            }
            return FinalList;

        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            int IATAComm = 0;
            //decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                if (StNew.Count > 0)
                {
                    STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                    TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                    IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                }
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                //STHT.Add("Tds", Math.Round((double.Parse(Dis.ToString()) * double.Parse(TDS)) / 100, 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        private ArrayList RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR, float srvCharge)
        {

            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            ArrayList Comb = new ArrayList();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln, srvCharge));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln, srvCharge));
                        ln++;
                    }

                }

            }
            Comb.Add(Final);
            return Comb;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, float srvCharge)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
            //InfSrvTax = 0, InfTF = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;
            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT + srvCharge;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare + srvCharge;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax + srvCharge;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT + srvCharge;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare + srvCharge;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax + srvCharge;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;

            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = srvCharge;// item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
            float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = srvCharge;//OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }
        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        private string getAirlineName(string airl, List<AirlineList> AirList)
        {
            string airl1 = "";
            try
            {
                airl1 = ((from ar in AirList where ar.AirlineCode == airl select ar).ToList())[0].AlilineName;
            }
            catch { airl1 = airl; }
            return airl1;
        }
        private string GetMonthName(int monthNo)
        {

            Dictionary<int, string> month = new Dictionary<int, string>();

            month.Add(01, "JAN");
            month.Add(02, "FEB");
            month.Add(03, "MAR");
            month.Add(04, "APR");
            month.Add(05, "MAY");
            month.Add(06, "JUN");
            month.Add(07, "JUL");
            month.Add(08, "AUG");
            month.Add(09, "SEP");
            month.Add(10, "OCT");
            month.Add(11, "NOV");
            month.Add(12, "DEC");

            return month[monthNo];

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }
                if (airMrkArray.Length > 0)
                {
                    if (Trip == "I")
                    {
                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                        {
                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                        }
                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                        {
                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                        }
                    }
                    else
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }
        public List<FlightSearchResults> PaxFareDetails(string xml, string mainxml, string SegRefKey, string PaxType, string VCarrier)
        {
            List<FlightSearchResults> PFI = new List<FlightSearchResults>();
            FlightSearchResults PXFARE = new FlightSearchResults();
            XNamespace Pns = "http://www.travelport.com/schema/air_v42_0";
            XNamespace Common = "http://www.travelport.com/schema/common_v42_0";
            XDocument xd = XDocument.Parse(xml);

            XDocument xd1 = XDocument.Parse(mainxml);
            var fare = xd1.Descendants(Pns + "LowFareSearchRsp").Descendants(Pns + "FareInfoList");
            var hosttokwn = xd1.Descendants(Pns + "LowFareSearchRsp").Descendants(Pns + "HostTokenList");
            var prcinfo = xd.Root;
            //var prcs = xd.Descendants(Pns + "LowFareSearchRsp").Descendants(Pns + "AirPricingSolution");
            //var prcinfo1 = prcinfo; 
            string prckey = ""; double TotalPrice = 0; double BasePrice = 0; double Taxes = 0; string FareType = ""; string Carrier = "";
            //string key = xd.Root.Attributes("Key").First().Value;
            prckey = xd.Root.Attributes("Key").First().Value;
            TotalPrice = Convert.ToDouble(xd.Root.Attributes("TotalPrice").First().Value.Remove(0, 3));
            if (TravarenaUtility.Left(xd.Root.Attributes("TotalPrice").First().Value.ToUpper().Trim(), 3) == "INR")
                TotalPrice = Convert.ToDouble(xd.Root.Attributes("TotalPrice").First().Value.Remove(0, 3));
            else
                TotalPrice = xd.Root.Attributes("ApproximateTotalPrice").Any() == true ? Convert.ToDouble(xd.Root.Attributes("ApproximateTotalPrice").First().Value.Remove(0, 3)) : Convert.ToDouble(xd.Root.Attributes("EquivalentTotalPrice").First().Value.Remove(0, 3));

            if (TravarenaUtility.Left(xd.Root.Attributes("BasePrice").First().Value.Substring(0, 3).ToUpper().Trim(), 3) == "INR")
                BasePrice = Convert.ToDouble(xd.Root.Attributes("BasePrice").First().Value.Remove(0, 3));
            else
                BasePrice = xd.Root.Attributes("EquivalentBasePrice").Any() == true ? Convert.ToDouble(xd.Root.Attributes("EquivalentBasePrice").First().Value.Remove(0, 3)) : Convert.ToDouble(xd.Root.Attributes("ApproximateBasePrice").First().Value.Remove(0, 3));
            Taxes = xd.Root.Attributes("Taxes").Any() == true ? Convert.ToDouble(xd.Root.Attributes("Taxes").First().Value.Remove(0, 3)) : 0;
            if (PaxType == "ADT")
            {
                Carrier = xd.Root.Attributes("PlatingCarrier").Any() == true ? xd.Root.Attributes("PlatingCarrier").First().Value : VCarrier;
                PXFARE.ValiDatingCarrier = Carrier;
                PXFARE.OperatingCarrier = Carrier;

                //try
                //{
                //    PXFARE.PricingMethod = xd.Root.Attributes("PricingMethod").Any() == true ? xd.Root.Attributes("PricingMethod").First().Value : "";
                //}
                //catch (Exception ex)
                //{ PXFARE.PricingMethod = ""; }
            }
            try
            {
                FareType = "";// xd.Descendants(Pns + "FareRulesFilter").Any() == true ? xd.Descendants(Pns + "FareRulesFilter").Descendants(Pns + "Refundability").Attributes("Value").First().Value : "Refundable";
            }
            catch (Exception ex) { FareType = "NonRefundable"; }
            //FareType = xd.Root.Attribute("Refundable") == null ? "NonRefundable" : Convert.ToBoolean(xd.Root.Attributes("Refundable").First().Value) == true ? "Refundable" : "Non Refundable";
            //== true ?
            var bookinginfo = xd.Root.Elements(Pns + "BookingInfo");


            var bkInfo = from bk in bookinginfo.Where(x => x.Attribute("SegmentRef").Value == SegRefKey)
                         select new
                         {
                             CabinClass = bk.Attributes("CabinClass").First().Value,
                             FareInfoRef = bk.Attributes("FareInfoRef").First().Value,
                             RBD = bk.Attributes("BookingCode").First().Value,
                             SegmentRef = bk.Attributes("SegmentRef").First().Value,
                             HostTokenList = bk.Attributes("HostTokenRef").Any() == false ? "" : bk.Attributes("HostTokenRef").First().Value

                         };
            var TaxBreakUp = from tx in xd.Root.Elements(Pns + "TaxInfo")//.Where(x => x.Attribute("SegmentRef").Value == SegRefKey)
                             select new
                             {
                                 Category = tx.Attributes("Category").First().Value,
                                 Amount = tx.Attributes("Amount").First().Value.Remove(0, 3),
                             };

            var fareinfo = from fr in fare.Descendants(Pns + "FareInfo").Where(x => x.Attribute("Key").Value == bkInfo.First().FareInfoRef.ToString())//&& x.Attribute("PassengerTypeCode").Value == PaxType
                           select new
                           {
                               FareBasis = fr.Attributes("FareBasis").First().Value,
                               NotValidBefore = fr.Attributes("NotValidBefore").Any() == true ? fr.Attributes("NotValidBefore").First().Value : "",
                               NotValidAfter = fr.Attributes("NotValidAfter").Any() == true ? fr.Attributes("NotValidAfter").First().Value : "",
                               FareKey = fr.Attributes("Key").First().Value,
                               BaggageType = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "NumberOfPieces").Any() == true ? "UNIT" : "WEIGHT",
                               BaggageWeight = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Value").Any() == true ? fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Value").First().Value : "NA",
                               BaggageWeightType = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Unit").Any() == true ? fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Unit").First().Value : "NA",
                               BaggagePieces = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "NumberOfPieces").Any() == true ? fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "NumberOfPieces").First().Value : "NA",
                               FareRuleKey = fr.Descendants(Pns + "FareRuleKey").First().Value,
                               AccCode = fr.Descendants(Common + "AccountCode").Any() == true ? fr.Descendants(Common + "AccountCode").Attributes("Code").First().Value : "",
                               PassengerTypeCode = fr.Attributes("PassengerTypeCode").Any() == true ? fr.Attributes("PassengerTypeCode").First().Value : "",
                               Origin = fr.Attributes("Origin").Any() == true ? fr.Attributes("Origin").First().Value : "",
                               Destination = fr.Attributes("Destination").Any() == true ? fr.Attributes("Destination").First().Value : "",
                               EffectiveDate = fr.Attributes("EffectiveDate").Any() == true ? fr.Attributes("EffectiveDate").First().Value : "",
                               DepartureDate = fr.Attributes("DepartureDate").Any() == true ? fr.Attributes("DepartureDate").First().Value : "",
                               Amount = fr.Attributes("Amount").Any() == true ? fr.Attributes("Amount").First().Value : "",
                               TaxAmount = fr.Attributes("TaxAmount").Any() == true ? fr.Attributes("TaxAmount").First().Value : "",
                               FareFamily = fr.Attributes("FareFamily").Any() == true ? fr.Attributes("FareFamily").First().Value : "",
                           };
            string k = "";
            string faretype = "";
            try
            {
                //  PXFARE.PricingMethod = xd.Root.Attributes("PricingMethod").Any() == true ? xd.Root.Attributes("PricingMethod").First().Value : "";

                if (xd.Root.Descendants(Pns + "BookingInfo") != null)
                    if (xd.Root.Descendants(Pns + "BookingInfo").First().Attribute("BookingCode") != null)
                        if (xd.Root.Descendants(Pns + "BookingInfo").First().Attribute("BookingCode").Value == "S")
                        {
                            faretype = "CRP";
                        }
                        else
                        {
                            faretype = "NRM";
                        }

            }
            catch (Exception ex)
            { faretype = ""; }



            // PXFARE.FareCalc = xd.Descendants(Pns + "FareCalc").Any() == true ? xd.Descendants(Pns + "FareCalc").First().Value : "";
            // PXFARE.AccountCode = fareinfo.First().AccCode;

            PXFARE.TotalFare = Convert.ToSingle(TotalPrice);
            PXFARE.TotalTax = Convert.ToSingle(Taxes);




            if (PaxType == "ADT")
            {
                PXFARE.AdtBfare = Convert.ToSingle(BasePrice);
                PXFARE.AdtFar = faretype;// fareinfo.First().FareFamily;
                PXFARE.HegFareType = fareinfo.First().FareFamily;
                PXFARE.AdtFareType = FareType;
                PXFARE.AdtCabin = bkInfo.Any() == true ? bkInfo.First().CabinClass : "";
            }
            else if (PaxType == "CNN")
            {

                PXFARE.ChdBFare = Convert.ToSingle(BasePrice);
                PXFARE.ChdFar = faretype;//fareinfo.First().FareFamily;
                PXFARE.HegFareType = fareinfo.First().FareFamily;
                PXFARE.ChdfareType = FareType;
                PXFARE.ChdCabin = bkInfo.Any() == true ? bkInfo.First().CabinClass : "";
            }
            else if (PaxType == "INF")
            {
                PXFARE.InfBfare = Convert.ToSingle(BasePrice);
                PXFARE.InfFar = faretype;// fareinfo.First().FareFamily;
                PXFARE.HegFareType = fareinfo.First().FareFamily;
                PXFARE.InfFareType = FareType;
                PXFARE.InfCabin = bkInfo.Any() == true ? bkInfo.First().CabinClass : "";

            }

            string Searchvalue = bkInfo.Any() == true ? bkInfo.First().HostTokenList : "";


            var hosttokwns = from bk in hosttokwn.Elements(Common + "HostToken").Where(x => x.Attribute("Key").Value == Searchvalue)
                             select new
                             {
                                 HostTokentext = bk.Value,


                             };
            string sss1 = Searchvalue;
            string sss2 = hosttokwns.Any() == true ? hosttokwns.First().HostTokentext : "";
            PXFARE.Searchvalue = sss1 + "~~~" + sss2;
            PXFARE.fareBasis = fareinfo.First().FareBasis;
            if (PaxType == "CNN")
            {
                PXFARE.ChdFarebasis = fareinfo.First().FareBasis;
            }
            else if (PaxType == "INF")
            {
                PXFARE.InfFarebasis = fareinfo.First().FareBasis;
            }

            PXFARE.RBD = bkInfo.Any() == true ? bkInfo.First().RBD : "";

            // PXFARE.FareInfoRef = bkInfo.First().FareInfoRef;
            //  PXFARE.SegmentRef = bkInfo.First().SegmentRef;
            // PXFARE.NotValidBefore = fareinfo.First().NotValidBefore;
            // PXFARE.NotValidAfter = fareinfo.First().NotValidAfter;
            if (fareinfo.First().BaggageType == "WEIGHT")
                PXFARE.BagInfo = fareinfo.First().BaggageWeight + " " + fareinfo.First().BaggageWeightType + " Baggage Included.";
            else
                PXFARE.BagInfo = fareinfo.First().BaggagePieces + " Piece(s) Baggage included.";

            foreach (var txb in TaxBreakUp)
            {
                if (txb.Category == "IN")
                    PXFARE.AdtIN = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "JN")
                //    PXFARE.JN = Convert.ToDouble(txb.Amount);
                else if (txb.Category == "YQ")
                    PXFARE.AdtFSur = Convert.ToSingle(txb.Amount);
                else if (txb.Category == "WO")
                    PXFARE.AdtWO = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "YM")
                //    PXFARE.AdtYM = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "YR")
                //    #region GST
                //    PXFARE.YR = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "CGST")
                //    PXFARE.CGST = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "SGST")
                //    PXFARE.SGST = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "UTGST")
                //    PXFARE.UTGST = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "IGST")
                //    PXFARE.IGST = Convert.ToSingle(txb.Amount);
                //else if (txb.Category == "K3")
                //    PXFARE.K3 = Convert.ToSingle(txb.Amount);
                //    #endregion
            }
            #region Change and Cancel Panality
            try
            {
                if (xd.Descendants(Pns + "CancelPenalty").Any() == true)
                {
                    //if (xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Amount").Any() == true)
                    //{ PXFARE.CancelPenalty = xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Amount").First().Value; }
                    //else if (xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Percentage").Any() == true)
                    //{ PXFARE.CancelPenalty = xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Percentage").First().Value + " Percentage"; }
                    //else
                    //{ PXFARE.CancelPenalty = "Not Available"; }
                }
                else
                {// PXFARE.CancelPenalty = "Not Available"; 
                }
            }
            catch (Exception exx)
            {
                // PXFARE.CancelPenalty = "Not Available";
            }

            try
            {
                if (xd.Descendants(Pns + "ChangePenalty").Any() == true)
                {
                    if (xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Amount").Any() == true)
                    {
                        //PXFARE.ChangePenalty = xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Amount").First().Value;
                    }
                    else if (xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Percentage").Any() == true)
                    {
                        //PXFARE.ChangePenalty = xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Percentage").First().Value + " Percentage"; 
                    }
                    else
                    {
                        // PXFARE.ChangePenalty = "Not Available"; 
                    }
                }
                else
                {
                    //  PXFARE.ChangePenalty = "Not Available";
                }
            }
            catch (Exception exx)
            {
                //PXFARE.ChangePenalty = "Not Available";
            }

            #endregion

            PXFARE.FareRule = xd.Descendants(Pns + "FareRulesFilter").Any() == true ? xd.Descendants(Pns + "FareRulesFilter").Descendants(Pns + "Refundability").Attributes("Value").First().Value : "Not Available";
            PFI.Add(PXFARE);

            return PFI;
        }
        private Hashtable ServiceTax(string VC, double TotBFWI, double TotBFWOI, double FS, string Trip)
        {
            DataTable dtTax = new DataTable();
            Hashtable AirlineCharges = new Hashtable();
            SqlCommand sqlcom = new SqlCommand();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            sqlcom = new SqlCommand("ServiceCharge", conn);
            sqlcom.Parameters.Add("@vc", SqlDbType.VarChar).Value = VC;
            sqlcom.Parameters.Add("@trip", SqlDbType.VarChar).Value = Trip;
            sqlcom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(sqlcom);
            da.Fill(dtTax);
            try
            {
                if ((dtTax.Rows.Count > 0))
                {
                    AirlineCharges.Add("STax", Math.Round(((TotBFWI * Convert.ToDouble(dtTax.Rows[0]["SrvTax"])) / 100), 0));
                    // Math.Round(((TotBFWI * dtTax.Rows(0)("SrvTax")) / 100), 0)
                    AirlineCharges.Add("TF", Math.Round((((TotBFWOI + FS)
                                        * Convert.ToDouble(dtTax.Rows[0]["TranFee"]))
                                        / 100), 0));
                    AirlineCharges.Add("IATAComm", dtTax.Rows[0]["IATAComm"]);
                }
                else
                {
                    AirlineCharges.Add("STax", 0);
                    //  AirlineCharges.Add("STaxP", 0)
                    AirlineCharges.Add("TF", 0);
                    AirlineCharges.Add("IATAComm", 0);
                }

            }
            catch (Exception ex)
            {
                AirlineCharges.Add("STax", 0);
                // AirlineCharges.Add("STaxP", 0)
                AirlineCharges.Add("TF", 0);
                AirlineCharges.Add("IATAComm", 0);
            }

            return AirlineCharges;
        }
    }
}
