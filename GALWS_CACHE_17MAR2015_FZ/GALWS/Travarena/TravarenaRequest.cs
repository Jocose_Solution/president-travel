﻿using STD.BAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.Travarena
{

    public class TravarenaRequest
    {
        string UserName;
        string Password;
        string ServiceUrl;
        string TargetCode;
        string SecurityGUID;


        public TravarenaRequest(string username, string password, string serviceUrl, string targetCode, string securityGUID)
        {
            UserName = username;
            Password = password;
            ServiceUrl = serviceUrl;
            TargetCode = targetCode;
            SecurityGUID = securityGUID;
        }
        public string GetLowFareSearch(FlightSearch searchInput, ref string exep, string TTrip, string TTripType, bool RTSF)
        {
            string strRes = "";
            try
            {

                StringBuilder LowFareSearchReq = new StringBuilder();
                LowFareSearchReq.Append("<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:api='http://apiapps.riyawings.com/'>");
                LowFareSearchReq.Append("<soap:Body>");
                LowFareSearchReq.Append("<api:FlightAvailability>");
                LowFareSearchReq.Append("<api:objSecurity>");
                LowFareSearchReq.Append("<api:WebTerminal>RAGAU030010101</api:WebTerminal>");
                LowFareSearchReq.Append("<api:WebUserName>villa1</api:WebUserName>");
                LowFareSearchReq.Append("<api:WebPassword>u2pws01</api:WebPassword>");
                LowFareSearchReq.Append("<api:WebIp>122.176.101.130</api:WebIp>");
                LowFareSearchReq.Append("<api:WebAppType>API</api:WebAppType>");
                LowFareSearchReq.Append("</api:objSecurity>");
                LowFareSearchReq.Append("<api:strAvailability>");
                LowFareSearchReq.Append("<![CDATA[<Availability>");
                LowFareSearchReq.Append("<SegmentType>" + searchInput.Trip.ToString() + "</SegmentType>");
                LowFareSearchReq.Append("<TripType>" + searchInput.TripType.ToString().Substring(0, 1) + "</TripType>");
                if ((searchInput.TripType == TripType.OneWay && RTSF == false && searchInput.Trip == Trip.D) || (searchInput.TripType == TripType.OneWay && RTSF == false && searchInput.Trip == Trip.I))
                {
                    if (TTripType == "OutBound")
                    {

                        LowFareSearchReq.Append("<Itineary>");
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.DepDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate, 2) + "/" + TravarenaUtility.Right(searchInput.DepDate, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                        LowFareSearchReq.Append("</Itineary>");
                    }
                    else
                    {
                        LowFareSearchReq.Append("<Itineary>");
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.RetDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.RetDate, 2) + "/" + TravarenaUtility.Right(searchInput.RetDate, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                        LowFareSearchReq.Append("</Itineary>");
                    }


                }
                else if (searchInput.TripType == TripType.RoundTrip && RTSF == false && searchInput.Trip == Trip.D)
                {
                    if (TTripType == "OutBound")
                    {

                        LowFareSearchReq.Append("<Itineary>");
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.DepDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate, 2) + "/" + TravarenaUtility.Right(searchInput.DepDate, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                        LowFareSearchReq.Append("</Itineary>");

                    }
                    else
                    {

                        LowFareSearchReq.Append("<Itineary>");
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.RetDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.RetDate, 2) + "/" + TravarenaUtility.Right(searchInput.RetDate, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                        LowFareSearchReq.Append("</Itineary>");
                    }
                }
                else if (searchInput.TripType == TripType.RoundTrip && RTSF == true && searchInput.Trip == Trip.D)
                {

                    LowFareSearchReq.Append("<Itineary>");
                    LowFareSearchReq.Append("<Item>");
                    LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Origin>");
                    LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Destination>");
                    LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.DepDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate, 2) + "/" + TravarenaUtility.Right(searchInput.DepDate, 4) + "</Date>");
                    LowFareSearchReq.Append("</Item>");
                    LowFareSearchReq.Append("<Item>");
                    LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Origin>");
                    LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Destination>");
                    LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.RetDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.RetDate, 2) + "/" + TravarenaUtility.Right(searchInput.RetDate, 4) + "</Date>");
                    LowFareSearchReq.Append("</Item>");
                    LowFareSearchReq.Append("</Itineary>");
                }
                else if (searchInput.TripType == TripType.RoundTrip && RTSF == false && searchInput.Trip == Trip.I)
                {
                    LowFareSearchReq.Append("<Itineary>");
                    LowFareSearchReq.Append("<Item>");
                    LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Origin>");
                    LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Destination>");
                    LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.DepDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate, 2) + "/" + TravarenaUtility.Right(searchInput.DepDate, 4) + "</Date>");
                    LowFareSearchReq.Append("</Item>");
                    LowFareSearchReq.Append("<Item>");
                    LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Origin>");
                    LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Destination>");
                    LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.RetDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.RetDate, 2) + "/" + TravarenaUtility.Right(searchInput.RetDate, 4) + "</Date>");
                    LowFareSearchReq.Append("</Item>");
                    LowFareSearchReq.Append("</Itineary>");

                }
                else if (searchInput.TripType == TripType.MultiCity && RTSF == false)
                {
                    LowFareSearchReq.Append("<Itineary>");
                    LowFareSearchReq.Append("<Item>");
                    LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtDepCity, 3) + "</Origin>");
                    LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtArrCity, 3) + "</Destination>");
                    LowFareSearchReq.Append("<Date>" + TravarenaUtility.Mid(searchInput.DepDate, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate, 2) + "/" + TravarenaUtility.Right(searchInput.DepDate, 4) + "</Date>");
                    LowFareSearchReq.Append("</Item>");

                    if (!string.IsNullOrEmpty(searchInput.HidTxtArrCity2) && !string.IsNullOrEmpty(searchInput.DepDate2))
                    {
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity2, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity2, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Right(searchInput.DepDate2, 3) + "/" + TravarenaUtility.Mid(searchInput.DepDate2, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate2, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                    }
                    if (!string.IsNullOrEmpty(searchInput.HidTxtArrCity3) && !string.IsNullOrEmpty(searchInput.DepDate3))
                    {
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity3, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity3, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Right(searchInput.DepDate3, 3) + "/" + TravarenaUtility.Mid(searchInput.DepDate3, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate3, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                    }
                    if (!string.IsNullOrEmpty(searchInput.HidTxtArrCity4) && !string.IsNullOrEmpty(searchInput.DepDate4))
                    {
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity4, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity4, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Right(searchInput.DepDate4, 3) + "/" + TravarenaUtility.Mid(searchInput.DepDate4, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate4, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                    }
                    if (!string.IsNullOrEmpty(searchInput.HidTxtArrCity5) && !string.IsNullOrEmpty(searchInput.DepDate5))
                    {
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity5, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity5, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Right(searchInput.DepDate5, 3) + "/" + TravarenaUtility.Mid(searchInput.DepDate5, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate5, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                    }
                    if (!string.IsNullOrEmpty(searchInput.HidTxtArrCity6) && !string.IsNullOrEmpty(searchInput.DepDate6))
                    {
                        LowFareSearchReq.Append("<Item>");
                        LowFareSearchReq.Append("<Origin>" + TravarenaUtility.Left(searchInput.HidTxtArrCity6, 3) + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + TravarenaUtility.Left(searchInput.HidTxtDepCity6, 3) + "</Destination>");
                        LowFareSearchReq.Append("<Date>" + TravarenaUtility.Right(searchInput.DepDate6, 3) + "/" + TravarenaUtility.Mid(searchInput.DepDate6, 3, 2) + "/" + TravarenaUtility.Left(searchInput.DepDate6, 4) + "</Date>");
                        LowFareSearchReq.Append("</Item>");
                    }
                    LowFareSearchReq.Append("</Itineary>");
                }
                LowFareSearchReq.Append("<AirlineIdentification>");
                LowFareSearchReq.Append("<AirlineId>AI</AirlineId>");
                LowFareSearchReq.Append("<AirlineId>SG</AirlineId>");
                LowFareSearchReq.Append("</AirlineIdentification>");
                LowFareSearchReq.Append("<Paxreference>");
                LowFareSearchReq.Append("<Adults>" + searchInput.Adult + "</Adults>");
                LowFareSearchReq.Append("<Childs>" + searchInput.Child + "</Childs>");
                LowFareSearchReq.Append("<Infants>" + searchInput.Infant + "</Infants>");
                LowFareSearchReq.Append("</Paxreference>");
                LowFareSearchReq.Append("<HostAccess>Y</HostAccess>");
                LowFareSearchReq.Append("<ClassType>Economy</ClassType>");
                LowFareSearchReq.Append("<FareType>N</FareType>");
                LowFareSearchReq.Append("</Availability>]]>");
                LowFareSearchReq.Append("</api:strAvailability>");
                LowFareSearchReq.Append("</api:FlightAvailability>");
                LowFareSearchReq.Append("</soap:Body>");
                LowFareSearchReq.Append("</soap:Envelope>");
                strRes = TravarenaUtility.GetResponse("http://testapi.travarena.com/travarenaws/rws.asmx", LowFareSearchReq.ToString());
                TravarenaUtility.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "Availability_" + TTripType + "_" + TTrip + RTSF.ToString() + "_Req");
                TravarenaUtility.SaveXml(strRes, SecurityGUID, "Availability_" + TTripType + "_" + TTrip + RTSF.ToString() + "_Res");

            }
            catch (Exception ex)
            {

            }
            return strRes;
        }

        public string GetFareQuoteRequest(DataSet dsO, ref string exep)
        {
            string strRes = "";
            try
            {
                StringBuilder LowFareSearchReq = new StringBuilder();
                LowFareSearchReq.Append("<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:api='http://apiapps.riyawings.com/'>");
                LowFareSearchReq.Append("<soap:Body>");
                LowFareSearchReq.Append("<api:SellRequest>");
                LowFareSearchReq.Append("<api:objSecurity>");
                LowFareSearchReq.Append("<api:WebTerminal>RAGAU030010101</api:WebTerminal>");
                LowFareSearchReq.Append("<api:WebUserName>villa1</api:WebUserName>");
                LowFareSearchReq.Append("<api:WebPassword>u2pws01</api:WebPassword>");
                LowFareSearchReq.Append("<api:WebIp>122.176.101.130</api:WebIp>");
                LowFareSearchReq.Append("<api:WebAppType>API</api:WebAppType>");
                LowFareSearchReq.Append("</api:objSecurity>");
                LowFareSearchReq.Append("<api:strSellRequest>");
                LowFareSearchReq.Append("<![CDATA[<SellRequest>");
                LowFareSearchReq.Append("<TrackId>" + dsO.Tables[0].Rows[0]["Searchvalue"].ToString() + "</TrackId>");
                LowFareSearchReq.Append("<TripType>" + dsO.Tables[0].Rows[0]["TripType"].ToString() + "</TripType>");
                LowFareSearchReq.Append("<SegmentType>" + dsO.Tables[0].Rows[0]["Trip"].ToString() + "</SegmentType>");
                LowFareSearchReq.Append("<NoofAdults>" + dsO.Tables[0].Rows[0]["Adult"].ToString() + "</NoofAdults>");
                LowFareSearchReq.Append("<NoofChild>" + dsO.Tables[0].Rows[0]["Child"].ToString() + "</NoofChild>");
                LowFareSearchReq.Append("<NoofInfants>" + dsO.Tables[0].Rows[0]["Infant"].ToString() + "</NoofInfants>");
                LowFareSearchReq.Append("<FareType>" + dsO.Tables[0].Rows[0]["sno"].ToString().Split(':')[0] + "</FareType>");
                LowFareSearchReq.Append("<ItinearyDetails>");
                for (int i = 0; i < dsO.Tables.Count; i++)
                {
                    LowFareSearchReq.Append("<Segments>");
                    for (int j = 0; j < dsO.Tables[i].Rows.Count; j++)
                    {
                        LowFareSearchReq.Append("<item>");
                        LowFareSearchReq.Append("<FlightID>" + dsO.Tables[0].Rows[0]["flightidentification"].ToString() + "</FlightID>");
                        LowFareSearchReq.Append("<CarrierId>" + dsO.Tables[0].Rows[0]["operatingcarrier"].ToString() + "</CarrierId>");
                        LowFareSearchReq.Append("<Origin>" + dsO.Tables[0].Rows[0]["depairportcode"].ToString() + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + dsO.Tables[0].Rows[0]["arrairportcode"].ToString() + "</Destination>");
                        LowFareSearchReq.Append("<DepartureDateTime>" + dsO.Tables[0].Rows[0]["depdatelcc"].ToString() + "</DepartureDateTime>");
                        LowFareSearchReq.Append("<ArrivalDateTime>" + dsO.Tables[0].Rows[0]["arrdatelcc"].ToString() + "</ArrivalDateTime>");
                        LowFareSearchReq.Append("<ClassCode>" + dsO.Tables[0].Rows[0]["AdtCabin"].ToString() + "</ClassCode>");
                        LowFareSearchReq.Append("</item>");
                    }
                    LowFareSearchReq.Append("</Segments>");
                }
                LowFareSearchReq.Append("</ItinearyDetails>");
                LowFareSearchReq.Append("</SellRequest>]]>");
                LowFareSearchReq.Append("</api:strSellRequest>");
                LowFareSearchReq.Append("</api:SellRequest>");
                LowFareSearchReq.Append("</soap:Body>");
                LowFareSearchReq.Append("</soap:Envelope>");
                strRes = TravarenaUtility.GetResponse("http://testapi.travarena.com/travarenaws/rws.asmx", LowFareSearchReq.ToString());
                TravarenaUtility.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "SellRequest_" + "_Req");
                TravarenaUtility.SaveXml(strRes, SecurityGUID, "SellRequest_" + "_Res");

            }
            catch (Exception ex)
            {

            }
            return strRes;
        }
        public string ReGetFareQuoteRequest(DataTable dsO, ref string exep,ref Dictionary<string, string> Log)
        {
            string strRes = "";
            try
            {
                StringBuilder LowFareSearchReq = new StringBuilder();
                LowFareSearchReq.Append("<soap:Envelope xmlns:soap='http://www.w3.org/2003/05/soap-envelope' xmlns:api='http://apiapps.riyawings.com/'>");
                LowFareSearchReq.Append("<soap:Body>");
                LowFareSearchReq.Append("<api:SellRequestVersion2>");
                LowFareSearchReq.Append("<api:objSecurity>");
                LowFareSearchReq.Append("<api:WebTerminal>RAGAU030010101</api:WebTerminal>");
                LowFareSearchReq.Append("<api:WebUserName>villa1</api:WebUserName>");
                LowFareSearchReq.Append("<api:WebPassword>u2pws01</api:WebPassword>");
                LowFareSearchReq.Append("<api:WebIp>122.176.101.130</api:WebIp>");
                LowFareSearchReq.Append("<api:WebAppType>API</api:WebAppType>");
                LowFareSearchReq.Append("</api:objSecurity>");
                LowFareSearchReq.Append("<api:strSellRequest>");
                LowFareSearchReq.Append("<![CDATA[<SellRequest_V2>");
                LowFareSearchReq.Append("<TrackId>" + dsO.Rows[0]["Searchvalue"].ToString() + "</TrackId>");
                LowFareSearchReq.Append("<TripType>" + dsO.Rows[0]["TripType"].ToString() + "</TripType>");
                LowFareSearchReq.Append("<SegmentType>" + dsO.Rows[0]["Trip"].ToString() + "</SegmentType>");
                LowFareSearchReq.Append("<NoofAdult>" + dsO.Rows[0]["Adult"].ToString() + "</NoofAdult>");
                LowFareSearchReq.Append("<NoofChild>" + dsO.Rows[0]["Child"].ToString() + "</NoofChild>");
                LowFareSearchReq.Append("<NoofInfant>" + dsO.Rows[0]["Infant"].ToString() + "</NoofInfant>");
                LowFareSearchReq.Append("<FareType>" + dsO.Rows[0]["sno"].ToString().Split(':')[0] + "</FareType>");
                LowFareSearchReq.Append("<ItinearyDetails>");
                
                    LowFareSearchReq.Append("<Segments>");
                    for (int j = 0; j < dsO.Rows.Count; j++)
                    {
                        LowFareSearchReq.Append("<item>");
                        LowFareSearchReq.Append("<FlightID>" + dsO.Rows[0]["flightidentification"].ToString() + "</FlightID>");
                        LowFareSearchReq.Append("<CarrierId>" + dsO.Rows[0]["operatingcarrier"].ToString() + "</CarrierId>");
                        LowFareSearchReq.Append("<Origin>" + dsO.Rows[0]["depairportcode"].ToString() + "</Origin>");
                        LowFareSearchReq.Append("<Destination>" + dsO.Rows[0]["arrairportcode"].ToString() + "</Destination>");
                        LowFareSearchReq.Append("<DepartureDateTime>" + dsO.Rows[0]["depdatelcc"].ToString() + "</DepartureDateTime>");
                        LowFareSearchReq.Append("<ArrivalDateTime>" + dsO.Rows[0]["arrdatelcc"].ToString() + "</ArrivalDateTime>");
                        LowFareSearchReq.Append("<ClassCode>" + dsO.Rows[0]["AdtCabin"].ToString() + "</ClassCode>");
                        LowFareSearchReq.Append("</item>");
                    }
                    LowFareSearchReq.Append("</Segments>");
             
                LowFareSearchReq.Append("</ItinearyDetails>");
                LowFareSearchReq.Append("</SellRequest_V2>]]>");
                LowFareSearchReq.Append("</api:strSellRequest>");
                LowFareSearchReq.Append("</api:SellRequestVersion2>");
                LowFareSearchReq.Append("</soap:Body>");
                LowFareSearchReq.Append("</soap:Envelope>");
                strRes = TravarenaUtility.GetResponse("http://testapi.travarena.com/travarenaws/rws.asmx", LowFareSearchReq.ToString());
                TravarenaUtility.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "RESellRequest_" + "_Req");
                TravarenaUtility.SaveXml(strRes, SecurityGUID, "RESellRequest_" + "_Res");
                Log.Add("RepriceReq", LowFareSearchReq.ToString());
                // MULTIUtitlity.SaveFile(LowFareSearchReq.ToString(), "BookReq");
                Log.Add("RepriceResp", strRes);

            }
            catch (Exception ex)
            {

            }
            return strRes;
        }

        
    }

}
