﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GALWS.Travarena
{
    
    public class TravarenaUtility
    {

        public static string PostXml(string url, string xml, string Userid, string Pwd)
        {
            string strRes = "";
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                     | SecurityProtocolType.Tls11
                                     | SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    byte[] requestBytes = System.Text.Encoding.ASCII.GetBytes(xml);
                    request.Method = "POST";
                    request.Timeout = 300000;
                    request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                    request.Accept = "text/xml";
                    // request.KeepAlive = true;
                    //string authInfo = Userid + ":" + Pwd;
                    //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                    //request.Headers["Authorization"] = "Basic " + authInfo;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(requestBytes, 0, requestBytes.Length);
                        requestStream.Close();
                    }
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        // FlightSearchResults g = new FlightSearchResults();
                        Stream s = response.GetResponseStream();
                        if ((response.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            s = new GZipStream(s, CompressionMode.Decompress);
                        }
                        //Stream s = response.GetResponseStream();
                        StreamReader rdr = new StreamReader(s);
                        strRes = rdr.ReadToEnd();
                    }
                }
            }
            catch (WebException wex)
            {
                try
                {
                    strRes = "Error: " + new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();

                    // exep = " PostXML  :" + wex.Message + " StackTrace:" + wex.StackTrace;
                }
                catch { strRes = "Error: "; }
            }

            catch (Exception ex)
            {
                strRes = "Error: " + ex.Message + " StackTrace: " + ex.StackTrace;
                //   exep = exep + " PostXML :" + ex.Message + " StackTrace: " + ex.StackTrace;
            }

            return strRes;
        }

        public static string GetResponse(string url, string requestData)
        {
            string responseXML = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestData);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                     responseXML = new StreamReader(responseStream).ReadToEnd();
                     return responseXML;
                }               
            }
            catch (WebException webEx)
            {
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                String responseMessage = new StreamReader(stream).ReadToEnd();
            }
            return responseXML;
        }
        private static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                using (StreamReader streamRead = new StreamReader(streamResponse))
                {
                    Char[] readBuffer = new Char[256];
                    int count = streamRead.Read(readBuffer, 0, 256);

                    while (count > 0)
                    {
                        String resultData = new String(readBuffer, 0, count);
                        rawResponse.Append(resultData);
                        count = streamRead.Read(readBuffer, 0, 256);
                    }
                }
            }
            return rawResponse.ToString();
        }


        public static string ConvertToAgeFromDOB(string birthday)
        {
            //IFormatProvider culture = new CultureInfo("en-US", true);
            //DateTime dt = DateTime.Parse(birthday, culture);

            DateTime data;//= Convert.ToDateTime(birthday);
            DateTime.TryParseExact(birthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out data);
            DateTime now = DateTime.Today;
            int age = now.Year - data.Year;
            if (now < data.AddYears(age)) age--;
            if (age.ToString().Length == 1)
                return "0" + age.ToString();
            else
                return age.ToString();
        }
        public static void SaveXml(string Res, string securityToken, string filename)
        {
            string newFileName = Res + DateTime.Now.ToString("hh:mm:ss");

            string activeDir = ConfigurationManager.AppSettings["UapiLog"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + securityToken + @"\";
            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                XDocument xmlDoc = XDocument.Parse(Res);

                xmlDoc.Save(activeDir + @"\" + filename + ".xml");
            }
            catch (Exception ex)
            {
            }
        }
        public static string[] Split(string input, string seperator)
        {
            return input.Split(new string[1] { seperator }, StringSplitOptions.None);
        }
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }
        public static string Mid(string param, int startIndex, int endIndex)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, endIndex);
            //return the result of the operation
            return result;
        }
        public static string RemoveAlpha(string s)
        {
            return Regex.Replace(s, "[A-Za-z]", "");
        }

        public static void RemoveElementWithXDocument(string xmlFile, string nodeName, string elementName, string elementAttribute)
        {
            XDocument xDocument = XDocument.Load(xmlFile);
            try
            {
                foreach (XElement profileElement in xDocument.Descendants(nodeName))
                {
                    if (profileElement.Element(elementName).Value == elementAttribute)
                    {
                        profileElement.Remove();
                    }
                }
                xDocument.Save(xmlFile);
            }
            catch (Exception ex)
            {

            }
        }
        public static string RemoveElementWithXDocumentNode(string xmlFile, string nodeName, string elementName, string elementAttribute)
        {
            XDocument xDocument = XDocument.Load(xmlFile);
            try
            {
                foreach (XElement profileElement in xDocument.Descendants(nodeName))
                {

                    profileElement.Remove();
                }
                xDocument.Save(xmlFile);
            }
            catch (Exception ex)
            {

            }
            return xDocument.ToString();
        }
    }
}
