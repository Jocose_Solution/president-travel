﻿using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GALWS.Travarena
{
    public class TravarenaPricing
    {
        public ArrayList GetFareQuote(DataSet Crd, DataSet dsO, ref Dictionary<string, string> Log, ref string exep, string securityGUID)
        {
            ArrayList result = new ArrayList();
            float TotalAmount = 0;
            string responseXml = "";
            try
            {
                string username = "", password = "", serviceUrl = "", targetCode = "";
                TravarenaRequest obj = new TravarenaRequest(username, password, targetCode, serviceUrl, securityGUID);
                responseXml = obj.GetFareQuoteRequest(dsO, ref exep);
                #region Pricing
                if (responseXml.Contains("<Error>") == false)
                {

                    XNamespace soap = "http://www.w3.org/2003/05/soap-envelope";
                    XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                    XNamespace xmlns = "http://apiapps.riyawings.com/";
                    string xmlr = "";

                    xmlr = responseXml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty);
                    xmlr = responseXml.Replace("xmlns=\"http://apiapps.riyawings.com/\"", string.Empty);


                    XDocument xd = XDocument.Parse(xmlr);
                    var flts = xd.Descendants(soap + "Body").Descendants("SellRequestResult").ToArray();
                    string xmlrs = flts[0].ToString().Replace("&lt;", "<")
                                                       .Replace("&amp;", "&")
                                                       .Replace("&gt;", ">")
                                                       .Replace("&quot;", "\"")
                                                       .Replace("&apos;", "'");
                    xmlrs = xmlrs.Replace("<?xml", "<xml");
                    xmlrs = xmlrs.Replace("?>", "/>");
                    XDocument xd2 = XDocument.Parse(xmlrs);

                    foreach (var AvlR in xd2.Descendants("SellResponse").Descendants("ItinearyDetails"))
                    {
                        foreach (var Items in AvlR.Descendants("Segments"))
                        {
                            var FlightDetails = Items.Descendants("FlightDetails");
                            var FareDescription = Items.Descendants("FareDescription");
                            var ADTFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "ADT");
                            TotalAmount += float.Parse(ADTFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                            var CHDFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "CHD");
                            TotalAmount += float.Parse(CHDFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                            var INFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "INF");
                            TotalAmount += float.Parse(INFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                        }
                    }
                }

                #endregion
                result.Add(TotalAmount);
            }
            catch (Exception ex)
            {
                result.Add(TotalAmount);
                exep = exep + ex.Message + ex.StackTrace.ToString();
            }
            return result;
        }
        public ArrayList ReGetFareQuote(DataSet Crd, DataTable dsO, ref Dictionary<string, string> Log, ref string exep, string securityGUID,out string SellReferenceId)
        {
            ArrayList result = new ArrayList();
            float TotalAmount = 0;
            string responseXml = "", SellReferenceId2="";
            try
            {
                string username = "", password = "", serviceUrl = "", targetCode = "";
                TravarenaRequest obj = new TravarenaRequest(username, password, targetCode, serviceUrl, securityGUID);
                responseXml = obj.ReGetFareQuoteRequest(dsO, ref exep, ref Log);
                #region Pricing
                if (responseXml.Contains("<Error>") == false)
                {

                    XNamespace soap = "http://www.w3.org/2003/05/soap-envelope";
                    XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                    XNamespace xmlns = "http://apiapps.riyawings.com/";
                    string xmlr = "";

                    xmlr = responseXml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty);
                    xmlr = responseXml.Replace("xmlns=\"http://apiapps.riyawings.com/\"", string.Empty);


                    XDocument xd = XDocument.Parse(xmlr);
                    var flts = xd.Descendants(soap + "Body").Descendants("SellRequestVersion2Result").ToArray();
                    string xmlrs = flts[0].ToString().Replace("&lt;", "<")
                                                       .Replace("&amp;", "&")
                                                       .Replace("&gt;", ">")
                                                       .Replace("&quot;", "\"")
                                                       .Replace("&apos;", "'");
                    xmlrs = xmlrs.Replace("<?xml", "<xml");
                    xmlrs = xmlrs.Replace("?>", "/>");
                    XDocument xd2 = XDocument.Parse(xmlrs);

                    SellReferenceId2 = xd2.Descendants("SellResponse").First().Element("SellReferenceId").Value;

                    foreach (var AvlR in xd2.Descendants("SellResponse").Descendants("ItinearyDetails"))
                    {
                        foreach (var Items in AvlR.Descendants("Segments"))
                        {
                            var FlightDetails = Items.Descendants("FlightDetails");
                            var FareDescription = Items.Descendants("FareDescription");
                            var ADTFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "ADT");
                            TotalAmount += float.Parse(ADTFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                            var CHDFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "CHD");
                            TotalAmount += float.Parse(CHDFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                            var INFARELIST = FareDescription.Descendants("PaxFareDetails").Where(x => x.Element("PaxType").Value == "INF");
                            TotalAmount += float.Parse(INFARELIST.First().Element("OtherInfo").Element("GrossAmount").Value.ToString());
                        }
                    }
                }

                #endregion
                result.Add(TotalAmount);
            }
            catch (Exception ex)
            {
                result.Add(TotalAmount);
                exep = exep + ex.Message + ex.StackTrace.ToString();
            }
            SellReferenceId = SellReferenceId2;
            return result;
        }
    }
}
