﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Reflection;

namespace STD.BAL
{

    public class CacheRereshResp
    {
        public string TrackId { get; set; }
        public float CacheTotFare { get; set; }
        public float CacheNetFare { get; set; }
        public float NewTotFare { get; set; }
        public float NewNetFare { get; set; }
    }


    public class CacheRereshRespList
    {

        public CacheRereshResp ChangeFareO { get; set; }
        public CacheRereshResp ChangeFareR { get; set; }
    }

    public class FltSearchForCacheResult
    {
        public string ConStr { get; set; }

        public FltSearchForCacheResult(string connectionString)
        {
            ConStr = connectionString;
        }


        public ArrayList GetFltResult(string userId, string userType, DataSet dsO, DataSet dsR, bool isroundTripNRML, out CacheRereshRespList listFareChange)
        {
            ArrayList resultList = new ArrayList();
            listFareChange = new CacheRereshRespList();
            if (isroundTripNRML)
            {

               
                ArrayList rListO = new ArrayList();
                ArrayList rListR = new ArrayList();
                CacheRereshRespList listFareChangeO = new CacheRereshRespList();
                CacheRereshRespList listFareChangeR = new CacheRereshRespList();
                rListO = GetFltResultOneWay(userId, userType, dsO, isroundTripNRML, out listFareChangeO);
                rListR = GetFltResultOneWay(userId, userType, dsR, isroundTripNRML, out listFareChangeR);
                resultList.Add(rListO[0]);
                resultList.Add(rListR[0]);
                listFareChange.ChangeFareO = listFareChangeO.ChangeFareO;
                listFareChange.ChangeFareR = listFareChangeR.ChangeFareO;  
            }
            else
            {
                resultList = GetFltResultOneWay(userId, userType, dsO,  isroundTripNRML, out listFareChange);
        
            }


            return resultList;

        }


        public ArrayList GetFltResultOneWay(string userId, string userType, DataSet dsO,  bool isroundTripNRML, out CacheRereshRespList listFareChange)
        {
            STD.BAL.FlightCommonBAL FCBAL = new STD.BAL.FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            List<STD.Shared.UserFlightSearch> FCSHARED = new List<STD.Shared.UserFlightSearch>();
            FCSHARED = FCBAL.GetUserFlightSearch(userId, userType);
            DataRow row = dsO.Tables[0].Rows[0];
            ArrayList resultList = new ArrayList();
            listFareChange = new CacheRereshRespList();

            STD.Shared.FlightSearch req = new STD.Shared.FlightSearch();
            if (FCSHARED.Count > 0)
            {
                // TripType=rdbOneWay&txtDepCity1=New%20Delhi(DEL)&txtArrCity1=Mumbai(BOM)&hidtxtDepCity1=DEL,IN&hidtxtArrCity1=BOM,IN
                // &Adult=1&Child=0&Infant=0&Cabin=&txtAirline=Air%20India(AI)
                //  &hidtxtAirline=Air%20India,AI&txtDepDate=20/02/2015
                //    &txtRetDate=20/02/2015&RTF=FALSE
                //   &NStop=FALSE&RTF=FALSE&Trip=D&GRTF=FALSE
                req.Trip1 = "D";
                req.Trip = req.Trip1 == "D" ? STD.Shared.Trip.D : STD.Shared.Trip.I;

                req.Adult = Convert.ToInt16(dsO.Tables[0].Rows[0]["Adult"]);
                req.Child = Convert.ToInt16(dsO.Tables[0].Rows[0]["Child"]);
                req.Infant = Convert.ToInt16(dsO.Tables[0].Rows[0]["Infant"]);
                req.AgentType = FCSHARED[0].AgentType;

                req.AirLine = dsO.Tables[0].Rows[0]["AirLineName"].ToString() + "(" + dsO.Tables[0].Rows[0]["MarketingCarrier"].ToString() + ")";  //context.Request["AirLine"].ToString();
                req.HidTxtAirLine = dsO.Tables[0].Rows[0]["AirLineName"].ToString() + "," + dsO.Tables[0].Rows[0]["MarketingCarrier"].ToString();


                req.DepartureCity = dsO.Tables[0].Rows[0]["DepartureCityName"].ToString() + "(" + dsO.Tables[0].Rows[0]["DepartureLocation"].ToString() + ")";//20150311 context.Request["DepartureCity"].ToString() 060315;
                string depdate = dsO.Tables[0].Rows[0]["DepartureDate"].ToString().Trim();
                req.DepDate = depdate.Length == 8 ? STD.BAL.Utility.Right(depdate, 2) + "/" + STD.BAL.Utility.Mid(depdate, 4, 2) + "/" + STD.BAL.Utility.Left(depdate, 4) : STD.BAL.Utility.Left(depdate, 2) + "/" + STD.BAL.Utility.Mid(depdate, 2, 2) + "/20" + STD.BAL.Utility.Right(depdate, 2);
                req.HidTxtDepCity = dsO.Tables[0].Rows[0]["OrgDestFrom"].ToString() + ",IN"; // context.Request["HidTxtDepCity"].ToString();


                req.RTF = false;
                req.GDSRTF = false; 
                //if (isroundTripNRML)
                //{
                //    req.ArrivalCity = dsR.Tables[0].Rows[0]["DepartureCityName"].ToString() + "(" + dsR.Tables[0].Rows[0]["DepAirportCode"].ToString() + ")";
                //    req.HidTxtArrCity = dsR.Tables[0].Rows[0]["OrgDestFrom"].ToString() + ",IN";
                //    string retdate = dsR.Tables[0].Rows[0]["DepartureDate"].ToString().Trim();
                //    req.RetDate = retdate.Length == 8 ? STD.BAL.Utility.Right(retdate, 2) + "/" + STD.BAL.Utility.Mid(retdate, 4, 2) + "/" + STD.BAL.Utility.Left(retdate, 4) : STD.BAL.Utility.Left(retdate, 2) + "/" + STD.BAL.Utility.Mid(retdate, 2, 2) + "/20" + STD.BAL.Utility.Right(retdate, 2);

                //    req.TripType1 = "rdbRoundTrip";// "rdbRoundTrip"; context.Request["TripType1"].ToString();
                //    req.TripType = STD.Shared.TripType.RoundTrip;


                //}
                //else
                //{
                    DataRow[] datarowR = dsO.Tables[0].Select("Flight=2");
                    string retdate = "";
                    if (datarowR.Count() > 0)
                    {
                        req.RTF = true;
                        req.GDSRTF = true;
                        req.ArrivalCity = datarowR[0]["DepartureCityName"].ToString() + "(" + datarowR[0]["DepartureLocation"].ToString() + ")";
                        req.HidTxtArrCity = datarowR[0]["DepartureLocation"].ToString() + ",IN";
                        retdate = datarowR[0]["DepartureDate"].ToString().Trim();

                        req.RetDate = retdate.Length == 8 ? STD.BAL.Utility.Right(retdate, 2) + "/" + STD.BAL.Utility.Mid(retdate, 4, 2) + "/" + STD.BAL.Utility.Left(retdate, 4) : STD.BAL.Utility.Left(retdate, 2) + "/" + STD.BAL.Utility.Mid(retdate, 2, 2) + "/20" + STD.BAL.Utility.Right(retdate, 2);
                        req.TripType1 = "rdbRoundTrip";// "rdbRoundTrip"; context.Request["TripType1"].ToString();
                        req.TripType = STD.Shared.TripType.RoundTrip;
                    }
                    else
                    {
                        req.ArrivalCity = dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["ArrivalCityName"].ToString() + "(" + dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["ArrivalLocation"].ToString() + ")";
                        req.HidTxtArrCity = dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["ArrivalLocation"].ToString() + ",IN";

                        //retdate = dsO.Tables[0].Rows[dsO.Tables[0].Rows.Count - 1]["DepartureDate"].ToString().Trim();
                        req.RetDate = req.DepDate; //retdate.Length == 8 ? STD.BAL.Utility.Right(retdate, 2) + "/" + STD.BAL.Utility.Mid(retdate, 4, 2) + "/" + STD.BAL.Utility.Left(retdate, 4) : STD.BAL.Utility.Left(retdate, 2) + "/" + STD.BAL.Utility.Mid(retdate, 2, 2) + "/20" + STD.BAL.Utility.Right(retdate, 2);

                        req.TripType1 = "rdbOneWay";// "rdbRoundTrip"; context.Request["TripType1"].ToString();
                        req.TripType = STD.Shared.TripType.OneWay;

                    }



               // }


                req.DISTRID = "SPRING";
             




                req.IsCorp = Convert.ToBoolean(dsO.Tables[0].Rows[0]["IsCorp"]);//context.Request["IsCorp"]);
                req.NStop = false;
                req.OwnerId = FCSHARED[0].UserId;
                req.Cabin = "";
                string provider = dsO.Tables[0].Rows[0]["MarketingCarrier"].ToString();
                if (provider == "AI" || provider == "9W" || provider == "UK")
                {
                    req.Provider = "1G";
                    req.Cabin = dsO.Tables[0].Rows[0]["AdtCabin"].ToString().Trim() == "E" ? "Y" : "C";
                }
                else
                {
                    req.Provider = provider;
                }


                req.TypeId = FCSHARED[0].TypeId;
                req.UserType = FCSHARED[0].UserType;
                req.TDS = STD.BAL.Data.Calc_TDS(ConStr, req.OwnerId);
                req.UID = req.OwnerId;

                STD.Shared.IFlt objI = new STD.BAL.FltService();
                List<List<STD.Shared.FlightSearchResults>> rList = new List<List<STD.Shared.FlightSearchResults>>();
                //List<STD.Shared.FlightSearchResults> resultList1 = new List<STD.Shared.FlightSearchResults>( objI.FltSearchResult(req).ToArray(typeof(STD.Shared.FlightSearchResults)));
                bool isSplFare = false; 
                //if (dsO.Tables[0].Rows[0]["AdtFareType"].ToString().ToLower().Contains("special"))
                //{
                //   rList = objI.FltSearchResultCoupon(req).OfType<List<STD.Shared.FlightSearchResults>>().ToList();
                //   isSplFare = true;
                //}
                //else
                //{
                    rList = objI.FltSearchResult(req).OfType<List<STD.Shared.FlightSearchResults>>().ToList();
                //}
                //if (isroundTripNRML)
                //{
                //    CacheRereshResp resp = new CacheRereshResp();
                //    CacheRereshResp resp1 = new CacheRereshResp();
                //    resultList.Add(GetActualFlight(rList[0], dsO, out resp).Cast<object>().ToArray());
                //    resultList.Add(GetActualFlight(rList[1], dsR, out resp1).Cast<object>().ToArray());

                //    listFareChange.ChangeFareO = resp;
                //    listFareChange.ChangeFareR = resp1;
                //}
                //else
                //{
                    CacheRereshResp resp = new CacheRereshResp();
                    resultList.Add(GetActualFlight(rList[0], dsO, out resp, isSplFare).Cast<object>().ToArray());
                    listFareChange.ChangeFareO = resp;
                //}

            }

            return resultList;

        }




        public ArrayList GetActualFlight(List<STD.Shared.FlightSearchResults> rList, DataSet ds,out CacheRereshResp resp,bool isSplFare)
        {
            resp = new CacheRereshResp();
            List<STD.Shared.FlightSearchResults> result = new List<STD.Shared.FlightSearchResults>();
            StringBuilder key = new StringBuilder();
            ArrayList arr = new ArrayList();
            
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    key.Append(row["FlightIdentification"].ToString().Trim());
                    key.Append(row["AdtCabin"].ToString().Trim());
                    key.Append(row["ValiDatingCarrier"].ToString().Trim());
                    key.Append(row["Stops"].ToString().Trim());
                    key.Append(row["DepartureDate"].ToString().Trim());
                    key.Append(row["ArrivalDate"].ToString().Trim());
                    key.Append(row["DepartureTime"].ToString().Trim());
                    key.Append(row["ArrivalTime"].ToString().Trim());

                }

                var linumList = rList.Where(x => x.FlightIdentification.Trim() == ds.Tables[0].Rows[0]["FlightIdentification"].ToString().Trim()
                                      &&  x.AdtCabin.Trim().ToUpper() == ds.Tables[0].Rows[0]["AdtCabin"].ToString().Trim().ToUpper()
                                       && x.ValiDatingCarrier.Trim().ToUpper() == ds.Tables[0].Rows[0]["ValiDatingCarrier"].ToString().Trim().ToUpper()
                                        && x.Stops.Trim().ToUpper() == ds.Tables[0].Rows[0]["Stops"].ToString().Trim().ToUpper()
                                         && x.DepartureDate.Trim().ToUpper() == ds.Tables[0].Rows[0]["DepartureDate"].ToString().Trim().ToUpper()
                                          && x.ArrivalDate.Trim().ToUpper() == ds.Tables[0].Rows[0]["ArrivalDate"].ToString().Trim().ToUpper()
                                          ).ToList().Select(x => x.LineNumber).Distinct().ToArray();


            
                for (int i = 0; i < linumList.Count(); i++)
                {
                    var mlist = rList.Where(x => x.LineNumber == linumList[i]).ToList();

                    StringBuilder key1 = new StringBuilder();
                    for (int j = 0; j < mlist.Count; j++)
                    {
                        key1.Append(mlist[j].FlightIdentification.Trim());
                        key1.Append(mlist[j].AdtCabin.Trim());
                        key1.Append(mlist[j].ValiDatingCarrier.Trim());
                        key1.Append(mlist[j].Stops.Trim());
                        key1.Append(mlist[j].DepartureDate.Trim());
                        key1.Append(mlist[j].ArrivalDate.Trim());
                        key1.Append(mlist[j].DepartureTime.Trim());
                        key1.Append(mlist[j].ArrivalTime.Trim());
                    }

                    if (key.ToString().ToUpper().Trim() == key1.ToString().ToUpper().Trim())
                    {
                        result = mlist;

                        resp.NewNetFare = result[0].NetFare;
                        resp.NewTotFare = result[0].TotalFare;
                        resp.CacheNetFare = float.Parse(ds.Tables[0].Rows[0]["NetFare"].ToString());
                        resp.CacheTotFare = float.Parse(ds.Tables[0].Rows[0]["TotFare"].ToString());

                        if (isSplFare)
                        {
                            string orgSno = ds.Tables[0].Rows[0]["sno"].ToString().Split('/')[0].ToLower();
                            string newSno = result[0].sno.Split('/')[0].Trim().ToLower();

                            if (orgSno == newSno)
                            {
                                break;
                            }
                        }
                        else                           
                        {
                            break;
                        }

                      
                       
                    }

                }

              
                try
                {
                    for (int ii = 0; ii < result.Count; ii++)
                    {
                        PropertyInfo[] infos = result[ii].GetType().GetProperties();

                        Dictionary<string, object> dix = new Dictionary<string, object>();

                        foreach (PropertyInfo info in infos)
                        {
                            try
                            {
                                if (info.Name.Trim() == "ProductDetailQualifier")
                                {
                                    dix.Add(info.Name, "CACHE_REFRESH");
                                }
                                else
                                {
                                    dix.Add(info.Name, info.GetValue(result[ii], null));
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }

                        arr.Add(dix);
                    }
                }
                catch (Exception ex)
                {

                }

           

            return arr;//Newtonsoft.Json.JsonConvert.DeserializeObject<object[]>(r);;

        }

    }
}
