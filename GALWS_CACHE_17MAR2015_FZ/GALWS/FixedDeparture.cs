﻿using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Collections.Concurrent;
using System.Data.SqlClient;
using System.Configuration;
using STD.DAL;

namespace STD.BAL
{
    public class FixedDeparture
    {
        private SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        List<string> ResultList = new List<string>();

        public List<string> FltSearchResult(FlightSearch searchParam, bool isCoupon, string aircodeWithProvider, bool isLCC, HttpContext context, string callApi)
        {
            ArrayList vlist1 = new ArrayList();
            ArrayList vlist = new ArrayList();


            // List<Task> cList = new List<Task>();
            ConcurrentBag<Task> cList = new ConcurrentBag<Task>();
            HttpContext.Current = context;

            if (searchParam.Trip == Trip.D)
            {
                if (isLCC)
                {
                    if (aircodeWithProvider.ToUpper().Contains("SS") && callApi.Trim().ToUpper() != "6E")
                    {
                        int a = 0;
                        SequeneFare objSeq = new SequeneFare(Con.ConnectionString);
                        if (a == 0)
                        {
                            a = 1;
                            if (aircodeWithProvider.ToUpper().Contains("SS") && isCoupon == false)
                            {
                                string resultJsonSequence = "";
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "SG";
                                resultJsonSequence = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objSeq.GetSequenceFareList(newFltSearch, "FDD", "")) + ",\"Provider\":\"FDD\"}";
                                if (resultJsonSequence.Length > 100)
                                {
                                    ResultList.Add(resultJsonSequence);
                                }
                            }

                        }
                    }
                    else
                    {

                    }

                   
                }
                else
                {
                    if (searchParam.TripType == TripType.MultiCity)
                    {
                        cList.Add(Task.Run(() => ArrangeServiceCall(searchParam.HidTxtAirLine, "SPL:1G", searchParam, isCoupon, context)));

                    }
                    else
                    {

                        if (aircodeWithProvider.ToUpper().Contains("AI1"))
                        {
                            //Thread thAI = new Thread(() => ArrangeServiceCall("AI", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                            //thAI.Start();
                            //vlist.Add(thAI);
                            //vlist1.Add(DateTime.Now);

                            string resultJsonCache = "";
                            if (aircodeWithProvider.ToUpper().Contains("AI") && isCoupon == false)
                            {
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "AI";
                                CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                                resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "AI")) + ",\"Provider\":\"AINRML\"}";
                            }
                            if (resultJsonCache.Length > 100)
                            {
                                ResultList.Add(resultJsonCache);
                            }
                            else
                            {
                                if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "AI")
                                    cList.Add(Task.Run(() => ArrangeServiceCall("AI", aircodeWithProvider, searchParam, isCoupon, context)));
                            }

                        }
                     

                     
                    }
                }


            }
            else if (searchParam.Trip == Trip.I)
            {
                if (isLCC)
                {
                    if (aircodeWithProvider.ToUpper().Contains("SS") && callApi.Trim().ToUpper() != "6E")
                    {
                        int a = 0;
                        SequeneFare objSeq = new SequeneFare(Con.ConnectionString);
                        if (a == 0)
                        {
                            a = 1;
                            if (aircodeWithProvider.ToUpper().Contains("SS") && isCoupon == false)
                            {
                                string resultJsonSequence = "";
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "SG";
                                resultJsonSequence = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objSeq.GetSequenceFareList(newFltSearch, "FDD", "")) + ",\"Provider\":\"FDD\"}";
                                if (resultJsonSequence.Length > 100)
                                {
                                    ResultList.Add(resultJsonSequence);
                                }
                            }
                        }
                    }

                }
                else
                {
                    // if (searchParam.HidTxtAirLine.ToUpper().Trim() != "6E" || searchParam.HidTxtAirLine.ToUpper().Trim() != "SG")
                    //if (searchParam.HidTxtAirLine.ToUpper().Trim() != "6E" && searchParam.HidTxtAirLine.ToUpper().Trim() != "SG" && searchParam.HidTxtAirLine.ToUpper().Trim() != "G8" && searchParam.HidTxtAirLine.ToUpper().Trim() != "AK")
                    //{
                    //    cList.Add(Task.Run(() => ArrangeServiceCall(searchParam.HidTxtAirLine, "SPL:1G", searchParam, isCoupon, context)));
                    //}

                }

            }


            // Task.WhenAll(cList).Wait(300000);

            var continuation = Task.WhenAll(cList);
            try
            {
                continuation.Wait(300000);
            }
            catch (AggregateException)
            { }



            //int counter = 0;
            //while ((counter < vlist.Count))
            //{
            //    Thread TH = (Thread)vlist[counter];
            //    if ((TH.ThreadState == ThreadState.WaitSleepJoin))
            //    {
            //        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
            //        if ((DIFF.Seconds > 45))
            //        {
            //            TH.Abort();
            //            counter += 1;
            //        }
            //    }
            //    else if ((TH.ThreadState == ThreadState.Stopped))
            //    {
            //        counter += 1;
            //    }
            //}





            return ResultList;
        }


        public async Task ArrangeServiceCall(string airCode, string aircodeWithProvider, FlightSearch searchParam, bool isCoupon, HttpContext context)
        {

            string[] sep = { airCode };

            if (aircodeWithProvider.Contains("SPL"))
            {
                sep = new string[] { "SPL" };
            }


            string[] sep1 = { "-" };
            string prov = aircodeWithProvider.ToUpper().Split(sep, StringSplitOptions.None)[1].Split(sep1, StringSplitOptions.None)[0];
            await CallSearch(searchParam, airCode, prov.Replace(":", ""), isCoupon, context);// new Thread(new ThreadStart(SpicejetAvailability_R));



        }



        public async Task CallSearch(FlightSearch searchParam, string aircode, string provider, bool isCoupon, HttpContext context)
        {
            string resultJson = "";
            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
            newFltSearch.HidTxtAirLine = aircode;//aircode == "SPL" ? "" :
            await Task.Delay(0);
            try
            {
                if (isCoupon)
                { newFltSearch.Provider = aircode; }
                else
                {
                    newFltSearch.Provider = provider;
                }
                STD.Shared.IFlt objI = new STD.BAL.FltService();
                HttpContext.Current = context;
                //if (isCoupon)
                //{
                //    resultJson = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objI.FltSearchResultCoupon(newFltSearch)) + ",\"Provider\":\"" + aircode + "CPN\"}";
                //}
                //else
                //{
                    resultJson = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objI.FltSearchResult(newFltSearch)) + ",\"Provider\":\"" + aircode + "NRML\"}";
                //}
            }
            catch (Exception ex) { }

            ResultList.Add(resultJson);
        }

    }
}
