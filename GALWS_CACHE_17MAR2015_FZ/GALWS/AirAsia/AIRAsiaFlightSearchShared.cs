﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.AirAsia
{
    class AIRAsiaFlightSearchShared
    {
        public string FarebasisCode { get; set; }
        public string FareClass { get; set; }
        public decimal BaseFare { get; set; }//ABfare = 0, ATax = 0, AOT = 0
        public decimal Tax { get; set; }
        public decimal OtherTax { get; set; }
        public string RuleNumber { get; set; }
        public string PaxType { get; set; }
    }
    public class AirAsiaSSR
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string SSRType { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public string WayType { get; set; }
    }
    public class AirAsiaSeat
    {
        public string Code { get; set; }
        public string SeatType { get; set; }
        public decimal Amount { get; set; }
        public string SeatGroup { get; set; }
        public string SeatDesignator { get; set; }
        public string Sector { get; set; }
    }
    public class SeatMapFinal
    {
        public List<SeatMap> SeatMapDetails_Final { get; set; }       
        public int Columns { get; set; }      
        public int Rows { get; set; }      
        public List<Alpha> Alpha { get; set; }
    }
  
    public class SeatMap
    {
        public string ArrivalStation { get; set; }
       
        public string DepartureStation { get; set; }
       
        public string EquipmentType { get; set; }
       
        public string AvailableUnits { get; set; }
       
        public string EquipmentCategory { get; set; }
       
        public string Aircraft { get; set; }
       
        public string FlightNumber { get; set; }
       
        public string FlightTime { get; set; }
       
        public List<SeatMapDetails> SeatMapDetails { get; set; }
       
        public string Error { get; set; }
        public int Columns { get; set; }
       
        public int Rows { get; set; }
       
        public string EquipmentTypeSuffix { get; set; }
       
        public string ClassOfService { get; set; }
    }
   
    public class SeatMapDetails
    {
       
        public short Deck { get; set; }
       
        public int MaxRows { get; set; }
       
        public int MaxColumn { get; set; }
       
        public string AircraftName { get; set; }
       
        public string EquipmentCategory { get; set; }
       
        public int AvailableUnits { get; set; }
       
        public List<SeatList> SeatListDetails { get; set; }
    }
  
    public class SeatList
    {
       
        public int RowNo { get; set; }
       
        public int ColumnNo { get; set; }
       
        public bool Assignable { get; set; }
       
        public int SeatSet { get; set; }
       
        public short SeatAngle { get; set; }
       
        public string SeatAvailability { get; set; }
       
        public string SeatDesignator { get; set; }
       
        public string SeatType { get; set; }
       
        public string TravelClassCode { get; set; }
       
        public short SeatGroup { get; set; }
       
        public bool PremiumSeatIndicator { get; set; }
       
        public decimal SeatFee { get; set; }
       
        public decimal CGST { get; set; }
       
        public decimal SGST { get; set; }
       
        public decimal IGST { get; set; }
       
        public decimal UGST { get; set; }
       
        public string SeatStatus { get; set; }
       
        public string ExitSeats { get; set; }
       
        public string Message { get; set; }
       
        public string SeatAlignment { get; set; }
       
        public string FlightNumber { get; set; }
       
        public string FlightTime { get; set; }
       
        public bool Paid { get; set; }
       
        public List<Characteristic> Characteristic { get; set; }
       
        public string OptionalServiceRef { get; set; }
       
        public string Group { get; set; }
       
        public string ClassOfService { get; set; }
       
        public string Equipment { get; set; }
       
        public string Carrier { get; set; }
       
        public string SegmentRef { get; set; }
    }
    public class Alpha
    {
        public string Value { get; set; }
    }
    public class Characteristic
    {
        public string Value { get; set; }
    }
    public class CurrencyRate
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CurrancyCode { get; set; }
        public string CurrancyName { get; set; }
        public string CurrancySymble { get; set; }
        public decimal ExchangeRate { get; set; }
        public string UpdateDate { get; set; }
        public int IDS { get; set; }
    }
    //public class MySeatDetails
    //{
    //    public string Pax { get; set; }
    //    public string Sector { get; set; }
    //    public string Seat { get; set; }
    //    public string Amount { get; set; }
    //    public string HtmlID { get; set; }
    //    public string SeatAlignment { get; set; }
    //    public string FName { get; set; }
    //    public string LName { get; set; }
    //    public string Title { get; set; }
    //    public string Tpcode { get; set; }
    //    public string FlightNumber { get; set; }
    //    public string FlightTime { get; set; }

    //    public string OptionalServiceRef { get; set; }
    //    public string Group { get; set; }
    //    public string ClassOfService { get; set; }
    //    public string Equipment { get; set; }
    //    public string Carrier { get; set; }
    //    public bool Paid { get; set; }
    //    // Fill the missing properties for your data
    //}
    //public class SeatRequest
    //{
    //    public int Adult { get; set; }
    //    public int Child { get; set; }
    //    public int Infant { get; set; }
    //    public string Provider { get; set; }
    //    public string ValidatingCarrier { get; set; }
    //    public string Trip { get; set; }
    //    public List<SeatRequestSegment> AirSegList { get; set; }
    //    public List<UAPIPrvtFareCode> PrvtFareCodeList { get; set; }
    //    public float AdtFare { get; set; }
    //    public float ChdFare { get; set; }
    //    public float TotalFare { get; set; }
    //    public string TCCode { get; set; }
    //    public string IDType { get; set; }
    //    public string AdtCabin { get; set; }
    //    public string ChdCabin { get; set; }
    //    public string InfCabin { get; set; }
    //}
    //public class SeatRequestSegment
    //{
    //    public string Key { get; set; }
    //    public string Group { get; set; }
    //    public string Conx { get; set; }
    //    public string Carrier { get; set; }
    //    public string FlightNumber { get; set; }
    //    public string Origin { get; set; }
    //    public string Destination { get; set; }
    //    public string DepartureTime { get; set; }
    //    public string ArrivalTime { get; set; }
    //    public string ETicketability { get; set; }
    //    public string Equipment { get; set; }
    //    public string Provider { get; set; }
    //    public string AdtRbd { get; set; }
    //    public string AdtFBCode { get; set; }
    //    public string ChdRbd { get; set; }
    //    public string AChdFBCode { get; set; }
    //    public string InfRbd { get; set; }
    //    public string InfFBCode { get; set; }
    //}
    //public class Seat
    //{
        
    //    public int Counter { get; set; }
        
    //    public int PaxId { get; set; }
        
    //    public string OrderId { get; set; }
        
    //    public string SeatDesignator { get; set; }
        
    //    public int Amount { get; set; }
        
    //    public string Origin { get; set; }
        
    //    public string Destination { get; set; }
        
    //    public string VC { get; set; }
        
    //    public string CreatedDate { get; set; }
        
    //    public string FlightNumber { get; set; }
        
    //    public string FlightTime { get; set; }
        
    //    public string SeatAlignment { get; set; }
        
    //    public string OptionalServiceRef { get; set; }
        
    //    public string Group { get; set; }
        
    //    public string ClassOfService { get; set; }
        
    //    public string Equipment { get; set; }
        
    //    public string Carrier { get; set; }
        
    //    public bool Paid { get; set; }
        
    //    public bool IsSeat { get; set; }
        
    //    public string SeatStatus { get; set; }
    //}
    //public class AssignSeat
    //{
    //    public string Airline { get; set; }

    //    public int Columns { get; set; }

    //    public int Rows { get; set; }

    //    public List<SeatMapFinal> SeatMapAll { get; set; }

    //    public List<LNBCORPSHARED.Traveller.TDPaxInfo> PaxListDetails { get; set; }

    //    public string Error { get; set; }

    //    public string Provider { get; set; }
    //}

 
}
