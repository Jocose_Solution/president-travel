﻿using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace GALWS
{
    public static class KeyGenerator
    {
        public static ArrayList AddSuperLineNum(this ArrayList @this, STD.Shared.TripType triptype, bool isSpl)
        {
            //this ArrayList @this, TrpType triptype, bool isSpl
            //this ArrayList @this, STD.Shared.TripType triptype , bool isSpl
            List<string> subKeyO = new List<string>();
            List<string> subKeyR = new List<string>();



            // if (triptype == TrpType.M || triptype == TrpType.O || triptype == TrpType.R)
            if (triptype == STD.Shared.TripType.MultiCity || triptype == STD.Shared.TripType.OneWay || triptype == STD.Shared.TripType.RoundTrip)
            {
                List<List<string>> subkeyList = new List<List<string>>();

                @this.ToArray().ToList().ForEach(x =>
                {
                    ArrayList lst = x as ArrayList;
                    for (int i = 0; i < lst.Count; i++)
                    {
                        if (subkeyList.Count > i)
                            subkeyList[i].AddRange(((List<FlightSearchResults>)lst[i]).Select(y => y.MainKey).ToList());
                        else
                            subkeyList.Add(((List<FlightSearchResults>)lst[i]).Select(y => y.MainKey).ToList());
                    }

                });


                // subKeyO = subKeyO.Distinct().ToList();
                // int subLn = 1;



                for (int j = 0; j < subkeyList.Count; j++)
                {

                    subkeyList[j] = subkeyList[j].Distinct().ToList();
                    int lno = 1;
                    subkeyList[j].ForEach(y =>
                    {

                        @this.ToArray().ToList().ForEach(x =>
                        {
                            for (int i = 0; i < ((ArrayList)x).Count; i++)
                            {

                                List<FlightSearchResults> lst = ((ArrayList)x)[i] as List<FlightSearchResults>;

                                if (lst.Where(p => p.MainKey == y).ToList().Count > 0)
                                {
                                    lst.Where(p => p.MainKey == y).ToList().ForEach(z => { z.SLineNumber = lno.ToString().Trim(); z.SubSLineNumber = (i + 1).ToString(); });

                                }

                                // subLn++;
                            }

                        });

                        lno++;
                    });

                }






            }

            else if (triptype == STD.Shared.TripType.RoundTrip && isSpl == false)
            {
                //else if (triptype == STD.Shared.TripType.RoundTrip && isSpl == false)
                subKeyO = new List<string>();
                subKeyR = new List<string>();
                @this.ToArray().ToList().ForEach(x =>
                {
                    ArrayList lst = x as ArrayList;
                    subKeyO.AddRange(((List<FlightSearchResults>)lst[0]).Select(y => y.SubKey).ToList());
                    subKeyR.AddRange(((List<FlightSearchResults>)lst[1]).Select(y => y.SubKey).ToList());
                });

                subKeyO = subKeyO.Distinct().ToList();
                subKeyR = subKeyR.Distinct().ToList();

                @this.ToArray().ToList().ForEach(x =>
                {
                    int lno = 1;
                    int lnr = 1;
                    ArrayList lst = x as ArrayList;
                    subKeyO.ForEach(y =>
                    {
                        if (((List<FlightSearchResults>)lst[0]).Where(p => p.SubKey == y).ToList().Count() > 0)
                        {
                            ((List<FlightSearchResults>)lst[0]).Where(p => p.SubKey == y).ToList().ForEach(z => { z.SLineNumber = lno.ToString().Trim(); });
                            lno++;
                        }
                    });

                    subKeyR.ForEach(y =>
                    {
                        if (((List<FlightSearchResults>)lst[1]).Where(p => p.SubKey == y).ToList().Count() > 0)
                        {
                            ((List<FlightSearchResults>)lst[1]).Where(p => p.SubKey == y).ToList().ForEach(z => { z.SLineNumber = lnr.ToString().Trim(); });
                            lnr++;
                        }
                    });

                });


                //@this.ToArray().ToList().ForEach(x =>
                //{
                //    List<FlightSearchResults> lst = x as List<FlightSearchResults>;


                //});



            }
            else
            {
                subKeyO = new List<string>();
                @this.ToArray().ToList().ForEach(x =>
                {
                    List<FlightSearchResults> lst = ((ArrayList)x)[0] as List<FlightSearchResults>;
                    subKeyO.AddRange(lst.Select(y => y.MainKey).ToList());
                });

                subKeyO = subKeyO.Distinct().ToList();
                int subLn = 1;

                subKeyO.ForEach(y =>
                {
                    int lno = 1;
                    @this.ToArray().ToList().ForEach(x =>
                    {
                        List<FlightSearchResults> lst = ((ArrayList)x)[0] as List<FlightSearchResults>;

                        lst.Where(p => p.MainKey == y).ToList().ForEach(z => { z.SLineNumber = lno.ToString().Trim(); z.SubSLineNumber = subLn.ToString(); });

                        subLn++;

                    });
                    lno++;
                });
            }



            return @this;
        }

        public static ArrayList AddDisplayFare1(this ArrayList @this, List<FlightFareType> listFareType)
        {
            if (listFareType != null && listFareType.Count > 0)
            {
                // DisplayFareType
                //SELECT FareType, DisplayName FROM FareTypeMaster order by FareType asc
                string FareType = "NRM";
                string query = "";
                //query = "(FareType = '" + FareType + "' )";
                //DataRow[] foundRows = dt.Select(query);

                @this.ToArray().ToList().ForEach(x =>
                {
                    for (int i = 0; i < ((ArrayList)x).Count; i++)
                    {

                        List<FlightSearchResults> lst = ((ArrayList)x)[i] as List<FlightSearchResults>;

                        if (lst.Where(p => p.MainKey == p.MainKey).ToList().Count > 0)
                        {
                            lst.Where(p => p.MainKey == p.MainKey).ToList().ForEach(z =>
                            {
                                z.DisplayFareType = "NRM";
                            }
                            );

                        }

                        // subLn++;
                    }

                });



            }

            return @this;
        }

        public static ArrayList AddSuperLineNumNew(this ArrayList @this, STD.Shared.TripType triptype, bool isSpl, List<FlightFareType> listFareType)
        {
            //this ArrayList @this, TrpType triptype, bool isSpl
            //this ArrayList @this, STD.Shared.TripType triptype , bool isSpl
            List<string> subKeyO = new List<string>();
            List<string> subKeyR = new List<string>();



            // if (triptype == TrpType.M || triptype == TrpType.O || triptype == TrpType.R)
            if (triptype == STD.Shared.TripType.MultiCity || triptype == STD.Shared.TripType.OneWay || triptype == STD.Shared.TripType.RoundTrip)
            {
                List<List<string>> subkeyList = new List<List<string>>();

                @this.ToArray().ToList().ForEach(x =>
                {
                    ArrayList lst = x as ArrayList;
                    for (int i = 0; i < lst.Count; i++)
                    {
                        if (subkeyList.Count > i)
                            subkeyList[i].AddRange(((List<FlightSearchResults>)lst[i]).Select(y => y.MainKey).ToList());
                        else
                            subkeyList.Add(((List<FlightSearchResults>)lst[i]).Select(y => y.MainKey).ToList());
                    }

                });


                // subKeyO = subKeyO.Distinct().ToList();
                // int subLn = 1;



                for (int j = 0; j < subkeyList.Count; j++)
                {

                    subkeyList[j] = subkeyList[j].Distinct().ToList();
                    int lno = 1;
                    subkeyList[j].ForEach(y =>
                    {

                        @this.ToArray().ToList().ForEach(x =>
                        {
                            for (int i = 0; i < ((ArrayList)x).Count; i++)
                            {

                                List<FlightSearchResults> lst = ((ArrayList)x)[i] as List<FlightSearchResults>;


                                if (lst.Where(p => p.MainKey == y).ToList().Count > 0)
                                {
                                    lst.Where(p => p.MainKey == y).ToList().ForEach(z => { z.SLineNumber = lno.ToString().Trim(); z.SubSLineNumber = (i + 1).ToString();
                                        z.DisplayFareType = listFareType.Where(p => p.FareType == z.AdtFar).Any() ? listFareType.Where(p => p.FareType == z.AdtFar).First().DisplayName : z.AdtFar;
                                    });

                                }

                                // subLn++;
                            }

                        });

                        lno++;
                    });

                }






            }

            else if (triptype == STD.Shared.TripType.RoundTrip && isSpl == false)
            {
                //else if (triptype == STD.Shared.TripType.RoundTrip && isSpl == false)
                subKeyO = new List<string>();
                subKeyR = new List<string>();
                @this.ToArray().ToList().ForEach(x =>
                {
                    ArrayList lst = x as ArrayList;
                    subKeyO.AddRange(((List<FlightSearchResults>)lst[0]).Select(y => y.SubKey).ToList());
                    subKeyR.AddRange(((List<FlightSearchResults>)lst[1]).Select(y => y.SubKey).ToList());
                });

                subKeyO = subKeyO.Distinct().ToList();
                subKeyR = subKeyR.Distinct().ToList();

                @this.ToArray().ToList().ForEach(x =>
                {
                    int lno = 1;
                    int lnr = 1;
                    ArrayList lst = x as ArrayList;
                    subKeyO.ForEach(y =>
                    {
                        if (((List<FlightSearchResults>)lst[0]).Where(p => p.SubKey == y).ToList().Count() > 0)
                        {
                            ((List<FlightSearchResults>)lst[0]).Where(p => p.SubKey == y).ToList().ForEach(z => { z.SLineNumber = lno.ToString().Trim();
                                z.DisplayFareType = listFareType.Where(p => p.FareType == z.AdtFar).Any() ? listFareType.Where(p => p.FareType == z.AdtFar).First().DisplayName : z.AdtFar;
                            });
                            lno++;
                        }
                    });

                    subKeyR.ForEach(y =>
                    {
                        if (((List<FlightSearchResults>)lst[1]).Where(p => p.SubKey == y).ToList().Count() > 0)
                        {
                            ((List<FlightSearchResults>)lst[1]).Where(p => p.SubKey == y).ToList().ForEach(z => { z.SLineNumber = lnr.ToString().Trim();
                                z.DisplayFareType = listFareType.Where(p => p.FareType == z.AdtFar).Any() ? listFareType.Where(p => p.FareType == z.AdtFar).First().DisplayName : z.AdtFar;
                            });
                            lnr++;
                        }
                    });

                });


                //@this.ToArray().ToList().ForEach(x =>
                //{
                //    List<FlightSearchResults> lst = x as List<FlightSearchResults>;


                //});



            }
            else
            {
                subKeyO = new List<string>();
                @this.ToArray().ToList().ForEach(x =>
                {
                    List<FlightSearchResults> lst = ((ArrayList)x)[0] as List<FlightSearchResults>;
                    subKeyO.AddRange(lst.Select(y => y.MainKey).ToList());
                });

                subKeyO = subKeyO.Distinct().ToList();
                int subLn = 1;

                subKeyO.ForEach(y =>
                {
                    int lno = 1;
                    @this.ToArray().ToList().ForEach(x =>
                    {
                        List<FlightSearchResults> lst = ((ArrayList)x)[0] as List<FlightSearchResults>;


                        lst.Where(p => p.MainKey == y).ToList().ForEach(z =>
                        {
                            z.SLineNumber = lno.ToString().Trim(); z.SubSLineNumber = subLn.ToString();
                            z.DisplayFareType = listFareType.Where(p => p.FareType == z.AdtFar).Any() ? listFareType.Where(p => p.FareType == z.AdtFar).First().DisplayName : z.AdtFar;
                        });

                        subLn++;

                    });
                    lno++;
                });
            }



            return @this;
        }

    }
}
