﻿using STD.BAL.TBO.BOOKSHARED;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Collections;

namespace STD.BAL.TBO
{
    public class TBOBook
    {

        public string TBOFightBook(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, ref ArrayList TktNoArray, string Constr, ref string bookingID)
        {
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";

            string exep = "";

            try
            {

                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");

                TboBookReq objBookReq = new TboBookReq();

                objBookReq.EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);
                TBOAuthProcess objTbProcess = new TBOAuthProcess();
                string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), objBookReq.EndUserIp);
                objBookReq.TokenId = strTokenID;

                string[] sno = Convert.ToString(FltDT.Rows[0]["sno"]).Split(':');

                objBookReq.TraceId = sno[1];
                objBookReq.ResultIndex = sno[0];
                bool islcc = Convert.ToBoolean(sno[2]);


                objBookReq.Passengers = new List<Passenger>();

                List<Passenger> paxList = new List<Passenger>();

                string[] sep = { "itzDanata420" };
                string[] FareArray = Convert.ToString(FltDT.Rows[0]["Searchvalue"]).Split(sep, StringSplitOptions.RemoveEmptyEntries);

                Fare fareMain = JsonConvert.DeserializeObject<STD.BAL.TBO.Fare>(FareArray[0]);
                List<FareBreakdown> fareBrkup = JsonConvert.DeserializeObject<List<FareBreakdown>>(FareArray[1]);

                #region FareQuote
                FareQuote.FareQuoteResp fResp = GetFareQuote(Crd, objBookReq.TraceId, objBookReq.ResultIndex, ref Log, ref exep);


                decimal totfare = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]);

                if (totfare > 0 && Convert.ToDecimal(fResp.Response.Results.Fare.OfferedFare) > 0 && totfare < Convert.ToDecimal(fResp.Response.Results.Fare.OfferedFare))
                {
                    goto FareMisMatch;
                }

                #endregion


                #region adult

                if (ADTPax.Count() > 0)
                {
                    for (int i = 0; i < ADTPax.Count(); i++)
                    {
                        Passenger pax = new Passenger();
                        pax.FirstName = Convert.ToString(ADTPax[i]["FName"]);
                        pax.LastName = Convert.ToString(ADTPax[i]["LName"]);
                        pax.IsLeadPax = i == 0 ? true : false;
                        pax.Title = Convert.ToString(ADTPax[i]["Title"]);
                        pax.DateOfBirth = ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";
                        pax.AddressLine1 = ConfigurationManager.AppSettings["companyaddress1"];
                        pax.AddressLine2 = ConfigurationManager.AppSettings["companyaddress2"];
                        pax.City = ConfigurationManager.AppSettings["companycity"];
                        pax.CountryCode = "IN";
                        pax.CountryName = "India";//ConfigurationManager.AppSettings["companyname"];
                        pax.ContactNo = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]);
                        pax.Email = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]);
                        pax.Fare = new STD.BAL.TBO.BOOKSHARED.Fare();
                        pax.PaxType = 1;
                        STD.BAL.TBO.BOOKSHARED.Fare paxFare = new STD.BAL.TBO.BOOKSHARED.Fare();
                        FareBreakdown frb = fareBrkup.Where(x => x.PassengerType == 1).FirstOrDefault();
                        paxFare.BaseFare = frb.BaseFare / frb.PassengerCount;
                        paxFare.YQTax = frb.YQTax / frb.PassengerCount;
                        paxFare.AdditionalTxnFeeOfrd = 0;
                        paxFare.AdditionalTxnFeePub = frb.AdditionalTxnFeePub / frb.PassengerCount;
                        paxFare.Tax = frb.Tax / frb.PassengerCount;
                        paxFare.AdditionalTxnFee = 0;
                        pax.Meal = new STD.BAL.TBO.BOOKSHARED.Meal();
                        pax.Seat = new STD.BAL.TBO.BOOKSHARED.Seat();
                        string gndr = Convert.ToString(ADTPax[i]["Gender"]).Trim().ToUpper();
                        pax.Gender = gndr == "M" ? 1 : 2;
                        pax.Fare = paxFare;
                        paxList.Add(pax);

                    }


                }
                #endregion

                #region Child

                if (CHDPax.Count() > 0)
                {
                    for (int i = 0; i < CHDPax.Count(); i++)
                    {
                        Passenger pax = new Passenger();
                        pax.FirstName = Convert.ToString(CHDPax[i]["FName"]);
                        pax.LastName = Convert.ToString(CHDPax[i]["LName"]);
                        pax.IsLeadPax = i == 0 ? true : false;
                        pax.Title = Convert.ToString(CHDPax[i]["Title"]);
                        pax.DateOfBirth = CHDPax[i]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";
                        pax.AddressLine1 = ConfigurationManager.AppSettings["companyaddress1"];
                        pax.AddressLine2 = ConfigurationManager.AppSettings["companyaddress2"];
                        pax.City = ConfigurationManager.AppSettings["companycity"];
                        pax.CountryCode = "IN";
                        pax.CountryName = "India";//ConfigurationManager.AppSettings["companyaddress1"];
                        pax.ContactNo = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]);
                        pax.Email = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]);
                        pax.Fare = new STD.BAL.TBO.BOOKSHARED.Fare();
                        pax.PaxType = 2;
                        STD.BAL.TBO.BOOKSHARED.Fare paxFare = new STD.BAL.TBO.BOOKSHARED.Fare();
                        FareBreakdown frb = fareBrkup.Where(x => x.PassengerType == 2).FirstOrDefault();
                        paxFare.BaseFare = frb.BaseFare / frb.PassengerCount;
                        paxFare.YQTax = frb.YQTax / frb.PassengerCount;
                        paxFare.AdditionalTxnFeeOfrd = 0;
                        paxFare.AdditionalTxnFeePub = frb.AdditionalTxnFeePub / frb.PassengerCount;
                        paxFare.Tax = frb.Tax / frb.PassengerCount;
                        paxFare.AdditionalTxnFee = 0;
                        pax.Meal = new STD.BAL.TBO.BOOKSHARED.Meal();
                        pax.Seat = new STD.BAL.TBO.BOOKSHARED.Seat();
                        string gndr = Convert.ToString(CHDPax[i]["Gender"]).Trim().ToUpper();
                        pax.Gender = gndr == "M" ? 1 : 2;
                        pax.Fare = paxFare;
                        paxList.Add(pax);

                    }


                }
                #endregion

                #region Infant

                if (INFPax.Count() > 0)
                {
                    for (int i = 0; i < INFPax.Count(); i++)
                    {
                        Passenger pax = new Passenger();
                        pax.FirstName = Convert.ToString(INFPax[i]["FName"]);
                        pax.LastName = Convert.ToString(INFPax[i]["LName"]);
                        pax.IsLeadPax = i == 0 ? true : false;
                        pax.Title = Convert.ToString(INFPax[i]["Title"]);
                        pax.DateOfBirth = INFPax[i]["DOB"].ToString().Split('/')[2] + "-" + INFPax[i]["DOB"].ToString().Split('/')[1] + "-" + INFPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";
                        pax.AddressLine1 = ConfigurationManager.AppSettings["companyaddress1"];
                        pax.AddressLine2 = ConfigurationManager.AppSettings["companyaddress2"];
                        pax.City = ConfigurationManager.AppSettings["companycity"];
                        pax.CountryCode = "IN";
                        pax.CountryName = "India";//ConfigurationManager.AppSettings["companyaddress1"];
                        pax.ContactNo = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]);
                        pax.Email = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]);
                        pax.Fare = new STD.BAL.TBO.BOOKSHARED.Fare();
                        pax.PaxType = 3;
                        STD.BAL.TBO.BOOKSHARED.Fare paxFare = new STD.BAL.TBO.BOOKSHARED.Fare();
                        FareBreakdown frb = fareBrkup.Where(x => x.PassengerType == 3).FirstOrDefault();
                        paxFare.BaseFare = frb.BaseFare / frb.PassengerCount;
                        paxFare.YQTax = frb.YQTax / frb.PassengerCount;
                        paxFare.AdditionalTxnFeeOfrd = 0;
                        paxFare.AdditionalTxnFeePub = frb.AdditionalTxnFeePub / frb.PassengerCount;
                        paxFare.Tax = frb.Tax / frb.PassengerCount;
                        paxFare.AdditionalTxnFee = 0;
                        pax.Fare = paxFare;
                        string gndr = Convert.ToString(INFPax[i]["Gender"]).Trim().ToUpper();
                        pax.Gender = gndr == "M" ? 1 : 2;
                        pax.Meal = new STD.BAL.TBO.BOOKSHARED.Meal();
                        pax.Seat = new STD.BAL.TBO.BOOKSHARED.Seat();
                        paxList.Add(pax);

                    }


                }
                #endregion

                objBookReq.Passengers = paxList;
                string strJsonData = JsonConvert.SerializeObject(objBookReq);
                Log.Add("BookReq", strJsonData);
                TBOUtitlity.SaveFile(strJsonData, "BookReq");
                string strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.BookUrl, strJsonData);
                Log.Add("BookResp", strJsonResponse);
                TBOUtitlity.SaveFile(strJsonResponse, "BookRes");
                TktNoArray = new ArrayList();

                STD.BAL.TBO.BookRespShared.RootObject result = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(strJsonResponse);
                bookingID = "";
                if (result.Response.Response.TicketStatus == 1)
                {
                    PNR = result.Response.Response.PNR;
                    bookingID = result.Response.Response.BookingId.ToString();
                    //for (int t = 0; t < result.Response.Response.FlightItinerary.Passenger.Count; t++)
                    //{
                    //    TktNoArray.Add(result.Response.Response.FlightItinerary.Passenger[t].Ticket);

                    //}
                }
                else
                {
                    PNR = result.Response.Response.PNR + "-FQ";
                    bookingID = result.Response.Response.BookingId.ToString();
                }

                exep = exep + Convert.ToString(result.Response.Error.ErrorMessage) + Convert.ToString(result.Response.Response.Message) + Convert.ToString(result.Response.Response.SSRMessage);


            FareMisMatch:
                exep = exep + "Fare Amount Diffrence new fare:" + fResp.Response.Results.Fare.OfferedFare.ToString() + " old Fare:" + totfare.ToString();

            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
                PNR = PNR + "-FQ";
            }
            finally
            {
                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                objCommonBAL.InsertTBOBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                    , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
                    , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
                    , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            }

            return PNR;


        }


        public void NonLCCTIcket(DataSet Crd, string TokenId, string TraceId, string PNR, string BookingId, ref ArrayList TktNoArray, string Constr)
        {
            Dictionary<string, string> Log = new Dictionary<string, string>();
            STD.BAL.TBO.BOOKSHARED.TboNonLCCTktReq objReq = new TboNonLCCTktReq();
            string exep = "";

            try
            {
                objReq.EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);

                TBOAuthProcess objTbProcess = new TBOAuthProcess();
                string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), objReq.EndUserIp);
                objReq.TokenId = strTokenID;
                objReq.BookingId = BookingId;
                objReq.TraceId = TraceId;
                objReq.PNR = PNR;

                string strJsonData = JsonConvert.SerializeObject(objReq);
                Log.Add("TktReq", strJsonData);
                TBOUtitlity.SaveFile(strJsonData, "TicketReq");
                string strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.TicketUrl, strJsonData);
                Log.Add("TktResp", strJsonResponse);
                TBOUtitlity.SaveFile(strJsonResponse, "TicketRes");

                TktNoArray = new ArrayList();

                STD.BAL.TBO.BookRespShared.RootObject result = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(strJsonResponse);

                if (result.Response.Response.TicketStatus == 1)
                {
                    PNR = result.Response.Response.PNR;

                    for (int t = 0; t < result.Response.Response.FlightItinerary.Passenger.Count; t++)
                    {
                        TktNoArray.Add(result.Response.Response.FlightItinerary.Passenger[t].Ticket.TicketNumber);

                    }
                }
                else
                {
                    PNR = result.Response.Response.PNR + "-FQ";
                }

                exep = exep + Convert.ToString(result.Response.Error.ErrorMessage) + Convert.ToString(result.Response.Response.Message) + Convert.ToString(result.Response.Response.SSRMessage);

            }
            catch (Exception ex)
            {


            }



        }

        public string TBOFightBookLCC(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, DataTable MBDT, ref ArrayList TktNoArray, string Constr)
        {
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";

            STD.BAL.TBO.BookRespShared.RootObject result = new BookRespShared.RootObject();
            string exep = "";
            bool refundStatus = false;
            try
            {
                TboBookReqLCC objBookReq = new TboBookReqLCC();
                List<PassengerLCC> paxList = new List<PassengerLCC>();
                try
                {
                    DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                    DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                    DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");

                    objBookReq.EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);
                    TBOAuthProcess objTbProcess = new TBOAuthProcess();
                    string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), objBookReq.EndUserIp);
                    objBookReq.TokenId = strTokenID;

                    string[] sno = Convert.ToString(FltDT.Rows[0]["sno"]).Split(':');

                    objBookReq.TraceId = sno[1];
                    objBookReq.ResultIndex = sno[0];

                    string rsltIndex = objBookReq.ResultIndex;
                    if (Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["TripType"]).Trim().ToUpper() == "R" && Convert.ToString(FltDT.Rows[0]["Trip"]).ToUpper().Trim() == "D")
                    {
                        string[] sno1 = Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["sno"]).Split(':');
                        rsltIndex = sno[0] + "," + sno1[0];
                    }

                    bool islcc = Convert.ToBoolean(sno[2]);
                    objBookReq.PreferredCurrency = "INR";


                    objBookReq.Passengers = new List<PassengerLCC>();



                    string[] sep = { "itzDanata420" };
                    string[] FareArray = Convert.ToString(FltDT.Rows[0]["Searchvalue"]).Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    Fare fareMain = JsonConvert.DeserializeObject<STD.BAL.TBO.Fare>(FareArray[0]);
                    List<FareBreakdown> fareBrkup = JsonConvert.DeserializeObject<List<FareBreakdown>>(FareArray[1]);

                    #region FareQuote



                    FareQuote.FareQuoteResp fResp = GetFareQuote(Crd, objBookReq.TraceId, rsltIndex, ref Log, ref exep);
                    decimal BgPr = 0;
                    //if (MBDT.Rows.Count > 0)
                    //{

                    //    for (int jj=0; jj < MBDT.Rows.Count; jj++)
                    //    {
                    //        BgPr = BgPr + Convert.ToDecimal(MBDT.Rows[jj]["BaggagePrice"]) + Convert.ToDecimal(MBDT.Rows[jj]["MealPrice"]);
                    //    }
                    //}

                    decimal totfare = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]) + BgPr;

                    if (totfare > 0 && Convert.ToDecimal(fResp.Response.Results.Fare.OfferedFare) > 0 && totfare + 1 < Convert.ToDecimal(fResp.Response.Results.Fare.OfferedFare))
                    {
                        exep = exep + "Fare Amount Diffrence new fare:" + fResp.Response.Results.Fare.OfferedFare.ToString() + " old Fare:" + totfare.ToString();
                        refundStatus = true;
                        goto FareMisMatch;
                    }


                    #endregion


                    #region adult

                    if (ADTPax.Count() > 0)
                    {
                        for (int i = 0; i < ADTPax.Count(); i++)
                        {
                            PassengerLCC pax = new PassengerLCC();
                            pax.FirstName = Convert.ToString(ADTPax[i]["FName"]);
                            pax.LastName = Convert.ToString(ADTPax[i]["LName"]);
                            pax.IsLeadPax = i == 0 ? true : false;
                            pax.Title = Convert.ToString(ADTPax[i]["Title"]);
                            if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["DOB"])) && Convert.ToString(ADTPax[i]["DOB"]).Trim().Length > 7)
                            {
                                pax.DateOfBirth = ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";

                            }

                            //pax.DateOfBirth = ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";
                            pax.AddressLine1 = ConfigurationManager.AppSettings["companyaddress1"];
                            pax.AddressLine2 = ConfigurationManager.AppSettings["companyaddress2"];
                            pax.City = ConfigurationManager.AppSettings["companycity"];
                            pax.CountryCode = "IN";
                            pax.CountryName = "India";//ConfigurationManager.AppSettings["companyname"];
                            pax.ContactNo = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim();
                            pax.Email = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]);
                            pax.Fare = new STD.BAL.TBO.BOOKSHARED.Fare();
                            pax.PaxType = 1;
                            STD.BAL.TBO.BOOKSHARED.Fare paxFare = new STD.BAL.TBO.BOOKSHARED.Fare();
                            FareBreakdown frb = fareBrkup.Where(x => x.PassengerType == 1).FirstOrDefault();
                            paxFare.BaseFare = frb.BaseFare / frb.PassengerCount;
                            paxFare.YQTax = frb.YQTax / frb.PassengerCount;
                            paxFare.AdditionalTxnFeeOfrd = 0;
                            paxFare.AdditionalTxnFeePub = frb.AdditionalTxnFeePub / frb.PassengerCount;
                            paxFare.Tax = frb.Tax / frb.PassengerCount;
                            paxFare.AdditionalTxnFee = 0;
                            pax.MealDynamic = new List<STD.BAL.TBO.BOOKSHARED.MealDynamic>();
                            pax.Baggage = new List<STD.BAL.TBO.BOOKSHARED.Baggage>();
                            string gndr = Convert.ToString(ADTPax[i]["Gender"]).Trim().ToUpper();

                            if (gndr == "M")
                            {
                                pax.Gender = 1;
                            }
                            else
                            {
                                pax.Gender = 2;
                            }



                            if (MBDT.Rows.Count > 0)
                            {

                                DataRow[] MBDR = MBDT.Select("PaxId=" + ADTPax[i]["PaxId"], "PaxId ASC");

                                for (int mb = 0; mb < MBDR.Count(); mb++)
                                {
                                    if (Convert.ToDecimal(MBDR[mb]["MealPrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.MealDynamic objMeal = new STD.BAL.TBO.BOOKSHARED.MealDynamic();
                                        string[] mbdesc = Convert.ToString(MBDR[mb]["MealDesc"]).Split('_');

                                        objMeal.AirlineDescription = Convert.ToString(mbdesc[2].Trim());
                                        objMeal.Code = Convert.ToString(MBDR[mb]["MealCode"]);
                                        objMeal.Description = Convert.ToInt32(mbdesc[0]);
                                        objMeal.Origin = Convert.ToString(mbdesc[4].Trim());
                                        objMeal.Destination = Convert.ToString(mbdesc[3].Trim());
                                        objMeal.Price = Convert.ToInt32(Math.Floor(Convert.ToDecimal(MBDR[mb]["MealPrice"].ToString())));
                                        objMeal.WayType = Convert.ToInt32(mbdesc[1]);
                                        objMeal.Currency = "INR";
                                        pax.MealDynamic.Add(objMeal);

                                    }


                                    if (Convert.ToDecimal(MBDR[mb]["BaggagePrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.Baggage objBag = new STD.BAL.TBO.BOOKSHARED.Baggage();

                                        string[] mbdesc = Convert.ToString(MBDR[mb]["BaggageDesc"]).Split('_');
                                        objBag.Code = Convert.ToString(MBDR[mb]["BaggageCode"]);
                                        objBag.Description = Convert.ToInt32(mbdesc[0]);
                                        objBag.Origin = Convert.ToString(mbdesc[4].Trim());
                                        objBag.Destination = Convert.ToString(mbdesc[3].Trim());
                                        objBag.Price = Convert.ToInt32(Math.Floor(Convert.ToDecimal(MBDR[mb]["BaggagePrice"].ToString())));
                                        objBag.WayType = Convert.ToInt32(mbdesc[1]);
                                        objBag.Weight = Convert.ToInt32(mbdesc[2]);
                                        objBag.Currency = "INR";
                                        pax.Baggage.Add(objBag);

                                    }


                                }
                            }

                            pax.Fare = paxFare;
                            paxList.Add(pax);

                        }


                    }
                    #endregion

                    #region Child

                    if (CHDPax.Count() > 0)
                    {
                        for (int i = 0; i < CHDPax.Count(); i++)
                        {
                            PassengerLCC pax = new PassengerLCC();
                            pax.FirstName = Convert.ToString(CHDPax[i]["FName"]);
                            pax.LastName = Convert.ToString(CHDPax[i]["LName"]);
                            pax.IsLeadPax = i == 0 ? true : false;
                            pax.Title = Convert.ToString(CHDPax[i]["Title"]);
                            pax.DateOfBirth = CHDPax[i]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";
                            pax.AddressLine1 = ConfigurationManager.AppSettings["companyaddress1"];
                            pax.AddressLine2 = ConfigurationManager.AppSettings["companyaddress2"];
                            pax.City = ConfigurationManager.AppSettings["companycity"];
                            pax.CountryCode = "IN";
                            pax.CountryName = "India";//ConfigurationManager.AppSettings["companyaddress1"];
                            pax.ContactNo = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim();
                            pax.Email = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]);
                            pax.Fare = new STD.BAL.TBO.BOOKSHARED.Fare();
                            pax.PaxType = 2;
                            STD.BAL.TBO.BOOKSHARED.Fare paxFare = new STD.BAL.TBO.BOOKSHARED.Fare();
                            FareBreakdown frb = fareBrkup.Where(x => x.PassengerType == 2).FirstOrDefault();
                            paxFare.BaseFare = frb.BaseFare / frb.PassengerCount;
                            paxFare.YQTax = frb.YQTax / frb.PassengerCount;
                            paxFare.AdditionalTxnFeeOfrd = 0;
                            paxFare.AdditionalTxnFeePub = frb.AdditionalTxnFeePub / frb.PassengerCount;
                            paxFare.Tax = frb.Tax / frb.PassengerCount;
                            paxFare.AdditionalTxnFee = 0;
                            pax.MealDynamic = new List<STD.BAL.TBO.BOOKSHARED.MealDynamic>();
                            pax.Baggage = new List<STD.BAL.TBO.BOOKSHARED.Baggage>();
                            string gndr = Convert.ToString(CHDPax[i]["Gender"]).Trim().ToUpper();

                            if (gndr == "M")
                            {
                                pax.Gender = 1;
                            }
                            else
                            {
                                pax.Gender = 2;
                            }

                            if (MBDT.Rows.Count > 0)
                            {
                                DataRow[] MBDR = MBDT.Select("PaxId=" + CHDPax[i]["PaxId"], "PaxId ASC");

                                for (int mb = 0; mb < MBDR.Count(); mb++)
                                {
                                    if (Convert.ToDecimal(MBDR[mb]["MealPrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.MealDynamic objMeal = new STD.BAL.TBO.BOOKSHARED.MealDynamic();
                                        string[] mbdesc = Convert.ToString(MBDR[mb]["MealDesc"]).Split('_');

                                        objMeal.AirlineDescription = Convert.ToString(mbdesc[2].Trim());
                                        objMeal.Code = Convert.ToString(MBDR[mb]["MealCode"]);
                                        objMeal.Description = Convert.ToInt32(mbdesc[0]);
                                        objMeal.Origin = Convert.ToString(mbdesc[4].Trim());
                                        objMeal.Destination = Convert.ToString(mbdesc[3].Trim());
                                        objMeal.Price = Convert.ToInt32(Math.Floor(Convert.ToDecimal(MBDR[mb]["MealPrice"].ToString())));
                                        objMeal.WayType = Convert.ToInt32(mbdesc[1]);
                                        objMeal.Currency = "INR";
                                        pax.MealDynamic.Add(objMeal);

                                    }


                                    if (Convert.ToDecimal(MBDR[mb]["BaggagePrice"]) > 0)
                                    {
                                        STD.BAL.TBO.BOOKSHARED.Baggage objBag = new STD.BAL.TBO.BOOKSHARED.Baggage();

                                        string[] mbdesc = Convert.ToString(MBDR[mb]["BaggageDesc"]).Split('_');
                                        objBag.Code = Convert.ToString(MBDR[mb]["BaggageCode"]);
                                        objBag.Description = Convert.ToInt32(mbdesc[0]);
                                        objBag.Origin = Convert.ToString(mbdesc[4].Trim());
                                        objBag.Destination = Convert.ToString(mbdesc[3].Trim());
                                        objBag.Price = Convert.ToInt32(Math.Floor(Convert.ToDecimal(MBDR[mb]["BaggagePrice"].ToString())));
                                        objBag.WayType = Convert.ToInt32(mbdesc[1]);
                                        objBag.Weight = Convert.ToInt32(mbdesc[2]);
                                        objBag.Currency = "INR";
                                        pax.Baggage.Add(objBag);

                                    }
                                }

                            }


                            pax.Fare = paxFare;
                            paxList.Add(pax);

                        }


                    }
                    #endregion

                    #region Infant

                    if (INFPax.Count() > 0)
                    {
                        for (int i = 0; i < INFPax.Count(); i++)
                        {
                            PassengerLCC pax = new PassengerLCC();
                            pax.FirstName = Convert.ToString(INFPax[i]["FName"]);
                            pax.LastName = Convert.ToString(INFPax[i]["LName"]);
                            pax.IsLeadPax = i == 0 ? true : false;
                            pax.Title = Convert.ToString(INFPax[i]["Title"]);
                            pax.DateOfBirth = INFPax[i]["DOB"].ToString().Split('/')[2] + "-" + INFPax[i]["DOB"].ToString().Split('/')[1] + "-" + INFPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00";
                            pax.AddressLine1 = ConfigurationManager.AppSettings["companyaddress1"];
                            pax.AddressLine2 = ConfigurationManager.AppSettings["companyaddress2"];
                            pax.City = ConfigurationManager.AppSettings["companycity"];
                            pax.CountryCode = "IN";
                            pax.CountryName = "India";//ConfigurationManager.AppSettings["companyaddress1"];
                            pax.ContactNo = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim();
                            pax.Email = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]);
                            pax.Fare = new STD.BAL.TBO.BOOKSHARED.Fare();
                            pax.PaxType = 3;
                            STD.BAL.TBO.BOOKSHARED.Fare paxFare = new STD.BAL.TBO.BOOKSHARED.Fare();
                            FareBreakdown frb = fareBrkup.Where(x => x.PassengerType == 3).FirstOrDefault();
                            paxFare.BaseFare = frb.BaseFare / frb.PassengerCount;
                            paxFare.YQTax = frb.YQTax / frb.PassengerCount;
                            paxFare.AdditionalTxnFeeOfrd = 0;
                            paxFare.AdditionalTxnFeePub = frb.AdditionalTxnFeePub / frb.PassengerCount;
                            paxFare.Tax = frb.Tax / frb.PassengerCount;
                            paxFare.AdditionalTxnFee = 0;
                            pax.Fare = paxFare;

                            string gndr = Convert.ToString(INFPax[i]["Gender"]).Trim().ToUpper();

                            if (gndr == "M")
                            {
                                pax.Gender = 1;
                            }
                            else
                            {
                                pax.Gender = 2;
                            }
                            pax.MealDynamic = new List<STD.BAL.TBO.BOOKSHARED.MealDynamic>();
                            pax.Baggage = new List<STD.BAL.TBO.BOOKSHARED.Baggage>();
                            paxList.Add(pax);

                        }


                    }
                    #endregion
                }
                catch (Exception ex1)
                {
                    exep = exep + ex1.Message + Convert.ToString(ex1.StackTrace);
                    refundStatus = true;
                    goto FareMisMatch;
                }
                objBookReq.Passengers = paxList;
                objBookReq.IsBaseCurrencyRequired = "true";
                string strJsonData = JsonConvert.SerializeObject(objBookReq);
                if (Log.ContainsKey("BookReq") == true) { Log["BookReq"] = Log["BookReq"] + strJsonData; } else { Log.Add("BookReq", strJsonData); }
                TBOUtitlity.SaveFile(strJsonData, "TicketReq_" + objBookReq.ResultIndex);
                string strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.TicketUrl, strJsonData);
                if (Log.ContainsKey("BookResp") == true) { Log["BookResp"] = Log["BookResp"] + strJsonResponse; } else { Log.Add("BookResp", strJsonResponse); }
                TBOUtitlity.SaveFile(strJsonResponse, "TicketRes_" + objBookReq.ResultIndex);
                TktNoArray = new ArrayList();

                result = JsonConvert.DeserializeObject<STD.BAL.TBO.BookRespShared.RootObject>(strJsonResponse);

                if (result.Response.Response.TicketStatus == 1)
                {
                    PNR = result.Response.Response.PNR;

                    for (int t = 0; t < result.Response.Response.FlightItinerary.Passenger.Count; t++)
                    {
                        TktNoArray.Add(result.Response.Response.FlightItinerary.Passenger[t].Ticket.TicketNumber);

                    }
                }
                else
                {
                    PNR = result.Response.Response.PNR + "-FQ";
                }

               // exep = exep + Convert.ToString(result.Response.Error.ErrorMessage) + Convert.ToString(result.Response.Response.Message) + Convert.ToString(result.Response.Response.SSRMessage);


            FareMisMatch:
                {
                    if (refundStatus)
                    {
                        PNR = PNR + "-FQ-FRM";
                    } 
                }

            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
                PNR = PNR + "-FQ-EXP";



            }
            finally
            {

                try
                {
                    exep = exep + Convert.ToString(result.Response.Error.ErrorMessage);
                }
                catch { }
                try
                {
                    exep = exep + Convert.ToString(result.Response.Response.Message) + Convert.ToString(result.Response.Response.SSRMessage);
                }
                catch { }

                try
                {
                    exep = exep + Convert.ToString(result.Response.Response.SSRMessage);
                }
                catch { }

                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                objCommonBAL.InsertTBOBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                    , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
                    , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
                    , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            }

            return PNR;
        }


        public STD.BAL.TBO.FareQuote.FareQuoteResp GetFareQuote(DataSet Crd, string traceId, string rowIndex, ref Dictionary<string, string> Log, ref string exep)
        {
            STD.BAL.TBO.FareQuote.FareQuoteResp result = new FareQuote.FareQuoteResp();
            Reprice objReprice = new Reprice();

            try
            {
                objReprice.EndUserIp = Convert.ToString(Crd.Tables[0].Rows[0]["ServerIp"]);
                TBOAuthProcess objTbProcess = new TBOAuthProcess();
                string strTokenID = objTbProcess.GetTBOToken(ServiceUrl.loginUrl, Convert.ToString(Crd.Tables[0].Rows[0]["CorporateID"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), objReprice.EndUserIp);
                objReprice.TokenId = strTokenID;
                objReprice.ResultIndex = rowIndex;
                objReprice.TraceId = traceId;

                string strJsonData = JsonConvert.SerializeObject(objReprice);
                if (Log.ContainsKey("RepriceReq") == true) { Log["RepriceReq"] = Log["RepriceReq"] + strJsonData; } else { Log.Add("RepriceReq", strJsonData); }
                //Log.Add("RepriceReq", strJsonData);
                TBOUtitlity.SaveFile(strJsonData, "FareQuoteReq_" + rowIndex);
                string strJsonResponse = TBOAuthProcess.GetResponse(ServiceUrl.FareQuoteUrl, strJsonData);
                //Log.Add("RepriceResp", strJsonResponse);
                if (Log.ContainsKey("RepriceResp") == true) { Log["RepriceResp"] = Log["RepriceResp"] + strJsonResponse; } else { Log.Add("RepriceResp", strJsonResponse); }
                TBOUtitlity.SaveFile(strJsonResponse, "FareQuoteRes_" + rowIndex);
                result = JsonConvert.DeserializeObject<STD.BAL.TBO.FareQuote.FareQuoteResp>(strJsonResponse);
            }
            catch (Exception ex)
            {

                exep = exep + ex.Message + ex.StackTrace.ToString();
            }

            return result;


        }


    }
}
