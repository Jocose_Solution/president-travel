﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.BAL.TBO
{
  public   class ServiceUrl
  {
        //public static string loginUrl { get { return "http://tboapi.travelboutiqueonline.com/SharedAPI/SharedData.svc/rest/authenticate"; } }

        //public static string SearchUrl { get { return "http://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Search"; } }

        //public static string BookUrl { get { return "http://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Book"; } }

        //public static string FareQuoteUrl { get { return "http://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareQuote"; } }

        //public static string TicketUrl { get { return "http://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Ticket"; } }

        //public static string SSRUrl { get { return "http://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SSR"; } }

        //public static string FareRuleUrl { get { return "http://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareRule"; } }




        public static string loginUrl { get { return "http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate"; } }

        public static string SearchUrl { get { return "http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Search"; } }

        public static string BookUrl { get { return "http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Book"; } }

        public static string FareQuoteUrl { get { return "http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareQuote"; } }

        public static string TicketUrl { get { return "http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket"; } }

        public static string SSRUrl { get { return "http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SSR"; } }
        public static string FareRuleUrl { get { return "http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareRule"; } }

    }
}
