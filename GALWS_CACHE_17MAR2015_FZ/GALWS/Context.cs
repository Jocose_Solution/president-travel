﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace STD.BAL
{
    /// <summary>
    /// Context Class For Delegating Trans
    /// </summary>
    public class TransContext
    {
        private ITrans Trans;
        public TransContext(ITrans Trans)
        {
            this.Trans = Trans;
        }
        /// <summary>
         ///Delegate to actual algorithm
        /// </summary>
        /// <typeparam name="T">Return type trans process</typeparam>
        /// <returns></returns>
        public T ContextCall<T>() where T : class
        {
            return Trans.ProcessRequest<T>();
        }
        //public T ContextCallCoupon<T>() where T : class
        //{
        //    return Trans.ProcessRequestCoupon<T>();
        //}
    }

}