﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using STD.BAL;
using STD.DAL;
using STD.Shared;

namespace GALWS
{
   public class GDSCancellation
    {
        public string GDSFightCancellation(string PNR, string TicketNo, string Constr, DataRow[] paxdtail, string Sector)
        {
            string ResponseStatus = "Faild"; FlightCancellationDAL objfltbal = new FlightCancellationDAL(Constr);
            try
            {
                GALTransanctions objgaltrans = new GALTransanctions();
                FlightSearch searchParam = new FlightSearch();
                Credentials objCrd = new Credentials(Constr);
                List<CredentialList> CrdList = objCrd.GetServiceCredentials("");

                FltRequest onjfltcancel = new FltRequest(searchParam, CrdList);

                string url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                string Userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                string Password = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
                string BookingURL = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].BookingURL;
                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                // Step 1 Begin Session
                string tokenid = StratSesstions(PNR, url, Userid, Password, "http://webservices.galileo.com/BeginSession", "", onjfltcancel, objgaltrans);
                //Step 2  Retriv PNR
                string retriveReq = PNRBFRetrieveReq(PNR, tokenid);
                string retriveResp = objgaltrans.PostXml(url, retriveReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");

                objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), TicketNo, retriveReq, retriveResp, "Retrive");
                SaveFile(PNR + "_PNRBFRetrieveReq_", retriveReq);
                SaveFile(PNR + "_PNRBFRetrieveResp_", retriveResp);
                if (!retriveResp.Contains("<ErrorCode>0001</ErrorCode>") && retriveResp != "")
                {
                    //Parsing XML from Retrive PNR Response FNameInfo
                    XDocument xmlDocReq = XDocument.Parse(retriveResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                    XElement retrivepnrXML = xmlDocReq.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("PNRBFRetrieve");
                    
                    string[] sctorarray = Sector.Split(',');
                    string newPNR = PNR, CancelReq = "", CancelResp = "", DivideReq = "", DivideResp = "", retriveReq2 = "", retriveResp2 = "";
                    string EndTransactionReq = "", EndTransactionResp = "", EndTransactionReq1 = "", EndTransactionResp1 = "", EndTransactionReq2 = "", EndTransactionResp2 = "";
                    string EndTransactionReq3 = "", EndTransactionResp3 = "", EndTransactionReq5 = "", EndTransactionResp5 = "";
                    #region single passanger cancellation
                    if (retrivepnrXML.Elements("LNameInfo").Count() == paxdtail.Length)
                    {
                        string MarriageNum = "";
                        //Step 3 Cancel PNR
                        if (retrivepnrXML.Elements("AirSeg").Count() == sctorarray.Length)
                            CancelReq = SegCancelModsReq("FF", retrivepnrXML, sctorarray, tokenid);
                        else
                        {
                            CancelReq = SegCancelModsReq("", retrivepnrXML, sctorarray, tokenid);

                            for (int i = 0; i < sctorarray.Length; i++)
                            {
                                string[] Segment = sctorarray[i].Split(':');
                                IEnumerable<XElement> AirSeg = retrivepnrXML.Elements("AirSeg").Where(x => x.Element("StartAirp").Value.ToUpper() == Segment[0].ToString().ToUpper() && x.Element("EndAirp").Value.ToUpper() == Segment[1].ToString().ToUpper());
                                if (AirSeg.Count() > 0)
                                {
                                     MarriageNum = AirSeg.ElementAt(0).Element("MarriageNum").Value;
                                     break;
                                }
                            }
                        }

                        if (MarriageNum == "")
                        {
                            CancelResp = objgaltrans.PostXml(url, CancelReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                            SaveFile(PNR + "_CancelReq_", CancelReq);
                            SaveFile(PNR + "_CancelResp_", CancelResp);
                            objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), PNR, CancelReq, CancelResp, "Cancel");

                            //Step 4 End Transaction
                            EndTransactionReq1 = EndTransactionModsReq(tokenid, "PAX", "E");
                            EndTransactionResp1 = objgaltrans.PostXml(url, EndTransactionReq1, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                        }
                        //Step 5 End Transaction 
                        if (EndTransactionResp1.Contains("<ErrorCode>"))
                        {
                            if (EndTransactionResp1.Contains("FILED FARES CANCELLED") )
                            {
                                EndTransactionReq = EndTransactionModsReq(tokenid, "", "E");
                                EndTransactionResp = objgaltrans.PostXml(url, EndTransactionReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                SaveFile(PNR + "_EndTransactionReq_", EndTransactionReq);
                                SaveFile(PNR + "_EndTransactionResp_", EndTransactionResp);

                                if (!EndTransactionResp.Contains("<ErrorCode>") && EndTransactionResp != "")
                                {
                                    ResponseStatus = "Cancelled";
                                    XDocument Endtrans1 = XDocument.Parse(EndTransactionResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                                    XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("EndTransaction");
                                    if (Endtrans1Resp != null)
                                        newPNR = Endtrans1Resp.Element("EndTransactResponse").Element("RecLoc").Value;

                                }
                            }
                        }

                        else if (EndTransactionResp1 != "" && EndTransactionResp1.Contains("<ErrorCode>") != true)
                        {
                            XDocument Endtrans1 = XDocument.Parse(EndTransactionResp1.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                            XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("EndTransaction");
                            if (Endtrans1Resp != null)
                                newPNR = Endtrans1Resp.Element("EndTransactResponse").Element("RecLoc").Value;

                            ResponseStatus = "Cancelled";
                        }

                        SaveFile(PNR + "_EndTransactionReq1_", EndTransactionReq1);
                        SaveFile(PNR + "_EndTransactionResp1_", EndTransactionResp1);

                        objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), ResponseStatus.ToString(), EndTransactionReq1, EndTransactionResp1, "EndTransaction1");
                        objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), "", EndTransactionReq, EndTransactionResp, "EndTransaction2");
                    }
                    #endregion
                    #region Multipl passanger cancellation
                    else
                    {
                       
                        //Step 3 Divid PNR  
                        DivideReq = BookingDivideReq(tokenid, retrivepnrXML, paxdtail);
                        DivideResp = objgaltrans.PostXml(url, DivideReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                        SaveFile(PNR + "_DivideReq_", DivideReq);
                        SaveFile(PNR + "_DivideResp_", DivideResp);
                        objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), "", DivideReq, DivideResp, "Divide");
                        if (DivideResp != "" && !DivideResp.Contains("ErrorCode"))
                        {
                            Boolean samepax = true;
                            //Parsing XML from Divide PNR Response for ensore same pax present for cancellation
                            XDocument xmlDocDivide = XDocument.Parse(DivideResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                            XElement DivideXML = xmlDocDivide.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFDivide_4").Element("PNRBFRetrieve");
                            foreach (DataRow paxnames in paxdtail)
                            {
                                IEnumerable<XElement> LNameInfo = DivideXML.Elements("LNameInfo").Where(x => x.Element("LName").Value.ToUpper() == paxnames["LName"].ToString().ToUpper());
                                IEnumerable<XElement> FNameInfo = DivideXML.Elements("FNameInfo").Where(x => x.Element("FName").Value.ToUpper() == paxnames["FName"].ToString().ToUpper() + paxnames["MName"].ToString().ToUpper() + paxnames["Title"].ToString().ToUpper());
                                if (LNameInfo.Count() == 0 && FNameInfo.Count() == 0)
                                {
                                    samepax = false;
                                    goto CONTINUE;
                                }
                            }
                        CONTINUE: ;
                            if (samepax)
                            {
                                //Step 4 End Transaction for new pnr
                                EndTransactionReq1 = EndTransactionModsReq(tokenid, "JAN", "F");
                                EndTransactionResp1 = objgaltrans.PostXml(url, EndTransactionReq1, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                SaveFile(PNR + "_EndTransactionReq1_", EndTransactionReq1);
                                SaveFile(PNR + "_EndTransactionResp1_", EndTransactionResp1);

                                //Parsing XML from fist End Transaction PNR Response regarding New PNR
                                if (EndTransactionResp1 != "")
                                {
                                    XDocument Endtrans1 = XDocument.Parse(EndTransactionResp1.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                                    XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("PNRBFRetrieve");

                                    if (Endtrans1Resp != null)
                                        newPNR = Endtrans1Resp.Element("DividedBookingInfo").Element("RLoc").Value;
                                }
                                        //Step 5 once again Second  End Transaction for old pnr
                                        EndTransactionReq2 = EndTransactionModsReq(tokenid, "JAN", "E");
                                        EndTransactionResp2 = objgaltrans.PostXml(url, EndTransactionReq2, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                        SaveFile(PNR + "_EndTransactionReq2_", EndTransactionReq2);
                                        SaveFile(PNR + "_EndTransactionResp2_", EndTransactionResp2);

                                        if (EndTransactionResp2.Contains("<ErrorCode>"))
                                        {
                                            if (EndTransactionResp2.Contains("<Text>ITIN/NAME CHANGE - FILED FARES CANCELLED</Text>"))
                                            {
                                                //Step 6 End Transaction Third for Store Fare (OLD PNR)
                                                EndTransactionReq3 = EndTransactionModsReq(tokenid, "", "E");
                                                EndTransactionResp3 = objgaltrans.PostXml(url, EndTransactionReq3, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                                SaveFile(PNR + "_EndTransactionReq3_", EndTransactionReq3);
                                                SaveFile(PNR + "_EndTransactionResp3_", EndTransactionResp3);
                                            }
                                        }
                                objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), "", EndTransactionReq1, EndTransactionResp1, "EndTransaction1");
                                objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), newPNR, EndTransactionReq2, EndTransactionResp2, "EndTransaction2");
                                objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), "", EndTransactionReq3, EndTransactionResp3, "EndTransaction3_4");
                                //Step 7 Session END
                                EndSesstions(PNR, url, Userid, Password, "http://webservices.galileo.com/EndSession", tokenid, "1", onjfltcancel, objgaltrans);
                                
                                // Step 8 Begin Session
                                string tokenid2 = StratSesstions(PNR, url, Userid, Password, "http://webservices.galileo.com/BeginSession", "1", onjfltcancel, objgaltrans);
                                tokenid = tokenid2;
                                //Step 9 Retrive New PNR
                                retriveReq2 = PNRBFRetrieveReq(newPNR, tokenid);
                                retriveResp2 = objgaltrans.PostXml(url, retriveReq2, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                SaveFile(PNR + "_PNRBFRetrieveReq2_", retriveReq2);
                                SaveFile(PNR + "_PNRBFRetrieveResp2_", retriveResp2);
                                objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), "", retriveReq2, retriveResp2, "Retrive2");
                                if (retriveResp2 != "" && !retriveResp2.Contains("ErrorCode"))
                                {
                                    string MarriageNum = "";
                                    //10 Cancel Segment/s (ALL segments)
                                    if (retrivepnrXML.Elements("AirSeg").Count() == sctorarray.Length)
                                        CancelReq = SegCancelModsReq("FF", retrivepnrXML, sctorarray, tokenid);
                                    else
                                    {
                                        CancelReq = SegCancelModsReq("", retrivepnrXML, sctorarray, tokenid);
                                        for (int i = 0; i < sctorarray.Length; i++)
                                        {
                                            string[] Segment = sctorarray[i].Split(':');
                                            IEnumerable<XElement> AirSeg = retrivepnrXML.Elements("AirSeg").Where(x => x.Element("StartAirp").Value.ToUpper() == Segment[0].ToString().ToUpper() && x.Element("EndAirp").Value.ToUpper() == Segment[1].ToString().ToUpper());
                                            if (AirSeg.Count() > 0)
                                            {
                                                 MarriageNum = AirSeg.ElementAt(0).Element("MarriageNum").Value;
                                                 break;
                                            }
                                        }
                                    }

                                    if (MarriageNum == "")
                                    {
                                        CancelResp = objgaltrans.PostXml(BookingURL, CancelReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                        SaveFile(PNR + "_CancelReq_", CancelReq);
                                        SaveFile(PNR + "_CancelResp_", CancelResp);
                                        objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), PNR, CancelReq, CancelResp, "Cancel");

                                        //Step 11 End Transaction 
                                        EndTransactionReq5 = EndTransactionModsReq(tokenid, "JAN", "E");
                                        EndTransactionResp5 = objgaltrans.PostXml(url, EndTransactionReq5, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                        SaveFile(PNR + "_EndTransactionReq5_", EndTransactionReq5);
                                        SaveFile(PNR + "_EndTransactionResp5_", EndTransactionResp5);

                                        //Step 12 End Transaction 
                                        if (EndTransactionResp5.Contains("<ErrorCode>"))
                                        {
                                            if (EndTransactionResp5.Contains("<Text>ITIN/NAME CHANGE - FILED FARES CANCELLED</Text>"))
                                            {
                                                EndTransactionReq = EndTransactionModsReq(tokenid, "", "E");
                                                EndTransactionResp = objgaltrans.PostXml(url, EndTransactionReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                                                SaveFile(PNR + "_EndTransactionReq_", EndTransactionReq);
                                                SaveFile(PNR + "_EndTransactionResp_", EndTransactionResp);

                                                if (!EndTransactionResp.Contains("<ErrorCode>"))
                                                    ResponseStatus = "Cancelled";
                                            }
                                        }
                                        else if (!EndTransactionResp5.Contains("<ErrorCode>") && EndTransactionResp5 != "")
                                            ResponseStatus = "Cancelled";
                                    }
                                   

                                    objfltbal.InsertGDSCancleLog(paxdtail[0]["OrderId"].ToString(), ResponseStatus.ToString(), EndTransactionReq5 + EndTransactionReq, EndTransactionResp5 + EndTransactionResp, "EndTransaction5_6");
                                }
                            }
                        }
                    }
#endregion
                }
                //Step 13 Session END
                EndSesstions(PNR, url, Userid, Password, "http://webservices.galileo.com/EndSession", tokenid, "", onjfltcancel, objgaltrans);
                                
            }
            catch (Exception ex)
            {
               // ITZERRORLOG.ExecptionLogger.FileHandling("GDSFightCancellation", "Error_001", ex, "GDSCancellation");
            }

            return ResponseStatus;
        }
        public string PNRBFRetrieveReq(string PNR, string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");

            XmlBuilder.Append("<SubmitXmlOnSession xmlns='http://webservices.galileo.com'>");
            XmlBuilder.Append("<Token>" + Token + "</Token>");
            XmlBuilder.Append("<LDVOverride />");

            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_32>");
            XmlBuilder.Append("<PNRBFRetrieveMods>");
            XmlBuilder.Append("<PNRAddr>");
            XmlBuilder.Append("<FileAddr /><CodeCheck />");
            XmlBuilder.Append("<RecLoc>" + PNR + "</RecLoc>");
            XmlBuilder.Append("</PNRAddr>");
            XmlBuilder.Append("</PNRBFRetrieveMods>");
            XmlBuilder.Append("</PNRBFManagement_32>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append("</SubmitXmlOnSession>");
            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");

            return XmlBuilder.ToString();
        }
        //DataTable PaxDT, string FName, string MName, string Title, string LName, 
        public string SegCancelModsReq(string AllSeg, XElement retriveInfo, string[] sector, string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");

            XmlBuilder.Append("<SubmitXmlOnSession xmlns='http://webservices.galileo.com'>");
            XmlBuilder.Append("<Token>" + Token + "</Token>");
            XmlBuilder.Append("<LDVOverride />");

            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_32>");
            XmlBuilder.Append("<SegCancelMods>");
            XmlBuilder.Append("<CancelSegAry>");
            XmlBuilder.Append("");
            if (AllSeg == "FF")
                XmlBuilder.Append("<CancelSeg ><Tok/><SegNum>" + AllSeg + "</SegNum></CancelSeg>");
            else
            {
                for (int i = 0; i < sector.Length; i++)
                {
                    string[] Segment = sector[i].Split(':');
                    IEnumerable<XElement> AirSeg = retriveInfo.Elements("AirSeg").Where(x => x.Element("StartAirp").Value.ToUpper() == Segment[0].ToString().ToUpper() && x.Element("EndAirp").Value.ToUpper() == Segment[1].ToString().ToUpper());
                    if (AirSeg.Count() > 0)
                        XmlBuilder.Append("<CancelSeg ><Tok/><SegNum>" + AirSeg.ElementAt(0).Element("SegNum").Value + "</SegNum></CancelSeg>");
                }
            }
            XmlBuilder.Append("");
            XmlBuilder.Append("</CancelSegAry>");
            XmlBuilder.Append("</SegCancelMods>");
            XmlBuilder.Append("</PNRBFManagement_32>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append("</SubmitXmlOnSession>");

            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");
            return XmlBuilder.ToString();
        }

        public string EndTransactionModsReq(string Token, string RcvdFrom, string ETInd)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");

            XmlBuilder.Append("<SubmitXmlOnSession xmlns='http://webservices.galileo.com'>");
            XmlBuilder.Append("<Token>" + Token + "</Token>");
            XmlBuilder.Append("<LDVOverride />");

            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_32 xmlns=''>");
            XmlBuilder.Append("<EndTransactionMods><EndTransactRequest>");
            XmlBuilder.Append("<ETInd>" + ETInd + "</ETInd>");
            if (RcvdFrom != "")
                XmlBuilder.Append("<RcvdFrom>" + RcvdFrom + "</RcvdFrom>");
            XmlBuilder.Append("</EndTransactRequest></EndTransactionMods>");
            XmlBuilder.Append("</PNRBFManagement_32>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append("</SubmitXmlOnSession>");
            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");
            return XmlBuilder.ToString();
        }

        public string BookingDivideReq(string Token, XElement retriveInfo, DataRow[] paxdt)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");

            XmlBuilder.Append("<SubmitXmlOnSession xmlns='http://webservices.galileo.com'>");
            XmlBuilder.Append("<Token>" + Token + "</Token>");
            XmlBuilder.Append("<LDVOverride />");

            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFDivide_4 xmlns=''>");
            XmlBuilder.Append("<BookingDivideMods>");
            XmlBuilder.Append("<Divide>");
            XmlBuilder.Append("<Type>DP</Type>");
            XmlBuilder.Append("<DivideAry>");

            try
            {
                foreach (DataRow paxnames in paxdt)
                {
                    IEnumerable<XElement> LNameInfo = retriveInfo.Elements("LNameInfo").Where(x => x.Element("LName").Value.ToUpper() == paxnames["LName"].ToString().ToUpper());
                    IEnumerable<XElement> FNameInfo = retriveInfo.Elements("FNameInfo").Where(x => x.Element("FName").Value.ToUpper() == paxnames["FName"].ToString().ToUpper() + paxnames["MName"].ToString().ToUpper() + paxnames["Title"].ToString().ToUpper());
                    if (LNameInfo.Count() > 0 && FNameInfo.Count() > 0)
                        XmlBuilder.Append("<DivideItem><PsgrNum>" + FNameInfo.ElementAt(0).Element("AbsNameNum").Value + "</PsgrNum></DivideItem>");
                }
            }
            catch (Exception ex)
            {
              //  ITZERRORLOG.ExecptionLogger.FileHandling("BookingDivideReq", "Error_001", ex, "GDSCancellation");
            }

            XmlBuilder.Append("</DivideAry>");
            XmlBuilder.Append("</Divide>");
            XmlBuilder.Append("</BookingDivideMods>");
            XmlBuilder.Append("</PNRBFDivide_4>");
            XmlBuilder.Append("</Request>");
            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion
            XmlBuilder.Append("</SubmitXmlOnSession>");
            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");
            return XmlBuilder.ToString();
        }

        public string StorePriceReq(string Token, string CRSInd, string PCC, string FareDataExistsInd, DataTable PaxDT, string FName, string MName, string Title, string LName)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");

            XmlBuilder.Append("<SubmitXmlOnSession xmlns='http://webservices.galileo.com'>");
            XmlBuilder.Append("<Token>" + Token + "</Token>");
            XmlBuilder.Append("<LDVOverride />");

            XmlBuilder.Append("<Request>");
            XmlBuilder.Append("<PNRBFManagement_18 xmlns=''>");
            XmlBuilder.Append("<StorePriceMods>");
            int k = 1;
            XmlBuilder.Append("<PsgrMods><PsgrAry>");
            for (int i = 0; i < PaxDT.Rows.Count; i++)
            {
                if (PaxDT.Rows[i]["FName"].ToString() + PaxDT.Rows[i]["MName"].ToString() + PaxDT.Rows[i]["LName"].ToString() + PaxDT.Rows[i]["Title"].ToString() != FName + MName + LName + Title)
                {
                    XmlBuilder.Append("<Psgr>");
                    XmlBuilder.Append("<LNameNum>0" + k.ToString() + "</LNameNum>");
                    XmlBuilder.Append("<PsgrNum>01</PsgrNum>");
                    XmlBuilder.Append("<AbsNameNum>0" + k.ToString() + "</AbsNameNum>");
                    XmlBuilder.Append("<PIC>" + PaxDT.Rows[i]["PaxType"].ToString() + "</PIC>");
                    XmlBuilder.Append("<TIC />");
                    XmlBuilder.Append("</Psgr>");
                    k++;
                }
            }
            XmlBuilder.Append("</PsgrAry></PsgrMods>");

            XmlBuilder.Append("<SegSelection>");
            XmlBuilder.Append("<ReqAirVPFs>Y</ReqAirVPFs>");
            XmlBuilder.Append("<SegRangeAry>");
            XmlBuilder.Append("<SegRange>");
            XmlBuilder.Append("<StartSeg>00</StartSeg>");
            XmlBuilder.Append("<EndSeg>00</EndSeg>");
            XmlBuilder.Append("<FareType>P</FareType>");

            XmlBuilder.Append("<PFQual>");
            XmlBuilder.Append("<CRSInd>" + CRSInd + "</CRSInd>");
            XmlBuilder.Append("<PCC>" + PCC + "</PCC>");
            XmlBuilder.Append("<Acct></Acct>");
            XmlBuilder.Append("<Contract />");
            XmlBuilder.Append("<PublishedFaresInd>" + FareDataExistsInd + "</PublishedFaresInd>");
            XmlBuilder.Append("<Type>A</Type>");
            XmlBuilder.Append("</PFQual>");

            XmlBuilder.Append("</SegRange>");
            XmlBuilder.Append("</SegRangeAry>");
            XmlBuilder.Append("</SegSelection>");

            XmlBuilder.Append("</StorePriceMods>");
            XmlBuilder.Append("</PNRBFManagement_18>");
            XmlBuilder.Append("</Request>");

            #region Filters
            XmlBuilder.Append("<Filter>");
            XmlBuilder.Append("<_ xmlns='' />");
            XmlBuilder.Append("</Filter>");
            #endregion

            XmlBuilder.Append("</SubmitXmlOnSession>");
            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");
            return XmlBuilder.ToString();
        }

        private static void SaveFile(string module, string Res)
        {
            try
            {
                string newFileName = module + DateTime.Now.ToString("hh_mm_ss");
                string activeDir = ConfigurationManager.AppSettings["GDSCancelSavePath"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";// +securityToken + @"\";
                DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
                if (!Directory.Exists(objDirectoryInfo.FullName))
                {
                    Directory.CreateDirectory(activeDir);
                }

                string path = activeDir + newFileName + ".txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.Write(Res);
                    }
                }
            }
            catch (Exception ex)
            {
               // ITZERRORLOG.ExecptionLogger.FileHandling("SaveFile", "Error_001", ex, "GDSCancellation");
            }
        }

        public int GDSFightCancellation_ANDStorPrice(DataTable PaxDT, string FName, string MName, string Title, string LName, string PNR, string TicketNo, string Remark, string Constr)
        {
            int ResponseStatus = 0; FlightCancellationDAL objfltbal = new FlightCancellationDAL(Constr);
            try
            {
                GALTransanctions objgaltrans = new GALTransanctions();
                FlightSearch searchParam = new FlightSearch();
                Credentials objCrd = new Credentials(Constr);
                List<CredentialList> CrdList = objCrd.GetServiceCredentials("");

                FltRequest onjfltcancel = new FltRequest(searchParam, CrdList);

                string url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                string Userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                string Password = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
                string BookingURL = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].BookingURL;
                // Step 1 Begin Session
                string SSReq = onjfltcancel.BeginSessionReq();
                string SSResp = objgaltrans.PostXml(url, SSReq, Userid, Password, "http://webservices.galileo.com/BeginSession");

                SaveFile(PNR + "_BeginSessionReq_", SSReq);
                SaveFile(PNR + "_BeginSessionResp_", SSResp);
                XDocument xmlresp = XDocument.Parse(SSResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty));

                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                string tokenid = xmlresp.Element(soap + "Envelope").Element(soap + "Body").Element("BeginSessionResponse").Element("BeginSessionResult").Value;
                //Step 2  Retriv PNR
                string retriveReq = PNRBFRetrieveReq(PNR, tokenid);
                string retriveResp = objgaltrans.PostXml(url, retriveReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");

                objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), TicketNo, retriveReq, retriveResp, "Retrive");
                SaveFile(PNR + "_PNRBFRetrieveReq_", retriveReq);
                SaveFile(PNR + "_PNRBFRetrieveResp_", retriveResp);

                //Parsing XML from Retrive PNR Response FNameInfo
                XDocument xmlDocReq = XDocument.Parse(retriveResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                XElement retrivepnrXML = xmlDocReq.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("PNRBFRetrieve");
                IEnumerable<XElement> FNameInfo = retrivepnrXML.Elements("FNameInfo");
                IEnumerable<XElement> LNameInfo = retrivepnrXML.Elements("LNameInfo");

                string newPNR = PNR;
                string CancelReq = "", CancelResp = "", DivideReq = "", DivideResp = "", StorePriceReqss = "", StorePriceResp = "", retriveReq2 = "", retriveResp2 = "";
                string EndTransactionReq = "", EndTransactionResp = "", EndTransactionReq1 = "", EndTransactionResp1 = "", EndTransactionReq2 = "", EndTransactionResp2 = "";
                string EndTransactionReq3 = "", EndTransactionResp3 = "", EndTransactionReq4 = "", EndTransactionResp4 = "", EndTransactionReq5 = "", EndTransactionResp5 = "";
                if (LNameInfo.Count() == 1)
                {
                    //Step 3 Cancel PNR
                    //CancelReq = SegCancelModsReq("FF", tokenid);
                    CancelResp = objgaltrans.PostXml(url, CancelReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_CancelReq_", CancelReq);
                    SaveFile(PNR + "_CancelResp_", CancelResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), PNR, retriveReq, retriveReq, "Cancel");

                    //Step 4 End Transaction
                    EndTransactionReq1 = EndTransactionModsReq(tokenid, "PAX", "E");
                    EndTransactionResp1 = objgaltrans.PostXml(url, EndTransactionReq1, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    //Step 5 End Transaction 
                    if (EndTransactionResp1.Contains("ErrorCode"))
                    {
                        EndTransactionReq = EndTransactionModsReq(tokenid, "", "E");
                        EndTransactionResp = objgaltrans.PostXml(url, EndTransactionReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                        SaveFile(PNR + "_EndTransactionReq_", EndTransactionReq);
                        SaveFile(PNR + "_EndTransactionResp_", EndTransactionResp);

                        if (!EndTransactionResp.Contains("ErrorCode") && EndTransactionResp != "")
                        {
                            ResponseStatus = 1;
                            XDocument Endtrans1 = XDocument.Parse(EndTransactionResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                            XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("EndTransaction");
                            if (Endtrans1Resp != null)
                                newPNR = Endtrans1Resp.Element("EndTransactResponse").Element("RecLoc").Value;

                        }
                    }
                    else if (EndTransactionResp1 != "" && EndTransactionResp1.Contains("ErrorCode") != true)
                    {
                        XDocument Endtrans1 = XDocument.Parse(EndTransactionResp1.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                        XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("EndTransaction");
                        if (Endtrans1Resp != null)
                            newPNR = Endtrans1Resp.Element("EndTransactResponse").Element("RecLoc").Value;

                        ResponseStatus = 1;
                    }
                    SaveFile(PNR + "_EndTransactionReq1_", EndTransactionReq1);
                    SaveFile(PNR + "_EndTransactionResp1_", EndTransactionResp1);

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), ResponseStatus.ToString(), EndTransactionReq1, EndTransactionResp1, "EndTransaction1");
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), newPNR, EndTransactionReq, EndTransactionResp, "EndTransaction2");
                }
                else
                {

                    string PsgrNum = "1", AbsNameNum = "1", LNameNum = "1";
                    foreach (var paxname in LNameInfo)
                    {
                        if (paxname.Element("LName").Value.ToUpper() == LName.ToUpper())
                        {
                            LNameNum = paxname.Element("LNameNum").Value;
                        }
                    }
                    foreach (var paxname in FNameInfo)
                    {
                        if (paxname.Element("FName").Value.ToUpper() == (FName + MName + Title).ToUpper())
                        {
                            PsgrNum = paxname.Element("PsgrNum").Value;
                            AbsNameNum = paxname.Element("AbsNameNum").Value;
                        }
                    }
                    string OwningCRS = retrivepnrXML.Element("GenPNRInfo").Element("OwningCRS").Value;
                    string PCC = retrivepnrXML.Element("GenPNRInfo").Element("CurAgncyPCC").Value;
                    string FareDataExistsInd = retrivepnrXML.Element("GenPNRInfo").Element("FareDataExistsInd").Value;
                    //Step 3 Divid PNR  
                    //DivideReq = BookingDivideReq(AbsNameNum, tokenid);
                    DivideResp = objgaltrans.PostXml(url, DivideReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_DivideReq_", DivideReq);
                    SaveFile(PNR + "_DivideResp_", DivideResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", DivideReq, DivideResp, "Divide");

                    //Step 4 End Transaction for new pnr
                    EndTransactionReq1 = EndTransactionModsReq(tokenid, "JAN", "F");
                    EndTransactionResp1 = objgaltrans.PostXml(url, EndTransactionReq1, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq1_", EndTransactionReq1);
                    SaveFile(PNR + "_EndTransactionResp1_", EndTransactionResp1);
                    //Parsing XML from fist End Transaction PNR Response regarding New PNR

                    if (EndTransactionResp1 != "")
                    {
                        XDocument Endtrans1 = XDocument.Parse(EndTransactionResp1.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                        XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("PNRBFRetrieve");

                        if (Endtrans1Resp != null)
                            newPNR = Endtrans1Resp.Element("DividedBookingInfo").Element("RLoc").Value;
                    }
                    //Step 5 once again Second  End Transaction for old pnr
                    EndTransactionReq2 = EndTransactionModsReq(tokenid, "JAN", "E");
                    EndTransactionResp2 = objgaltrans.PostXml(url, EndTransactionReq2, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq2_", EndTransactionReq2);
                    SaveFile(PNR + "_EndTransactionResp2_", EndTransactionResp2);

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", EndTransactionReq1, EndTransactionResp1, "EndTransaction1");
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), newPNR, EndTransactionReq2, EndTransactionResp2, "EndTransaction2");

                    //Step 6 Prce stor old pnr
                    StorePriceReqss = StorePriceReq(tokenid, OwningCRS, PCC, FareDataExistsInd, PaxDT, FName, MName, Title, LName);
                    StorePriceResp = objgaltrans.PostXml(url, StorePriceReqss, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_StorePriceReq_", StorePriceReqss);
                    SaveFile(PNR + "_StorePriceResp_", StorePriceResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", StorePriceReqss, StorePriceResp, "StorePrice");

                    //Step 5 once again Second  End Transaction for old pnr
                    EndTransactionReq3 = EndTransactionModsReq(tokenid, "JAN", "E");
                    EndTransactionResp3 = objgaltrans.PostXml(url, EndTransactionReq3, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq3_", EndTransactionReq3);
                    SaveFile(PNR + "_EndTransactionResp3_", EndTransactionResp3);
                    // objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", EndTransactionReq3, EndTransactionResp3, "EndTransaction3");

                    //Step 6 End Transaction Third for Store Fare (OLD PNR)
                    EndTransactionReq4 = EndTransactionModsReq(tokenid, "", "E");
                    EndTransactionResp4 = objgaltrans.PostXml(url, EndTransactionReq4, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq4_", EndTransactionReq4);
                    SaveFile(PNR + "_EndTransactionResp4_", EndTransactionResp4);

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", EndTransactionReq3 + EndTransactionReq4, EndTransactionResp3 + EndTransactionResp4, "EndTransaction3_4");

                    //Step 7 Retrive New PNR
                    retriveReq2 = PNRBFRetrieveReq(newPNR, tokenid);
                    retriveResp2 = objgaltrans.PostXml(url, retriveReq2, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_PNRBFRetrieveReq2_", retriveReq2);
                    SaveFile(PNR + "_PNRBFRetrieveResp2_", retriveResp2);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", retriveReq2, retriveResp2, "Retrie2");

                    //10 Cancel Segment/s (ALL segments)
                    //CancelReq = SegCancelModsReq("FF", tokenid);
                    CancelResp = objgaltrans.PostXml(BookingURL, CancelReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_CancelReq_", CancelReq);
                    SaveFile(PNR + "_CancelResp_", CancelResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), PNR, retriveReq, retriveReq, "Cancel");

                    //Step 11 End Transaction 
                    EndTransactionReq5 = EndTransactionModsReq(tokenid, "JAN", "E");
                    EndTransactionResp5 = objgaltrans.PostXml(url, EndTransactionReq5, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq5_", EndTransactionReq5);
                    SaveFile(PNR + "_EndTransactionResp5_", EndTransactionResp5);
                    //Step 12 End Transaction 
                    if (EndTransactionResp5.Contains("ErrorCode"))
                    {
                        EndTransactionReq = EndTransactionModsReq(tokenid, "", "E");
                        EndTransactionResp = objgaltrans.PostXml(url, EndTransactionReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                        SaveFile(PNR + "_EndTransactionReq_", EndTransactionReq);
                        SaveFile(PNR + "_EndTransactionResp_", EndTransactionResp);
                        ResponseStatus = 1;
                    }
                    else if (!EndTransactionResp5.Contains("ErrorCode") && EndTransactionResp5 != "")
                        ResponseStatus = 1;

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), ResponseStatus.ToString(), EndTransactionReq5 + EndTransactionReq, EndTransactionResp5 + EndTransactionResp, "EndTransaction5_6");
                }

                //Step 13 Session END
                string SeesionEndReq = onjfltcancel.EndSessionReq(tokenid);
                string SeesionEndResp = objgaltrans.PostXml(url, SeesionEndReq, Userid, Password, "http://webservices.galileo.com/EndSession");
                SaveFile(PNR + "_EndSessionReq_", SeesionEndReq);
                SaveFile(PNR + "_EndSessionResp_", SeesionEndResp);
            }
            catch (Exception ex)
            {
               // ITZERRORLOG.ExecptionLogger.FileHandling("GDSFightCancellation_ANDStorPrice", "Error_001", ex, "GDSCancellation");
            }

            return ResponseStatus;
        }
        public int GDSFightCancellation_BY_OnePax(DataTable PaxDT, string FName, string MName, string Title, string LName, string PNR, string TicketNo, string Remark, string Constr)
        {
            int ResponseStatus = 0; FlightCancellationDAL objfltbal = new FlightCancellationDAL(Constr);
            try
            {
                GALTransanctions objgaltrans = new GALTransanctions();
                FlightSearch searchParam = new FlightSearch();
                Credentials objCrd = new Credentials(Constr);
                List<CredentialList> CrdList = objCrd.GetServiceCredentials("");

                FltRequest onjfltcancel = new FltRequest(searchParam, CrdList);

                string url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                string Userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                string Password = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
                string BookingURL = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].BookingURL;
                // Step 1 Begin Session
                string SSReq = onjfltcancel.BeginSessionReq();
                string SSResp = objgaltrans.PostXml(url, SSReq, Userid, Password, "http://webservices.galileo.com/BeginSession");

                SaveFile(PNR + "_BeginSessionReq_", SSReq);
                SaveFile(PNR + "_BeginSessionResp_", SSResp);
                XDocument xmlresp = XDocument.Parse(SSResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty));

                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                string tokenid = xmlresp.Element(soap + "Envelope").Element(soap + "Body").Element("BeginSessionResponse").Element("BeginSessionResult").Value;
                //Step 2  Retriv PNR
                string retriveReq = PNRBFRetrieveReq(PNR, tokenid);
                string retriveResp = objgaltrans.PostXml(url, retriveReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");

                objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), TicketNo, retriveReq, retriveResp, "Retrive");
                SaveFile(PNR + "_PNRBFRetrieveReq_", retriveReq);
                SaveFile(PNR + "_PNRBFRetrieveResp_", retriveResp);

                //Parsing XML from Retrive PNR Response FNameInfo
                XDocument xmlDocReq = XDocument.Parse(retriveResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                XElement retrivepnrXML = xmlDocReq.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("PNRBFRetrieve");
                IEnumerable<XElement> FNameInfo = retrivepnrXML.Elements("FNameInfo");
                IEnumerable<XElement> LNameInfo = retrivepnrXML.Elements("LNameInfo");
                string newPNR = PNR;
                string CancelReq = "", CancelResp = "", DivideReq = "", DivideResp = "", StorePriceReqss = "", StorePriceResp = "", retriveReq2 = "", retriveResp2 = "";
                string EndTransactionReq = "", EndTransactionResp = "", EndTransactionReq1 = "", EndTransactionResp1 = "", EndTransactionReq2 = "", EndTransactionResp2 = "";
                string EndTransactionReq3 = "", EndTransactionResp3 = "", EndTransactionReq4 = "", EndTransactionResp4 = "", EndTransactionReq5 = "", EndTransactionResp5 = "";
                if (LNameInfo.Count() == 1)
                {
                    //Step 3 Cancel PNR
                    // CancelReq = SegCancelModsReq("FF", tokenid);
                    CancelResp = objgaltrans.PostXml(url, CancelReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_CancelReq_", CancelReq);
                    SaveFile(PNR + "_CancelResp_", CancelResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), PNR, retriveReq, retriveReq, "Cancel");

                    //Step 4 End Transaction
                    EndTransactionReq1 = EndTransactionModsReq(tokenid, "PAX", "E");
                    EndTransactionResp1 = objgaltrans.PostXml(url, EndTransactionReq1, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    //Step 5 End Transaction 
                    if (EndTransactionResp1.Contains("ErrorCode"))
                    {
                        EndTransactionReq = EndTransactionModsReq(tokenid, "", "E");
                        EndTransactionResp = objgaltrans.PostXml(url, EndTransactionReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                        SaveFile(PNR + "_EndTransactionReq_", EndTransactionReq);
                        SaveFile(PNR + "_EndTransactionResp_", EndTransactionResp);

                        if (!EndTransactionResp.Contains("ErrorCode") && EndTransactionResp != "")
                        {
                            ResponseStatus = 1;
                            XDocument Endtrans1 = XDocument.Parse(EndTransactionResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                            XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("EndTransaction");
                            if (Endtrans1Resp != null)
                                newPNR = Endtrans1Resp.Element("EndTransactResponse").Element("RecLoc").Value;

                        }
                    }
                    else if (EndTransactionResp1 != "" && EndTransactionResp1.Contains("ErrorCode") != true)
                    {
                        XDocument Endtrans1 = XDocument.Parse(EndTransactionResp1.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                        XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("EndTransaction");
                        if (Endtrans1Resp != null)
                            newPNR = Endtrans1Resp.Element("EndTransactResponse").Element("RecLoc").Value;

                        ResponseStatus = 1;
                    }

                    SaveFile(PNR + "_EndTransactionReq1_", EndTransactionReq1);
                    SaveFile(PNR + "_EndTransactionResp1_", EndTransactionResp1);

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), ResponseStatus.ToString(), EndTransactionReq1, EndTransactionResp1, "EndTransaction1");
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", EndTransactionReq, EndTransactionResp, "EndTransaction2");
                }
                else
                {

                    string PsgrNum = "1", AbsNameNum = "1", LNameNum = "1";
                    foreach (var paxname in LNameInfo)
                    {
                        if (paxname.Element("LName").Value.ToUpper() == LName.ToUpper())
                        {
                            LNameNum = paxname.Element("LNameNum").Value;
                        }
                    }
                    foreach (var paxname in FNameInfo)
                    {
                        if (paxname.Element("FName").Value.ToUpper() == (FName + MName + Title).ToUpper())
                        {
                            PsgrNum = paxname.Element("PsgrNum").Value;
                            AbsNameNum = paxname.Element("AbsNameNum").Value;
                        }
                    }
                    string OwningCRS = retrivepnrXML.Element("GenPNRInfo").Element("OwningCRS").Value;
                    string PCC = retrivepnrXML.Element("GenPNRInfo").Element("CurAgncyPCC").Value;
                    string FareDataExistsInd = retrivepnrXML.Element("GenPNRInfo").Element("FareDataExistsInd").Value;
                    //Step 3 Divid PNR  
                    //DivideReq = BookingDivideReq(AbsNameNum, tokenid);
                    DivideResp = objgaltrans.PostXml(url, DivideReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_DivideReq_", DivideReq);
                    SaveFile(PNR + "_DivideResp_", DivideResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", DivideReq, DivideResp, "Divide");

                    //Step 4 End Transaction for new pnr
                    EndTransactionReq1 = EndTransactionModsReq(tokenid, "JAN", "F");
                    EndTransactionResp1 = objgaltrans.PostXml(url, EndTransactionReq1, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq1_", EndTransactionReq1);
                    SaveFile(PNR + "_EndTransactionResp1_", EndTransactionResp1);
                    //Parsing XML from fist End Transaction PNR Response regarding New PNR

                    if (EndTransactionResp1 != "")
                    {
                        XDocument Endtrans1 = XDocument.Parse(EndTransactionResp1.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty).Replace("xmlns=\"\"", String.Empty));
                        XElement Endtrans1Resp = Endtrans1.Element(soap + "Envelope").Element(soap + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_32").Element("PNRBFRetrieve");

                        if (Endtrans1Resp != null)
                            newPNR = Endtrans1Resp.Element("DividedBookingInfo").Element("RLoc").Value;
                    }


                    //Step 5 once again Second  End Transaction for old pnr
                    EndTransactionReq2 = EndTransactionModsReq(tokenid, "JAN", "E");
                    EndTransactionResp2 = objgaltrans.PostXml(url, EndTransactionReq2, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq2_", EndTransactionReq2);
                    SaveFile(PNR + "_EndTransactionResp2_", EndTransactionResp2);

                    //Step 6 End Transaction Third for Store Fare (OLD PNR)
                    EndTransactionReq3 = EndTransactionModsReq(tokenid, "", "E");
                    EndTransactionResp3 = objgaltrans.PostXml(url, EndTransactionReq3, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq3_", EndTransactionReq3);
                    SaveFile(PNR + "_EndTransactionResp3_", EndTransactionResp3);

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", EndTransactionReq1, EndTransactionResp1, "EndTransaction1");
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), newPNR, EndTransactionReq2, EndTransactionResp2, "EndTransaction2");
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", EndTransactionReq3, EndTransactionResp3, "EndTransaction3_4");

                    //Step 7 Retrive New PNR
                    retriveReq2 = PNRBFRetrieveReq(newPNR, tokenid);
                    retriveResp2 = objgaltrans.PostXml(url, retriveReq2, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_PNRBFRetrieveReq2_", retriveReq2);
                    SaveFile(PNR + "_PNRBFRetrieveResp2_", retriveResp2);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), "", retriveReq2, retriveResp2, "Retrie2");

                    //10 Cancel Segment/s (ALL segments)
                    //CancelReq = SegCancelModsReq("FF", tokenid);
                    CancelResp = objgaltrans.PostXml(BookingURL, CancelReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_CancelReq_", CancelReq);
                    SaveFile(PNR + "_CancelResp_", CancelResp);
                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), PNR, retriveReq, retriveReq, "Cancel");

                    //Step 11 End Transaction 
                    EndTransactionReq5 = EndTransactionModsReq(tokenid, "JAN", "E");
                    EndTransactionResp5 = objgaltrans.PostXml(url, EndTransactionReq5, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                    SaveFile(PNR + "_EndTransactionReq5_", EndTransactionReq5);
                    SaveFile(PNR + "_EndTransactionResp5_", EndTransactionResp5);
                    //Step 12 End Transaction 
                    if (EndTransactionResp5.Contains("ErrorCode"))
                    {
                        EndTransactionReq = EndTransactionModsReq(tokenid, "", "E");
                        EndTransactionResp = objgaltrans.PostXml(url, EndTransactionReq, Userid, Password, "http://webservices.galileo.com/SubmitXmlOnSession");
                        SaveFile(PNR + "_EndTransactionReq_", EndTransactionReq);
                        SaveFile(PNR + "_EndTransactionResp_", EndTransactionResp);

                        ResponseStatus = 1;
                    }
                    else if (!EndTransactionResp5.Contains("ErrorCode") && EndTransactionResp5 != "")
                        ResponseStatus = 1;

                    objfltbal.InsertGDSCancleLog(PaxDT.Rows[0]["Orderid"].ToString(), ResponseStatus.ToString(), EndTransactionReq5 + EndTransactionReq, EndTransactionResp5 + EndTransactionResp, "EndTransaction5_6");
                }

                //Step 13 Session END
                string SeesionEndReq = onjfltcancel.EndSessionReq(tokenid);
                string SeesionEndResp = objgaltrans.PostXml(url, SeesionEndReq, Userid, Password, "http://webservices.galileo.com/EndSession");
                SaveFile(PNR + "_EndSessionReq_", SeesionEndReq);
                SaveFile(PNR + "_EndSessionResp_", SeesionEndResp);
            }
            catch (Exception ex)
            {
               // ITZERRORLOG.ExecptionLogger.FileHandling("GDSFightCancellation_BY_OnePax", "Error_001", ex, "GDSCancellation");
            }

            return ResponseStatus;
        }

        protected void EndSesstions(string PNR, string url, string userid, string password, string ActionName, string tokenids, string TrnsNo, FltRequest onjfltcancel, GALTransanctions objgaltrans)
        {
            string SeesionEndReq = onjfltcancel.EndSessionReq(tokenids);
            string SeesionEndResp = objgaltrans.PostXml(url, SeesionEndReq, userid, password, ActionName);
            SaveFile(PNR + "_EndSessionReq" + TrnsNo + "_", SeesionEndReq);
            SaveFile(PNR + "_EndSessionResp" + TrnsNo + "_", SeesionEndResp);
        }
        protected string StratSesstions(string PNR, string url, string userid, string password, string ActionName, string TrnsNo, FltRequest onjfltcancel, GALTransanctions objgaltrans)
        {
            try
            {
                string SSReq = onjfltcancel.BeginSessionReq();
                string SSResp = objgaltrans.PostXml(url, SSReq, userid, password, ActionName);

                SaveFile(PNR + "_BeginSessionReq" + TrnsNo + "_", SSReq);
                SaveFile(PNR + "_BeginSessionResp" + TrnsNo + "_", SSResp);
                XDocument xmlresp = XDocument.Parse(SSResp.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty));

                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                return xmlresp.Element(soap + "Envelope").Element(soap + "Body").Element("BeginSessionResponse").Element("BeginSessionResult").Value;
            }
            catch (Exception ex)
            {
               // ITZERRORLOG.ExecptionLogger.FileHandling("StratSesstions", "Error_001", ex, "GDSCancellation");
                return "";
            }
          
        }
    }
}
