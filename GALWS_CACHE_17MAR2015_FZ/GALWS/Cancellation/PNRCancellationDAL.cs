﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using OnlineCancellationSHARED;
namespace OnlineCancellationDAL
{

    public class PNRCancellationDAL
    {
        SqlCommand cmd;
        private SqlDatabase DBHelper;
        public PNRCancellationDAL(string connStr)
        {
            DBHelper = new SqlDatabase(connStr);
        }

        public List<SegmentDetail> GetCancellationSegmentDetails(string OrderID, string PaxID, string Action)
        {
            List<SegmentDetail> ObjS = new List<SegmentDetail>();
            try
            {
                SqlCommand cmd = new SqlCommand("USP_GetCancellationSegment");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", OrderID.Trim());
                cmd.Parameters.AddWithValue("@PaxId", PaxID.Trim());
                cmd.Parameters.AddWithValue("@Operation", Action.Trim());
                var ds = DBHelper.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            for (int s = 0; s < ds.Tables[0].Rows.Count; s++)
                            {
                                ObjS.Add(new SegmentDetail
                                {
                                    PaxId = Convert.ToString(ds.Tables[0].Rows[s]["PAXPAXID"]),
                                    PaxTitle = Convert.ToString(ds.Tables[0].Rows[s]["TITLE"]),
                                    PaxFName = Convert.ToString(ds.Tables[0].Rows[s]["FNAME"]),
                                    PaxLName = Convert.ToString(ds.Tables[0].Rows[s]["LNAME"]),
                                    FltId = Convert.ToString(ds.Tables[0].Rows[s]["FLTID"]),
                                    Sector = Convert.ToString(ds.Tables[0].Rows[s]["SECTOR"]),
                                    Segment = Convert.ToString(ds.Tables[0].Rows[s]["SEGMENT"]),
                                    TicketNumber = Convert.ToString(ds.Tables[0].Rows[s]["TICKETNUMBER"]),
                                    DeptDate = Convert.ToString(ds.Tables[0].Rows[s]["DEPDATE"]),
                                    DeptTime = Convert.ToString(ds.Tables[0].Rows[s]["DEPTIME"]),
                                    ArrDate = Convert.ToString(ds.Tables[0].Rows[s]["ARRDATE"]),
                                    ArrTime = Convert.ToString(ds.Tables[0].Rows[s]["ARRTIME"]),
                                    Class = Convert.ToString(ds.Tables[0].Rows[s]["CLASS"]),
                                    VC = Convert.ToString(ds.Tables[0].Rows[s]["VC"]),
                                    CancelStatus = Convert.ToString(ds.Tables[0].Rows[s]["CANCELSTATUS"]),
                                    RefNo = Convert.ToString(ds.Tables[0].Rows[s]["CANCELID"]),
                                    DepAirCode = Convert.ToString(ds.Tables[0].Rows[s]["DEPCODE"]),
                                    ArrAirCode = Convert.ToString(ds.Tables[0].Rows[s]["ARRCODE"]),
                                    FltNo = Convert.ToString(ds.Tables[0].Rows[s]["FLTNUMBER"]),
                                    ReissueStatus = Convert.ToString(ds.Tables[0].Rows[s]["REISSUESTATUS"]),
                                    PaxType = Convert.ToString(ds.Tables[0].Rows[s]["PAXTYPE"]),
                                    //Start Change by birendra for cancellation
                                    Provider = Convert.ToString(ds.Tables[0].Rows[s]["Provider"]),
                                    GDSPNR = Convert.ToString(ds.Tables[0].Rows[s]["GDSPNR"]),
                                    AIRLINEPNR = Convert.ToString(ds.Tables[0].Rows[s]["AIRLINEPNR"]),
                                    CorpId = Convert.ToString(ds.Tables[0].Rows[s]["CORPID"]),
                                    Trip = Convert.ToString(ds.Tables[0].Rows[s]["TRIP"]),
                                    ADTFareBasis = Convert.ToString(ds.Tables[0].Rows[s]["ADTFAREBASIS"]),
                                    //END Change by birendra for cancellation
                                });
                            }
                        }
                    }
                }


                ds.Dispose();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
            }
            return ObjS;
        }
        public DataSet GetHdrDetails(string trackid)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd;
            try
            {
                cmd = new SqlCommand("GetFltHdr_Cancel");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TrackId", trackid.Trim());
                ds = DBHelper.ExecuteDataSet(cmd);


                ds.Dispose();
                cmd.Dispose();
            }
            catch (Exception EX)
            { }

            finally
            {

            }
            return ds;


        }

        public string CheckTicketStatusBySegment(string orderid, string paxid, string segment)
        {
            string lstPax = "";
            try
            {
                SqlCommand cmd = new SqlCommand("USP_STATUSCHECK_SEGMENT");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PAXID", !string.IsNullOrEmpty(Convert.ToString(paxid)) ? Convert.ToInt32(paxid.Trim()) : 0);
                cmd.Parameters.AddWithValue("@ORDERID", orderid != null ? orderid.Trim() : "");
                cmd.Parameters.AddWithValue("@SEGMENT", segment != null ? segment.Trim() : "");
                var ds = DBHelper.ExecuteDataSet(cmd);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        lstPax += Convert.ToString(ds.Tables[0].Rows[0]["FLAG"]);
                        lstPax += "_" + Convert.ToString(ds.Tables[0].Rows[0]["MESSGE"]);
                    }
                }
                ds.Dispose();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                
            }
            return lstPax;
        }
        public string GetFlightTrackID()
        {
            string result = "";
            try
            {
                //conn.Open();
                cmd = new SqlCommand();//"SP_GetFlightTrackID", conn);
                cmd.CommandText = "USP_Flt_GetTrackID";
                cmd.CommandType = CommandType.StoredProcedure;
                result = DBHelper.ExecuteScalar(cmd).ToString();// cmd.ExecuteScalar().ToString();
                cmd.Dispose();
            }
            catch (Exception ex)
            {

            }
            finally
            {

                //conn.Dispose();
            }

            return result;
        }
        public string AddCancelInfo(ObjCancelPNR obj)
        {
            string status = "Failure_0";
            int rows = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("AddTicketCancelInfo");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", obj.OrderID.Trim());
                cmd.Parameters.AddWithValue("@EXECID", obj.ExecID.Trim());
                cmd.Parameters.AddWithValue("@REQREM", obj.ReqRemarks.Trim());
                cmd.Parameters.AddWithValue("@IPADD", obj.IPAdd.Trim());
                cmd.Parameters.AddWithValue("@PAXSEG", obj.PAXSEG);
                rows = DBHelper.ExecuteNonQuery(cmd);
                if (rows > 0)
                {
                    status = "Success_" + rows;
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
            }
            return status;
        }

        public string AddCancelInfoCan(ObjCancelPNR obj)
        {
            string status = "Failure_0";
            int rows = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("AddTicketCancelInfoCan");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", obj.OrderID.Trim());
                cmd.Parameters.AddWithValue("@EXECID", obj.ExecID.Trim());
                cmd.Parameters.AddWithValue("@REQREM", obj.ReqRemarks.Trim());
                cmd.Parameters.AddWithValue("@IPADD", obj.IPAdd.Trim());
                cmd.Parameters.AddWithValue("@PAXSEG", obj.PAXSEG);
                rows = DBHelper.ExecuteNonQuery(cmd);
                if (rows > 0)
                {
                    status = "Success_" + rows;
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
            }
            return status;
        }

        public string AddCancelSegmentInfo(ObjCancelPNR obj)
        {
            string status = "Failure_0";
            int rows = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("USP_INSCancellationSegment");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", obj.OrderID.Trim());
                cmd.Parameters.AddWithValue("@PAXSEG", obj.PAXSEG);
                cmd.Parameters.AddWithValue("@Operation", "INS");
                rows = DBHelper.ExecuteNonQuery(cmd);
                if (rows > 0)
                {
                    status = "Success_" + rows;
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
            }
            return status;
        }
        public string InsertCancellationlLogs(string ORDERID, string REFNO, string REFUNDAMOUNT, string VC, string BKG_REQ, string BKG_RES, string CAN_REQ, string CAN_RES, string ADDPAY_REQ, string ADDPAY_RES, string BKGCOMMIT_REQ, string BKGCOMMIT_RES, string ERROR)
        {
            string isUpd = "Failure";
            try
            {
                ////for (int f = 0; f < objPNR.allLists[0].fltList.Count; f++)
                ////{
                SqlCommand cmd = new SqlCommand("USP_CAN_LPG");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", ORDERID != null ? ORDERID : "");
                cmd.Parameters.AddWithValue("@REFNO", REFNO != null ? REFNO : "");
                cmd.Parameters.AddWithValue("@REFUNDAMOUNT", REFUNDAMOUNT != null ? REFUNDAMOUNT : "");
                cmd.Parameters.AddWithValue("@VC", VC != null ? VC : "");
                cmd.Parameters.AddWithValue("@BKG_REQ", BKG_REQ != null ? BKG_REQ : "");
                cmd.Parameters.AddWithValue("@BKG_RES", BKG_RES != null ? BKG_RES : "");
                cmd.Parameters.AddWithValue("@CAN_REQ", CAN_REQ != null ? CAN_REQ : "");
                cmd.Parameters.AddWithValue("@CAN_RES", CAN_RES != null ? CAN_RES : "");
                cmd.Parameters.AddWithValue("@ADDPAY_REQ", ADDPAY_REQ != null ? ADDPAY_REQ : "");
                cmd.Parameters.AddWithValue("@ADDPAY_RES", ADDPAY_RES != null ? ADDPAY_RES : "");
                cmd.Parameters.AddWithValue("@BKGCOMMIT_REQ", BKGCOMMIT_REQ != null ? BKGCOMMIT_REQ : "");
                cmd.Parameters.AddWithValue("@BKGCOMMIT_RES", BKGCOMMIT_RES != null ? BKGCOMMIT_RES : "");
                cmd.Parameters.AddWithValue("@ERROR", ERROR != null ? ERROR : "");
                cmd.Parameters.AddWithValue("@ACTION", "INS");

                int rows = DBHelper.ExecuteNonQuery(cmd);
                if (rows != null)
                {
                    if (rows > 0)
                    {
                        isUpd = "Success";
                        cmd.Dispose();
                    }
                }
                ////}
            }
            catch (Exception ex)
            {
            }
            return isUpd;
        }
        public string UpdateCancelTicket(string ORDERID, string REFNO, string REFUNDAMOUNT, string Result, string TAID, string TAUSERID)
        {
            string isUpd = "Failure";
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateCancelTicket");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", ORDERID != null ? ORDERID : "");
                cmd.Parameters.AddWithValue("@REFNO", REFNO != null ? REFNO : "");
                cmd.Parameters.AddWithValue("@REFUNDAMOUNT", REFUNDAMOUNT != null ? REFUNDAMOUNT : "");
                cmd.Parameters.AddWithValue("@Result", Result != null ? Result : "");
                cmd.Parameters.AddWithValue("@TAID", TAID != null ? TAID : "");
                cmd.Parameters.AddWithValue("@TAUSERID", TAUSERID != null ? TAID : "");
                cmd.Parameters.AddWithValue("@ACTION", "UPDATE");

                int rows = DBHelper.ExecuteNonQuery(cmd);
                if (rows != null)
                {
                    if (rows > 0)
                    {
                        isUpd = "Success";
                        cmd.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return isUpd;
        }
        public string CheckUpdateCancelTicket(string ORDERID, string REFNO, string REFUNDAMOUNT, string Result, string TAID, string TAUSERID)
        {
            string isUpd = "Failure";
            try
            {
                SqlCommand cmd = new SqlCommand("SP_Check_UpdateCancelTicket");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDERID", ORDERID != null ? ORDERID : "");
                cmd.Parameters.AddWithValue("@REFNO", REFNO != null ? REFNO : "");
                cmd.Parameters.AddWithValue("@REFUNDAMOUNT", REFUNDAMOUNT != null ? REFUNDAMOUNT : "");
                cmd.Parameters.AddWithValue("@Result", Result != null ? Result : "");
                cmd.Parameters.AddWithValue("@TAID", TAID != null ? TAID : "");
                cmd.Parameters.AddWithValue("@TAUSERID", TAUSERID != null ? TAID : "");
                cmd.Parameters.AddWithValue("@ACTION", "UPDATE");

                int rows = DBHelper.ExecuteNonQuery(cmd);
                if (rows != null)
                {
                    if (rows > 0)
                    {
                        isUpd = "Success";
                        cmd.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return isUpd;
        }

        public string AddToLimit(string UserId, float Amount)
        {
            string result = "";
            try
            {
                cmd = new SqlCommand();//"SP_GetFlightTrackID", conn);
                cmd.CommandText = "AddToLimit";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId.Trim());
                cmd.Parameters.AddWithValue("@Amount", Amount);
                result = DBHelper.ExecuteScalar(cmd).ToString();// cmd.ExecuteScalar().ToString();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                result = "0";
            }

            return result;
        }
        public string UpdateCrdLimit(string UserId, float Amount)
        {
            string result = "";
            try
            {
                cmd = new SqlCommand();//"SP_GetFlightTrackID", conn);
                cmd.CommandText = "UpdateCrdLimit";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId.Trim());
                cmd.Parameters.AddWithValue("@Amount", Amount);
                result = DBHelper.ExecuteScalar(cmd).ToString();// cmd.ExecuteScalar().ToString();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                result = "0";
            }

            return result;
        }

        public string UpdateRefundToLedger(CanRefund canRefnd)
        {
            string Result = "Error";
            try
            {
                SqlCommand cmd = new SqlCommand("SP_UPDATE_LADGER_BY_REFUND");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TRACKID", canRefnd.TRACKID);
                 cmd.Parameters.AddWithValue("@PNRNO", canRefnd.PNRNO);
                 cmd.Parameters.AddWithValue("@AGENTID", canRefnd.AGENTID);
                 cmd.Parameters.AddWithValue("@AGENCYNAME", canRefnd.AGENCYNAME);
                 cmd.Parameters.AddWithValue("@Provider", canRefnd.Provider);
                 cmd.Parameters.AddWithValue("@TRIP", canRefnd.TRIP);
                 cmd.Parameters.AddWithValue("@AVAILBAL", canRefnd.AVAILBAL);
                 cmd.Parameters.AddWithValue("@DebitAmount", canRefnd.DebitAmount);
                 cmd.Parameters.AddWithValue("@CreditAmount", canRefnd.CreditAmount);
                 
                cmd.Parameters.AddWithValue("@RefNo", canRefnd.RefNo);
                cmd.Parameters.AddWithValue("@REFUNCHG", canRefnd.CancellationChg != null ? Convert.ToInt32(Convert.ToDecimal(canRefnd.CancellationChg.Trim())) : 0);
                cmd.Parameters.AddWithValue("@SVCCHARGE", canRefnd.ServiceChg != null ? Convert.ToInt32(Convert.ToDecimal(canRefnd.ServiceChg.Trim())) : 0);
                cmd.Parameters.AddWithValue("@REFUNDFARE", canRefnd.RefundAmt != null ? float.Parse(canRefnd.RefundAmt.Trim()) : 0);
                 DataSet ds = DBHelper.ExecuteDataSet(cmd);
                 if (ds != null)
                 {
                     if (ds.Tables.Count > 0)
                     {
                         if (ds.Tables[0].Rows.Count > 0)
                         {
                             Result = Convert.ToString(ds.Tables[0].Rows[0]["MSG"]);
                         }
                     }
                 }
            }
            catch (Exception ex)
            {
                
               
            }

            return Result;
        }

        public string GetCancellationCharge(CancellationCharge canCharge)
        {
            string result = "";
            try
            {
                cmd = new SqlCommand();//"SP_GetFlightTrackID", conn);
                cmd.CommandText = "SP_GET_CANCELLATION_CHARGE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AgentID", canCharge.AgentID.Trim());
                cmd.Parameters.AddWithValue("@AirlineCode", canCharge.AirlineCode);
                cmd.Parameters.AddWithValue("@Mode", canCharge.Mode);
                cmd.Parameters.AddWithValue("@Trip", canCharge.Trip);
                result = DBHelper.ExecuteScalar(cmd).ToString();// cmd.ExecuteScalar().ToString();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                result = "0";
            }

            return result;
        }
    }

}
