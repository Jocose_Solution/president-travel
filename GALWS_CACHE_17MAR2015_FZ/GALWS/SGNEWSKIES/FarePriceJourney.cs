﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using schemas.navitaire.com.WebServices.DataContracts.Booking;
using System.Collections;


namespace STD.Shared
{
    public class FarePriceJourney
    {
        public string ASC { get; set; }
        public string JSK { get; set; }
        public string FSK { get; set; }
        public string FBC { get; set; }
        public string CCD { get; set; }
        public string RNO { get; set; }
        public string FNO { get; set; } //All Three
        public string STD { get; set; } //Required For 
        public string STA { get; set; }//Sell Journey
        public string COS { get; set; }
        public string FCS { get; set; }
        [System.ComponentModel.DefaultValue(1)]
        public short SegCnt { get; set; }
        public Dictionary<string, string>[] Seg { get; set; }
        //public string EQT { get; set; }
        //public string AST { get; set; }//Available Seat
        [System.ComponentModel.DefaultValue(0)]
        public short FSQ { get; set; }
        public string PCS { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public Dictionary<string, string>[] Leg { get; set; }//
        public PaxFare[] PaxFare { get; set; }
        public decimal InfFare { get; set; }
        public decimal InfTax { get; set; }
        public string AVLCNT { get; set; }
    }
    public class FarePriceJourney_V4
    {
        public string ASC { get; set; }
        public string JSK { get; set; }
        public string FSK { get; set; }
        public string FBC { get; set; }
        public string CCD { get; set; }
        public string RNO { get; set; }
        public string FNO { get; set; } //All Three
        public string STD { get; set; } //Required For 
        public string STA { get; set; }//Sell Journey
        public string COS { get; set; }
        public string FCS { get; set; }
        [System.ComponentModel.DefaultValue(1)]
        public short SegCnt { get; set; }
        public Dictionary<string, string>[] Seg { get; set; }
        //public string EQT { get; set; }
        //public string AST { get; set; }//Available Seat
        [System.ComponentModel.DefaultValue(0)]
        public short FSQ { get; set; }
        public string PCS { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public Dictionary<string, string>[] Leg { get; set; }//
        public navitaire.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.PaxFare[] PaxFare { get; set; }
        public decimal InfFare { get; set; }
        public decimal InfTax { get; set; }
        public string AVLCNT { get; set; }
    }

    #region SGNav
    public class FarePriceJourney_SGV4
    {
        public string ASC { get; set; }
        public string JSK { get; set; }
        public string FSK { get; set; }
        public string FBC { get; set; }
        public string CCD { get; set; }
        public string RNO { get; set; }
        public string FNO { get; set; } //All Three
        public string STD { get; set; } //Required For 
        public string STA { get; set; }//Sell Journey
        public string COS { get; set; }
        public string FCS { get; set; }
        [System.ComponentModel.DefaultValue(1)]
        public short SegCnt { get; set; }
        public Dictionary<string, string>[] Seg { get; set; }
        //public string EQT { get; set; }
        //public string AST { get; set; }//Available Seat
        [System.ComponentModel.DefaultValue(0)]
        public short FSQ { get; set; }
        public string PCS { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public Dictionary<string, string>[] Leg { get; set; }//
        public navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.PaxFare[] PaxFare { get; set; }
        public decimal InfFare { get; set; }
        public decimal InfTax { get; set; }
        public string AVLCNT { get; set; }
    }
    #endregion

    #region 6ENav
    public class FarePriceJourney_6EV4
    {
        public string ASC { get; set; }
        public string JSK { get; set; }
        public string FSK { get; set; }
        public string FBC { get; set; }
        public string CCD { get; set; }
        public string RNO { get; set; }
        public string FNO { get; set; } //All Three
        public string STD { get; set; } //Required For 
        public string STA { get; set; }//Sell Journey
        public string COS { get; set; }
        public string FCS { get; set; }
        [System.ComponentModel.DefaultValue(1)]
        public short SegCnt { get; set; }
        public Dictionary<string, string>[] Seg { get; set; }
        //public string EQT { get; set; }
        //public string AST { get; set; }//Available Seat
        [System.ComponentModel.DefaultValue(0)]
        public short FSQ { get; set; }
        public string PCS { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public Dictionary<string, string>[] Leg { get; set; }//
        public navitaire.indigo.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.PaxFare[] PaxFare { get; set; }
        public decimal InfFare { get; set; }
        public decimal InfTax { get; set; }
        public string AVLCNT { get; set; }
    }
    #endregion
}
