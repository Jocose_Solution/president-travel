﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using STD.Shared;
//using ExceptionLog;

namespace STD.DAL
{
    public class CustInfoDAL
    {
        SqlConnection conn;
        SqlCommand cmd;
        int i = 0;

        public CustInfoDAL()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            
        }

        public DataTable Fetch_Cust_Info(string tid)//List<Shared.IBE.DAL>
        {
            DataTable MyDataTable = new DataTable();
            try
            {
                var list = new List<object>();
                conn.Open();
                cmd = new SqlCommand("SP_DIST_SELECT_CUST_INFO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TID", tid);
                SqlDataReader reader = cmd.ExecuteReader();
                MyDataTable.Load(reader);
            }
            catch (Exception ex)
            {
               
               // ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex,"FlightSearch");
                MyDataTable = null;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
            return MyDataTable;
        }

        //COMMENTED AS METHOD IS PRESENT IN FLIGHT COMMONDAL
        //public int Insert_Flight_PaxDetail(CustInfo ObjCI)
        //{
        //    try
        //    {
        //        conn.Open();
        //        cmd = new SqlCommand("SP_DIST_INSERT_FLT_PAXDETAILS", conn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@OrderID", ObjCI.ORDERID);
        //        cmd.Parameters.AddWithValue("@Title", ObjCI.TITLE);
        //        cmd.Parameters.AddWithValue("@FName", ObjCI.FNAME);
        //        cmd.Parameters.AddWithValue("@MName", ObjCI.MNAME);
        //        cmd.Parameters.AddWithValue("@LName", ObjCI.LNAME);
        //        cmd.Parameters.AddWithValue("@PaxType", ObjCI.PAXTYPE);
        //        cmd.Parameters.AddWithValue("@TicketNo", ObjCI.TICKETNUMBER);
        //        cmd.Parameters.AddWithValue("@DOB", ObjCI.DOB);
        //        cmd.Parameters.AddWithValue("@FFNumber", ObjCI.FFNUMBER);
        //        cmd.Parameters.AddWithValue("@FFAirline", ObjCI.FFAIRLINE);
        //        cmd.Parameters.AddWithValue("@MealType", ObjCI.MEALTYPE);
        //        cmd.Parameters.AddWithValue("@SeatType", ObjCI.SEATTYPE);
        //        cmd.Parameters.AddWithValue("@IsPrimary", ObjCI.ISPRIMARY);
        //        cmd.Parameters.AddWithValue("@InfAssociatePaxName", ObjCI.INFASSOCIATEPAXNAME);
        //        return cmd.ExecuteNonQuery();

        //    }

        //    catch
        //    {

        //        throw;

        //    }

        //    finally
        //    {

        //        cmd.Dispose();

        //        conn.Close();

        //        conn.Dispose();

        //    }

        //}

    }
}
