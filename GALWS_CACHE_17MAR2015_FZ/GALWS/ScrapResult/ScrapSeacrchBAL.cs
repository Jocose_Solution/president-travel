﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.Shared;
using System.Xml.Linq;
using System.Data;
using scrap;
using System.Web;
using System.Collections;
using GALWS;

namespace STD.BAL
{


    public class ScrapSeacrchBAL
    {

        string AccountID = "";
        string AccountPassword = "";
        string AccountIP = "";

        public ScrapSeacrchBAL(string accID, string accPass, string accIP)
        {
            AccountID = accID;
            AccountPassword = accPass;
            AccountIP = accIP;
        }
        // public List<FlightSearchResults> GetFlightSearch(FlightSearch srchInput, string FareType, List<CredentialList> CrdList, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge, HttpContext ctx, List<FlightCityList> CityList, List<AirlineList> Airlist)
        public List<FlightSearchResults> GetFlightSearch(FlightSearch srchInput, string FareType, List<CredentialList> CrdList, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, List<MISCCharges> MiscList, HttpContext ctx, List<FlightCityList> CityList, List<AirlineList> Airlist)
        {

            CredentialList crd = CrdList.Where(x => x.Provider.Trim().ToUpper() == srchInput.HidTxtAirLine.Trim().ToUpper()).ToList()[0];
            string exep = "";
            string res = GetSeachRequest(srchInput, crd, ScrapMathodSVCUrl.SvcURL, ScrapMathodSVCUrl.SrchMTHD, ref exep, ctx, FareType);

            //return ParseResult(res, srchInput, constr, FareType, CityList, Airlist, crd, SrvchargeList, MarkupDs, srvCharge);
            return ParseResult(res, srchInput, constr, FareType, CityList, Airlist, crd, SrvchargeList, MarkupDs, MiscList);

        }

        public string GetSeachRequest(FlightSearch srchInput, CredentialList crd, string url, string methodName, ref string exep, HttpContext ctx, string fareType)
        {

            StringBuilder ReqBldr = new StringBuilder();
            string result = "";
            try
            {
                string querydata = srchInput.RTF == true ? "Two;" : "One;";  //"One;DEL;BLR;21-MAY-2017;;1;0;0;;DEL;BLR;3;False;D";
                querydata += fareType == "INB" ? (srchInput.HidTxtDepCity.Split(',')[0] + ";" + srchInput.HidTxtArrCity.Split(',')[0] + ";") : (srchInput.HidTxtArrCity.Split(',')[0] + ";" + srchInput.HidTxtDepCity.Split(',')[0] + ";");

                string depdate = Convert.ToDateTime(Utility.Right(srchInput.DepDate, 4) + "-" + Utility.Mid(srchInput.DepDate, 3, 2) + "-" + Utility.Left(srchInput.DepDate, 2)).ToString("dd-MMM-yyyy");
                string retdate = Convert.ToDateTime(Utility.Right(srchInput.RetDate, 4) + "-" + Utility.Mid(srchInput.RetDate, 3, 2) + "-" + Utility.Left(srchInput.RetDate, 2)).ToString("dd-MMM-yyyy");
                querydata += (fareType == "INB" ? depdate : retdate) + ";" + (srchInput.RTF == true ? retdate + ";" : ";");  //date
                querydata += srchInput.Adult + ";" + srchInput.Child + ";" + srchInput.Infant + ";;"; //pax 
                querydata += fareType == "INB" ? (srchInput.HidTxtDepCity.Split(',')[0] + ";" + srchInput.HidTxtArrCity.Split(',')[0] + ";") : (srchInput.HidTxtArrCity.Split(',')[0] + ";" + srchInput.HidTxtDepCity.Split(',')[0] + ";");
                querydata += "3;False;" + srchInput.Trip;



                string crdls = srchInput.HidTxtAirLine.Trim().ToUpper() + "~" + crd.UserID + "/" + crd.Password + "/" + crd.Password;//"SG~STAN0004/Back@123/Back@123#G8~STAN0004/Back@123/Back@123#6E~STAN0004/Back@123/Back@123";


                // ReqBldr.Append(" <?xml version=\"1.0\" encoding=\"utf-8\"?>");
                //ReqBldr.Append(" <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
                //ReqBldr.Append("  <soap:Body>");
                //ReqBldr.Append(" <LCCDataSpecialAsyncNew xmlns=\"InFLLCCEngine\">");
                //ReqBldr.Append("  <AccountID>" + AccountID + "</AccountID>");
                //ReqBldr.Append("  <AccountPassword>" + AccountPassword + "</AccountPassword>");
                //ReqBldr.Append(" <AccountIP>" + AccountIP + "</AccountIP>");
                //ReqBldr.Append(" <QueryData>" + querydata + "</QueryData>");
                //ReqBldr.Append(" <Credentials>" + crdls + "</Credentials>");
                //ReqBldr.Append(" </LCCDataSpecialAsyncNew>");
                //ReqBldr.Append(" </soap:Body>");
                //ReqBldr.Append(" </soap:Envelope>");

                scrap.cb.GetData objGD = new scrap.cb.GetData(ctx);


                if (srchInput.HidTxtAirLine.Trim().ToUpper() == "SG")
                {
                    crdls = "SG~" + crd.UserID + "/" + crd.Password + "/" + crd.Password + "~G8~" + crd.UserID + "/" + crd.Password + "/" + crd.Password + "~6E~" + crd.UserID + "/" + crd.Password + "/" + crd.Password;
                    result = objGD.LCCDataAsync(AccountID, AccountPassword, AccountIP, querydata, crdls);
                }
                else if (srchInput.HidTxtAirLine.Trim().ToUpper() == "6E")
                {
                    STD.DAL.CommonDAL objC = new STD.DAL.CommonDAL();
                    result = objC.strScrapRespone(querydata);
                    if (result == "")
                    {
                        result = objGD.LCCDataSpecialAsyncNew(AccountID, AccountPassword, AccountIP, querydata, crdls);
                        objC.ScrapResponeChache(querydata, result);
                    }
                }
                else if (srchInput.HidTxtAirLine.Trim().ToUpper() == "G8")
                {
                    crdls = "G8~" + crd.UserID + "/" + crd.Password + "/" + crd.CorporateID + "~G8~" + crd.UserID + "/" + crd.Password + "/" + crd.CorporateID + "~G8~" + crd.UserID + "/" + crd.Password + "/" + crd.CorporateID;
                    result = objGD.LCCDataAsync(AccountID, AccountPassword, AccountIP, querydata, crdls); // objGD.GoAirAsync(AccountID, AccountPassword, AccountIP, querydata, crdls);//
                }
                //string result = ScrapUtility.PostXml(url, ReqBldr.ToString(), methodName, ref exep);
                try
                {
                    SaveResponse.SAVElOGFILE(querydata, "REQ", "TXT", "LCC", srchInput.HidTxtAirLine.Trim().ToUpper(), "SCRAP_CPN");
                    SaveResponse.SAVElOGFILE(result, "RES", "TXT", "LCC", srchInput.HidTxtAirLine.Trim().ToUpper(), "SCRAP_CPN");

                }
                catch (Exception EX)
                {
                    System.IO.File.AppendAllText("C:\\\\CPN_SP\\\\GetSeachRequest2" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", EX.StackTrace.ToString() + EX.Message + Environment.NewLine);
                }
            }
            catch (Exception EX)
            {
                System.IO.File.AppendAllText("C:\\\\CPN_SP\\\\GetSeachRequest3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", EX.StackTrace.ToString() + EX.Message + Environment.NewLine);
            }
            return result;

        }

        public string getXML(string[] res)
        {

            XElement identity = new XElement("identity");

            // Walk array of data
            for (int count = 0; count < res.Length; count++)
            {
                XElement elm = new XElement("id",
                  new XElement("name", res[count]),
                  new XElement("index", count));

                identity.Add(elm);
            }

            XElement xml = new XElement("xml", identity);
            return xml.ToString();
        }
        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        // public List<FlightSearchResults> ParseResult(string res, FlightSearch srchInput, string constr, string fareType, List<FlightCityList> CityList, List<AirlineList> Airlist, CredentialList crd, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, float srvCharge)
        public List<FlightSearchResults> ParseResult(string res, FlightSearch srchInput, string constr, string fareType, List<FlightCityList> CityList, List<AirlineList> Airlist, CredentialList crd, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, List<MISCCharges> MiscList)
        {


            //res = "1950;17560;4334;~1;8280;2167;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;N4TCT;9;NoClass;N4TCT;ADT;RP~1;8280;2167;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;N4TCT;9;NoClass;N4TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;N4TCT;9;NoClass;N4TCT;INF;RP@1;0;0240;EFT;6E;MCX;210517;0520;210517;0800;DEL;BLR;6E;6E;133;73G;Y;--Indigo#1950;17560;4334;~1;8280;2167;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;N4TCT;9;NoClass;N4TCT;ADT;RP~1;8280;2167;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;N4TCT;9;NoClass;N4TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;N4TCT;9;NoClass;N4TCT;INF;RP@1;0;0240;EFT;6E;MCX;210517;0620;210517;0900;DEL;BLR;6E;6E;458;73G;Y;--Indigo#1950;18514;4388;~1;8757;2194;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;MTCT;9;NoClass;MTCT;ADT;RP~1;8757;2194;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;MTCT;9;NoClass;MTCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;MTCT;9;NoClass;MTCT;INF;RP@1;0;0240;EFT;6E;MCX;210517;2040;210517;2320;DEL;BLR;6E;6E;131;73G;Y;--Indigo#1950;18514;4388;~1;8757;2194;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;MTCT;9;NoClass;MTCT;ADT;RP~1;8757;2194;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;MTCT;9;NoClass;MTCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;MTCT;9;NoClass;MTCT;INF;RP@1;0;0240;EFT;6E;MCX;210517;2240;220517;0120;DEL;BLR;6E;6E;977;73G;Y;--Indigo#1950;18514;4388;~1;8757;2194;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;MTCT;9;NoClass;MTCT;ADT;RP~1;8757;2194;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;MTCT;9;NoClass;MTCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;MTCT;9;NoClass;MTCT;INF;RP@1;0;0240;EFT;6E;MCX;210517;2345;220517;0225;DEL;BLR;6E;6E;4376;73G;Y;--Indigo()1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;0430;250517;0715;BLR;DEL;6E;6E;5154;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0240;EFT;6E;MCX;250517;0605;250517;0845;BLR;DEL;6E;6E;132;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0240;EFT;6E;MCX;250517;0705;250517;0945;BLR;DEL;6E;6E;6228;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;0835;250517;1120;BLR;DEL;6E;6E;838;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0305;EFT;6E;MCX;250517;0955;250517;1300;BLR;DEL;6E;6E;943;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;1300;250517;1545;BLR;DEL;6E;6E;246;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;1440;250517;1725;BLR;DEL;6E;6E;978;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;1700;250517;1945;BLR;DEL;6E;6E;3988;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;1800;250517;2045;BLR;DEL;6E;6E;783;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0250;EFT;6E;MCX;250517;1855;250517;2145;BLR;DEL;6E;6E;808;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0305;EFT;6E;MCX;250517;2000;250517;2305;BLR;DEL;6E;6E;6726;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0305;EFT;6E;MCX;250517;2100;260517;0005;BLR;DEL;6E;6E;305;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0300;EFT;6E;MCX;250517;2155;260517;0055;BLR;DEL;6E;6E;289;73G;Y;--Indigo#1950;15314;4608;~1;7157;2304;ADT;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;ADT;RP~1;7157;2304;CHD;1;REF;96;REFUNDABLE;LCC;975;LAST TKT DTE;INR;975;T8TCT;9;NoClass;T8TCT;CHD;RP~1;1000;0;INF;1;REF;96;REFUNDABLE;LCC;0;LAST TKT DTE;INR;0;T8TCT;9;NoClass;T8TCT;INF;RP@1;0;0245;EFT;6E;MCX;250517;2300;260517;0145;BLR;DEL;6E;6E;484;73G;Y;--Indigo";

            res = res + "$";
            string[] airSep = { "$" };
            string[] AirlinewiseResultlist = res.Split(airSep, StringSplitOptions.RemoveEmptyEntries);
            List<FlightSearchResults> MainlLIst = new List<FlightSearchResults>();
            for (int a = 0; a < AirlinewiseResultlist.Length; a++)
            {

                List<FlightSearchResults> AirWiseREsultLIst = new List<FlightSearchResults>();

                string[] InboundSep = { "()" };
                string[] OBIBList = AirlinewiseResultlist[a].Split(InboundSep, StringSplitOptions.RemoveEmptyEntries); // 0 index OBlist , 1 index flight IBlist

                List<FlightSearchResults> FsrListO = new List<FlightSearchResults>();
                List<FlightSearchResults> FsrListR = new List<FlightSearchResults>();

                for (int ob = 0; ob < OBIBList.Count(); ob++)
                {
                    //List<FlightSearchResults> SubResultLIst = new List<FlightSearchResults>();
                    string[] lineSep = { "#" };//{ "Y;--" };

                    string[] lineItems = OBIBList[ob].Split(lineSep, StringSplitOptions.RemoveEmptyEntries);  // Result Items

                    char[] FFSep = { '@' };
                    for (int l = 0; l < lineItems.Length; l++)
                    {

                        if (lineItems[l].Length > 20 && l % 2 == 0)
                        {
                            string[] flightFareList = lineItems[l].Split(FFSep, StringSplitOptions.RemoveEmptyEntries); // 0 index fare , 1 index flight info 

                            char[] stopSep = { '^' };
                            string[] fltlist = flightFareList[1].Split(stopSep, StringSplitOptions.RemoveEmptyEntries);






                            for (int fl = 0; fl < fltlist.Count(); fl++) // flight stop loop
                            {

                                FlightSearchResults fsr = new FlightSearchResults();
                                #region Flt Details
                                char[] Sep = { ';' };


                                string[] fltDtls = fltlist[fl].Split(Sep, StringSplitOptions.RemoveEmptyEntries);

                                if ((!fltlist[fl].Contains("MCX")) && fltDtls.Count() == 12)
                                {
                                    string aa = "1;1;1;1;1;1;" + fltlist[fl];
                                    fltDtls = aa.Split(Sep, StringSplitOptions.RemoveEmptyEntries);
                                }

                                //getXML(fltDtls);



                                fsr.depdatelcc = "20" + Utility.Right(fltDtls[6].Trim(), 2) + "-" + Utility.Mid(fltDtls[6].Trim(), 2, 2) + "-" + Utility.Left(fltDtls[6].Trim(), 2); ;
                                fsr.arrdatelcc = "20" + Utility.Right(fltDtls[8].Trim(), 2) + "-" + Utility.Mid(fltDtls[8].Trim(), 2, 2) + "-" + Utility.Left(fltDtls[8].Trim(), 2);
                                fsr.Departure_Date = Convert.ToDateTime(fsr.depdatelcc).ToString("dd MMM"); ;// legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                                fsr.Arrival_Date = Convert.ToDateTime(fsr.arrdatelcc).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                                fsr.DepartureDate = Convert.ToDateTime(fsr.depdatelcc).ToString("ddMMyy");
                                fsr.ArrivalDate = Convert.ToDateTime(fsr.arrdatelcc).ToString("ddMMyy");
                                //fsr.sno = lineItems[l + 1].Split('|')[1];
                                string strSno = "";
                                if (lineItems[l + 1].Split('|')[1].Contains("+"))
                                {
                                    strSno = lineItems[l + 1].Split('|')[1].Split('+')[fl];

                                    fsr.sno = strSno;
                                }
                                else
                                {
                                    fsr.sno = lineItems[l + 1].Split('|')[1];

                                }
                                fsr.Searchvalue = lineItems[l + 1].Split('|')[0];

                                fsr.FlightIdentification = fltDtls[14].Trim();
                                fsr.ValiDatingCarrier = fltDtls[12].Trim();

                                try
                                {
                                    fsr.AirLineName = Airlist.Where(x => x.AirlineCode.ToUpper().Trim() == fsr.ValiDatingCarrier.ToUpper().Trim()).ToList()[0].AlilineName;
                                }
                                catch { }



                                fsr.OperatingCarrier = fsr.ValiDatingCarrier;
                                fsr.MarketingCarrier = fltDtls[13].Trim();
                                //fsr.fareBasis = seg.Attribute("ResBookDesigCode").Value.Trim();



                                fsr.DepartureLocation = fltDtls[10].Trim();
                                fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, fsr.DepartureLocation);
                                fsr.DepartureTime = fltDtls[7].Trim();
                                fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, fsr.DepartureLocation);
                                //try { fsr.DepartureTerminal = seg.Element("DepartureAirport").Attribute("Terminal").Value.Trim(); }
                                //catch { }
                                fsr.DepAirportCode = fltDtls[10].Trim();



                                fsr.ArrivalLocation = fltDtls[11].Trim();
                                fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, fsr.ArrivalLocation);//fsr.ArrivalLocation;
                                fsr.ArrivalTime = fltDtls[9].Trim();
                                fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, fsr.ArrivalLocation);// fsr.ArrivalCityName;
                                fsr.Sector = fsr.DepartureLocation + ":" + fsr.ArrivalLocation;
                                //try { fsr.ArrivalTerminal = seg.Element("ArrivalAirport").Attribute("Terminal").Value.Trim(); }
                                //catch { }

                                fsr.ArrAirportCode = fsr.ArrivalLocation;
                                fsr.Trip = srchInput.Trip.ToString();
                                //fsr.EQ = seg.Element("Equipment").Attribute("AirEquipType").Value.Trim();
                                fsr.Provider = "SP";
                                //fsr.AdtFareType = xlFPricedItinerary.ElementAt(ptn).Attribute("FareType").Value.Trim();
                                fsr.TotDur = fltDtls[2].Trim();
                                fsr.Stops = (fltlist.Count() - 1) + "-Stop";
                                fsr.Leg = fl + 1;
                                fsr.LineNumber = l + 1;
                                if (srchInput.HidTxtAirLine.Trim().ToUpper() == "G8")
                                {
                                    fsr.sno = srchInput.HidTxtAirLine.Trim().ToUpper() + "~" + crd.UserID + "/" + crd.Password + "/" + crd.CorporateID;
                                }
                                //else
                                //{
                                //    fsr.sno = srchInput.HidTxtAirLine.Trim().ToUpper() + "~" + crd.UserID + "/" + crd.Password + "/" + crd.Password;
                                //}

                                #endregion

                                #region fare Details

                                string[] fareSep = { "~1;" };
                                string[] farelist = flightFareList[0].Split(fareSep, StringSplitOptions.RemoveEmptyEntries);
                                //fsr.Searchvalue = flightFareList[0];

                                for (int ft = 0; ft < farelist.Count(); ft++)
                                {

                                    string[] fareBrkSep = { ";" };
                                    string[] Paxfarelist = farelist[ft].Split(fareBrkSep, StringSplitOptions.RemoveEmptyEntries);

                                    getXML(Paxfarelist);
                                    if (ft == 0)
                                    {
                                        fsr.OriginalTF = float.Parse(Paxfarelist[1]);

                                    }

                                    if (farelist[ft].Contains("ADT;"))
                                    {
                                        fsr.Adult = Convert.ToInt32(Paxfarelist[3]);
                                        fsr.AdtFareType = Convert.ToString(Paxfarelist[6]);


                                        fsr.AdtBfare = float.Parse(Paxfarelist[0]) - float.Parse(Paxfarelist[1]);


                                        // //fsr.AdtCabin = objfrb.Element("Cabin").Value.Trim();
                                        fsr.AdtFSur = float.Parse(Paxfarelist[8]);
                                        fsr.AdtTax = float.Parse(Paxfarelist[1]);
                                        fsr.AdtOT = fsr.AdtTax - fsr.AdtFSur;
                                        fsr.AdtRbd = Paxfarelist[12];
                                        fsr.AdtFarebasis = Paxfarelist[15];
                                        fsr.AdtFare = float.Parse(Paxfarelist[0]);//fsr.AdtBfare + fsr.AdtTax;
                                        // if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                        // {
                                        //     fsr.AdtRbd = "";
                                        //     fsr.AdtFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                        // }
                                        // else
                                        // {
                                        //     fsr.AdtRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                        //     try
                                        //     {
                                        //         fsr.AdtFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();
                                        //     }
                                        //     catch { }

                                        // }

                                        fsr.FBPaxType = "ADT";

                                    }
                                    else if (farelist[ft].Contains("CHD;"))
                                    {
                                        fsr.ChdAvlStatus = "9";
                                        fsr.ChdBFare = float.Parse(Paxfarelist[0]) - float.Parse(Paxfarelist[1]);//float.Parse(Paxfarelist[0]);
                                        fsr.Child = Convert.ToInt32(Paxfarelist[3]);

                                        // //fsr.AdtCabin = objfrb.Element("Cabin").Value.Trim();
                                        fsr.ChdFSur = float.Parse(Paxfarelist[8]);
                                        fsr.ChdTax = float.Parse(Paxfarelist[1]);
                                        fsr.ChdOT = fsr.ChdTax - fsr.ChdFSur;
                                        fsr.ChdRbd = Paxfarelist[12];
                                        fsr.ChdFarebasis = Paxfarelist[15];

                                        fsr.ChdFare = float.Parse(Paxfarelist[0]);// fsr.ChdBFare + fsr.ChdTax;

                                    }
                                    else if (farelist[ft].Contains("INF;"))
                                    {
                                        fsr.InfAvlStatus = "9";
                                        fsr.InfBfare = float.Parse(Paxfarelist[0]) - float.Parse(Paxfarelist[1]);//float.Parse(Paxfarelist[0]);

                                        fsr.Infant = Convert.ToInt32(Paxfarelist[3]);
                                        // //fsr.AdtCabin = objfrb.Element("Cabin").Value.Trim();
                                        fsr.InfFSur = float.Parse(Paxfarelist[8]);
                                        fsr.InfTax = float.Parse(Paxfarelist[1]);
                                        fsr.InfOT = fsr.InfTax - fsr.InfFSur;
                                        fsr.InfRbd = Paxfarelist[12];
                                        fsr.InfFarebasis = Paxfarelist[15];
                                        fsr.InfFare = float.Parse(Paxfarelist[0]);// fsr.InfBfare + fsr.InfTax;

                                    }
                                }




                                #endregion

                                fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                                fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                                fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                                fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                                fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));




                                fsr.AdtAvlStatus = "9";
                                fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                                fsr.TotPax = fsr.Adult + fsr.Child;
                                fsr.AdtFar = "CPN";
                                fsr.SearchId = crd.CorporateID;
                                if (ob == 0)
                                {
                                    fsr.Flight = "1";
                                    if (fareType == "INB")
                                    {
                                        fsr.OrgDestFrom = srchInput.HidTxtDepCity.Split(',')[0];
                                        fsr.OrgDestTo = srchInput.HidTxtArrCity.Split(',')[0];
                                    }
                                    else
                                    {
                                        fsr.OrgDestFrom = srchInput.HidTxtArrCity.Split(',')[0];
                                        fsr.OrgDestTo = srchInput.HidTxtDepCity.Split(',')[0];
                                    }
                                    fsr.TripType = TripType.O.ToString();

                                    FsrListO.Add(fsr);

                                }
                                else
                                {
                                    fsr.Flight = "2";

                                    fsr.OrgDestFrom = srchInput.HidTxtArrCity.Split(',')[0];
                                    fsr.OrgDestTo = srchInput.HidTxtDepCity.Split(',')[0];
                                    fsr.TripType = TripType.R.ToString();
                                    FsrListR.Add(fsr);
                                }



                            }

                        }


                    }


                    if (ob == 0)
                    {
                        AirWiseREsultLIst.AddRange(FsrListO);
                    }
                }

                MainlLIst.AddRange(AirWiseREsultLIst);
            }


            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            //MainlLIst = GetGoAirResultListWithMarkup(MainlLIst, srchInput, SrvchargeList, MarkupDs, srvCharge, constr);//MiscList
            MainlLIst = GetGoAirResultListWithMarkup(MainlLIst, srchInput, SrvchargeList, MarkupDs, MiscList, constr);

            return objFltComm.AddFlightKey(MainlLIst, false);

        }



        //public List<FlightSearchResults> GetGoAirResultListWithMarkup(List<FlightSearchResults> inputList, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, float srvCharge, string ConnStr)
        public List<FlightSearchResults> GetGoAirResultListWithMarkup(List<FlightSearchResults> inputList, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, string ConnStr)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<FlightSearchResults> newList = new List<FlightSearchResults>();
            //ArrayList test = (ArrayList)inputList[0];
            ////SMS charge calc
            //float srvCharge = 0;
            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), "G8", searchInputs.UID);
            ////SMS charge calc end
            List<BaggageList> BagInfoList = new List<BaggageList>();
            // BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8");
            for (int i = 0; i < inputList.Count; i++)
            {

                //newList.Add(GoAirGetNewObject((FlightSearchResults)inputList[i], searchInputs, SrvChargeList, MarkupDs, srvCharge, objFltComm));
                newList.Add(GoAirGetNewObject((FlightSearchResults)inputList[i], searchInputs, SrvChargeList, MarkupDs, MiscList, objFltComm));
            }

            return newList;
        }
        //public FlightSearchResults GoAirGetNewObject(FlightSearchResults fsr, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, float srvCharge, FlightCommonBAL objFltComm)
        public FlightSearchResults GoAirGetNewObject(FlightSearchResults fsr, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, List<MISCCharges> MiscList, FlightCommonBAL objFltComm)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            var objNew = (FlightSearchResults)fsr.Clone();
            int TotalViaSector = 1;


            #region Fare Calculation
            float srvCharge = 0;
            float srvChargeAdt = 0;
            float srvChargeChd = 0;
            #region Adult
            if (objNew.Adult > 0)
            {
                //objNew.AdtOT = objNew.AdtTax - objNew.AdtFSur;
                //SMS charge add 

                #region Get MISC Markup Charges Date 06-03-2018
                try
                {
                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, objNew.ValiDatingCarrier, "CPN", Convert.ToString(objNew.AdtBfare), Convert.ToString(objNew.AdtFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch { srvChargeAdt = 0; }
                #endregion
                objNew.AdtOT = objNew.AdtOT + srvChargeAdt;// srvCharge;
                objNew.AdtTax = objNew.AdtTax + srvChargeAdt;//srvCharge;
                objNew.AdtFare = objNew.AdtFare + srvChargeAdt;//srvCharge;

                objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());
                objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());

                objNew.AdtDiscount1 = 0;  //-AdtComm  N               
                objNew.AdtSrvTax1 = 0;// added TO TABLE N               
                objNew.AdtEduCess = 0;  //N
                objNew.AdtHighEduCess = 0; //N
                #region Get Commission
                try
                {
                    CommDt.Clear();
                    STTFTDS.Clear();
                    #region Show hide Net Fare and Commission Calculation-Adult Pax
                    if (objNew.Leg == 1)
                    {
                        CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, objNew.AdtRbd, objNew.AdtCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "CPN", Convert.ToString(TotalViaSector));
                        if (CommDt != null && CommDt.Rows.Count > 0)
                        {
                            //CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, objNew.AdtRbd, objNew.AdtCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.fareBasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "CPN", Convert.ToString(TotalViaSector));
                            objNew.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            objNew.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.AdtDiscount1, objNew.AdtBfare, objNew.AdtFSur, searchInputs.TDS);
                            objNew.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            objNew.AdtDiscount = objNew.AdtDiscount1 - objNew.AdtSrvTax1;
                            objNew.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            objNew.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            objNew.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());

                            //objNew.AdtDiscount = 0;
                            //objNew.AdtTF = 0;
                            //objNew.AdtTds = 0;
                            //objNew.IATAComm = 0;

                            //objNew.AdtDiscount1 = 0;  //-AdtComm  
                            //objNew.AdtCB = 0;
                            //objNew.AdtSrvTax1 = 0;// added TO TABLE
                            //objNew.AdtSrvTax = 0;
                            //objNew.AdtDiscount = 0;
                            //objNew.AdtEduCess = 0;
                            //objNew.AdtHighEduCess = 0;
                        }
                        else
                        {
                            objNew.AdtDiscount = 0;
                            objNew.AdtTF = 0;
                            objNew.AdtTds = 0;
                            objNew.IATAComm = 0;

                            objNew.AdtDiscount1 = 0;  //-AdtComm  
                            objNew.AdtCB = 0;
                            objNew.AdtSrvTax1 = 0;// added TO TABLE
                            objNew.AdtSrvTax = 0;
                            objNew.AdtDiscount = 0;
                            objNew.AdtEduCess = 0;
                            objNew.AdtHighEduCess = 0;

                        }

                    }
                    else
                    {
                        objNew.AdtDiscount = 0;
                        objNew.AdtTF = 0;
                        objNew.AdtTds = 0;
                        objNew.IATAComm = 0;

                        objNew.AdtDiscount1 = 0;  //-AdtComm  
                        objNew.AdtCB = 0;
                        objNew.AdtSrvTax1 = 0;// added TO TABLE
                        objNew.AdtSrvTax = 0;
                        objNew.AdtDiscount = 0;
                        objNew.AdtEduCess = 0;
                        objNew.AdtHighEduCess = 0;

                    }
                    #endregion


                }
                catch (Exception ex)
                {
                    objNew.AdtDiscount = 0;
                    objNew.AdtTF = 0;
                    objNew.AdtTds = 0;
                    objNew.IATAComm = 0;

                    objNew.AdtDiscount1 = 0;  //-AdtComm  
                    objNew.AdtCB = 0;
                    objNew.AdtSrvTax1 = 0;// added TO TABLE
                    objNew.AdtSrvTax = 0;
                    objNew.AdtDiscount = 0;
                    objNew.AdtEduCess = 0;
                    objNew.AdtHighEduCess = 0;

                }
                #endregion






                //SMS charge add end
                //if (!string.IsNullOrEmpty(objNew.AdtFar) && objNew.AdtFar.Trim().ToUpper()=="N")
                //{
                //objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());
                //objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());

                //objNew.AdtDiscount1 = 0;  //-AdtComm  
                //objNew.AdtCB = 0;
                //objNew.AdtSrvTax1 = 0;// added TO TABLE
                //objNew.AdtDiscount = 0;
                //objNew.AdtEduCess = 0;
                //objNew.AdtHighEduCess = 0;
                //objNew.AdtTF = 0;
                //objNew.AdtTds = 0;
                //}
                //else
                //{
                //    objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier, fsr.AdtFare, searchInputs.Trip.ToString());
                //    objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, fsr.AdtFare, searchInputs.Trip.ToString());

                //    CommDt.Clear();
                //    CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, objNew.AdtRbd, objNew.AdtCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo);
                //    objNew.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                //    objNew.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                //    STTFTDS.Clear();
                //    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.AdtDiscount1, objNew.AdtBfare, objNew.AdtFSur, searchInputs.TDS);
                //    objNew.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                //    objNew.AdtDiscount = objNew.AdtDiscount1 - objNew.AdtSrvTax1;
                //    objNew.AdtEduCess = 0;
                //    objNew.AdtHighEduCess = 0;
                //    objNew.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                //    objNew.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                //}

                //if (searchInputs.IsCorp == true)
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.AdtFare.ToString()));
                //        objNew.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        objNew.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }
            #endregion

            #region Child
            if (objNew.Child > 0)
            {
                // objNew.ChdOT = objNew.ChdTax - objNew.ChdFSur;
                //SMS charge add 

                #region Get MISC Markup Charges Date 06-03-2018
                try
                {
                    srvChargeChd = objFltComm.MISCServiceFee(MiscList, objNew.ValiDatingCarrier, "CPN", Convert.ToString(objNew.ChdBFare), Convert.ToString(objNew.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                }
                catch { srvChargeChd = 0; }
                #endregion


                objNew.ChdOT = objNew.ChdOT + srvChargeChd;//srvCharge;
                objNew.ChdTax = objNew.ChdTax + srvChargeChd;//srvCharge;
                objNew.ChdFare = objNew.ChdFare + srvChargeChd;//srvCharge;


                objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.ChdFare, objNew.Trip.ToString());
                objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());

                objNew.ChdDiscount1 = 0;  //-AdtComm  N               
                objNew.ChdSrvTax1 = 0;// added TO TABLE N               
                objNew.ChdEduCess = 0;  //N
                objNew.ChdHighEduCess = 0; //N

                #region  Get Child Commission
                try
                {
                    CommDt.Clear();
                    STTFTDS.Clear();
                    #region Show hide Net Fare and Commission Calculation-Child Pax
                    if (objNew.Leg == 1)
                    {
                        CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, objNew.ChdRbd, objNew.ChdCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "CPN", Convert.ToString(TotalViaSector));
                        if (CommDt != null && CommDt.Rows.Count > 0)
                        {
                            //CommDt = objFltComm.GetFltComm_WithouDB(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, objNew.ChdRbd, objNew.ChdCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "CPN", Convert.ToString(TotalViaSector));
                            objNew.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            objNew.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.ChdDiscount1, objNew.ChdBFare, objNew.ChdFSur, searchInputs.TDS);
                            objNew.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            objNew.ChdDiscount = objNew.ChdDiscount1 - objNew.ChdSrvTax1;
                            objNew.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                            objNew.ChdTds = float.Parse(STTFTDS["Tds"].ToString());


                            //objNew.ChdDiscount = 0;
                            //objNew.ChdCB = 0;
                            //objNew.ChdSrvTax = 0;
                            //objNew.ChdTF = 0;
                            //objNew.ChdTds = 0;

                            //objNew.ChdDiscount1 = 0;  //-AdtComm  N
                            //objNew.ChdCB = 0;
                            //objNew.ChdSrvTax1 = 0;// added TO TABLE N
                            //objNew.ChdDiscount = 0;
                            //objNew.ChdEduCess = 0;  //N
                            //objNew.ChdHighEduCess = 0; //N
                            //objNew.ChdTF = 0;
                            //objNew.ChdTds = 0;
                        }
                        else
                        {

                        }

                    }
                    else
                    {

                    }
                    #endregion

                    
                }
                catch (Exception ex)
                {
                    objNew.ChdDiscount = 0;
                    objNew.ChdCB = 0;
                    objNew.ChdSrvTax = 0;
                    objNew.ChdTF = 0;
                    objNew.ChdTds = 0;

                    objNew.ChdDiscount1 = 0;  //-AdtComm  N
                    objNew.ChdCB = 0;
                    objNew.ChdSrvTax1 = 0;// added TO TABLE N
                    objNew.ChdDiscount = 0;
                    objNew.ChdEduCess = 0;  //N
                    objNew.ChdHighEduCess = 0; //N
                    objNew.ChdTF = 0;
                    objNew.ChdTds = 0;
                }

                #endregion

                //objNew.ChdDiscount1 = 0;  //-AdtComm  
                //objNew.ChdCB = 0;
                //objNew.ChdSrvTax1 = 0;// added TO TABLE
                //objNew.ChdDiscount = 0;
                //objNew.ChdEduCess = 0;
                //objNew.ChdHighEduCess = 0;
                //objNew.ChdTF = 0;
                //objNew.ChdTds = 0;

                //SMS charge add end
                //if (!string.IsNullOrEmpty(objNew.AdtFar) && objNew.AdtFar.Trim().ToUpper()=="N")
                //{
                //    objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.ChdFare, objNew.Trip.ToString());
                //    objNew.ChdDiscount1 = 0;  //-AdtComm  
                //    objNew.ChdCB = 0;
                //    objNew.ChdSrvTax1 = 0;// added TO TABLE
                //    objNew.ChdDiscount = 0;
                //    objNew.ChdEduCess = 0;
                //    objNew.ChdHighEduCess = 0;
                //    objNew.ChdTF = 0;
                //    objNew.ChdTds = 0;
                //}
                //else
                //{
                //    objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier, objNew.ChdFare, objNew.Trip.ToString());

                //    CommDt.Clear();
                //    CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, objNew.ChdRbd, objNew.ChdCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo);
                //    objNew.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                //    objNew.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                //    STTFTDS.Clear();
                //    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.ChdDiscount1, objNew.ChdBFare, objNew.ChdFSur, searchInputs.TDS);
                //    objNew.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                //    objNew.ChdDiscount = objNew.ChdDiscount1 - objNew.ChdSrvTax1;
                //    objNew.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                //    objNew.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                //    objNew.ChdEduCess = 0;
                //    objNew.ChdHighEduCess = 0;
                //}
                //if (searchInputs.IsCorp == true)
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.ChdFare.ToString()));
                //        objNew.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        objNew.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }
            #endregion

            #region Infant
            if (objNew.Infant > 0)
            {
                // objNew.InfOT = objNew.InfTax - objNew.InfFSur;
                //if (searchInputs.IsCorp == true)
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.InfBfare.ToString()), decimal.Parse(objNew.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.InfFare.ToString()));
                //        objNew.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        objNew.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }

            #endregion

            objNew.STax = (objNew.AdtSrvTax * objNew.Adult) + (objNew.ChdSrvTax * objNew.Child) + (objNew.InfSrvTax * objNew.Infant);
            objNew.TFee = (objNew.AdtTF * objNew.Adult) + (objNew.ChdTF * objNew.Child);// +(objlist.InfTF * objlist.Infant);
            objNew.TotDis = (objNew.AdtDiscount * objNew.Adult) + (objNew.ChdDiscount * objNew.Child);
            objNew.TotCB = (objNew.AdtCB * objNew.Adult) + (objNew.ChdCB * objNew.Child);
            objNew.TotTds = (objNew.AdtTds * objNew.Adult) + (objNew.ChdTds * objNew.Child);// +objFS.InfTds;
            objNew.TotMrkUp = (objNew.ADTAdminMrk * objNew.Adult) + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAdminMrk * objNew.Child) + (objNew.CHDAgentMrk * objNew.Child);
            objNew.TotMgtFee = (objNew.AdtMgtFee * objNew.Adult) + (objNew.ChdMgtFee * objNew.Child) + (objNew.InfMgtFee * objNew.Infant);
            //objNew.TotalTax = objNew.TotalTax + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.TotalTax = objNew.TotalTax + (srvChargeAdt * objNew.Adult) + (srvChargeChd * objNew.Child);
            //objNew.TotalFare = objNew.TotalFare + objNew.TotMrkUp + objNew.STax + objNew.TFee + objNew.TotMgtFee + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.TotalFare = objNew.TotalFare + objNew.TotMrkUp + objNew.STax + objNew.TFee + objNew.TotMgtFee + (srvChargeAdt * objNew.Adult) + (srvChargeChd * objNew.Child);
            objNew.NetFare = (objNew.TotalFare + objNew.TotTds) - (objNew.TotDis + objNew.TotCB + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAgentMrk * objNew.Child));
            srvCharge = srvChargeAdt + srvChargeChd;
            objNew.OriginalTT = srvCharge;
            #endregion


            return objNew;



        }


        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;            
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                try
                {
                    List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
                    if (StNew.Count > 0)
                    {
                        STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                        TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                        IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                    }
                }
                catch
                { }

                //STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                //TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                //IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }
                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
    }
}
