﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace STD.BAL
{
    public class ScrapBookBAL
    {
        public string ScrapFightBook(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, ref ArrayList TktNoArray, string Constr, HttpContext ctx)
        {
            string exep = "";
            string queryData = "";
            string lcccredentials = Convert.ToString(FltDT.Rows[0]["sno"]).Trim();
            string userData = "";
            string agencyData = "";
            string miscData = "";
            string exchangeRate = "AED-18.83,SGD-47.00,THB-1.84,USD-65.04,$-65.04,SAR-17.32,NPR-0.620,CNY-10.15,LKR-0.47,OMR-168.29";
            string paxlistString = "";
            FlightCommonBAL objcommBal = new FlightCommonBAL(Constr);
           
            string orderID=Convert.ToString(FltDT.Rows[0]["Track_id"]);
            Random obj = new Random();
            string PNR = "SP" + obj.Next(999)+"-FQ";
            string response="";

            try
            {


                List<string> paxArray = new List<string>();
                //DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                //DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                //DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");

                char[] sep = { ';' };
                string[] values = Convert.ToString(FltDT.Rows[0]["Searchvalue"]).Split(sep, StringSplitOptions.RemoveEmptyEntries);

                string fltNo = "";

                for (int i = 0; i < FltDT.Rows.Count; i++)
                {
                    fltNo += Convert.ToString(FltDT.Rows[0]["FlightIdentification"]) + "-";

                }
                //8183,914^SG,152-189-BOM,DEL,150116,0730,150116,0950,22121515392ADTS,
                queryData = values[1] + "," + values[2] + ",^" + VC + "," + fltNo + "," + Convert.ToString(FltDT.Rows[0]["DepartureLocation"]) + "," + Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["ArrivalLocation"]) + ","
                             + Convert.ToString(FltDT.Rows[0]["DepartureDate"]) + "," + Convert.ToString(FltDT.Rows[0]["DepartureTime"]) + "," + Convert.ToString(FltDT.Rows[0]["ArrivalDate"]) + "," + Convert.ToString(FltDT.Rows[0]["ArrivalTime"]) + "," + "";

                // Mrs.*MRIDULA*GUPTA*9821162776


                for (int p = 0; p < PaxDs.Tables[0].Rows.Count; p++)
                {
                    string pax = Convert.ToString(PaxDs.Tables[0].Rows[p]["Title"]) + ".*" + Convert.ToString(PaxDs.Tables[0].Rows[p]["FName"]) + "*" + Convert.ToString(PaxDs.Tables[0].Rows[p]["LName"]) + "*" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]);
                    paxArray.Add(pax);

                    paxlistString += pax + "~";
                }

                //Mr NetraDalvi~ BON BON LANE, OFF J P ROAD ~ MAHARASHTRA ~ MAHARASHTRA ~ 0 ~ 400053 ~abc@xyz.com~9821162776
                userData = Convert.ToString(PaxDs.Tables[0].Rows[0]["Title"]) + " " + Convert.ToString(PaxDs.Tables[0].Rows[0]["FName"]) + " " + Convert.ToString(PaxDs.Tables[0].Rows[0]["LName"]) + "~" + ConfigurationManager.AppSettings["companyaddress1"] + "~" + ConfigurationManager.AppSettings["companycity"] + "~" + ConfigurationManager.AppSettings["companystate"] + "~0~" + ConfigurationManager.AppSettings["companyzip"] + "~" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "~" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]);

                //Agency Name~AgencyName~V 4,Karkardooma~Mumbai~MH~India~400003~agencyemail~+912240748400
                agencyData = ConfigurationManager.AppSettings["companyname"] + "~" + ConfigurationManager.AppSettings["companyname"] + "~" + ConfigurationManager.AppSettings["companyaddress1"] + "~" + ConfigurationManager.AppSettings["companycity"] + "~" + ConfigurationManager.AppSettings["companycity"] + "~India~" + ConfigurationManager.AppSettings["companyzip"] + "~" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "~" + Convert.ToString(ConfigurationManager.AppSettings["companyphone"]);

                string departdate = Convert.ToDateTime("20"+Utility.Right(FltDT.Rows[0]["DepartureDate"].ToString(), 2) + "-" + Utility.Mid(FltDT.Rows[0]["DepartureDate"].ToString(), 2, 2) + "-" + Utility.Left(FltDT.Rows[0]["DepartureDate"].ToString(), 2)).ToString("dd-MMM-yyyy").Replace("/","-"); ;
                //1,0,0,JSAVER,BOM,IXB,15-Jan-2016,,Mumbai,Bagdogra~EFT-LCC
                miscData = Convert.ToString(FltDT.Rows[0]["Adult"]) + "," + Convert.ToString(FltDT.Rows[0]["Child"]) + "," + Convert.ToString(FltDT.Rows[0]["Infant"]) + "," + Convert.ToString(FltDT.Rows[0]["AdtFarebasis"]) + "," + Convert.ToString(FltDT.Rows[0]["DepartureLocation"]) + "," + Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["ArrivalLocation"]) + "," + departdate + ",," + Convert.ToString(FltDT.Rows[0]["DepartureLocation"]) + "," + Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["ArrivalLocation"]) + "~EFT-LCC";

                scrap.cb.GetData objGD = new scrap.cb.GetData(ctx);

               // GetData objGD = new GetData();
                
            
                if (VC == "G8")
                {
                response=   objGD.GoAirPNR(queryData, lcccredentials, "", paxArray.ToArray(), userData, agencyData, miscData, "", exchangeRate);
                    
                }
                else if (VC == "SG")
                {
                    response = objGD.SpicePNR(queryData, lcccredentials, "", paxArray.ToArray(), userData, agencyData, miscData, "", exchangeRate);

                }
                else if (VC == "6E")
                {
                    response = objGD.IndigoPNR(queryData, lcccredentials, "", paxArray.ToArray(), userData, agencyData, miscData, "", exchangeRate);
                    PNR = response;
                }
                else
                {
                    response = exep += "different airline.";
                }


            }
            catch (Exception ex)
            {

                exep += ex.Message + " stack Trace:" + ex.StackTrace;
            }

            finally
            {
               
                string request = "<root><queryData>" + queryData + "</queryData><lcccredentials>" + lcccredentials + "</lcccredentials>";
                request += "<userData>" + userData + "</userData><agencyData>" + agencyData + "</agencyData><miscData>" + miscData + "</miscData><paxlist>" + paxlistString + "</paxlist></root>";

                objcommBal.InserScrapBookingLog(orderID, VC, PNR, request, response, exep);


            }


            return PNR;
        }
    }
}
