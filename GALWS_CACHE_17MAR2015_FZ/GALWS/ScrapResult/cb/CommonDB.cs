﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Net.Mail;
using Microsoft.VisualBasic;
using System.Threading;


namespace scrap.cb
{
    public class CommonDB
    {
        // Methods

        public static SqlDataReader DataReaderAny(string ConnectionServer, string ConnectionString, string sSQL)
        {

            SqlConnection connection = OpenConnAny(ConnectionServer, ConnectionString);
            SqlCommand command = new SqlCommand(sSQL, connection);
            SqlDataReader reader = null;
            reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            command.Dispose();
            return reader;
        }



        public static SqlConnection GetConnection(string DBName)
        {
            SqlConnection connection = null;
            if (connection != null)
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                return connection;
            }
            if (DBName == "")
            {
                connection = new SqlConnection("Data Source=" + CommonFunction.DecryptData(ConfigurationManager.ConnectionStrings["SQLDB12"].ToString()));
            }
            else
            {
                connection = new SqlConnection("Data Source=" + CommonFunction.DecryptData(ConfigurationManager.ConnectionStrings[DBName].ToString()));
            }
            connection.Open();
            return connection;
        }











        public static string GetValue(SqlDataReader rs, string strFieldName)
        {
            if (rs != null)
            {
                if (strFieldName.Length == 0)
                {
                    return "";
                }
                if (!rs.IsDBNull(0))
                {
                    string str2 = rs[strFieldName].ToString();
                    if (str2.Length == 0)
                    {
                        str2 = "";
                    }
                    return str2;
                }
            }
            return "";
        }



        public static SqlConnection OpenConnAny(string ConnectionServer, string ConnectionString)
        {
            SqlConnection connection = new SqlConnection("Server=" + ConnectionServer + ";" + ConnectionString);
            try
            {
                connection.Open();
            }
            catch (Exception)
            {
            }
            return connection;
        }








    }
}