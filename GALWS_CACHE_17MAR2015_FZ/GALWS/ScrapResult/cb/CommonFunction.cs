﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Net.Mail;
using Microsoft.VisualBasic;
using System.Threading;
using System.Runtime.Serialization.Json;
using System.Xml.Linq;

namespace scrap.cb
{
    public class CommonFunction
    {
        // Fields
        public static byte[] IV;
        private static bool customXertificateValidation(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            return (cert.Subject == cert.Subject);
        }

        public static XmlDocument GoAirParse(string json)
        {
            XmlDocument document = new XmlDocument();
            using (XmlDictionaryReader reader = JsonReaderWriterFactory.CreateJsonReader(Encoding.UTF8.GetBytes(json), XmlDictionaryReaderQuotas.Max))
            {
                document.LoadXml(XElement.Load(reader).ToString());
            }
            return document;
        }




        // Methods
        // static CommonFunction();
        // public CommonFunction();

        public static string AirasiaParse(string SearchParam, string Html, CookieContainer cookies, string ExchangeRates)
        {
            string str = "";
            string str2 = "";
            try
            {
                HtmlDocument document = new HtmlDocument
                {
                    OptionFixNestedTags = false
                };
                document.LoadHtml(Html);
                if (document != null)
                {
                    HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//table[@class='table avail-table']");
                    Hashtable[] flttable = new Hashtable[nodes.Count];
                    Hashtable[] faretable = new Hashtable[nodes.Count];
                    int index = 0;
                    foreach (HtmlNode node in (IEnumerable<HtmlNode>)nodes)
                    {
                        flttable[index] = new Hashtable();
                        faretable[index] = new Hashtable();
                        string str3 = "";
                        HtmlDocument document2 = new HtmlDocument
                        {
                            OptionFixNestedTags = false
                        };
                        document2.LoadHtml(node.InnerHtml);
                        foreach (HtmlNode node2 in (IEnumerable<HtmlNode>)document2.DocumentNode.SelectNodes("//tr[@class='fare-light-row'] | //tr[@class='fare-dark-row']"))
                        {
                            HtmlDocument document3 = new HtmlDocument
                            {
                                OptionFixNestedTags = false
                            };
                            document3.LoadHtml(node2.InnerHtml);
                            HtmlNodeCollection nodes3 = document3.DocumentNode.SelectNodes("//*[@type='radio']");
                            if (nodes3 != null)
                            {
                                foreach (HtmlNode node3 in (IEnumerable<HtmlNode>)nodes3)
                                {
                                    if (node3 != null)
                                    {
                                        if (str == "")
                                        {
                                            str = node3.Attributes["value"].Value;
                                        }
                                        else
                                        {
                                            str = str + "#" + node3.Attributes["value"].Value;
                                        }
                                    }
                                }
                            }
                            if (str != "")
                            {
                                string[] strArray = str.Split(new char[] { '#' })[0].Split(new char[] { '|' })[1].Split(new char[] { '^' });
                                string str4 = "";
                                string str5 = "";
                                foreach (string str6 in strArray)
                                {
                                    if (str6.Length > 0)
                                    {
                                        string[] strArray2 = str6.Split(new char[] { '~' });
                                        string str7 = strArray2[0] + " " + strArray2[1];
                                        string str8 = strArray2[4];
                                        string str9 = strArray2[6];
                                        string str10 = ConvertDate(strArray2[5]);
                                        string str11 = ConvertTime(strArray2[5]);
                                        string str12 = ConvertDate(strArray2[7]);
                                        string str13 = ConvertTime(strArray2[7]);
                                        if (str3 == "")
                                        {
                                            str3 = str7;
                                        }
                                        else
                                        {
                                            str3 = str3 + "," + str7;
                                        }
                                        if (str4 == "")
                                        {
                                            str4 = str7 + "~" + str8 + "~" + str9 + "~" + str10 + "~" + str11 + "~" + str12 + "~" + str13;
                                        }
                                        else
                                        {
                                            string str22 = str4;
                                            str4 = str22 + "^" + str7 + "~" + str8 + "~" + str9 + "~" + str10 + "~" + str11 + "~" + str12 + "~" + str13;
                                        }
                                    }
                                }
                                foreach (string str14 in str.Split(new char[] { '#' }))
                                {
                                    string loginUrl = "https://booking.airasia.com/";
                                    string refurl = "https://booking.airasia.com/Flight/Select";
                                    if (str5 == "")
                                    {
                                        string postURL = "https://booking.airasia.com/Flight/PriceItinerary?SellKeys%5B%5D=" + str14;
                                        string fareString = GetHtmlUsingCookies(loginUrl, postURL, refurl, cookies);
                                        str5 = FareAirParse(fareString, ExchangeRates) + "~" + str14.Split(new char[] { '~' })[1] + "~" + str14.Split(new char[] { '~' })[3] + "~";
                                    }
                                    else
                                    {
                                        string str19 = "https://booking.airasia.com/Flight/PriceItinerary?SellKeys%5B%5D=" + str14;
                                        string str20 = GetHtmlUsingCookies(loginUrl, str19, refurl, cookies);
                                        string str23 = str5;
                                        str5 = str23 + "^" + FareAirParse(str20, ExchangeRates) + "~" + str14.Split(new char[] { '~' })[1] + "~" + str14.Split(new char[] { '~' })[3] + "~";
                                    }
                                }
                                flttable[index][str3] = str4;
                                faretable[index][str3] = str5;
                                str4 = "";
                                str5 = "";
                                str = "";
                                str3 = "";
                            }
                        }
                        index++;
                    }
                    str2 = new ParsingClass().ParsingAirAsia(SearchParam, flttable, faretable);
                }
                return str2;
            }
            catch (Exception exception)
            {
                return ("Error Message:" + exception.Message);
            }
        }
        public static string PegasusParse(string SeachParam, string Html, CookieContainer cookies)
        {
            string innerText = "";
            string item = "";
            string str3 = "";
            string str4 = "";
            HtmlDocument document = new HtmlDocument
            {
                OptionFixNestedTags = true
            };
            document.LoadHtml(Html);
            if (document != null)
            {
                HtmlNode node = document.DocumentNode.SelectSingleNode("//form[@method='post']");
                if ((node != null) && (str3 == ""))
                {
                    str3 = node.Attributes["action"].Value.Replace("avail.aspx?", "").Replace("amp;", "");
                }
            }
            decimal num3 = 0M;
            decimal num4 = 0M;
            decimal num5 = 0M;
            decimal num6 = 0M;
            decimal num7 = 0M;
            decimal num8 = 0M;
            decimal num9 = 0M;
            decimal num10 = 0M;
            decimal num11 = 0M;
            decimal num12 = 0M;
            decimal num13 = 0M;
            decimal num14 = 0M;
            string str5 = "";
            string str7 = "";
            string str8 = "";
            string str9 = "";
            string str10 = "";
            string str11 = "";
            string str12 = "";
            string str13 = "";
            string str14 = "";
            string str16 = "";
            string str17 = "";
            string str18 = "";
            string str19 = "";
            string str20 = "";
            string str21 = "";
            string str22 = "";
            string str23 = "";
            string[] strArray = new string[5];
            string[] strArray2 = new string[5];
            string[] strArray3 = new string[13];
            string[] strArray4 = new string[4];
            int index = 0;
            try
            {
                HtmlDocument document2 = new HtmlDocument();
                HtmlDocument document3 = new HtmlDocument();
                HtmlDocument document4 = new HtmlDocument();
                document2.OptionFixNestedTags = true;
                document3.OptionFixNestedTags = true;
                document4.OptionFixNestedTags = true;
                document2.LoadHtml(Html);
                string innerHtml = document2.DocumentNode.SelectSingleNode("//td[@class='Booking_main-txt']").InnerHtml;
                document3.LoadHtml(innerHtml);
                document3.LoadHtml(innerHtml);
                foreach (HtmlNode node3 in (IEnumerable<HtmlNode>)document3.DocumentNode.SelectNodes("//span[@class='Availability_whitenormal_height']"))
                {
                    strArray4[index] = node3.InnerText.Replace("\r", "").Replace("\n", "").Replace("&nbsp", "").Replace("\t", "").Replace(";", "");
                    index++;
                }
                if (index == 1)
                {
                    index++;
                }
                HtmlNodeCollection nodes2 = document3.DocumentNode.SelectNodes("//table[@class='Availability_Table1_fare-frame']");
                Hashtable[] flttable = new Hashtable[nodes2.Count];
                Hashtable[] faretable = new Hashtable[nodes2.Count];
                int num16 = 0;
                foreach (HtmlNode node4 in (IEnumerable<HtmlNode>)nodes2)
                {
                    flttable[num16] = new Hashtable();
                    faretable[num16] = new Hashtable();
                    string html = node4.InnerHtml;
                    document2.LoadHtml(html);
                    HtmlNodeCollection nodes3 = document2.DocumentNode.SelectNodes("//tr[@class='Availability_fare-cell']");
                    int num17 = 0;
                    foreach (HtmlNode node5 in (IEnumerable<HtmlNode>)nodes3)
                    {
                        if (node5.ChildNodes.Count == 0x17)
                        {
                            int num18 = 0;
                            string str26 = node5.InnerHtml;
                            document4.LoadHtml(str26);
                            foreach (HtmlNode node6 in (IEnumerable<HtmlNode>)document4.DocumentNode.SelectNodes("//td[@class='Availability_fare-cell']"))
                            {
                                strArray3[num18] = node6.InnerText.Replace("\r", "").Replace("\t", "").Replace("\n", "").Replace("&nbsp", "").Replace("&thinsp;", "");
                                num18++;
                                if (node6.InnerHtml.Contains("Connecting Flights"))
                                {
                                    List<string> list = new List<string>();
                                    HtmlDocument document5 = new HtmlDocument
                                    {
                                        OptionFixNestedTags = false
                                    };
                                    document5.LoadHtml(node6.InnerHtml);
                                    if (document5 != null)
                                    {
                                        HtmlNode node7 = document5.DocumentNode.SelectSingleNode("//table[@rules='all']");
                                        if (node7 != null)
                                        {
                                            HtmlDocument document6 = new HtmlDocument
                                            {
                                                OptionFixNestedTags = false
                                            };
                                            document6.LoadHtml(node7.InnerHtml);
                                            if (document6 != null)
                                            {
                                                foreach (HtmlNode node8 in (IEnumerable<HtmlNode>)document6.DocumentNode.SelectNodes("//tr"))
                                                {
                                                    HtmlDocument document7 = new HtmlDocument
                                                    {
                                                        OptionFixNestedTags = true
                                                    };
                                                    document7.LoadHtml(node8.InnerHtml);
                                                    if (document7 != null)
                                                    {
                                                        foreach (HtmlNode node9 in (IEnumerable<HtmlNode>)document7.DocumentNode.SelectNodes("//td"))
                                                        {
                                                            if (node9 != null)
                                                            {
                                                                innerText = node9.InnerText;
                                                                item = item + "~" + innerText;
                                                            }
                                                        }
                                                    }
                                                    list.Add(item);
                                                    item = null;
                                                }
                                            }
                                        }
                                        if (list.Count > 1)
                                        {
                                            strArray = list[1].Split(new char[] { '~' });
                                            strArray2 = list[2].Split(new char[] { '~' });
                                        }
                                        list.Clear();
                                    }
                                }
                            }
                            foreach (HtmlNode node10 in (IEnumerable<HtmlNode>)document4.DocumentNode.SelectNodes("//td[@class='Availability_whitebold_border-left AvailTip']"))
                            {
                                strArray3[num18] = node10.InnerText.Replace("\r", "").Replace("\t", "").Replace("\n", "").Replace("&nbsp", "").Replace("&thinsp;", "");
                                num18++;
                                if (node10.InnerHtml.Contains("radio"))
                                {
                                    HtmlDocument document8 = new HtmlDocument
                                    {
                                        OptionFixNestedTags = false
                                    };
                                    document8.LoadHtml(node10.InnerHtml);
                                    string[] strArray5 = document8.DocumentNode.SelectSingleNode("//input [@type='radio']").Attributes["onclick"].Value.Replace("SetHdSelectedFare(", "").Replace(");", "").Replace("'", "").Split(new char[] { ',' });
                                    int num = Convert.ToInt16(strArray5[0]);
                                    int num2 = Convert.ToInt16(strArray5[1]);
                                    string str28 = ExtractViewStateOnlyGA(Html);
                                    switch (num)
                                    {
                                        case 1:
                                            str4 = string.Format(string.Concat(new object[] { "ToolkitScriptManager1_HiddenField=&__EVENTTARGET=ctlBooking$ctlNext$lnkNext&__EVENTARGUMENT=&__VIEWSTATE={0}&OutboundFlight=on&ctlBooking$ctlOutbound$rptAvailability$ctl01$rptFares$ctl00$hdFareInformationId=&ctlBooking$ctlOutbound$rptAvailability$ctl01$rptFares$ctl01$hdFareInformationId=", num2, "&ReturnFlight=on&ctlBooking$ctlReturn$rptAvailability$ctl01$rptFares$ctl00$hdFareInformationId=&ctlBooking$ctlReturn$rptAvailability$ctl01$rptFares$ctl01$hdFareInformationId=", num2, "&selectedSegment_1=", num2, "&selectedSegment_2=", num2 }), str28);
                                            break;

                                        case 2:
                                            str4 = string.Format(string.Concat(new object[] { "ToolkitScriptManager1_HiddenField=&__EVENTTARGET=ctlBooking$ctlNext$lnkNext&__EVENTARGUMENT=&__VIEWSTATE={0}&OutboundFlight=on&ctlBooking$ctlOutbound$rptAvailability$ctl01$rptFares$ctl00$hdFareInformationId=&ctlBooking$ctlOutbound$rptAvailability$ctl01$rptFares$ctl01$hdFareInformationId=", num2, "&ReturnFlight=on&ctlBooking$ctlReturn$rptAvailability$ctl01$rptFares$ctl00$hdFareInformationId=&ctlBooking$ctlReturn$rptAvailability$ctl01$rptFares$ctl01$hdFareInformationId=", num2, "&selectedSegment_1=", num2, "&selectedSegment_2=", num2 }), str28);
                                            break;
                                    }
                                    HttpWebRequest request = WebRequest.Create("http://airpegasus.booksecure.net/avail.aspx?" + str3) as HttpWebRequest;
                                    request.Method = "POST";
                                    request.ServicePoint.Expect100Continue = false;
                                    request.Headers.Add("Accept-Encoding", "gzip, deflate");
                                    request.Headers.Add("UA-CPU: x86");
                                    request.Headers.Add("Cache-Control: no-cache");
                                    request.Referer = "http://airpegasus.booksecure.net/avail.aspx?" + str3;
                                    request.Accept = "image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, application/x-shockwave-flash, */*";
                                    request.Headers.Add("Accept-Language: en-US,en;q=0.5");
                                    request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0";
                                    request.ContentType = "application/x-www-form-urlencoded";
                                    request.KeepAlive = true;
                                    request.CookieContainer = cookies;
                                    request.ProtocolVersion = HttpVersion.Version10;
                                    ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)Delegate.Combine(ServicePointManager.ServerCertificateValidationCallback, new RemoteCertificateValidationCallback(CommonFunction.customXertificateValidation));
                                    StreamWriter writer = new StreamWriter(request.GetRequestStream());
                                    writer.Write(str4);
                                    writer.Close();
                                    string str27 = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
                                    HtmlDocument document9 = new HtmlDocument
                                    {
                                        OptionFixNestedTags = true
                                    };
                                    document9.LoadHtml(str27);
                                    if (document9 != null)
                                    {
                                        foreach (HtmlNode node13 in (IEnumerable<HtmlNode>)document9.DocumentNode.SelectSingleNode("//div[@id='ctlSegmentedSummaryInfo_rptSummarySegments_ctl00_pnlSummarySegmentHeader']").SelectNodes("//td[@style='width: 20%']"))
                                        {
                                            if (node13.InnerText.Contains(":"))
                                            {
                                                str9 = node13.InnerText;
                                                str7 = str7 + "~" + str9;
                                            }
                                        }
                                        string s = str7.Replace("\n", "").Replace("\r", "").Split(new char[] { '~' })[2].Split(new char[] { ' ' })[0x34];
                                        str10 = DateTime.ParseExact(s, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("ddMMyy");
                                        str7 = null;
                                    }
                                    HtmlDocument document10 = new HtmlDocument
                                    {
                                        OptionFixNestedTags = true
                                    };
                                    document10.LoadHtml(str27);
                                    if (document10 != null)
                                    {
                                        HtmlNode node14 = document10.DocumentNode.SelectSingleNode("//table[@id='ctlSegmentedSummaryInfo_rptSummarySegments_ctl00_tblPassengers']");
                                        HtmlDocument document11 = new HtmlDocument
                                        {
                                            OptionFixNestedTags = true
                                        };
                                        document11.LoadHtml(node14.InnerHtml);
                                        if (document11 != null)
                                        {
                                            HtmlNode node15 = document11.DocumentNode.SelectSingleNode("//table");
                                            HtmlDocument document12 = new HtmlDocument
                                            {
                                                OptionFixNestedTags = true
                                            };
                                            document12.LoadHtml(node15.InnerHtml);
                                            if (document12 != null)
                                            {
                                                foreach (HtmlNode node16 in (IEnumerable<HtmlNode>)document12.DocumentNode.SelectNodes("//tr"))
                                                {
                                                    HtmlDocument document13 = new HtmlDocument
                                                    {
                                                        OptionFixNestedTags = true
                                                    };
                                                    document13.LoadHtml(node16.InnerHtml);
                                                    if (document13 != null)
                                                    {
                                                        HtmlNodeCollection nodes10 = document13.DocumentNode.SelectNodes("//td");
                                                        if (nodes10 != null)
                                                        {
                                                            if (nodes10[0].InnerText.Contains("1 Adult"))
                                                            {
                                                                num12 = Convert.ToDecimal(nodes10[1].InnerText.Replace(",", "").Replace("INR", ""));
                                                                num11 = Convert.ToDecimal(nodes10[3].InnerText.Replace(",", "").Replace("\r", "").Replace("\n", "").Replace("INR", ""));
                                                            }
                                                            if (nodes10[0].InnerText.Contains("1 child"))
                                                            {
                                                                num14 = Convert.ToDecimal(nodes10[1].InnerText.Replace(",", "").Replace("INR", ""));
                                                                num13 = Convert.ToDecimal(nodes10[3].InnerText.Replace(",", "").Replace("INR", "").Replace("\r", "").Replace("\n", ""));
                                                            }
                                                            if (nodes10[0].InnerText.Contains("1 Infant"))
                                                            {
                                                                num10 = Convert.ToDecimal(nodes10[1].InnerText.Replace(",", "").Replace("INR", ""));
                                                                num9 = Convert.ToDecimal(nodes10[3].InnerText.Replace(",", "").Replace("INR", "").Replace("\r", "").Replace("\n", ""));
                                                                num8 = num10 + num9;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        num3 = Convert.ToDecimal(document10.DocumentNode.SelectSingleNode("//span[@id='ctlSegmentedSummaryInfo_rptSummarySegments_ctl00_lblSegTotal_1']").InnerText.Replace(",", "").Replace("INR", ""));
                                    }
                                    num7 = num12 + num11;
                                    num6 = num14 + num13;
                                    num5 = num8;
                                    num4 = num11;
                                    str5 = string.Concat(new object[] { "ADT,", num7, "~CHD,", num6, "~INF,", num5, "~", num3, "~", num4 });
                                }
                            }
                            str14 = DateTime.ParseExact(strArray3[3], "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                            str13 = DateTime.ParseExact(strArray3[4], "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                            str12 = DateTime.ParseExact(strArray3[0].Split(new char[] { '-' })[1].ToString().TrimStart(new char[0]), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("ddMMyy");
                            if (((strArray[3] != null) && (strArray[4] != null)) && ((strArray2[3] != null) && (strArray2[4] != null)))
                            {
                                str17 = DateTime.ParseExact(strArray[3], "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                                str16 = DateTime.ParseExact(strArray[4], "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                                str20 = DateTime.ParseExact(strArray2[3], "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                                str19 = DateTime.ParseExact(strArray2[4], "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                            }
                            strArray3[7] = strArray3[7].Replace("-", " ");
                            strArray3[5] = strArray3[5].Replace(" ", "");
                            if (num17 == 0)
                            {
                                if (strArray3[5].Contains("0ConnectingFlights"))
                                {
                                    string str31 = PegasusAirName(strArray3[1]).ToUpper();
                                    string str32 = PegasusAirName(strArray3[2]).ToUpper();
                                    str11 = strArray3[7] + "~" + str31 + "~" + str32 + "~" + str12 + "~" + str14 + "~" + str10 + "~" + str13;
                                }
                                else
                                {
                                    string[] strArray8 = strArray3[7].Split(new char[] { ' ' });
                                    string[] strArray9 = strArray8[1].Split(new char[] { '/' });
                                    string str33 = strArray8[0] + " " + strArray9[0];
                                    string str34 = strArray8[0] + " " + strArray9[1];
                                    str21 = PegasusAirName(strArray[1]).ToUpper();
                                    str18 = PegasusAirName(strArray2[1]).ToUpper();
                                    str22 = PegasusAirName(strArray[2]).ToUpper();
                                    str23 = PegasusAirName(strArray2[2]).ToUpper();
                                    str11 = str33 + "~" + str21 + "~" + str22 + "~" + str12 + "~" + str17 + "~" + str12 + "~" + str16 + "^" + str34 + "~" + str18 + "~" + str23 + "~" + str12 + "~" + str20 + "~" + str10 + "~" + str19;
                                }
                                str8 = str5 + "~~~";
                                num17++;
                            }
                            else
                            {
                                if (strArray3[5].Contains("0ConnectingFlights"))
                                {
                                    string str35 = PegasusAirName(strArray3[1]).ToUpper();
                                    string str36 = PegasusAirName(strArray3[1]).ToUpper();
                                    str11 = strArray3[7] + "~" + str35 + "~" + str36 + "~" + str12 + "~" + str14 + "~" + str10 + "~" + str13;
                                }
                                else
                                {
                                    string[] strArray10 = strArray3[7].Split(new char[] { ' ' });
                                    string[] strArray11 = strArray10[1].Split(new char[] { '/' });
                                    string str37 = strArray10[0] + " " + strArray11[0];
                                    string str38 = strArray10[0] + " " + strArray11[1];
                                    str21 = PegasusAirName(strArray[1]).ToUpper();
                                    str18 = PegasusAirName(strArray2[1]).ToUpper();
                                    str22 = PegasusAirName(strArray[2]).ToUpper();
                                    str23 = PegasusAirName(strArray2[2]).ToUpper();
                                    str11 = str37 + "~" + str21 + "~" + str22 + "~" + str12 + "~" + str17 + "~" + str12 + "~" + str16 + "^" + str38 + "~" + str18 + "~" + str23 + "~" + str12 + "~" + str20 + "~" + str10 + "~" + str19;
                                }
                                str8 = str5 + "~~~";
                            }
                            string str39 = strArray3[7];
                            flttable[num16][str39] = str11;
                            faretable[num16][str39] = str8;
                            str10 = "";
                            str8 = "";
                            str11 = "";
                            str39 = "";
                        }
                    }
                    num16++;
                }
                ParsingClass class2 = new ParsingClass();
                return class2.ParsingPegasus(SeachParam, flttable, faretable);
            }
            catch (Exception)
            {
                return "Data is not Available..(^_^)";
            }
        }

        public static string PegasusAirName(string Code)
        {
            string str = Code;
            try
            {
                str = str.Replace("BENGALURU", "Bangalore").Replace("HUBBALLI", "Hubli").Replace("TRIVANDRUM Domestic Terminal", "Trivandrum").Replace("CHENNAI", "Chennai/Madras");
            }
            catch (Exception)
            {
            }
            return str;
        }
        public static object calculatetime(string time0, string time1)
        {
            try
            {
                string str2 = null;
                string str3 = null;
                string str4 = time0.Substring(0, 2);
                string str5 = time1.Substring(0, 2);
                string str6 = time0.Substring(2, 2);
                string str7 = time1.Substring(2, 2);
                int num = Convert.ToInt32(str4) - Convert.ToInt32(str5);
                int num2 = Convert.ToInt32(str6) - Convert.ToInt32(str7);
                if (num.ToString().Length < 2)
                {
                    str2 = "0" + num.ToString();
                }
                else
                {
                    str2 = num.ToString();
                }
                if (num2.ToString().Length < 2)
                {
                    str3 = "0" + num2.ToString();
                }
                else
                {
                    str3 = num2.ToString();
                }
                return (str2 + str3).Replace("-", "");
            }
            catch (Exception)
            {
                return "0";
            }
        }
        public static int checkdetails(string email, string mobile, string merchant)
        {
            int num = 0;
            SqlConnection connection = CommonDB.GetConnection("infl-reward");
            SqlCommand command = new SqlCommand("select * from infl_reward_userdetails where email = '" + email + "' and mobile = '" + mobile + "' and merchant='" + merchant + "'", connection);
            if (command.ExecuteReader().HasRows)
            {
                num = 1;
            }
            return num;
        }
        public static string convertdate(string date)
        {
            try
            {
                string s = date.Split(new char[] { ' ' })[0];
                string text1 = date.Split(new char[] { ' ' })[1];
                try
                {
                    return DateTime.ParseExact(s, "mm/dd/yyyy", CultureInfo.InvariantCulture).ToString("ddmmyy");
                }
                catch (Exception)
                {
                    return DateTime.ParseExact(s, "dd-mmm-yyyy", CultureInfo.InvariantCulture).ToString("ddmmyy");
                }
            }
            catch (Exception)
            {
                return "error message:-wrong parameter";
            }
        }
        public static DataSet ConvertHTMLTablesToDataSet(string HTML)
        {
            DataSet set = new DataSet();
            DataTable table = null;
            DataRow row = null;
            string pattern = "<table[^>]*>(.*?)</table>";
            string str2 = "<th[^>]*>(.*?)</th>";
            string str3 = "<tr[^>]*>(.*?)</tr>";
            string str4 = "<td[^>]*>(.*?)</td>";
            bool flag = false;
            int num = 0;
            int num2 = 0;
            foreach (Match match in Regex.Matches(HTML, pattern, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase))
            {
                num2 = 0;
                flag = false;
                table = new DataTable();
                if (match.Value.Contains("<th"))
                {
                    flag = true;
                    foreach (Match match2 in Regex.Matches(match.Value, str2, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase))
                    {
                        if (!table.Columns.Contains(match2.Groups[1].ToString()))
                        {
                            table.Columns.Add(match2.Groups[1].ToString().Replace("&nbsp;", ""));
                        }
                    }
                }
                else
                {
                    for (int i = 1; i <= Regex.Matches(Regex.Matches(Regex.Matches(match.Value, pattern, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase).ToString(), str3, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase).ToString(), str4, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase).Count; i++)
                    {
                        table.Columns.Add("Column " + i);
                    }
                }
                foreach (Match match3 in Regex.Matches(match.Value, str3, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase))
                {
                    if (!((num2 == 0) & flag))
                    {
                        row = table.NewRow();
                        num = 0;
                        MatchCollection matchs4 = Regex.Matches(match3.Value, str4, RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase);
                        foreach (Match match4 in matchs4)
                        {
                            if ((matchs4.Count - 1) != num)
                            {
                                row[num] = Convert.ToString(match4.Groups[1]).Replace("&nbsp;", "");
                            }
                            num++;
                        }
                        table.Rows.Add(row);
                    }
                    num2++;
                }
                set.Tables.Add(table);
            }
            return set;
        }
        public static string ConvertTime(string date)
        {
            try
            {
                string text1 = date.Split(new char[] { ' ' })[0];
                string text2 = date.Split(new char[] { ' ' })[1];
                try
                {
                    return DateTime.ParseExact(date, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                }
                catch (Exception)
                {
                    return DateTime.ParseExact(date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");
                }
            }
            catch (Exception)
            {
                return "Error message:-Wrong Parameter";
            }
        }
        public static string DecryptData(string PhoneNumber)
        {
            string str = PhoneNumber;
            string str2 = "";
            try
            {
                int length = 0;
                int startIndex = 0;
                if (str.Length == 0)
                {
                    str2 = "";
                }
                else
                {
                    length = str.Length;
                    while (startIndex < length)
                    {
                        char ch = (char)(Strings.Asc(str.Substring(startIndex, 1)) - 9);
                        str2 = str2 + ch;
                        startIndex++;
                    }
                }
                return str2;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string Encrypt(string stringToEncrypt, string SEncryptionKey)
        {
            if (!string.IsNullOrEmpty(stringToEncrypt))
            {
                try
                {
                    //object bytes = Encoding.UTF8.GetBytes(Strings.Left(SEncryptionKey, 8));
                    //DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                    //byte[] buffer = Encoding.UTF8.GetBytes(stringToEncrypt);
                    //MemoryStream stream = new MemoryStream();
                    //if (<Encrypt>o__SiteContainer0.<>p__Site1 == null)
                    //{
                    //    <Encrypt>o__SiteContainer0.<>p__Site1 = CallSite<Func<CallSite, Type, MemoryStream, object, CryptoStreamMode, CryptoStream>>.Create(Binder.InvokeConstructor(CSharpBinderFlags.None, typeof(CommonFunction), new CSharpArgumentInfo[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.IsStaticType | CSharpArgumentInfoFlags.UseCompileTimeType, null), CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType, null), CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null), CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.Constant | CSharpArgumentInfoFlags.UseCompileTimeType, null) }));
                    //}
                    //CryptoStream stream2 = <Encrypt>o__SiteContainer0.<>p__Site1.Target(<Encrypt>o__SiteContainer0.<>p__Site1, typeof(CryptoStream), stream, provider.CreateEncryptor((dynamic) bytes, IV), CryptoStreamMode.Write);
                    //stream2.Write(buffer, 0, buffer.Length);
                    //stream2.FlushFinalBlock();
                    //return Convert.ToBase64String(stream.ToArray());
                }
                catch (Exception exception)
                {
                    return exception.Message;
                }
            }
            return "";
        }
        public static string EncryptData(string PhoneNumber)
        {
            string str = PhoneNumber;
            string str2 = "";
            int length = 0;
            int startIndex = 0;
            if (str.Length == 0)
            {
                return "";
            }
            length = str.Length;
            while (startIndex < length)
            {
                char ch = Strings.Chr(Strings.Asc(str.Substring(startIndex, 1)) + 9);
                str2 = str2 + ch;
                startIndex++;
            }
            return str2;
        }
        public static string ExtractViewState(string s)
        {
            string str = "__VIEWSTATE";
            string str2 = "value=\"";
            int index = s.IndexOf(str);
            int startIndex = s.IndexOf(str2, index) + str2.Length;
            int num4 = s.IndexOf("\"", startIndex);
            return HttpUtility.UrlEncodeUnicode(s.Substring(startIndex, num4 - startIndex));
        }
        public static string ExtractViewStateOnlyGA(string s)
        {
            string str = "__VIEWSTATE";
            string str2 = "value=\"";
            int index = s.IndexOf(str);
            if (index < 0)
            {
                str = "viewState";
                index = s.IndexOf(str);
            }
            int startIndex = s.IndexOf(str2, index) + str2.Length;
            int num4 = s.IndexOf("\"", startIndex);
            return HttpUtility.UrlEncodeUnicode(s.Substring(startIndex, num4 - startIndex));
        }
        public static string FareAirParse(string fareString, string ExchangeRates)
        {
            string str = "";
            string str2 = "";
            ExchangeRates = "INFL," + ExchangeRates;
            decimal num = 1M;
            decimal num2 = 0M;
            decimal num3 = 0M;
            decimal num4 = 0M;
            decimal num5 = 0M;
            decimal num6 = 0M;
            decimal num7 = 0M;
            decimal num8 = 0M;
            decimal num9 = 0M;
            try
            {
                HtmlDocument document = new HtmlDocument
                {
                    OptionFixNestedTags = false
                };
                document.LoadHtml(fareString);
                if (document != null)
                {
                    foreach (HtmlNode node2 in (IEnumerable<HtmlNode>)document.DocumentNode.SelectSingleNode("//div[@class='js-accordion-body ']").ChildNodes)
                    {
                        if (node2.OriginalName.Equals("div"))
                        {
                            HtmlDocument document2 = new HtmlDocument
                            {
                                OptionFixNestedTags = false
                            };
                            document2.LoadHtml(node2.InnerHtml);
                            HtmlNodeCollection nodes = document2.DocumentNode.SelectNodes("div");
                            if (nodes[0].InnerText.Contains("Adult"))
                            {
                                str2 = nodes[1].InnerText.Split(new char[] { ';' })[1].Split(new char[] { ' ' })[2].Replace(",", "").Replace("\r", "").Replace("\n", "");
                                if (str2 != "INR")
                                {
                                    string[] strArray = ExchangeRates.Split(new string[] { str2 + "-" }, StringSplitOptions.None);
                                    if (strArray[1] != null)
                                    {
                                        num = Convert.ToDecimal(strArray[1].Split(new char[] { ',' })[0]);
                                    }
                                }
                                num5 = Convert.ToDecimal(nodes[1].InnerText.Split(new char[] { ';' })[1].Split(new char[] { ' ' })[1].Replace(",", ""));
                                num5 *= num;
                                num4 = Convert.ToDecimal(nodes[1].InnerText.Split(new char[] { '&' })[0]);
                                decimal decimal1 = num5 * num4;
                            }
                            else if (nodes[0].InnerText.Contains("Kid"))
                            {
                                num6 = Convert.ToDecimal(nodes[1].InnerText.Split(new char[] { ';' })[1].Split(new char[] { ' ' })[1].Replace(",", "")) * num;
                                num8 = Convert.ToDecimal(nodes[1].InnerText.Split(new char[] { '&' })[0]);
                                decimal decimal2 = num6 * num8;
                            }
                            else if (nodes[0].InnerText.Contains("Infant"))
                            {
                                num7 = Convert.ToDecimal(nodes[1].InnerText.Split(new char[] { ';' })[1].Split(new char[] { ' ' })[1].Replace(",", "")) * num;
                                num9 = Convert.ToDecimal(nodes[1].InnerText.Split(new char[] { '&' })[0]);
                                decimal decimal3 = num7 * num9;
                            }
                            else if (nodes[0].InnerText.Contains("Fares"))
                            {
                                num2 = Convert.ToDecimal(nodes[0].ChildNodes[3].InnerText.Split(new char[] { ' ' })[12].Replace(",", "")) * num;
                            }
                        }
                    }
                    HtmlNode node4 = document.DocumentNode.SelectSingleNode("//div[@class='section-total-display-price']").SelectSingleNode("span");
                    if (node4 != null)
                    {
                        num3 = Convert.ToDecimal(node4.InnerText.Replace(",", "").Replace("\n", "").Replace("\r", "")) * num;
                    }
                    decimal num10 = num3 - num2;
                    decimal num11 = num10 / (num4 + num8);
                    str = string.Concat(new object[] { "ADT,", num5, "~CHD,", num6, "~INF,", num7, "~", num3, "~", num11 });
                }
                return str;
            }
            catch (Exception exception)
            {
                return ("Error Message:" + exception.Message);
            }
        }
        public static string FareParse(string Data, string paxs)
        {
            string str = "";
            HtmlDocument document = new HtmlDocument();
            HtmlDocument document2 = new HtmlDocument();
            decimal num = 0M;
            decimal num2 = 0M;
            bool flag = false;
            decimal num3 = 0M;
            string str2 = "";
            document.OptionFixNestedTags = true;
            document2.OptionFixNestedTags = true;
            if (Data == "")
            {
                return str;
            }
            document.LoadHtml(Data);
            if (document != null)
            {
                HtmlNode node = document.DocumentNode.SelectSingleNode("//table[@class='priceSummary']");
                if (node != null)
                {
                    string innerHtml = node.InnerHtml;
                    document.LoadHtml(innerHtml);
                    HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("tr");
                    int num4 = 0;
                    foreach (HtmlNode node2 in (IEnumerable<HtmlNode>)nodes)
                    {
                        if (num4 >= 3)
                        {
                            break;
                        }
                        HtmlNode node3 = node2.ChildNodes[1];
                        if (node3 != null)
                        {
                            string str4 = node3.InnerText.Replace("\n", "").Replace("\r", "").Replace("\t", "");
                            if (str4.Contains("Adult"))
                            {
                                HtmlNode node4 = node2.ChildNodes[3];
                                if ((str2 == "") && str4.Contains("Adult"))
                                {
                                    num2 = Convert.ToDecimal(node4.InnerText.Replace(",", ""));
                                    str2 = "ADT," + (Convert.ToDecimal(node4.InnerText.Replace(",", "")) / Convert.ToDecimal(paxs.Split(new char[] { ',' })[0]));
                                }
                            }
                            else if (!str4.Contains("Adult") && (num4 == 0))
                            {
                                str2 = "ADT,0";
                            }
                            if (str4.Contains("Child"))
                            {
                                HtmlNode node5 = node2.ChildNodes[3];
                                if (str4.Contains("Child"))
                                {
                                    num2 += Convert.ToDecimal(node5.InnerText.Replace(",", ""));
                                    str2 = "CHD," + (Convert.ToDecimal(node5.InnerText.Replace(",", "")) / Convert.ToDecimal(paxs.Split(new char[] { ',' })[1]));
                                }
                            }
                            else if (!str4.Contains("Child") && (num4 == 1))
                            {
                                str2 = "CHD,0";
                            }
                            if (str4.Contains("Infant ") && (num4 == 2))
                            {
                                HtmlNode node6 = node2.ChildNodes[3];
                                if (str4.Contains("Infant"))
                                {
                                    num2 += Convert.ToDecimal(node6.InnerText.Replace(",", ""));
                                    str2 = "INF," + (Convert.ToDecimal(node6.InnerText.Replace(",", "")) / Convert.ToDecimal(paxs.Split(new char[] { ',' })[2]));
                                }
                            }
                            else if (str4.Contains("Infant ") && (num4 == 1))
                            {
                                HtmlNode node7 = node2.ChildNodes[3];
                                if (str4.Contains("Infant"))
                                {
                                    flag = true;
                                    num2 += Convert.ToDecimal(node7.InnerText.Replace(",", ""));
                                    str2 = "CHD,0~INF," + (Convert.ToDecimal(node7.InnerText.Replace(",", "")) / Convert.ToDecimal(paxs.Split(new char[] { ',' })[2]));
                                }
                            }
                            else if ((!str4.Contains("Infant") && (num4 == 2)) && !flag)
                            {
                                str2 = "INF,0";
                            }
                            if (str == "")
                            {
                                str = str2;
                                str2 = "";
                            }
                            else if (str2 != "")
                            {
                                str = str + "~" + str2;
                                str2 = "";
                            }
                        }
                        num4++;
                    }
                }
            }
            document.LoadHtml(Data);
            num = Convert.ToDecimal(document.DocumentNode.SelectSingleNode("//h4[@class='left selet-total-price']").InnerText.Split(new char[] { ':' })[1].Replace(",", "").Replace(" ", ""));
            num3 = num - num2;
            num3 /= Convert.ToDecimal(paxs.Split(new char[] { ',' })[0]);
            object obj2 = str;
            return string.Concat(new object[] { obj2, "~", num, "~", num3 });
        }
        public static string FillData(Hashtable[] FlightTable, Hashtable[] FareTable)
        {
            Hashtable[] hashtableArray = FlightTable;
            for (int i = 0; i < hashtableArray.Length; i++)
            {
                Hashtable hashtable1 = hashtableArray[i];
            }
            return "Success";
        }
        static string GetDailyExChangeRate(string Currency)
        {
            string str = "";
            try
            {
                string str2 = DecryptData(ConfigurationManager.AppSettings.Get("ConnectionServer"));
                string connectionString = "Server=" + str2 + ";" + DecryptData(ConfigurationManager.AppSettings.Get("ConnectionString"));
                string cmdText = "SELECT INR FROM DailyExChangeRate where applicabledate = (Select max(applicabledate) from DailyExChangeRate)";
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand command = new SqlCommand(cmdText, connection);
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    try
                    {
                        if (reader.HasRows)
                        {
                            str = reader["INR"].ToString();
                        }
                    }
                    catch (Exception)
                    {
                    }
                    return str;
                }
                finally
                {
                    command.Dispose();
                    connection.Close();
                }
            }
            catch (Exception)
            {
            }
            return str;
        }
        public static string GetFareQuoteSpiceYQAspx(string Param, CookieContainer cookies)
        {
            string str = "";
            ConfigurationManager.AppSettings["CommissionSectors"].ToString();
            str = "http://book.spicejet.com/TaxAndFeeInclusiveDisplayAjax-resource.aspx?flightKeys=";
            HttpWebRequest request = WebRequest.Create(str + Param + "&numberOfMarkets=1&keyDelimeter=,") as HttpWebRequest;
            request.Method = "Get";
            request.ServicePoint.Expect100Continue = false;
            // request.Host = "book.spicejet.com";
            request.KeepAlive = true;
            request.Accept = "*/*";
            request.Headers.Add("X-Requested-With: XMLHttpRequest");
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36";
            request.Referer = "http://book.spicejet.com/Select.aspx";
            request.Headers.Add("Accept-Encoding", " sdch");
            request.Headers.Add("Accept-Language: en-GB,en-US;q=0.8,en;q=0.6");
            request.CookieContainer = cookies;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());
            return reader.ReadToEnd();
        }
        public static string GetHtml(string url)
        {
            string str = "";
            try
            {
                using (StreamReader reader = new StreamReader(WebRequest.Create(url).GetResponse().GetResponseStream()))
                {
                    str = reader.ReadToEnd();
                    reader.Close();
                }
            }
            catch (Exception)
            {
            }
            return str;
        }
        public static string GetHtmlUsingCookies(string LoginUrl, string PostURL, string refurl, CookieContainer cookies)
        {
            string requestUriString = LoginUrl;
            requestUriString = PostURL;
            HttpWebRequest request = WebRequest.Create(requestUriString) as HttpWebRequest;
            request.Method = "GET";
            request.ServicePoint.Expect100Continue = false;
            request.Headers.Add("Accept-Encoding", "sdch");
            request.Headers.Add("Cache-Control: max-age=0");
            request.Referer = refurl;
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.Headers.Add("Accept-Language: en-US,en;q=0.8");
            request.Headers.Add("Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3");
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31";
            request.ContentType = "application/x-www-form-urlencoded";
            request.KeepAlive = true;
            request.CookieContainer = cookies;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback)Delegate.Combine(ServicePointManager.ServerCertificateValidationCallback, new RemoteCertificateValidationCallback(CommonFunction.customXertificateValidation));
            StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());
            string str = reader.ReadToEnd();
            reader.Close();
            return str;
        }
        public static string GetHtmlUsingCookiesAndGetMethod(string url)
        {
            string str = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                CookieContainer container = new CookieContainer();
                request.Method = "GET";
                request.ServicePoint.Expect100Continue = false;
                request.Headers.Add("Accept-Encoding", "sdch");
                request.Headers.Add("UA-CPU: x86");
                request.Headers.Add("Cache-Control: no-cache");
                request.Accept = "image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, application/x-shockwave-flash, */*";
                request.Headers.Add("Accept-Language: en-US");
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)";
                request.ContentType = "application/x-www-form-urlencoded";
                request.KeepAlive = true;
                request.CookieContainer = container;
                using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
                {
                    str = reader.ReadToEnd();
                    reader.Close();
                }
            }
            catch (Exception)
            {
            }
            return str;
        }
        public static string[,] GetMarkupComm(string Usercode, string MerchantID, string MerchantURL, string Authenticationdetails)
        {
            string str = null;
            string str2 = "";
            string str3 = "";
            string[] strArray = Strings.Split(Authenticationdetails, "#", -1, CompareMethod.Binary);
            try
            {
                string sSQL = null;
                sSQL = "select a.* from AgencyDetail a, adminlogin b where a.uid=b.uid and b.donecarduser='" + Usercode + "' and a.MerchantID='" + MerchantID + "'";
                SqlDataReader rs = CommonDB.DataReaderAny(strArray[2], strArray[3], sSQL);
                try
                {
                    if (rs.HasRows)
                    {
                        rs.Read();
                        str = CommonDB.GetValue(rs, "AgencyAirlineMarkup") + "^" + CommonDB.GetValue(rs, "PercORValue") + "^" + CommonDB.GetValue(rs, "AgencyAirlineValueMarkup") + "^" + CommonDB.GetValue(rs, "AgencyAirlineMarkup2") + "^" + CommonDB.GetValue(rs, "AgencyAirlineValueMarkup2") + "^" + CommonDB.GetValue(rs, "AgencyAirlineValueComm") + "^" + CommonDB.GetValue(rs, "AgencyAirlineValueComm2") + "^" + CommonDB.GetValue(rs, "ValidationCode") + "^" + CommonDB.GetValue(rs, "RefAgency") + "^" + CommonDB.GetValue(rs, "AgencyAirlineComm") + "^" + CommonDB.GetValue(rs, "AgencyAirlineCommYQ") + "^" + CommonDB.GetValue(rs, "AgentSaving") + "^" + CommonDB.GetValue(rs, "TDSRate") + "^" + CommonDB.GetValue(rs, "AgencyFixedMarkup");
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    rs.Close();
                    rs = null;
                }
                string[] strArray2 = str.Split(new char[] { '^' });
                string[] strArray3 = new string[6];
                strArray3[0] = strArray2[11];
                strArray3[1] = strArray2[9];
                strArray3[2] = strArray2[10];
                strArray3[3] = strArray2[5];
                strArray3[4] = strArray2[6];
                for (int j = 0; j <= (strArray3.Length - 2); j++)
                {
                    str2 = str2 + strArray3[j] + "^";
                }
                for (int k = 0; k <= (strArray2.Length - 1); k++)
                {
                    str3 = str3 + strArray2[k] + "^";
                }
            }
            catch (Exception)
            {
            }
            string[,] strArray4 = null;
            strArray4 = new string[2, 14];
            for (int i = 0; i <= strArray4.GetUpperBound(0); i++)
            {
                for (int m = 0; m <= strArray4.GetUpperBound(1); m++)
                {
                    strArray4[i, m] = "0";
                }
            }
            string[] strArray5 = (str3 + "#" + str2).Split(new char[] { '#' });
            string[] strArray6 = strArray5[0].Split(new char[] { '^' });
            string[] strArray7 = strArray5[1].Split(new char[] { '^' });
            try
            {
                strArray4[0, 0] = strArray6[0];
                strArray4[0, 1] = strArray6[1];
                strArray4[0, 2] = strArray6[2];
                strArray4[0, 3] = strArray6[3];
                strArray4[0, 4] = strArray6[4];
                strArray4[0, 5] = strArray6[5];
                strArray4[0, 6] = strArray6[6];
                strArray4[0, 7] = strArray6[7];
                strArray4[0, 8] = strArray6[8];
                strArray4[0, 9] = strArray6[9];
                strArray4[0, 10] = strArray6[10];
                strArray4[0, 11] = strArray6[11];
                strArray4[0, 12] = strArray6[12];
                strArray4[0, 13] = strArray6[13];
                strArray4[1, 11] = strArray7[0];
                strArray4[1, 9] = strArray7[1];
                strArray4[1, 10] = strArray7[2];
                strArray4[1, 5] = strArray7[3];
                strArray4[1, 6] = strArray7[4];
            }
            catch (Exception)
            {
            }
            return strArray4;
        }
        internal static void insertdetails(string EmailID, string Mobile, string CustomerName, string MerchantName, string Merchant)
        {
            SqlConnection connection = new SqlConnection();
            connection = CommonDB.GetConnection("INFL-REWARD");
            string str2 = "123456";
            try
            {
                SqlCommand command = new SqlCommand("insert into INFL_Reward_UserDetails(uid,password,UserName,Merchant,MerchantID,Mobile,email) values('" + EmailID + "','" + str2 + "','" + CustomerName + "','" + Merchant + "','INFL-REWARD','" + Mobile + "','" + EmailID + "')", connection);
                command.ExecuteNonQuery();
                command.Dispose();
                connection.Close();
            }
            catch (Exception)
            {
            }
        }
        public static string maildata(string code, string Merchant, string MerchantName, string EmailID, string CustomerName, string Mobile, string ProductType, string Validity, string MerchantID)
        {
            string str = "";
            try
            {
                int num = int.Parse(ConfigurationManager.AppSettings["RewardValidity"].ToString());
                DateTime now = DateTime.Now;
                DateTime time2 = DateTime.Now.AddDays((double)num);
                string str2 = "Days";
                try
                {
                    num = int.Parse(Validity);
                }
                catch (Exception)
                {
                    str2 = "Dates";
                }
                try
                {
                    if (Validity != "")
                    {
                        if (str2 == "Days")
                        {
                            num = int.Parse(Validity);
                            time2 = DateTime.Now.AddDays((double)num);
                        }
                        else
                        {
                            time2 = DateTime.ParseExact(Validity, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                    }
                    else
                    {
                        time2 = DateTime.Now.AddDays((double)num);
                    }
                }
                catch (Exception)
                {
                    time2 = DateTime.Parse(Validity);
                }
                SqlConnection connection = CommonDB.GetConnection("INFL-REWARD");
                new SqlCommand("insert into infl_reward_coupons(CouponCode,LastUpdateDate,Validity,MerchantCode,MerchantID,MerchantName,Email,CustomerName,Mobile,ProductType) values('" + code + "','" + now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + time2.ToString("yyyy-MM-dd HH:mm:ss") + "','" + Merchant + "','" + MerchantID + "','" + MerchantName + "','" + EmailID + "','" + CustomerName + "','" + Mobile + "','" + ProductType + "')", connection).ExecuteReader();
                connection.Close();
                return str;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string OfflineMarkupComm(string TxnData, string AirlineNew, string TotalPax, string Flag, string[,] MarkupComm)
        {
            string expression = null;
            string str3 = null;
            string str4 = null;
            string str5 = null;
            string str6 = null;
            string str7 = null;
            string str8 = null;
            string str9 = null;
            string str10 = null;
            string str11 = null;
            string str12 = null;
            expression = "";
            str8 = "";
            string[] strArray = TotalPax.Split(new char[] { ',' });
            TotalPax = Convert.ToString((int)((Convert.ToInt32(strArray[0]) + Convert.ToInt32(strArray[1])) + Convert.ToInt32(strArray[2])));
            string str13 = ConfigurationManager.AppSettings.Get("ServiceTax");
            ConfigurationManager.AppSettings.Get("PartDist");
            decimal num = 0M;
            decimal num2 = 0M;
            decimal num3 = 0M;
            decimal num4 = 0M;
            decimal num5 = 0M;
            int num6 = 0;
            ConfigurationManager.AppSettings.Get("CommOnFull");
            string str14 = ConfigurationManager.AppSettings.Get("CommOnYQ");
            Convert.ToInt32(ConfigurationManager.AppSettings.Get("YQ"));
            int num7 = 0;
            decimal num8 = 0M;
            int num9 = 0;
            string[] strArray2 = Strings.Split(str13, ",", -1, CompareMethod.Binary);
            num4 = Convert.ToDecimal(strArray2[0]);
            num5 = Convert.ToDecimal(strArray2[1]);
            Strings.Split(TxnData, "()", -1, CompareMethod.Binary);
            string[] strArray3 = AirlineNew.Split(new char[] { ',' });
            num9 = Convert.ToInt32(strArray3[1]);
            num6 = Convert.ToInt32(strArray3[2]);
            Convert.ToInt32((double)(Convert.ToDouble(num9) + Convert.ToDouble(num6)));
            str4 = MarkupComm[0, 0];
            string text1 = MarkupComm[0, 1];
            str5 = MarkupComm[0, 2];
            string text2 = MarkupComm[0, 3];
            string text3 = MarkupComm[0, 4];
            str6 = MarkupComm[0, 5];
            string text4 = MarkupComm[0, 6];
            string text5 = MarkupComm[0, 7];
            string text6 = MarkupComm[0, 8];
            expression = MarkupComm[0, 9];
            str3 = MarkupComm[0, 10];
            str11 = MarkupComm[0, 11];
            Convert.ToDecimal(MarkupComm[0, 12]);
            str12 = MarkupComm[0, 13];
            str10 = MarkupComm[1, 11];
            str8 = MarkupComm[1, 9];
            str9 = MarkupComm[1, 10];
            str7 = MarkupComm[1, 5];
            string text7 = MarkupComm[1, 6];
            string[] strArray4 = Strings.Split(expression, ",", -1, CompareMethod.Binary);
            string[] strArray5 = Strings.Split(str3, ",", -1, CompareMethod.Binary);
            string[] strArray6 = Strings.Split(str4, ",", -1, CompareMethod.Binary);
            string[] strArray7 = Strings.Split(str8, ",", -1, CompareMethod.Binary);
            string[] strArray8 = Strings.Split(str9, ",", -1, CompareMethod.Binary);
            Strings.Split(str10, ",", -1, CompareMethod.Binary);
            Strings.Split(str11, ",", -1, CompareMethod.Binary);
            string[] strArray9 = Strings.Split(str12, ",", -1, CompareMethod.Binary);
            string[] strArray10 = Strings.Split(str5, ",", -1, CompareMethod.Binary);
            string[] strArray11 = Strings.Split(str6, ",", -1, CompareMethod.Binary);
            string[] strArray12 = Strings.Split(str7, ",", -1, CompareMethod.Binary);
            ConfigurationManager.AppSettings.Get("FixedMarkup");
            if (strArray9.Length > 1)
            {
                for (int k = 0; k <= (strArray9.Length - 1); k++)
                {
                    string[] strArray13 = Strings.Split(strArray9[k], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray13[0], 1, 2) == strArray3[0])
                    {
                        Convert.ToDecimal(Strings.Mid(strArray13[0], 4));
                        break;
                    }
                }
            }
            if (strArray11.Length > 1)
            {
                for (int m = 0; m <= (strArray11.Length - 1); m++)
                {
                    string[] strArray14 = Strings.Split(strArray11[m], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray14[0], 1, 2) == strArray3[0])
                    {
                        num = Convert.ToDecimal(Strings.Mid(strArray14[0], 4));
                        break;
                    }
                }
            }
            if (strArray12.Length > 1)
            {
                for (int n = 0; n <= (strArray12.Length - 1); n++)
                {
                    string[] strArray15 = Strings.Split(strArray12[n], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray15[0], 1, 2) == strArray3[0])
                    {
                        Convert.ToDecimal(Strings.Mid(strArray15[0], 4));
                        break;
                    }
                }
            }
            if (strArray6.Length > 1)
            {
                for (int num13 = 0; num13 <= (strArray6.Length - 1); num13++)
                {
                    string[] strArray16 = Strings.Split(strArray6[num13], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray16[0], 1, 2) == strArray3[0])
                    {
                        num3 = Convert.ToDecimal(Strings.Mid(strArray16[0], 4));
                        if (Flag == "D")
                        {
                            num3 = Convert.ToDecimal((decimal)(num3 + Convert.ToDecimal(num4)));
                        }
                        else
                        {
                            num3 = Convert.ToDecimal((decimal)(num3 + Convert.ToDecimal(num5)));
                        }
                        break;
                    }
                }
            }
            if (strArray10.Length > 1)
            {
                for (int num14 = 0; num14 <= (strArray10.Length - 1); num14++)
                {
                    string[] strArray17 = Strings.Split(strArray10[num14], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray17[0], 1, 2) == strArray3[0])
                    {
                        num2 = Convert.ToDecimal(Strings.Mid(strArray17[0], 4));
                        break;
                    }
                }
            }
            for (int i = 0; i <= (strArray4.Length - 1); i++)
            {
                string[] strArray18 = Strings.Split(strArray4[i], ",", -1, CompareMethod.Binary);
                if (Strings.Mid(strArray18[0], 1, 2) == strArray3[0])
                {
                    num8 = Math.Round((decimal)(num9 * (Convert.ToDecimal(Strings.Mid(strArray18[0], 4)) / 100M))) + Math.Round((decimal)(num * Convert.ToInt32(TotalPax)));
                    break;
                }
            }
            for (int j = 0; j <= (strArray5.Length - 1); j++)
            {
                string[] strArray19 = Strings.Split(strArray5[j], ",", -1, CompareMethod.Binary);
                if ((Strings.Mid(strArray19[0], 1, 2) == strArray3[0]) && (str14 == "Yes"))
                {
                    num8 += Math.Round((decimal)((num6 - num7) * (Convert.ToDecimal(Strings.Mid(strArray19[0], 4)) / 100M)));
                    break;
                }
            }
            if (!string.IsNullOrEmpty(str8) && (strArray7.Length > 1))
            {
                for (int num17 = 0; num17 <= (strArray7.Length - 1); num17++)
                {
                    string[] strArray20 = Strings.Split(strArray7[num17], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray20[0], 1, 2) == strArray3[0])
                    {
                        Convert.ToDecimal(Strings.Mid(strArray20[0], 4));
                        break;
                    }
                }
            }
            if (!string.IsNullOrEmpty(str9) && (strArray8.Length > 1))
            {
                for (int num18 = 0; num18 <= (strArray8.Length - 1); num18++)
                {
                    string[] strArray21 = Strings.Split(strArray8[num18], ",", -1, CompareMethod.Binary);
                    if (Strings.Mid(strArray21[0], 1, 2) == strArray3[0])
                    {
                        Convert.ToDecimal(Strings.Mid(strArray21[0], 4));
                        break;
                    }
                }
            }
            return (Convert.ToString(num2) + "$" + Convert.ToString(num8));
        }

        public static string PostSOAPXml(string url, string xml, string SAction)
        {
            string str = "";
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(xml);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml; charset=utf-8";
                request.Headers.Add("SOAPAction", SAction);
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException("POST failed. Received HTTP {response.StatusCode}");
                }
                Stream responseStream = response.GetResponseStream();
                Encoding encoding = Encoding.GetEncoding("utf-8");
                StreamReader reader = new StreamReader(responseStream, encoding);
                str = reader.ReadToEnd();
                reader.Close();
                response.Close();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
            return str;
        }
        public static string PostXml(string url, string xml)
        {
            string str = "";
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(xml);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml; charset=utf-8";
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException("POST failed. Received HTTP {response.StatusCode}");
                }
                Stream responseStream = response.GetResponseStream();
                Encoding encoding = Encoding.GetEncoding("utf-8");
                StreamReader reader = new StreamReader(responseStream, encoding);
                str = reader.ReadToEnd();
                reader.Close();
                response.Close();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
            return str;
        }
        public static string RandomCouponcode(string MerchantCode)
        {
            bool flag = false;
            string str = MerchantCode;
            string str2 = "";
            do
            {
                int num = 11;
                StringBuilder builder = new StringBuilder();
                Random random = new Random();
                while (0 < num)
                {
                    builder.Append("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"[random.Next("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".Length)]);
                    num--;
                }
                str2 = str + builder.ToString();
                SqlConnection connection = CommonDB.GetConnection("INFL-REWARD");
                SqlDataReader reader = new SqlCommand("select * from infl_reward_coupons where CouponCode = '" + str2 + "'", connection).ExecuteReader();
                if (reader.HasRows)
                {
                    flag = true;
                }
                reader.Close();
                connection.Close();
            }
            while (!flag);
            return str2;
        }
        public static void SaveLog(string name, string data, string ServerPath)
        {
            string str = Convert.ToString(DateTime.Now);
            str = DateTime.Now.ToString("yyyyMMddHHmmss");
            try
            {
                string str2 = name + str + ".txt";
                FileStream stream = new FileStream(ServerPath + "/" + str2, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
                string str3 = data;
                writer.WriteLine(str3);
                writer.Close();
                stream.Close();
            }
            catch (Exception)
            {
            }
        }
        public static void SaveXmlLog(string name, string data, string ServerPath)
        {
            string str = Convert.ToString(DateTime.Now);
            str = DateTime.Now.ToString("yyyyMMddHHmmss");
            try
            {
                string str2 = name + str + ".xml";
                FileStream stream = new FileStream(ServerPath + "/" + str2, FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);
                string str3 = data;
                writer.WriteLine(str3);
                writer.Close();
                stream.Close();
            }
            catch (Exception)
            {
            }
        }
        public static string encryptQueryString(string strQueryString)
        {

            return Encrypt(strQueryString, "!#$a54?3");
        }


        public static string Sendmail(string ToEmail, string FromEmail, string Subject, string code, string BCcEmail, string MerchantName, string Name, string ProductType, string ServerPath, string Validity)
        {
            MailMessage message = new MailMessage();
            string str = DateTime.Now.ToString();
            string str2 = encryptQueryString(code);
            string str3 = encryptQueryString(MerchantName);
            string str4 = str2 + "~" + str3;
            string newValue = "";
            string path = "";
            string str7 = "1";
            if (Validity != "")
            {
                str7 = Validity;
            }
            if (ProductType.ToUpper() == "ECOM")
            {
                newValue = "http://www.rewarddestination.com/ProductDisplay.aspx?i=" + str4;
                path = HttpContext.Current.Server.MapPath("Ecom.html");
            }
            else if (ProductType.ToUpper() == "MOVIE")
            {
                newValue = "http://www.rewarddestination.com/MovieBooking.aspx?i=" + str4;
                path = HttpContext.Current.Server.MapPath("MovieTicket.html");
            }
            else if (ProductType.ToUpper() == "AFFILIATE")
            {
                newValue = "http://www.rewarddestination.com/OnlineStore.aspx?i=" + str4;
                path = HttpContext.Current.Server.MapPath("Affiliate.html");
            }
            message.To.Add(new MailAddress(ToEmail));
            message.From = new MailAddress(FromEmail, MerchantName);
            message.Subject = Subject;
            StreamReader reader = File.OpenText(path);
            string data = reader.ReadToEnd();
            reader.Close();
            data = data.Replace("#NAME", Name).Replace("#MERCHENT", MerchantName).Replace("#URL", newValue).Replace("#Month#", str7);
            message.Body = data;
            message.IsBodyHtml = true;
            try
            {
                SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"))
                {
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings.Get("SMTPPort")),
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPUserID"), ConfigurationManager.AppSettings.Get("SMTPPassword"))
                };
                try
                {
                    client.Send(message);
                    SaveLog("Email", data, ServerPath);
                    message = null;
                }
                catch (Exception)
                {
                }
                message = null;
            }
            catch (Exception)
            {
            }
            return str;
        }
        public static string ConvertDate(string date)
        {
            try
            {
                string s = date.Split(new char[] { ' ' })[0];
                string text1 = date.Split(new char[] { ' ' })[1];
                try
                {
                    return DateTime.ParseExact(s, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("ddMMyy");
                }
                catch (Exception)
                {
                    return DateTime.ParseExact(s, "dd-MMM-yyyy", CultureInfo.InvariantCulture).ToString("ddMMyy");
                }
            }
            catch (Exception)
            {
                return "Error message:-Wrong Parameter";
            }
        }


        public static string SpiceJetParse(string searchparam, string response, string fromsec, string tosec, string paxs, string fareType, string ftype, CookieContainer cookies, string merchantid)
        {
            try
            {
                string str = "";
                HtmlDocument document = new HtmlDocument
                {
                    OptionFixNestedTags = true
                };
                document.LoadHtml(response);
                string str2 = string.Empty;
                HtmlNode node = document.DocumentNode.SelectSingleNode("//div[@id='mainContent']");
                if (node != null)
                {
                    string innerHtml = node.InnerHtml;
                    document.LoadHtml(innerHtml);
                    HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//table[@class='availabilityTable']");
                    int index = 0;
                    Hashtable[] flttable = new Hashtable[nodes.Count];
                    Hashtable[] faretable = new Hashtable[nodes.Count];
                    foreach (HtmlNode node2 in (IEnumerable<HtmlNode>)nodes)
                    {
                        flttable[index] = new Hashtable();
                        faretable[index] = new Hashtable();
                        if (node2 != null)
                        {
                            string html = node2.InnerHtml;
                            HtmlDocument document2 = new HtmlDocument();
                            HtmlDocument document3 = new HtmlDocument
                            {
                                OptionFixNestedTags = true
                            };
                            document2.OptionFixNestedTags = true;
                            document2.LoadHtml(html);
                            HtmlNodeCollection nodes2 = document2.DocumentNode.SelectNodes(".//tr/@name");
                            if (nodes2 != null)
                            {
                                foreach (HtmlNode node3 in (IEnumerable<HtmlNode>)nodes2)
                                {
                                    if (node3 != null)
                                    {
                                        string str5 = node3.InnerHtml;
                                        document3.LoadHtml(str5);
                                        HtmlNode node4 = document3.DocumentNode.SelectSingleNode("//td[@class='fareCol2 fareColPad111 spicePlusColumn']");
                                        if (node4 != null)
                                        {
                                            foreach (HtmlNode node5 in (IEnumerable<HtmlNode>)node4.SelectNodes("//*[@type='radio']"))
                                            {
                                                if ((node5 != null) && (str2 == ""))
                                                {
                                                    str2 = node5.Attributes["value"].Value;
                                                }
                                                else if ((node5 != null) && (str2 != ""))
                                                {
                                                    str2 = str2 + "$" + node5.Attributes["value"].Value;
                                                }
                                            }
                                            string str6 = "";
                                            string str7 = "";
                                            string str8 = "";
                                            string[] strArray = str2.Split(new char[] { '$' });
                                            foreach (string str9 in strArray[0].Split(new char[] { '|' })[1].Split(new char[] { '^' }))
                                            {
                                                if (str9.Length > 0)
                                                {
                                                    string[] strArray2 = str9.Split(new char[] { '~' });
                                                    string str10 = strArray2[0] + " " + strArray2[1];
                                                    string str11 = strArray2[4];
                                                    string str12 = strArray2[6];
                                                    string str13 = ConvertDate(strArray2[5]);
                                                    string str14 = ConvertTime(strArray2[5]);
                                                    string str15 = ConvertDate(strArray2[7]);
                                                    string str16 = ConvertTime(strArray2[7]);
                                                    if (str8 == "")
                                                    {
                                                        str8 = str10;
                                                    }
                                                    else
                                                    {
                                                        str8 = str8 + "," + str10;
                                                    }
                                                    if (str6 == "")
                                                    {
                                                        str6 = str10 + "~" + str11 + "~" + str12 + "~" + str13 + "~" + str14 + "~" + str15 + "~" + str16;
                                                    }
                                                    else
                                                    {
                                                        string str20 = str6;
                                                        str6 = str20 + "^" + str10 + "~" + str11 + "~" + str12 + "~" + str13 + "~" + str14 + "~" + str15 + "~" + str16;
                                                    }
                                                }
                                            }
                                            flttable[index][str8] = str6;
                                            foreach (string str17 in strArray)
                                            {
                                                string fareQuoteSpiceYQAspx = GetFareQuoteSpiceYQAspx(str17, cookies);
                                                if (str7 == "")
                                                {
                                                    str7 = FareParse(fareQuoteSpiceYQAspx, paxs) + "~" + str17.Split(new char[] { '~' })[1] + "~" + str17.Split(new char[] { '~' })[3];
                                                }
                                                else
                                                {
                                                    string str21 = str7;
                                                    str7 = str21 + "^" + FareParse(fareQuoteSpiceYQAspx, paxs) + "~" + str17.Split(new char[] { '~' })[1] + "~" + str17.Split(new char[] { '~' })[3];
                                                }
                                            }
                                            faretable[index][str8] = str7;
                                            str2 = string.Empty;
                                        }
                                    }
                                }
                            }
                        }
                        index++;
                    }
                    str = new ParsingClass().ParsingSpice(searchparam, flttable, faretable);
                }
                return str;
            }
            catch (Exception)
            {
                return "()";
            }
        }

    }
}