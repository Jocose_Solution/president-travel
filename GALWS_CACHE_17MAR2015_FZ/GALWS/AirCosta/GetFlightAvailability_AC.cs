﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;
using STD.Shared;
namespace STD.BAL
{
    public class GetFlightAvailability_AC
    {
        string ConnStr = "";
        public GetFlightAvailability_AC()
        { }
        public GetFlightAvailability_AC(string ConnectionString)
        {
            ConnStr = ConnectionString;
        }
        public List<FlightSearchResults> AirCostaAvilability(FlightSearch searchInputs, List<FlightCityList> CityList, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, string UserId, string Password, string OfficeId, float srvCharge, bool IsRoundTrip, bool IsSplFare, string AirlineName)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            string exception = "";
            try
            {
                DataTable CommDt = new DataTable();
                Hashtable STTFTDS = new Hashtable();
                FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
                string AvailReqXML = GetFlightAvailability_REQ(UserId, Password, OfficeId, searchInputs, IsRoundTrip, IsSplFare);
                string AvailEncyptedREQ = Encrypt(AvailReqXML);
                string AvailEncyptedRES = SearchAvailability(AvailEncyptedREQ, "https://www.aircosta.in/AirCostaWS/wsFlight.asmx");
                string AvailEncyptedRESXML = Decrypt(AvailEncyptedRES);
                XDocument xdoc = XDocument.Parse(AvailEncyptedRESXML);
                string ErrorStatus = xdoc.Descendants("OnlineAvailabilityRS").Descendants("Errors").Attributes("Status").First().Value;
                if (ErrorStatus.Trim() == "FALSE")
                {
                    var FareAndFlight = from IMP in xdoc.Descendants("OnlineAvailabilityRS").Descendants("Flights")
                                        select new
                                        {
                                            Trip = IMP.Attribute("ReturnAvailability").Value,
                                            CabinName = IMP.Attribute("CabinName").Value,
                                            ActualOrigin = IMP.Attribute("Origin").Value,
                                            ActualDestination = IMP.Attribute("Destination").Value,
                                            FlightDetails = IMP.Descendants("FlightDetails")// from IMPFlight in IMP.Descendants("FlightDetails"),

                                        };
                    List<FlightSearchResults> ONEWAYFLIGHTLIST = new List<FlightSearchResults>();
                    List<FlightSearchResults> ROUNDTRIPLIST = new List<FlightSearchResults>();
                    //if (FareAndFlight) { }
                    foreach (var fareflighttrip in FareAndFlight)
                    {
                        int LineNo = 1;
                        foreach (var fltdtl in fareflighttrip.FlightDetails)
                        {
                            string TripType = "";
                            if (fareflighttrip.Trip == "0")
                            {
                                TripType = "OneWay";
                            }
                            else { TripType = "RoundTrip"; }
                            string ActualOrigin = ""; string ActualDestination = "";
                            ActualOrigin = fareflighttrip.ActualOrigin.Trim().ToUpper();
                            ActualDestination = fareflighttrip.ActualDestination.Trim().ToUpper();
                            var FlightSeg = from IMPFlight in fltdtl.Descendants("Flight")
                                            select new
                                            {
                                                SNO = IMPFlight.Attribute("OrderNo").Value,
                                                FlightNo = IMPFlight.Attribute("FlightNo").Value,
                                                FlightDate = IMPFlight.Attribute("FlightDate").Value,
                                                Origin = IMPFlight.Attribute("Origin").Value,
                                                Destination = IMPFlight.Attribute("Destination").Value,
                                                OriginCityName = IMPFlight.Attribute("OriginCityName").Value,
                                                DestinationCityName = IMPFlight.Attribute("DestinationCityName").Value,
                                                RBD = IMPFlight.Attribute("RBD").Value,
                                                StopsCount = IMPFlight.Attribute("StopsCount").Value,
                                                DepartureTime = IMPFlight.Attribute("DepartureTime").Value,
                                                ArrivalTime = IMPFlight.Attribute("ArrivalTime").Value,
                                                FlightDuration = IMPFlight.Attribute("FlightDuration").Value,
                                                DayChange = IMPFlight.Attribute("DayChange").Value,
                                            };
                            var FareAndTax = from FFare in fltdtl.Descendants("Fare")
                                             select new
                                             {
                                                 Basefare = FFare.Attribute("BaseFare").Value,
                                                 PaxType = FFare.Attribute("PaxType").Value,
                                                 Surcharge = FFare.Attribute("Surcharge").Value,
                                                 TotalTax = FFare.Attribute("Taxes").Value,
                                                 FareBasisCode = FFare.Attribute("FareBasisCode").Value,
                                                 CancellationAllowed = FFare.Attribute("Cancellation_Allowed").Value,
                                                 BaggageAllowed = FFare.Attribute("BaggageAllowed").Value,
                                                 taxes = from TaxItem in FFare.Descendants("Tax")
                                                         select new
                                                         {
                                                             TAXTYPE = TaxItem.Attribute("TaxNature").Value,
                                                             TAXAMT = TaxItem.Attribute("Amount").Value,
                                                             SERCHRG = TaxItem.Attribute("Surcharge").Value
                                                         }
                                             };
                            double BaseFareADT_PF = 0; double BaseFareCHD_PF = 0; double BaseFareINF_PF = 0; string Pax_Type_PF = ""; string TAXBADT_PF = ""; string TAXBCHD_PF = ""; string TAXBINF_PF = "";
                            double SurchargeADT_PF = 0; double SurchargeCHD_PF = 0; double SurchargeINF_PF = 0; double TOTALTAXADT_PF = 0; double TOTALTAXCHD_PF = 0; double TOTALTAXINF_PF = 0; string FareBasisCodeADT_PF = ""; string FareBasisCodeCHD_PF = ""; string FareBasisCodeINF_PF = ""; string TaxBreakupADT_PF = ""; string TaxBreakupCHD_PF = ""; string TaxBreakupINF_PF = "";
                            double IN_ADT_PF = 0; double TC_ADT_PF = 0; double JN_ADT_PF = 0; double YQ_ADT_PF = 0; double WO_ADT_PF = 0; double IC_ADT_PF = 0; double IN_CHD_PF = 0; double TC_CHD_PF = 0; double JN_CHD_PF = 0; double YQ_CHD_PF = 0; double WO_CHD_PF = 0; double IC_CHD_PF = 0; double IN_INF_PF = 0; double TC_INF_PF = 0; double JN_INF_PF = 0; double YQ_INF_PF = 0; double WO_INF_PF = 0; double IC_INF_PF = 0; string Refundable_ADT_PF = ""; string Refundable_CHD_PF = ""; string Refundable_INF_PF = ""; string Bag_ADT_PF = ""; string Bag_CHD_PF = ""; string Bag_INF_PF = "";
                            #region Tax and fare
                            //Tax and fare
                            foreach (var TaxB in FareAndTax)
                            {
                                string TAXBREAKUP = "";
                                Pax_Type_PF = TaxB.PaxType;
                                foreach (var tax in TaxB.taxes)
                                {
                                    TAXBREAKUP = TAXBREAKUP + tax.TAXTYPE + ":";
                                    TAXBREAKUP = TAXBREAKUP + tax.TAXAMT + "#";
                                }
                                string[] Taxstr = TAXBREAKUP.Split('#');
                                string[] Taxstr1 = { };
                                double IN = 0; double TC = 0; double JN = 0; double YQ = 0; double WO = 0; double IC = 0;
                                for (int i = 0; i <= Taxstr.Length - 2; i++)
                                {
                                    if (Taxstr[i].Contains("IN"))
                                    {
                                        Taxstr1 = Taxstr[i].Split(':');
                                        IN = IN + Convert.ToDouble(Taxstr1[1]);
                                    }
                                    else if (Taxstr[i].Contains("TC"))
                                    {
                                        Taxstr1 = Taxstr[i].Split(':');
                                        TC = TC + Convert.ToDouble(Taxstr1[1]);
                                    }
                                    else if (Taxstr[i].Contains("JN"))
                                    {
                                        Taxstr1 = Taxstr[i].Split(':');
                                        JN = JN + Convert.ToDouble(Taxstr1[1]);
                                    }
                                    else if (Taxstr[i].Contains("YQ"))
                                    {
                                        Taxstr1 = Taxstr[i].Split(':');
                                        YQ = YQ + Convert.ToDouble(Taxstr1[1]);
                                    }
                                    else if (Taxstr[i].Contains("WO"))
                                    {
                                        Taxstr1 = Taxstr[i].Split(':');
                                        WO = WO + Convert.ToDouble(Taxstr1[1]);
                                    }
                                    else if (Taxstr[i].Contains("IC"))
                                    {
                                        Taxstr1 = Taxstr[i].Split(':');
                                        IC = IC + Convert.ToDouble(Taxstr1[1]);
                                    }
                                }
                                if (Pax_Type_PF == "ADT")
                                {

                                    if (TaxB.CancellationAllowed.ToString().Trim() == "1") { Refundable_ADT_PF = "Refundable"; } else { Refundable_ADT_PF = "Non Refundable"; }
                                    Bag_ADT_PF = TaxB.BaggageAllowed.ToString().Trim();
                                    BaseFareADT_PF = Convert.ToDouble(TaxB.Basefare);
                                    SurchargeADT_PF = Convert.ToDouble(TaxB.Surcharge);
                                    TOTALTAXADT_PF = Convert.ToDouble(TaxB.TotalTax);
                                    FareBasisCodeADT_PF = TaxB.FareBasisCode;
                                    TAXBADT_PF = TAXBREAKUP;
                                    IN_ADT_PF = IN; TC_ADT_PF = TC; JN_ADT_PF = JN; YQ_ADT_PF = YQ; WO_ADT_PF = WO; IC_ADT_PF = IC;

                                }
                                else if (Pax_Type_PF == "CHD")
                                {
                                    if (TaxB.CancellationAllowed.ToString().Trim() == "1") { Refundable_CHD_PF = "Refundable"; } else { Refundable_CHD_PF = "Non Refundable"; }
                                    Bag_CHD_PF = TaxB.BaggageAllowed.ToString().Trim();
                                    BaseFareCHD_PF = Convert.ToDouble(TaxB.Basefare);
                                    SurchargeCHD_PF = Convert.ToDouble(TaxB.Surcharge);
                                    TOTALTAXCHD_PF = Convert.ToDouble(TaxB.TotalTax);
                                    FareBasisCodeCHD_PF = TaxB.FareBasisCode;
                                    TAXBCHD_PF = TAXBREAKUP;
                                    IN_CHD_PF = IN; TC_CHD_PF = TC; JN_CHD_PF = JN; YQ_CHD_PF = YQ; WO_CHD_PF = WO; IC_CHD_PF = IC;
                                }
                                else if (Pax_Type_PF == "INF")
                                {
                                    if (TaxB.CancellationAllowed.ToString().Trim() == "1") { Refundable_INF_PF = "Refundable"; } else { Refundable_INF_PF = "Non Refundable"; }
                                    Bag_INF_PF = TaxB.BaggageAllowed.ToString().Trim();
                                    BaseFareINF_PF = Convert.ToDouble(TaxB.Basefare);
                                    //PaxType = "INF";
                                    SurchargeINF_PF = Convert.ToDouble(TaxB.Surcharge);
                                    TOTALTAXINF_PF = Convert.ToDouble(TaxB.TotalTax);
                                    FareBasisCodeINF_PF = TaxB.FareBasisCode;
                                    TAXBINF_PF = TAXBREAKUP;
                                    IN_INF_PF = IN; TC_INF_PF = TC; JN_INF_PF = JN; YQ_INF_PF = YQ; WO_INF_PF = WO; IC_INF_PF = IC;
                                }




                            }
                            //End Tax and fare
                            #endregion Tax and fare
                            //Segment
                            //int StopCountint = 0; //FlightSeg.Count() - 1;
                            int StopCountint = Convert.ToInt32(Convert.ToInt32(FlightSeg.Count()) - 1 + Convert.ToInt32(fltdtl.Descendants("Flight").Attributes("StopsCount").First().Value));
                            int Leg = 1;
                            int TotFlightDuration = 0;
                            foreach (var FlightDur in FlightSeg)
                            {
                                TotFlightDuration = TotFlightDuration + Convert.ToInt32(FlightDur.FlightDuration.ToString().Trim());
                            }
                            foreach (var segment in FlightSeg)
                            {
                                string FlightNo = ""; string FlightDate = ""; string Origin = ""; string Destination = ""; string OriginCityName = ""; string DestinationCityName = "";
                                string RBD = ""; string StopsCount = ""; string DepartureTime = ""; string ArrivalTime = ""; string DayChange = ""; string FlightDuration = "";
                                string SNO = "";
                                SNO = segment.SNO.ToString().Trim();
                                FlightNo = segment.FlightNo.ToString().Trim();
                                FlightDate = segment.FlightDate.ToString().Trim();
                                Origin = segment.Origin.ToString().Trim();
                                Destination = segment.Destination.ToString().Trim();
                                OriginCityName = segment.OriginCityName.ToString().Trim();
                                DestinationCityName = segment.DestinationCityName.ToString().Trim();
                                RBD = segment.RBD.ToString().Trim();
                                StopsCount = segment.StopsCount.ToString().Trim();
                                DepartureTime = segment.DepartureTime.ToString().Trim();
                                ArrivalTime = segment.ArrivalTime.ToString().Trim();
                                FlightDuration = TotFlightDuration.ToString().Trim();
                                DayChange = segment.DayChange.ToString().Trim();

                                string SegmentReqXML = GetFlightInformation_REQ(UserId, Password, OfficeId, FlightNo, FlightDate, Origin, Destination);
                                string SegmentEncyptedREQ = Encrypt(SegmentReqXML);
                                string SegmentEncyptedRES = GetFlightInfo(SegmentEncyptedREQ, "https://www.aircosta.in/AirCostaWS/wsFlight.asmx");
                                string SegmentEncyptedRESXML = Decrypt(SegmentEncyptedRES);
                                XDocument xdocseg = XDocument.Parse(SegmentEncyptedRESXML);
                                string SegOrigin = ""; string SegDest = ""; string DepartureDateTime = ""; string DepartureTerminal = ""; string ArrivalDateTime = ""; string ArrivalTerminal = "";
                                string DepartureDateTime1 = ""; string DepartureTerminal1 = ""; string ArrivalDateTime1 = ""; string ArrivalTerminal1 = "";
                                var FlightSegment = from FSeg in xdocseg.Descendants("FlightInformationRS").Descendants("Flight").Descendants("Segment")
                                                    select new
                                                    {

                                                        SegOrigin = FSeg.Attribute("Origin").Value,
                                                        SegDest = FSeg.Attribute("Destination").Value,
                                                        SegOrderNo = FSeg.Attribute("OrderNo").Value,
                                                        //SegDtl = FSeg.Value 
                                                        
                                                        //SegOrigin = FSeg.Descendants("Segment").Attributes("Origin").First().Value,
                                                        //SegDest = FSeg.Descendants("Segment").Attributes("Destination").First().Value,
                                                        //SegOrderNo = FSeg.Descendants("Segment").Attributes("OrderNo").First().Value,
                                                        //SegDtl = FSeg.Descendants("Segment"),
                                                       // DepTimeSeg = FSeg.Descendants("Segment").Descendants("Scheduled").Attributes("DepartureDateTime").First().Value,
                                                        //ArrTimeSeg = FSeg.Descendants("Segment").Descendants("Scheduled").Attributes("ArrivalDateTime").First().Value
                                                       
                                                        SegDtl = from FSM in FSeg.Descendants("Scheduled")
                                                                 select new
                                                                 {
                                                                     DDTIME = FSM.Attribute("DepartureDateTime").Value,
                                                                     DT = FSM.Attribute("DepartureTerminal").Value,
                                                                     AATIME = FSM.Attribute("ArrivalDateTime").Value,
                                                                     AT = FSM.Attribute("ArrivalTerminal").Value
                                                                     
                                                                 }


                                                    };
                                
                                foreach (var segmentdtls in FlightSegment)
                                {
                                    SegOrigin = segmentdtls.SegOrigin.ToString();
                                    SegDest = segmentdtls.SegDest.ToString();
                                   //SegOrigin=segmentdtls.SegDtl.
                                    //var SegMainDtls = from FSegMain in segmentdtls.SegDtl
                                    //                  select new
                                    //                  {
                                    //                      DDTIME = FSegMain.Descendants("Scheduled").Attributes("DepartureDateTime").First().Value,
                                    //                      DT = FSegMain.Descendants("Scheduled").Attributes("DepartureTerminal").First().Value,
                                    //                      AATIME = FSegMain.Descendants("Scheduled").Attributes("ArrivalDateTime").First().Value,
                                    //                      AT = FSegMain.Descendants("Scheduled").Attributes("ArrivalTerminal").First().Value,
                                    //                      //DDTIME = FSegMain.Descendants("Scheduled").Attributes("DepartureDateTime").First().Value,
                                    //                      //DT = FSegMain.Descendants("Scheduled").Attributes("DepartureTerminal").First().Value,
                                    //                      //AATIME = FSegMain.Descendants("Scheduled").Attributes("ArrivalDateTime").First().Value,
                                    //                      //AT = FSegMain.Descendants("Scheduled").Attributes("ArrivalTerminal").First().Value,

                                    //                  };
                                    foreach (var segmaindtls in segmentdtls.SegDtl )
                                    {
                                        DepartureDateTime1 = segmaindtls.DDTIME.ToString();
                                        DepartureTerminal1 = segmaindtls.DT.ToString();
                                        ArrivalDateTime1 = segmaindtls.AATIME.ToString();
                                        ArrivalTerminal1 = segmaindtls.AT.ToString();
                                        #region FlightList
                                        // string SegOrigin = ""; string SegDest = ""; string DepartureDateTime = ""; string DepartureTerminal = ""; string ArrivalDateTime = ""; string ArrivalTerminal = "";
                                        //SegOrigin = Origin.ToString().Trim();
                                        //SegDest = Destination.ToString().Trim();
                                        DepartureDateTime = DepartureDateTime1.ToString().Trim();
                                        DepartureTerminal = DepartureTerminal1.ToString().Trim();
                                        ArrivalDateTime = ArrivalDateTime1.ToString().Trim();
                                        ArrivalTerminal = ArrivalTerminal1.ToString().Trim();
                                        #region ListCreation
                                        FlightSearchResults objFS = new FlightSearchResults();

                                        objFS.OrgDestFrom = ActualOrigin;
                                        objFS.OrgDestTo = ActualDestination;

                                        objFS.DepartureLocation = SegOrigin;
                                        objFS.DepartureCityName = getCityName(SegOrigin, CityList);
                                        objFS.DepAirportCode = "";
                                        objFS.DepartureTerminal = DepartureTerminal;
                                        string DepartureDate = Utility.Right(DepartureDateTime.Trim().Split(' ')[0].Trim(), 2) + Utility.Mid(DepartureDateTime.Trim().Split(' ')[0].Trim(), 4, 2) + Utility.Mid(DepartureDateTime.Trim().Split(' ')[0].Trim(), 2, 2);
                                        objFS.DepartureDate = DepartureDate;
                                        objFS.Departure_Date = Utility.Right(DepartureDateTime.Trim().Split(' ')[0], 2) + " " + Utility.datecon(Utility.Mid(DepartureDateTime.Trim().Split(' ')[0], 4, 2));
                                        objFS.DepartureTime = DepartureDateTime.Trim().Split(' ')[1];


                                        objFS.ArrivalLocation = SegDest;
                                        objFS.ArrivalCityName = getCityName(SegDest, CityList);
                                        objFS.ArrAirportCode = "";
                                        objFS.ArrivalTerminal = ArrivalTerminal;


                                        string ArrivalDate = Utility.Right(ArrivalDateTime.Trim().Split(' ')[0].Trim(), 2) + Utility.Mid(ArrivalDateTime.Trim().Split(' ')[0].Trim(), 4, 2) + Utility.Mid(ArrivalDateTime.Trim().Split(' ')[0].Trim(), 2, 2);
                                        objFS.ArrivalDate = ArrivalDate;
                                        objFS.Arrival_Date = Utility.Right(ArrivalDateTime.Trim().Split(' ')[0], 2) + " " + Utility.datecon(Utility.Mid(ArrivalDateTime.Trim().Split(' ')[0], 4, 2));
                                        objFS.ArrivalTime = ArrivalDateTime.Trim().Split(' ')[1];

                                        objFS.depdatelcc = DepartureDateTime.Trim().Split(' ')[0].Trim();
                                        objFS.arrdatelcc = ArrivalDateTime.Trim().Split(' ')[0].Trim();

                                        if (searchInputs.RTF == true)
                                            objFS.Sector = Utility.Left(searchInputs.HidTxtDepCity, 3) + ":" + Utility.Left(searchInputs.HidTxtArrCity, 3) + ":" + Utility.Left(searchInputs.HidTxtDepCity, 3);
                                        else
                                            objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                                        objFS.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(FlightDuration.Trim()));
                                        //objFS.Stops = StopCountint + "-Stop";
                                        objFS.Stops = StopCountint + "-Stop";

                                        objFS.Adult = searchInputs.Adult;
                                        objFS.Child = searchInputs.Child;
                                        objFS.Infant = searchInputs.Infant;
                                        string AilineCode = FlightNo.Trim().Substring(0, 2);
                                        objFS.TotPax = searchInputs.Adult + searchInputs.Child + searchInputs.Infant;
                                        objFS.MarketingCarrier = FlightNo.Trim().Substring(0, 2);
                                        objFS.OperatingCarrier = AilineCode;
                                        objFS.FlightIdentification = FlightNo.Trim().Remove(0, 2);
                                        objFS.sno = SNO.Trim();
                                        objFS.ValiDatingCarrier = AilineCode;
                                        objFS.AirLineName = AirlineName;
                                        objFS.AvailableSeats = "";
                                        objFS.AdtCabin = "E";
                                        objFS.ChdCabin = "";
                                        objFS.InfCabin = "";
                                        objFS.AdtRbd = "";
                                        objFS.ChdRbd = "";
                                        objFS.InfRbd = "";

                                        objFS.AdtFareType = Refundable_ADT_PF.Trim();
                                        objFS.AdtFarebasis = FareBasisCodeADT_PF.Trim();
                                        objFS.ChdfareType = Refundable_CHD_PF.Trim();//need to verify
                                        objFS.ChdFarebasis = FareBasisCodeCHD_PF.Trim();
                                        objFS.InfFareType = Refundable_INF_PF.Trim();//need to verify
                                        objFS.InfFarebasis = FareBasisCodeINF_PF.Trim();
                                        objFS.LineNumber = LineNo;
                                        objFS.Leg = Leg;
                                        if (TripType == "OneWay")
                                        {
                                            objFS.Flight = "1";
                                            objFS.FType = "OutBound";
                                            objFS.TripType = "O";
                                        }
                                        else
                                        {
                                            objFS.Flight = "2";
                                            objFS.FType = "InBound";
                                            objFS.TripType = "R";
                                        }
                                        objFS.fareBasis = FareBasisCodeADT_PF.Trim();
                                        objFS.FBPaxType = "ADT";
                                        objFS.RBD = RBD.Trim();

                                        double BaseFareADT = 0; double TaxADT = 0; double YQADT = 0;
                                        double BaseFareCHD = 0; double TaxCHD = 0; double YQCHD = 0;
                                        double BaseFareINF = 0; double TaxINF = 0; double YQINF = 0;
                                        double TotalFareADT = 0; double TotalFareCHD = 0; double TotalFareINF = 0;


                                        BaseFareADT = BaseFareADT_PF;
                                        TaxADT = TOTALTAXADT_PF;
                                        YQADT = YQ_ADT_PF;
                                        TotalFareADT = BaseFareADT + TaxADT + YQADT;
                                        objFS.AdtBreakPoint = "";
                                        objFS.AdtAvlStatus = "";
                                        objFS.BagInfo = Bag_ADT_PF.Trim() + "K";


                                        //FAREADT
                                        #region ADT
                                        if (searchInputs.Adult > 0)
                                        {
                                            //objFS.AdtFare=
                                            objFS.AdtFare = (float)(BaseFareADT + TaxADT + YQADT);
                                            objFS.AdtBfare = float.Parse(BaseFareADT.ToString());
                                            objFS.AdtTax = float.Parse(Convert.ToString(TaxADT + YQADT));
                                            objFS.AdtFSur = float.Parse(YQ_ADT_PF.ToString());
                                            objFS.AdtWO = float.Parse(WO_ADT_PF.ToString());
                                            objFS.AdtIN = float.Parse(IN_ADT_PF.ToString());
                                            objFS.AdtJN = float.Parse(JN_ADT_PF.ToString());
                                            objFS.AdtYR = 0;
                                            objFS.AdtOT = float.Parse(Convert.ToString(TaxADT - WO_ADT_PF - IN_ADT_PF - JN_ADT_PF)); ;// float.Parse(TaxADT.ToString()) + srvCharge;

                                            objFS.AdtQ = 0;

                                        }
                                        //SMS charge add 
                                        objFS.AdtOT = objFS.AdtOT + srvCharge;
                                        objFS.AdtTax = objFS.AdtTax + srvCharge;
                                        objFS.AdtFare = objFS.AdtFare + srvCharge;
                                        //SMS charge add end

                                        objFS.ADTAdminMrk = 0;//CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, AdtFare, searchInputs.Trip.ToString()); 
                                        objFS.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], AilineCode, objFS.AdtFare, searchInputs.Trip.ToString());
                                        objFS.ADTDistMrk = 0;
                                        CommDt.Clear();
                                        STTFTDS.Clear();
                                        //COMMISSION
                                        CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, "", objFS.AdtCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, "NRM", "");
                                        objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                        objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                        STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                                        objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                        objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                                        objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                        objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                        objFS.IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());                                        
                                        //COMMISSION
                                        if (searchInputs.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                                                objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }
                                        #endregion ADT
                                        #region CHD
                                        if (searchInputs.Child > 0)
                                        {
                                            BaseFareCHD = BaseFareCHD_PF;
                                            TaxCHD = TOTALTAXCHD_PF;
                                            YQCHD = YQ_CHD_PF;
                                            TotalFareCHD = BaseFareCHD + TaxCHD + YQCHD;

                                            objFS.ChdFare = (float)(TotalFareCHD);
                                            objFS.ChdBreakPoint = "";
                                            objFS.ChdAvlStatus = "";


                                            objFS.ChdBFare = float.Parse(BaseFareCHD.ToString());
                                            objFS.ChdTax = float.Parse(Convert.ToString(TaxCHD + YQCHD));
                                            objFS.ChdFSur = float.Parse(YQ_CHD_PF.ToString());
                                            objFS.ChdWO = float.Parse(WO_CHD_PF.ToString());
                                            objFS.ChdIN = float.Parse(IN_CHD_PF.ToString());
                                            objFS.ChdJN = float.Parse(JN_CHD_PF.ToString());
                                            objFS.ChdYR = 0;
                                            objFS.ChdOT = float.Parse(Convert.ToString(TaxCHD - WO_CHD_PF - IN_CHD_PF - JN_CHD_PF));

                                            objFS.ChdQ = 0;



                                            //SMS charge add 
                                            objFS.ChdOT = objFS.ChdOT + srvCharge;
                                            objFS.ChdTax = objFS.ChdTax + srvCharge;
                                            objFS.ChdFare = objFS.ChdFare + srvCharge;
                                            //SMS charge add end

                                            objFS.CHDAdminMrk = 0;//CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, AdtFare, searchInputs.Trip.ToString()); 
                                            objFS.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], AilineCode, objFS.ChdFare, searchInputs.Trip.ToString());
                                            objFS.CHDDistMrk = 0;
                                            CommDt.Clear();
                                            STTFTDS.Clear();
                                            //COMMISSION
                                            CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, "", objFS.ChdCabin, searchInputs.DepDate, objFS.OrgDestFrom + "-" + objFS.OrgDestTo, searchInputs.RetDate, objFS.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objFS.FlightIdentification, objFS.OperatingCarrier, objFS.MarketingCarrier, "NRM", "");
                                            objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                            objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                            STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                                            objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                            objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                                            objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                            objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                            //COMMISSION
                                            if (searchInputs.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                                                    objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }
                                        }

                                        #endregion CHD
                                        #region INF

                                        if (searchInputs.Infant > 0)
                                        {
                                            BaseFareINF = BaseFareINF_PF;
                                            TaxINF = TOTALTAXINF_PF;
                                            YQINF = YQ_INF_PF;
                                            TotalFareINF = BaseFareINF + TaxINF + YQINF;
                                            objFS.InfBreakPoint = "";
                                            objFS.InfAvlStatus = "";
                                            double InfTF = 0;

                                            objFS.InfBfare = float.Parse(Math.Round(BaseFareINF_PF, 0).ToString());
                                            objFS.InfFare = (float)(TotalFareINF);
                                            objFS.InfTax = float.Parse(Math.Round(TaxINF + YQ_INF_PF, 0).ToString());
                                            objFS.InfFSur = float.Parse(Math.Round(YQ_INF_PF, 0).ToString());
                                            objFS.InfIN = float.Parse(Math.Round(IN_INF_PF, 0).ToString());
                                            objFS.InfJN = float.Parse(Math.Round(JN_INF_PF, 0).ToString()); ;
                                            objFS.InfOT = float.Parse(Math.Round(TaxINF - IN_INF_PF - JN_INF_PF, 0).ToString()); ;
                                            //objFS.InfOT = 0;
                                            objFS.InfQ = 0;
                                            objFS.InfAdminMrk = 0;
                                            objFS.InfAgentMrk = 0;
                                            objFS.InfSrvTax = 0;
                                            objFS.InfTds = 0;
                                            objFS.InfCB = 0;
                                            if (searchInputs.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, AilineCode, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                                                    objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }
                                        }
                                        else
                                        {
                                            objFS.InfFare = 0;
                                            objFS.InfBfare = 0;
                                            objFS.InfFSur = 0;
                                            objFS.InfIN = 0;
                                            objFS.InfJN = 0;
                                            objFS.InfOT = 0;
                                            objFS.InfQ = 0;
                                        }
                                        #endregion INF

                                        #region OTHER
                                        objFS.OriginalTF = float.Parse((TotalFareADT * objFS.Adult + TotalFareCHD * objFS.Child).ToString());//Without Infant Fare
                                        objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                                        objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                                        objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                                        objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                                        objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                                        objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                                        objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                                        objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                                        objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                                        objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                                        objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                                        if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                                            objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                                        else
                                            objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                                        objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                                        objFS.Trip = searchInputs.Trip.ToString();

                                        objFS.IsCorp = searchInputs.IsCorp;
                                        objFS.Provider = "LCC";
                                        objFS.OriginalTT = srvCharge;
                                        objFS.Searchvalue = objFS.OrgDestFrom + objFS.OrgDestTo + objFS.depdatelcc + DateTime.Now.ToString();
                                        #endregion OTHER
                                        Leg = Leg + 1;
                                        if (TripType == "OneWay")
                                        {
                                            ONEWAYFLIGHTLIST.Add(objFS);
                                        }
                                        else
                                        {
                                            ROUNDTRIPLIST.Add(objFS);
                                        }

                                        //string strData = req;
                                        //string strData1 = res;
                                        //string FullPath = @"E:\Req_Res_AirVentura\" + "AVLREQ_" + objFS.Searchvalue.Replace(":", "").Replace("/", "") + ".xml";
                                        //string FullPath1 = @"E:\Req_Res_AirVentura\" + "AVLRES_" + objFS.Searchvalue.Replace(":", "").Replace("/", "") + ".xml";
                                        //SaveTextToFile(strData, FullPath);
                                        //SaveTextToFile(strData1, FullPath1);

                                        #endregion ListCreation
                                        #endregion FlightList
                                    }

                                }

                                 //DepartureDateTime1 = xdocseg.Descendants("Flight").Descendants("Segment").Descendants("Scheduled").Attributes("DepartureDateTime").First().Value;
                                 //DepartureTerminal1 = xdocseg.Descendants("Flight").Descendants("Segment").Descendants("Scheduled").Attributes("DepartureTerminal").First().Value;
                                 //ArrivalDateTime1 = xdocseg.Descendants("Flight").Descendants("Segment").Descendants("Scheduled").Attributes("ArrivalDateTime").First().Value;
                                 //ArrivalTerminal1 = xdocseg.Descendants("Flight").Descendants("Segment").Descendants("Scheduled").Attributes("ArrivalTerminal").First().Value;
                              
                                
                                


                            }
                            //End Segment

                            LineNo = LineNo + 1;
                        }

                    }
                    if (IsRoundTrip == false && IsSplFare == false)
                    {
                        Final = (ONEWAYFLIGHTLIST);
                    }
                    if (IsRoundTrip == true && IsSplFare == false)
                    {
                        Final = (ONEWAYFLIGHTLIST);
                    }

                    if (IsRoundTrip == true && IsSplFare == true)
                    {
                        if (ONEWAYFLIGHTLIST.Count > 0 && ROUNDTRIPLIST.Count > 0)
                            Final = MergeRoundTripFare(ONEWAYFLIGHTLIST, ROUNDTRIPLIST);

                    }

                }
            }
            catch (Exception ex)
            {
                exception = ex.ToString();
            }



            return Final;

        }
        //private string Encrypt(string XML)
        //{
        //    StringBuilder EncryptREQ = new StringBuilder();
        //    EncryptREQ.Append("<?xml version='1.0' encoding='utf-8'?>");
        //    EncryptREQ.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
        //    EncryptREQ.Append("<soap:Body>");
        //    EncryptREQ.Append("<Encrypt xmlns='http://tempuri.org/'>");
        //    EncryptREQ.Append("<EncryptionXML><![CDATA[" + XML + "]]></EncryptionXML>");
        //    //EncryptREQ.Append("<EncryptionXML>" + XML + "</EncryptionXML>");
        //    EncryptREQ.Append("</Encrypt>");
        //    EncryptREQ.Append("</soap:Body>");
        //    EncryptREQ.Append("</soap:Envelope>");
        //    string EncryptedXML = PostXml("http://b2b..com/AIRCOSTACRYPTOGRAPHY/AIRCOSTACRYPTOGRAPHY.asmx", EncryptREQ.ToString());
        //    return EncryptedXML;
        //}
        //private string Decrypt(string XML)
        //{
        //    StringBuilder DecryptREQ = new StringBuilder();
        //    DecryptREQ.Append("<?xml version='1.0' encoding='utf-8'?>");
        //    DecryptREQ.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
        //    DecryptREQ.Append("<soap:Body>");
        //    DecryptREQ.Append("<Decrypt xmlns='http://tempuri.org/'>");
        //    DecryptREQ.Append("<DecryptionXML>" + XML + "</DecryptionXML>");
        //    DecryptREQ.Append("</Decrypt>");
        //    DecryptREQ.Append("</soap:Body>");
        //    DecryptREQ.Append("</soap:Envelope>");
        //    string DecryptedXML = PostXml("http://b2b.looknbook.com/AIRCOSTACRYPTOGRAPHY/AIRCOSTACRYPTOGRAPHY.asmx", DecryptREQ.ToString());
        //    return DecryptedXML;
        //}
        private string Encrypt(string XML)
        {
            // AIRS.Cryptography.scCryptography
            //Object objString = (Object)XML;
            AIRS.Cryptography.scCryptography scCrypt = new AIRS.Cryptography.scCryptography();
            //string EncryptREQ = scCrypt.Encrypt(XML);
            object EncryptREQ = scCrypt.Encrypt(XML);
            return EncryptREQ.ToString();
        }

        private string Decrypt(string XML)
        {
            AIRS.Cryptography.scCryptography scDecrypt = new AIRS.Cryptography.scCryptography();
            string DecryptREQ = scDecrypt.Decrypt(XML);
            return DecryptREQ;
        }
        private string PostXml(string url, string xml)
        {
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(xml);
            string strResult = string.Empty;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "text/xml";
                request.KeepAlive = false;
                request.Timeout = 3600 * 1000;
                request.ReadWriteTimeout = 3600 * 1000;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        StreamReader reader = null;
                        Stream responseStream = response.GetResponseStream();
                        reader = new StreamReader(responseStream);
                        strResult = reader.ReadToEnd();
                        response.Close();
                        responseStream.Close();
                        reader.Close();
                        xmldoc.LoadXml(strResult);
                        strResult = xmldoc.InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.ToString();
            }
            return strResult;
        }
        private string GetFlightAvailability_REQ(string UserId, string Password, string OfficeId, FlightSearch SearchInput, bool IsRoundTrip, bool IsSplFare)
        {
            StringBuilder obj_SB = new StringBuilder();
            try
            {

                string ReturnDate = "";
                string[] depDate = SearchInput.DepDate.Split('/');
                string DepartureDate = depDate[2].ToString().Trim() + depDate[1].ToString().Trim() + depDate[0].ToString().Trim();


                obj_SB.Append("<OnlineAvailabilityRQ>");
                obj_SB.Append("<Authenticate UserLogin='" + UserId + "' Password='" + Password + "' OfficeId='" + OfficeId + "' />");
                if (IsRoundTrip == false && IsSplFare == false)
                {
                    obj_SB.Append("<Flight Origin='" + SearchInput.HidTxtDepCity.ToString().Trim().Split(',')[0] + "' Destination='" + SearchInput.HidTxtArrCity.ToString().Trim().Split(',')[0] + "' FlightDate='" + DepartureDate + "' DepartureTimeFrom='' DepartureTimeTo='' />");

                }
                if (IsRoundTrip == true && IsSplFare == false)
                {
                    string[] arrDate = SearchInput.RetDate.Split('/');
                    ReturnDate = arrDate[2].ToString().Trim() + arrDate[1].ToString().Trim() + arrDate[0].ToString().Trim();
                    obj_SB.Append("<Flight Origin='" + SearchInput.HidTxtArrCity.ToString().Trim().Split(',')[0] + "' Destination='" + SearchInput.HidTxtDepCity.ToString().Trim().Split(',')[0] + "' FlightDate='" + ReturnDate + "' DepartureTimeFrom='' DepartureTimeTo='' />");
                }
                if (IsRoundTrip == true && IsSplFare == true)
                {
                    obj_SB.Append("<Flight Origin='" + SearchInput.HidTxtDepCity.ToString().Trim().Split(',')[0] + "' Destination='" + SearchInput.HidTxtArrCity.ToString().Trim().Split(',')[0] + "' FlightDate='" + DepartureDate + "' DepartureTimeFrom='' DepartureTimeTo='' />");
                    string[] arrDate = SearchInput.RetDate.Split('/');
                    ReturnDate = arrDate[2].ToString().Trim() + arrDate[1].ToString().Trim() + arrDate[0].ToString().Trim();

                }
                obj_SB.Append("<Pax AdultCount='" + SearchInput.Adult.ToString() + "' ChildCount='" + SearchInput.Child.ToString() + "' InfantCount='" + SearchInput.Infant.ToString() + "' />");
                obj_SB.Append("<ReturnFlight ReturnDate='" + ReturnDate.Trim() + "' ReturnTimeFrom='' ReturnTimeTo='' />");

                obj_SB.Append("<DepartureFare BaseFareAdult='' BaseFareChild='' BaseFareInfant='' />");
                obj_SB.Append("<ReturnFare BaseFareAdult='' BaseFareChild='' BaseFareInfant='' />");
                obj_SB.Append("<Others CabinCode='Y' Nationality='IN' />");
                obj_SB.Append("</OnlineAvailabilityRQ>");
            }
            catch (Exception ex)
            {
            }
            return obj_SB.ToString();
        }
        private string GetFlightInformation_REQ(string UserId, string Password, string OfficeId, string FlightNo, string FlightDate, string Origin, string Destination)
        {
            StringBuilder obj_FlightInfo = new StringBuilder();
            try
            {
                obj_FlightInfo.Append("<FlightInformationRQ>");
                obj_FlightInfo.Append("<Authenticate UserLogin='" + UserId + "' Password='" + Password + "' OfficeId='" + OfficeId + "' />");
                obj_FlightInfo.Append("<FlightNumber>" + FlightNo + "</FlightNumber>");
                obj_FlightInfo.Append("<FlightDate>" + FlightDate + "</FlightDate>");
                obj_FlightInfo.Append("<Origin>" + Origin + "</Origin>");
                obj_FlightInfo.Append("<Destination>" + Destination + "</Destination>");
                obj_FlightInfo.Append("</FlightInformationRQ>");
            }
            catch (Exception ex)
            {
            }
            return obj_FlightInfo.ToString();
        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }
                if (airMrkArray.Length > 0)
                {
                    if (Trip == "I")
                    {
                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                        {
                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                        }
                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                        {
                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                        }
                    }
                    else
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00" + " hrs" + min.ToString("00") + " min";
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + " hrs" + " " + rmin.ToString("00") + " min";
            }

            return rslt;

        }
        public List<FlightSearchResults> MergeRoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR)
        {
            int ln = 1;//For Total Line No.
            int k = 1;
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            ArrayList Comb = new ArrayList();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            while (k <= LnOb)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();
                int l = LnIb;
                while (l <= LnIb)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    List<FlightSearchResults> st;
                    st = Merge(OB, IB, ln);
                    foreach (FlightSearchResults item in st)
                    {
                        Final.Add(item);
                    }
                    ln++;///Increment Total Ln
                    l++;//Increment IB Ln
                }
                k++; //Increment OW Ln
            }
            //Comb.Add(Final);
            return Final;
        }
        public List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0,
                ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtDiscount1 = 0, AdtCB = 0, AdtSrvTax = 0, AdtSrvTax1 = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0,
                CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdDiscount1 = 0, ChdCB = 0, ChdSrvTax = 0, ChdSrvTax1 = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0,
             InfSrvTax = 0, InfTF = 0;

            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax;

            ADTAdminMrk = ADTAdminMrk + item.ADTAdminMrk + itemib.ADTAdminMrk;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount;
            AdtDiscount1 = AdtDiscount1 + item.AdtDiscount1 + itemib.AdtDiscount1;
            AdtCB = AdtCB + item.AdtCB + itemib.AdtCB;
            AdtSrvTax = AdtSrvTax + item.AdtSrvTax + itemib.AdtSrvTax;
            AdtSrvTax1 = AdtSrvTax1 + item.AdtSrvTax1 + itemib.AdtSrvTax1;
            AdtTF = AdtTF + item.AdtTF + itemib.AdtTF;
            AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            IATAComm = IATAComm + item.IATAComm + itemib.IATAComm;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax;

            CHDAdminMrk = CHDAdminMrk + item.CHDAdminMrk + itemib.CHDAdminMrk;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
            ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
            ChdDiscount1 = ChdDiscount1 + item.ChdDiscount1 + itemib.ChdDiscount1;
            ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
            ChdSrvTax = ChdSrvTax + item.ChdSrvTax + itemib.ChdSrvTax;
            ChdSrvTax1 = ChdSrvTax1 + item.ChdSrvTax1 + itemib.ChdSrvTax1;
            ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
            ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;
            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant);

            float STax = (AdtSrvTax * Adult) + (ChdSrvTax * Child) + (InfSrvTax * Infant);
            float TFee = (AdtTF * Adult) + (ChdTF * Child) + (InfTF * Infant);
            float TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            float TotCB = (AdtCB * Adult) + (ChdCB * Child);
            float TotTds = (AdtTds * Adult) + (ChdTds * Child);// +InfTds;
            float TotMrkUp = (ADTAdminMrk * Adult) + (ADTAgentMrk * Adult) + (CHDAdminMrk * Child) + (CHDAgentMrk * Child);
            TotalFare = TotalFare + TotMrkUp + STax + TFee;
            float NetFare = (TotalFare + TotTds) - (TotDis + TotCB + (ADTAgentMrk * Adult) + (CHDAgentMrk * Child));

            #endregion
            foreach (FlightSearchResults a in OB)
            {
                var PrcF = (FlightSearchResults)a.Clone();
                PrcF.LineNumber = Ln;
                #region Adult
                PrcF.AdtFSur = AdtFSur;
                PrcF.AdtIN = AdtIN;
                PrcF.AdtJN = AdtJN;
                PrcF.AdtYR = AdtYR;
                PrcF.AdtBfare = AdtBfare;
                PrcF.AdtOT = AdtOT;
                PrcF.AdtFare = AdtFare;
                PrcF.AdtTax = AdtTax;
                PrcF.AdtWO = AdtWO;

                PrcF.ADTAdminMrk = ADTAdminMrk;
                PrcF.ADTAgentMrk = ADTAgentMrk;
                PrcF.AdtDiscount = AdtDiscount;
                PrcF.AdtDiscount1 = AdtDiscount1;
                PrcF.AdtCB = AdtCB;
                PrcF.AdtSrvTax = AdtSrvTax;
                PrcF.AdtSrvTax1 = AdtSrvTax1;
                PrcF.AdtTF = AdtTF;
                PrcF.AdtTds = AdtTds;
                PrcF.IATAComm = IATAComm;
                #endregion

                #region Child
                PrcF.ChdFSur = ChdFSur;
                PrcF.ChdWO = ChdWO;
                PrcF.ChdIN = ChdIN;
                PrcF.ChdJN = ChdJN;
                PrcF.ChdYR = ChdYR;
                PrcF.ChdBFare = ChdBFare;
                PrcF.ChdOT = ChdOT;
                PrcF.ChdFare = ChdFare;
                PrcF.ChdTax = ChdTax;

                PrcF.CHDAdminMrk = CHDAdminMrk;
                PrcF.CHDAgentMrk = CHDAgentMrk;
                PrcF.ChdDiscount = ChdDiscount;
                PrcF.ChdDiscount1 = ChdDiscount1;
                PrcF.ChdCB = ChdCB;
                PrcF.ChdSrvTax = ChdSrvTax;
                PrcF.ChdSrvTax1 = ChdSrvTax1;
                PrcF.ChdTF = ChdTF;
                PrcF.ChdTds = ChdTds;
                #endregion

                #region Infant
                PrcF.InfFare = InfFare;
                PrcF.InfBfare = InfBfare;
                PrcF.InfFSur = InfFSur;
                PrcF.InfIN = InfIN;
                PrcF.InfJN = InfJN;
                PrcF.InfOT = InfOT;
                PrcF.InfQ = InfQ;
                PrcF.InfTax = InfTax;
                #endregion

                #region Total
                PrcF.TotBfare = TotBfare;
                PrcF.TotalTax = TotalTax;
                PrcF.TotalFuelSur = TotalFuelSur;
                PrcF.TotalFare = TotalFare;
                PrcF.NetFare = NetFare;
                PrcF.STax = STax;
                PrcF.TFee = TFee;
                PrcF.TotDis = TotDis;
                PrcF.TotTds = TotTds;
                PrcF.TotMrkUp = TotMrkUp;
                PrcF.OriginalTF = OriginalTF;
                PrcF.OriginalTT = OriginalTT;
                #endregion
                Final.Add(PrcF);

            }

            foreach (FlightSearchResults b in IB)
            {
                var PrcF = (FlightSearchResults)b.Clone();
                PrcF.LineNumber = Ln;

                #region Adult
                PrcF.AdtFSur = AdtFSur;
                PrcF.AdtIN = AdtIN;
                PrcF.AdtJN = AdtJN;
                PrcF.AdtYR = AdtYR;
                PrcF.AdtBfare = AdtBfare;
                PrcF.AdtOT = AdtOT;
                PrcF.AdtFare = AdtFare;
                PrcF.AdtTax = AdtTax;
                PrcF.AdtWO = AdtWO;

                PrcF.ADTAdminMrk = ADTAdminMrk;
                PrcF.ADTAgentMrk = ADTAgentMrk;
                PrcF.AdtDiscount = AdtDiscount;
                PrcF.AdtDiscount1 = AdtDiscount1;
                PrcF.AdtCB = AdtCB;
                PrcF.AdtSrvTax = AdtSrvTax;
                PrcF.AdtSrvTax1 = AdtSrvTax1;
                PrcF.AdtTF = AdtTF;
                PrcF.AdtTds = AdtTds;
                PrcF.IATAComm = IATAComm;
                #endregion

                #region Child
                PrcF.ChdFSur = ChdFSur;
                PrcF.ChdWO = ChdWO;
                PrcF.ChdIN = ChdIN;
                PrcF.ChdJN = ChdJN;
                PrcF.ChdYR = ChdYR;
                PrcF.ChdBFare = ChdBFare;
                PrcF.ChdOT = ChdOT;
                PrcF.ChdFare = ChdFare;
                PrcF.ChdTax = ChdTax;

                PrcF.CHDAdminMrk = CHDAdminMrk;
                PrcF.CHDAgentMrk = CHDAgentMrk;
                PrcF.ChdDiscount = ChdDiscount;
                PrcF.ChdDiscount1 = ChdDiscount1;
                PrcF.ChdCB = ChdCB;
                PrcF.ChdSrvTax = ChdSrvTax;
                PrcF.ChdSrvTax1 = ChdSrvTax1;
                PrcF.ChdTF = ChdTF;
                PrcF.ChdTds = ChdTds;
                #endregion

                #region Infant
                PrcF.InfFare = InfFare;
                PrcF.InfBfare = InfBfare;
                PrcF.InfFSur = InfFSur;
                PrcF.InfIN = InfIN;
                PrcF.InfJN = InfJN;
                PrcF.InfOT = InfOT;
                PrcF.InfQ = InfQ;
                PrcF.InfTax = InfTax;
                #endregion

                #region Total
                PrcF.TotBfare = TotBfare;
                PrcF.TotalTax = TotalTax;
                PrcF.TotalFuelSur = TotalFuelSur;
                PrcF.TotalFare = TotalFare;
                PrcF.NetFare = NetFare;

                PrcF.STax = STax;
                PrcF.TFee = TFee;
                PrcF.TotDis = TotDis;
                PrcF.TotTds = TotTds;
                PrcF.TotMrkUp = TotMrkUp;
                PrcF.OriginalTF = OriginalTF;
                PrcF.OriginalTT = OriginalTT;
                #endregion

                Final.Add(PrcF);
            }

            return Final;
        }
        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }
        private string SearchAvailability(string XML, string SoapURL)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            StringBuilder.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
            StringBuilder.Append("<soap:Body>");
            StringBuilder.Append("<SearchAvailability xmlns='http://localhost/AirsWebServicesTP4/wsFlight'>");
            StringBuilder.Append("<strInput>" + XML + "</strInput>");
            StringBuilder.Append("</SearchAvailability>");
            StringBuilder.Append("</soap:Body>");
            StringBuilder.Append("</soap:Envelope>");
            string SearchAvailXML = PostXml(SoapURL, StringBuilder.ToString());
            return SearchAvailXML;

        }
        private string GetFlightInfo(string XML, string SoapURL)
        {
            StringBuilder StringBuilder = new StringBuilder();
            StringBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            StringBuilder.Append("<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
            StringBuilder.Append("<soap:Body>");
            StringBuilder.Append("<GetFlightInfo xmlns='http://localhost/AirsWebServicesTP4/wsFlight'>");
            StringBuilder.Append("<strInput>" + XML + "</strInput>");
            StringBuilder.Append("</GetFlightInfo>");
            StringBuilder.Append("</soap:Body>");
            StringBuilder.Append("</soap:Envelope>");
            string SearchAvailXML = PostXml(SoapURL, StringBuilder.ToString());
            return SearchAvailXML;

        }
    }
}
