﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STD.Shared
{
    /// <summary>
    /// Summary description for CredentialList
    /// </summary>
    public class CredentialList
    {
        public string CorporateID { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string LoginID { get; set; }
        public string LoginPWD { get; set; }
        public string AvailabilityURL { get; set; }
        public string BookingURL { get; set; }
        public Int32 Port { get; set; }
        public string Provider { get; set; }
        public string APISource { get; set; }
        public string CarrierAcc { get; set; }
        public string ServerIP { get; set; }
        public double INFBasic { get; set; }
        public double INFTax { get; set; }
        public string SearchType { get; set; }

        public bool Status { get; set; }
        public string ResultFrom { get; set; }
        public string CrdType { get; set; }

        public string Trip { get; set; }
        public string AirlineCode { get; set; }

        public bool Cache { get; set; }
        public bool WebResult { get; set; }


    }
}