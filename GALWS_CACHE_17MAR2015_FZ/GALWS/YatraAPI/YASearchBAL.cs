﻿using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace STD.BAL
{
    public class YASearchBAL
    {

        string IP;
        public YASearchBAL(string ip)
        {

            IP = ip;

        }

        public List<FlightSearchResults> GetYAAvailibility(FlightSearch objFlt, bool isSplFare, string fareType, bool isLCC, string svcUrl, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge)
        {
            List<FlightSearchResults> finalResult = new List<FlightSearchResults>();
            string exep = "";
            YANimbleRQ objNimble = new YANimbleRQ();
            string nimbleRes = objNimble.GetNimbleRequest(objFlt, isSplFare, fareType, isLCC, svcUrl, MedthodSVCUrl.NimbleMTHD, ref exep);
            if (!string.IsNullOrEmpty(nimbleRes))
            {
                string resp = YASearchRequest(objFlt, isSplFare, fareType, isLCC, svcUrl, MedthodSVCUrl.SrchMTHD, ref exep, nimbleRes);
                finalResult = ParseSearchResponse(resp, objFlt, isSplFare, SrvchargeList, MarkupDs, constr, srvCharge);
            }
            return finalResult;

        }


        private List<FlightSearchResults> ParseSearchResponse(string responseXml, FlightSearch searchInput, bool isSplFare, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge)
        {
            List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();

            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            try
            {

                string str = responseXml.Replace("xmlns:soapenv='\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                    .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                    .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                    .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                    .Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                    .Replace("ns1:", "");

                XDocument xmlDoc = XDocument.Parse(str);
                IEnumerable<XElement> xlFPricedItinerary = null;

                if (isSplFare)
                {
                    xlFPricedItinerary = from el in xmlDoc.Descendants("OTA_AirLowFareSearchRS").Descendants("PricedItineraries").Descendants("PricedItinerary")
                                         where el.Attribute("ReturnOnly").Value.Trim().ToLower() == "true"
                                         select el;
                }
                else
                {
                    xlFPricedItinerary = from el in xmlDoc.Descendants("OTA_AirLowFareSearchRS").Descendants("PricedItineraries").Descendants("PricedItinerary")
                                         select el;
                }



                for (int ptn = 0; ptn < xlFPricedItinerary.Count(); ptn++)
                {

                    IEnumerable<XElement> xlFAirODItnry = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItinerary").Descendants("OriginDestinationOptions");

                    IEnumerable<XElement> xlFAirPriceItnry = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo");

                    IEnumerable<XElement> xlFAirODItnries = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItinerary");//.Descendants("OriginDestinationOptions").Descendants("OriginDestinationOption");

                    #region Origin Destination

                    for (int odn = 0; odn < xlFAirODItnry.Count(); odn++)
                    {


                        IEnumerable<XElement> segmnt = xlFAirODItnry.ElementAt(odn).Descendants("FlightSegment");


                        for (int s = 0; s < segmnt.Count(); s++)
                        {

                            XElement seg = segmnt.ElementAt(s);

                            FlightSearchResults fsr = new FlightSearchResults();


                            fsr.Leg = odn + 1;
                            fsr.LineNumber = ptn + 1;
                            fsr.TotDur = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("Duration").Value.ToString();


                            xlFAirODItnries.ToList().ForEach(x => { fsr.Searchvalue = fsr.Searchvalue + x.ToString(); });

                            xlFAirPriceItnry.ToList().ForEach(x => { fsr.sno = fsr.sno + x.ToString(); });

                            //if (searchInput.Trip == Trip.I)
                            //{
                            //    flight = seg.TripIndicator;
                            //}
                            //if (isSplFare)
                            //{
                            //    fsr.Flight = (odn + 1).ToString();
                            //}
                            //else
                            //{
                            fsr.Flight = Convert.ToInt32(Math.Round(Convert.ToDecimal(xlFPricedItinerary.ElementAt(ptn).Attribute("SequenceNumber").Value.Trim()))).ToString();
                            //}


                            fsr.Stops = (segmnt.Count() - 1).ToString() + "-Stop";

                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;


                            fsr.depdatelcc = seg.Attribute("DepartureDateTime").Value.Trim();
                            fsr.arrdatelcc = seg.Attribute("ArrivalDateTime").Value.Trim();
                            fsr.Departure_Date = Convert.ToDateTime(fsr.depdatelcc).ToString("dd MMM"); ;// legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                            fsr.Arrival_Date = Convert.ToDateTime(fsr.arrdatelcc).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                            fsr.DepartureDate = Convert.ToDateTime(fsr.depdatelcc).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(fsr.arrdatelcc).ToString("ddMMyy");


                            fsr.FlightIdentification = seg.Attribute("FlightNumber").Value.Trim();
                            fsr.AirLineName = seg.Element("MarketingAirline").Attribute("Name").Value.Trim();

                            try { fsr.ValiDatingCarrier = seg.Element("ValidatingCarrier").Attribute("Code").Value.Trim(); }
                            catch { fsr.ValiDatingCarrier = seg.Element("MarketingAirline").Attribute("Code").Value.Trim(); }

                            fsr.OperatingCarrier = fsr.ValiDatingCarrier;
                            fsr.MarketingCarrier = seg.Element("MarketingAirline").Attribute("Code").Value.Trim();//seg.Airline.OperatingCarrier.Trim();
                            fsr.fareBasis = seg.Attribute("ResBookDesigCode").Value.Trim();

                            try
                            {

                                fsr.AdtFar = xlFPricedItinerary.ElementAt(ptn).Attribute("ApplyMarkup").Value.Trim(); //seg.Element("BookingClassAvail").Attribute("WebFareName").Value.ToString();
                               
                               
                            }
                            catch { fsr.AdtFar = ""; }


                            fsr.DepartureLocation = seg.Element("DepartureAirport").Attribute("LocationCode").Value.Trim();
                            fsr.DepartureCityName = seg.Element("DepartureAirport").Attribute("CityName").Value.Trim();
                            fsr.DepartureTime = Convert.ToDateTime(fsr.depdatelcc).ToString("HHmm");
                            fsr.DepartureAirportName = seg.Element("DepartureAirport").Attribute("AirPortName").Value.Trim();
                            try { fsr.DepartureTerminal = seg.Element("DepartureAirport").Attribute("Terminal").Value.Trim(); }
                            catch { }

                            fsr.DepAirportCode = seg.Element("DepartureAirport").Attribute("LocationCode").Value.Trim();

                            fsr.ArrivalLocation = seg.Element("ArrivalAirport").Attribute("LocationCode").Value.Trim();
                            fsr.ArrivalCityName = seg.Element("ArrivalAirport").Attribute("CityName").Value.Trim();
                            fsr.ArrivalTime = Convert.ToDateTime(fsr.arrdatelcc).ToString("HHmm");
                            fsr.ArrivalAirportName = seg.Element("ArrivalAirport").Attribute("AirPortName").Value.Trim();

                            try { fsr.ArrivalTerminal = seg.Element("ArrivalAirport").Attribute("Terminal").Value.Trim(); }
                            catch { }

                            fsr.ArrAirportCode = seg.Element("ArrivalAirport").Attribute("LocationCode").Value.Trim();
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.EQ = seg.Element("Equipment").Attribute("AirEquipType").Value.Trim();
                            fsr.Provider = "YA";
                            fsr.AdtFareType = xlFPricedItinerary.ElementAt(ptn).Attribute("FareType").Value.Trim();
                            //fsr.TotDur = GetTimeInHrsAndMin(seg.Duration);



                            IEnumerable<XElement> farebrkup = xlFAirPriceItnry.Descendants("PTC_FareBreakdowns").Descendants("PTC_FareBreakdown");


                            fsr.AdtCabin = xlFAirODItnry.ElementAt(odn).Descendants("FormData").Descendants("FareDifference").Descendants("TotalFare").FirstOrDefault().Attribute("Cabin").Value.Trim();
                            if (fsr.AdtCabin.ToLower().Trim()=="economy")
                            { fsr.AdtCabin = "Y"; }
                            else if (fsr.AdtCabin.ToLower().Trim()=="business")
                            { fsr.AdtCabin = "C"; }
                            
                            fsr.ChdCabin = fsr.AdtCabin;
                            fsr.InfCabin = fsr.AdtCabin;
                            if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                            {

                                fsr.AdtAvlStatus = xlFAirODItnry.ElementAt(odn).Descendants("FormData").Descendants("FBC").FirstOrDefault().Attribute("SeatToSell").Value.Trim();
                                fsr.ChdAvlStatus = fsr.AdtAvlStatus;
                                fsr.InfAvlStatus = fsr.AdtAvlStatus;
                            }
                            else
                            {
                                try { fsr.AdtAvlStatus = xlFPricedItinerary.ElementAt(ptn).Attribute("SeatToSell").Value.Trim(); }
                                catch { fsr.AdtAvlStatus = "9"; }

                                fsr.ChdAvlStatus = fsr.AdtAvlStatus;
                                fsr.InfAvlStatus = fsr.AdtAvlStatus;
                            }



                            #region fare calculation


                            IEnumerable<XElement> xItinTotalFare = xlFAirPriceItnry.Descendants("ItinTotalFare");

                            decimal tFeess = 0;
                            //IEnumerable<XElement> TFee = xItinTotalFare.Descendants("Fees").Descendants("Fee");
                            //for (int t = 0; t < TFee.Count(); t++)
                            //{
                            //    tFeess = tFeess + Convert.ToDecimal(TFee.ElementAt(t).Attribute("Amount").Value);
                            //}


                            try
                            {
                                fsr.BagInfo = xItinTotalFare.Descendants("FareBaggageAllowance").FirstOrDefault().Attribute("UnitOfMeasureQuantity").Value + " " + xItinTotalFare.Descendants("FareBaggageAllowance").FirstOrDefault().Attribute("UnitOfMeasure").Value;

                               // fsr.AdtFar = fsr.AdtFar + " Baggage Allowance ("+ fsr.BagInfo +")";
                            }
                            catch { }
                            fsr.OriginalTF = float.Parse(xItinTotalFare.Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value);

                            //fsr.OriginalTF = fsr.OriginalTF +float.Parse(tFeess.ToString());

                            //IEnumerable<XElement> xTotmrkp = xItinTotalFare.Descendants("Markups").Descendants("Markup");

                            //for (int tm = 0; tm < xTotmrkp.Count(); tm++)
                            //{
                            //    fsr.OriginalTF = fsr.OriginalTF + float.Parse(xTotmrkp.ElementAt(tm).Attribute("Amount").Value);

                            //}


                            float totalfareItn = 0;
                            for (int frb = 0; frb < farebrkup.Count(); frb++)
                            {

                                XElement objfrb = farebrkup.ElementAt(frb);

                                #region Adult
                                if (objfrb.Element("PassengerTypeQuantity").Attribute("Code").Value.Trim().ToUpper() == "ADT")
                                {

                                    //fsr.AdtAvlStatus = objfrb.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = float.Parse(objfrb.Descendants("PassengerFare").Descendants("BaseFare").FirstOrDefault().Attribute("Amount").Value);//(float)Math.Ceiling(Convert.ToDecimal(objfrb.Element("DisplayFareAmt").Value.Trim()));
                                    totalfareItn = totalfareItn + float.Parse(objfrb.Descendants("PassengerFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value) * fsr.Adult;

                                    //fsr.AdtCabin = objfrb.Element("Cabin").Value.Trim();
                                    //fsr.AdtFSur = 0;
                                    float AdtTax = 0;// float.Parse(objfrb.Element("DisplayTaxSum").Value);


                                    if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                    {
                                        fsr.AdtRbd = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                        fsr.AdtFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                    }
                                    else
                                    {
                                        fsr.AdtRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                        try
                                        {
                                            fsr.AdtFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();
                                        }
                                        catch { }

                                    }


                                    //fsr.AdtFarebasis = objfrb.Element("FBCode").Value.Trim();


                                    //string provider = "YA";// searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    //fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    // fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";
                                    // fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    //fsr.BagInfo = fsr.BagInfo + "-RTBP" + DateTime.Now.ToString();



                                    IEnumerable<XElement> TaxDetails = objfrb.Descendants("PassengerFare").Descendants("Taxes").Descendants("Tax");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxel = TaxDetails.ElementAt(td);

                                        if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            AdtTax = AdtTax + fsr.AdtWO;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            AdtTax = AdtTax + fsr.AdtJN;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            AdtTax = AdtTax + fsr.AdtIN;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "YQ")
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            AdtTax = AdtTax + fsr.AdtFSur;
                                        }
                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                        }

                                    }


                                    //IEnumerable<XElement> AdtMarkup = objfrb.Descendants("PassengerFare").Descendants("Markups").Descendants("Markup");


                                    //for (int m = 0; m < AdtMarkup.Count(); m++)
                                    //{
                                    //    fsr.AdtOT = fsr.AdtOT + float.Parse(AdtMarkup.ElementAt(m).Attribute("Amount").Value);
                                    //}




                                    fsr.AdtOT = (float)Math.Ceiling( fsr.AdtOT + (float.Parse(objfrb.Descendants("PassengerFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value)));// / searchInput.Adult) + (float.Parse(tFeess.ToString()) / searchInput.Adult);
                                    AdtTax = AdtTax +fsr.AdtOT;

                                    fsr.AdtTax = AdtTax;

                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;


                                }
                                #endregion


                                #region Child
                                if (objfrb.Element("PassengerTypeQuantity").Attribute("Code").Value.Trim().ToUpper() == "CHD")
                                {

                                    //fsr.AdtAvlStatus = objfrb.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = float.Parse(objfrb.Descendants("PassengerFare").Descendants("BaseFare").FirstOrDefault().Attribute("Amount").Value);//(float)Math.Ceiling(Convert.ToDecimal(objfrb.Element("DisplayFareAmt").Value.Trim()));
                                    totalfareItn = totalfareItn + float.Parse(objfrb.Descendants("PassengerFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value) * fsr.Child;

                                    //fsr.ChdCabin = objfrb.Element("Cabin").Value.Trim();
                                    //fsr.ChdFSur = 0;
                                    float ChdTax = 0;// float.Parse(objfrb.Element("DisplayTaxSum").Value);
                                    if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                    {
                                        fsr.ChdRbd = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                        fsr.ChdFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                    }
                                    else
                                    {
                                        fsr.ChdRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();

                                        try
                                        {
                                            fsr.ChdFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();
                                        }
                                        catch { }

                                    }//fsr.ChdFarebasis = objfrb.Element("FBCode").Value.Trim();


                                    //string provider = "YA";// searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    //fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    // fsr.fareBasis = fsr.ChdFarebasis;
                                    fsr.FBPaxType = "Chd";
                                    // fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    //fsr.BagInfo = fsr.BagInfo + "-RTBP" + DateTime.Now.ToString();



                                    IEnumerable<XElement> TaxDetails = objfrb.Descendants("PassengerFare").Descendants("Taxes").Descendants("Tax");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxel = TaxDetails.ElementAt(td);

                                        if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            ChdTax = ChdTax + fsr.ChdWO;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            ChdTax = ChdTax + fsr.ChdJN;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            ChdTax = ChdTax + fsr.ChdIN;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "YQ")
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            ChdTax = ChdTax + fsr.ChdFSur;
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                        }

                                    }


                                    //IEnumerable<XElement> ChdMarkup = objfrb.Descendants("PassengerFare").Descendants("Markups").Descendants("Markup");


                                    //for (int m = 0; m < ChdMarkup.Count(); m++)
                                    //{
                                    //    fsr.ChdOT = fsr.ChdOT + float.Parse(ChdMarkup.ElementAt(m).Attribute("Amount").Value);
                                    //}


                                    fsr.ChdOT = (float)Math.Ceiling(fsr.ChdOT + float.Parse(objfrb.Descendants("PassengerFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value));// / searchInput.Adult;
                                    ChdTax = ChdTax + (float)Math.Ceiling(fsr.ChdOT);

                                    fsr.ChdTax = ChdTax;

                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;


                                }



                                #endregion


                                #region Infant
                                if (objfrb.Element("PassengerTypeQuantity").Attribute("Code").Value.Trim().ToUpper() == "INF")
                                {


                                    //fsr.AdtAvlStatus = objfrb.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = float.Parse(objfrb.Descendants("PassengerFare").Descendants("BaseFare").FirstOrDefault().Attribute("Amount").Value);//(float)Math.Ceiling(Convert.ToDecimal(objfrb.Element("DisplayFareAmt").Value.Trim()));
                                    totalfareItn = totalfareItn + float.Parse(objfrb.Descendants("PassengerFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value) * fsr.Infant;
                                    //fsr.InfCabin = objfrb.Element("Cabin").Value.Trim();
                                    //fsr.InfFSur = 0;
                                    float InfTax = 0;// float.Parse(objfrb.Element("DisplayTaxSum").Value);

                                    if (!(fsr.MarketingCarrier == "6E" || fsr.MarketingCarrier == "G8" || fsr.MarketingCarrier == "SG"))
                                    {
                                        fsr.InfRbd = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();
                                        fsr.InfFarebasis = objfrb.Descendants("FareBasisCodes").Descendants("FareBasisCode").FirstOrDefault().Attribute("ActualFareBase").Value.Trim();

                                    }
                                    else
                                    {
                                        fsr.InfRbd = seg.Descendants("BookingClassAvail").FirstOrDefault().Attribute("ResBookDesigCode").Value.Trim();

                                        try
                                        {
                                            fsr.InfFarebasis = xlFAirODItnry.ElementAt(odn).Descendants("OriginDestinationOption").FirstOrDefault().Attribute("FareBasisCode").Value.Trim();

                                        }
                                        catch { }
                                    }

                                    //string provider = "YA";// searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    //fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    // fsr.fareBasis = fsr.InfFarebasis;

                                    // fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    //fsr.BagInfo = fsr.BagInfo + "-RTBP" + DateTime.Now.ToString();



                                    IEnumerable<XElement> TaxDetails = objfrb.Descendants("PassengerFare").Descendants("Taxes").Descendants("Tax");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxel = TaxDetails.ElementAt(td);

                                        if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            InfTax = InfTax + fsr.InfWO;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            InfTax = InfTax + fsr.InfJN;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            InfTax = InfTax + fsr.InfIN;
                                        }
                                        else if (taxel.Attribute("TaxCode").Value.Trim().ToUpper() == "YQ")
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));
                                            InfTax = InfTax + fsr.InfFSur;
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxel.Attribute("Amount").Value));

                                        }

                                    }


                                    //IEnumerable<XElement> InfMarkup = objfrb.Descendants("PassengerFare").Descendants("Markups").Descendants("Markup");


                                    //for (int m = 0; m < InfMarkup.Count(); m++)
                                    //{
                                    //    fsr.InfOT = fsr.InfOT + float.Parse(InfMarkup.ElementAt(m).Attribute("Amount").Value);
                                    //}


                                    fsr.InfOT =(float)Math.Ceiling( fsr.InfOT + float.Parse(objfrb.Descendants("PassengerFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value));// / searchInput.Adult;
                                    InfTax = InfTax + (float)Math.Ceiling(fsr.InfOT);

                                    fsr.InfTax = InfTax;

                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;


                                }
                                #endregion

                            }
                            float fareDiff = (float)Math.Ceiling(Convert.ToDecimal((fsr.OriginalTF - totalfareItn) / fsr.Adult));
                            fsr.AdtOT = fsr.AdtOT + fareDiff;
                            fsr.AdtTax = fsr.AdtTax + fareDiff;
                            fsr.AdtFare = fsr.AdtFare + fareDiff;

                            #endregion


                            if (isSplFare)
                            {
                                fsr.Sector = searchInput.HidTxtDepCity.Split(',')[0] + ":" + searchInput.HidTxtArrCity.Split(',')[0] + ":" + searchInput.HidTxtDepCity.Split(',')[0];

                            }
                            else
                            {
                                fsr.Sector = searchInput.HidTxtDepCity.Split(',')[0] + ":" + searchInput.HidTxtArrCity.Split(',')[0];

                            }

                            fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));

                            if (Convert.ToInt32(fsr.Flight) == 1)
                            {
                                fsr.OrgDestFrom = searchInput.HidTxtDepCity.Split(',')[0];
                                fsr.OrgDestTo = fsr.OrgDestFrom = searchInput.HidTxtArrCity.Split(',')[0];
                                fsr.TripType = TripType.O.ToString();
                                fsrList.Add(fsr);
                            }
                            else if (Convert.ToInt32(fsr.Flight) == 2)
                            {
                                fsr.OrgDestFrom = fsr.OrgDestFrom = searchInput.HidTxtArrCity.Split(',')[0];
                                fsr.OrgDestTo = fsr.OrgDestFrom = searchInput.HidTxtDepCity.Split(',')[0];
                                fsr.TripType = TripType.R.ToString();
                                RfsrList.Add(fsr);
                            }


                        }


                    }




                    #endregion





                }

            }
            catch (Exception ex)
            {
                throw ex;
                //string hh = ex.Message;

            }

            if (isSplFare)
            {

                FinalList = RoundTripFare(ArrangeLineNum(fsrList), ArrangeLineNum(RfsrList), 0);
            }
            else
            {
                FinalList = fsrList;

            }

            List<FlightSearchResults> FinalList1 = new List<FlightSearchResults>();

            FinalList1 = GetGoAirResultListWithMarkup(FinalList, searchInput, SrvchargeList, MarkupDs, srvCharge, constr);

            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            return objFltComm.AddFlightKey(FinalList1, isSplFare);
        }

        public string YASearchRequest(FlightSearch objFlt, bool isSplFare, string fareType, bool isLCC, string serviceUrl, string methodName, ref string exep, string supplierCode)
        {
            StringBuilder YASrchReq = new StringBuilder();
            string res = "";
            string midOfficeID = ConfigurationManager.AppSettings["MidOfficeAgentID"].ToString();

            YASrchReq.Append("<?xml version='1.0' encoding='UTF-8'?>");
            YASrchReq.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
            YASrchReq.Append("<soapenv:Body>");
            YASrchReq.Append("<OTA_AirLowFareSearchRQ EchoToken='0' SequenceNmbr='0' TransactionIdentifier='0' xmlns='http://www.opentravel.org/OTA/2003/05'>");
            YASrchReq.Append("<POS xmlns='http://www.opentravel.org/OTA/2003/05'>");
            YASrchReq.Append("<Source AgentSine='' PseudoCityCode='NPCK' TerminalID='1'>");
            YASrchReq.Append("<RequestorID ID='AFFILIATE' />");
            YASrchReq.Append("</Source>");
            YASrchReq.Append("<YatraRequests>");
            string spl = isSplFare == true ? " RequestType='SR' SplitResponses='false'" : "";

            string dest = "";

            if (objFlt.Trip == Trip.I)
            {
                dest = "INT";
            }

            YASrchReq.Append("<YatraRequest DoNotHitCache='true' DoNotCache='false' MidOfficeAgentID='" + midOfficeID + "' AffiliateID='YTFABTRAVEL' YatraRequestTypeCode='SMPA' " + spl + "   Destination='" + dest + "' />");//RequestType='SR' 
            YASrchReq.Append("</YatraRequests>");
            YASrchReq.Append("</POS>");
            YASrchReq.Append("<TravelerInfoSummary>");
            YASrchReq.Append("<AirTravelerAvail>");
            YASrchReq.Append("<PassengerTypeQuantity Code='ADT' Quantity='" + objFlt.Adult + "'/>");
            if (objFlt.Child > 0)
            {

                YASrchReq.Append("<PassengerTypeQuantity Code='CHD' Quantity='" + objFlt.Child + "'/>");
            }
            if (objFlt.Infant > 0)
            {

                YASrchReq.Append("<PassengerTypeQuantity Code='INF' Quantity='" + objFlt.Infant + "'/>");
            }

            YASrchReq.Append("</AirTravelerAvail>");
            YASrchReq.Append("</TravelerInfoSummary>");
            YASrchReq.Append("<SpecificFlightInfo>");
            YASrchReq.Append("<Airline Code='" + supplierCode + "'/>");
            YASrchReq.Append("</SpecificFlightInfo>");

            if (fareType == "INB")
            {
                YASrchReq.Append("<OriginDestinationInformation>");
                YASrchReq.Append("<DepartureDateTime>" + Utility.Right(objFlt.DepDate, 4) + "-" + Utility.Mid(objFlt.DepDate, 3, 2) + "-" + Utility.Left(objFlt.DepDate, 2) + "T00:00:00" + "</DepartureDateTime>");
                YASrchReq.Append("<OriginLocation CodeContext='IATA' LocationCode='" + objFlt.HidTxtDepCity.Split(',')[0] + "'>" + objFlt.HidTxtDepCity.Split(',')[0] + "</OriginLocation>");
                YASrchReq.Append("<DestinationLocation CodeContext='IATA' LocationCode='" + objFlt.HidTxtArrCity.Split(',')[0] + "'>" + objFlt.HidTxtArrCity.Split(',')[0] + "</DestinationLocation>");
                YASrchReq.Append("</OriginDestinationInformation>");

            }
            else if (fareType == "OutB")
            {
                YASrchReq.Append("<OriginDestinationInformation>");
                YASrchReq.Append("<DepartureDateTime>" + Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T00:00:00" + "</DepartureDateTime>");
                YASrchReq.Append("<OriginLocation CodeContext='IATA' LocationCode='" + objFlt.HidTxtArrCity.Split(',')[0] + "'>" + objFlt.HidTxtArrCity.Split(',')[0] + "</OriginLocation>");
                YASrchReq.Append("<DestinationLocation CodeContext='IATA' LocationCode='" + objFlt.HidTxtDepCity.Split(',')[0] + "'>" + objFlt.HidTxtDepCity.Split(',')[0] + "</DestinationLocation>");
                YASrchReq.Append("</OriginDestinationInformation>");

            }

            if (isSplFare)
            {
                YASrchReq.Append("<OriginDestinationInformation>");
                YASrchReq.Append("<DepartureDateTime>" + Utility.Right(objFlt.RetDate, 4) + "-" + Utility.Mid(objFlt.RetDate, 3, 2) + "-" + Utility.Left(objFlt.RetDate, 2) + "T00:00:00" + "</DepartureDateTime>");
                YASrchReq.Append("<OriginLocation CodeContext='IATA' LocationCode='" + objFlt.HidTxtArrCity.Split(',')[0] + "'>" + objFlt.HidTxtArrCity.Split(',')[0] + "</OriginLocation>");
                YASrchReq.Append("<DestinationLocation CodeContext='IATA' LocationCode='" + objFlt.HidTxtDepCity.Split(',')[0] + "'>" + objFlt.HidTxtDepCity.Split(',')[0] + "</DestinationLocation>");
                YASrchReq.Append("</OriginDestinationInformation>");
            }

            string gCabin = "";
            if (objFlt.Cabin == "Y")
            {
                gCabin = "Economy";
            }
            else if (objFlt.Cabin == "C")
            {
                gCabin = "Business";
            }
            else if (objFlt.Cabin == "F")
            {
                gCabin = "Business";
            }



            YASrchReq.Append("<TravelPreferences>");
            YASrchReq.Append("<CabinPref Cabin='" + gCabin + "'/>");
            if (!string.IsNullOrEmpty(objFlt.HidTxtAirLine))
            {
                YASrchReq.Append("<VendorPref Code='" + objFlt.HidTxtAirLine + "'/>");
            }

            YASrchReq.Append("</TravelPreferences>");
            YASrchReq.Append("</OTA_AirLowFareSearchRQ>");
            YASrchReq.Append("</soapenv:Body>");
            YASrchReq.Append("</soapenv:Envelope>");


            try
            {
                res = YAUtility.PostXml(MedthodSVCUrl.SvcURL, YASrchReq.ToString(), "", "", methodName, ref exep);
            }
            catch (Exception ex) { res = ex.Message + ex.StackTrace.ToString(); }
            YAUtility.SaveXml(YASrchReq.ToString(), "", "OTA_AirLowFareSearch_Req");
            YAUtility.SaveXml(res, "", "OTA_AirLowFareSearch_Res");
            return res;


        }

        private List<FlightSearchResults> ArrangeLineNum(List<FlightSearchResults> objO)
        {
            List<FlightSearchResults> final = new List<FlightSearchResults>();

            List<int> dln = objO.Select(x => x.LineNumber).Distinct().ToList();

            int k = 1;

            for (int i = 0; i < dln.Count; i++)
            {

                List<FlightSearchResults> newlnList = objO.Where(x => x.LineNumber == dln[i]).ToList();

                newlnList.ForEach(x => { x.LineNumber = k; });

                final.AddRange(newlnList);
                k++;
            }
            return final;
        }

        private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR, float srvCharge)
        {

            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            List<FlightSearchResults> Comb = new List<FlightSearchResults>();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            if (true)
            {
                #region 1 lnob<lnib
                for (int k = 1; k <= LnOb; k++)
                {
                    var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                    for (int l = 1; l <= LnIb; l++)
                    {
                        var IB = (from c in objR where c.LineNumber == l select c).ToList();
                        //List<FlightSearchResults> st = new List<FlightSearchResults>();

                        if (OB[OB.Count - 1].depdatelcc.Split('T')[0] == IB[0].depdatelcc.Split('T')[0])
                        {
                            int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                            int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                            if ((obtmin + 120) <= ibtmin)
                            {
                                // st = Merge(OB, IB, ln);
                                Final.AddRange(Merge(OB, IB, ln, srvCharge));
                                ln++;///Increment Total Ln
                            }
                        }
                        else
                        {
                            Final.AddRange(Merge(OB, IB, ln, srvCharge));
                            ln++;
                        }

                    }

                }
                #endregion
            }
            //else
            //{
            //    #region 1 lnob>lnib
            //    for (int k = 1; k <= LnIb; k++)
            //    {
            //        var OB = (from ct in objR where ct.LineNumber == k select ct).ToList();

            //        for (int l = LnIb + 1; l < LnOb; l++)
            //        {
            //            var IB = (from c in objO where c.LineNumber == l select c).ToList();
            //            //List<FlightSearchResults> st = new List<FlightSearchResults>();

            //            if (OB[OB.Count - 1].depdatelcc.Split('T')[0] == IB[0].depdatelcc.Split('T')[0])
            //            {
            //                int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
            //                int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

            //                if ((obtmin + 120) <= ibtmin)
            //                {
            //                    // st = Merge(OB, IB, ln);
            //                    Final.AddRange(Merge(OB, IB, ln, srvCharge));
            //                    ln++;///Increment Total Ln
            //                }
            //            }
            //            else
            //            {
            //                Final.AddRange(Merge(OB, IB, ln, srvCharge));
            //                ln++;
            //            }

            //        }

            //    }
            //    #endregion
            //}
            Comb.AddRange(Final);
            return Comb;
        }

        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, float srvCharge)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
            //InfSrvTax = 0, InfTF = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;

            float AdtDiscount1 = 0, AdtCB = 0, AdtSrvTax1 = 0, AdtDiscount = 0, AdtTF = 0, AdtTds = 0;
            float ChdDiscount1 = 0, ChdCB = 0, ChdSrvTax1 = 0, ChdDiscount = 0, ChdTF = 0, ChdTds = 0;
            float STax = 0, TFee = 0, TotDis = 0, TotCB = 0, TotTds = 0;

            float TotMgtFee = 0, AdtMgtFee = 0, ChdMgtFee = 0, InfMgtFee = 0;







            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT - srvCharge;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare - srvCharge;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax - srvCharge;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;

            AdtDiscount1 = AdtDiscount1 + item.AdtDiscount1 + itemib.AdtDiscount1;
            AdtCB = AdtCB + item.AdtCB + itemib.AdtCB; ;
            AdtSrvTax1 = AdtSrvTax1 + item.AdtSrvTax1 + itemib.AdtSrvTax1; ;
            AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount; ;
            AdtTF = AdtTF + item.AdtTF + itemib.AdtTF; ;
            AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            AdtMgtFee = AdtMgtFee + item.AdtMgtFee + itemib.AdtMgtFee;

            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT - srvCharge;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare - srvCharge;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax - srvCharge;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;


            ChdDiscount1 = ChdDiscount1 + item.ChdDiscount1 + itemib.ChdDiscount1;
            ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
            ChdSrvTax1 = ChdSrvTax1 + item.ChdSrvTax1 + itemib.ChdSrvTax1;
            ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
            ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
            ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;
            ChdMgtFee = ChdMgtFee + item.ChdMgtFee + itemib.ChdMgtFee;


            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            InfMgtFee = InfMgtFee + item.InfMgtFee + itemib.InfMgtFee;

            #endregion

            #region TOTAL

            STax = (AdtSrvTax1 * Adult) + (ChdSrvTax1 * Child);
            TFee = (AdtTF * Adult) + (ChdTF * Child);// +(objlist.InfTF * objlist.Infant);
            TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            TotCB = (AdtCB * Adult) + (ChdCB * Child);
            TotTds = (AdtTds * Adult) + (ChdTds * Child);
            TotMgtFee = (AdtMgtFee * Adult) + (ChdMgtFee * Child) + (InfMgtFee * Infant);

            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = srvCharge;// item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
            float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;


                y.AdtDiscount1 = AdtDiscount1;
                y.AdtCB = AdtCB;
                y.AdtSrvTax1 = AdtSrvTax1;
                y.AdtDiscount = AdtDiscount;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                y.AdtMgtFee = AdtMgtFee;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;


                y.ChdDiscount1 = ChdDiscount1;
                y.ChdCB = ChdCB;
                y.ChdSrvTax1 = ChdSrvTax1;
                y.ChdDiscount = ChdDiscount;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                y.ChdMgtFee = ChdMgtFee;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                y.InfMgtFee = InfMgtFee;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotCB = TotCB;
                y.TotTds = TotTds;
                y.TotMgtFee = TotMgtFee;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;

                y.AdtDiscount1 = AdtDiscount1;
                y.AdtCB = AdtCB;
                y.AdtSrvTax1 = AdtSrvTax1;
                y.AdtDiscount = AdtDiscount;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                y.AdtMgtFee = AdtMgtFee;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                y.ChdDiscount1 = ChdDiscount1;
                y.ChdCB = ChdCB;
                y.ChdSrvTax1 = ChdSrvTax1;
                y.ChdDiscount = ChdDiscount;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                y.ChdMgtFee = ChdMgtFee;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                y.InfMgtFee = InfMgtFee;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = srvCharge;//OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotCB = TotCB;
                y.TotTds = TotTds;
                y.TotMgtFee = TotMgtFee;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }


        #region Goair RTF
        public List<FlightSearchResults> GetGoAirResultListWithMarkup(List<FlightSearchResults> inputList, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, float srvCharge, string ConnStr)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            List<FlightSearchResults> newList = new List<FlightSearchResults>();
            //ArrayList test = (ArrayList)inputList[0];
            ////SMS charge calc
            //float srvCharge = 0;
            //srvCharge = objFltComm.GetMiscServiceCharge(searchInputs.Trip.ToString(), "G8", searchInputs.UID);
            ////SMS charge calc end
            List<BaggageList> BagInfoList = new List<BaggageList>();
            // BagInfoList = objFltComm.GetBaggage(searchInputs.Trip.ToString(), "G8");
            for (int i = 0; i < inputList.Count; i++)
            {

                newList.Add(GoAirGetNewObject((FlightSearchResults)inputList[i], searchInputs, SrvChargeList, MarkupDs, srvCharge, objFltComm));
            }

            return newList;
        }

        public FlightSearchResults GoAirGetNewObject(FlightSearchResults fsr, FlightSearch searchInputs, List<FltSrvChargeList> SrvChargeList, DataSet MarkupDs, float srvCharge, FlightCommonBAL objFltComm)
        {
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            var objNew = (FlightSearchResults)fsr.Clone();



            #region Fare Calculation

            #region Adult
            if (objNew.Adult > 0)
            {
                //objNew.AdtOT = objNew.AdtTax - objNew.AdtFSur;
                //SMS charge add 
                objNew.AdtOT = objNew.AdtOT + srvCharge;
                objNew.AdtTax = objNew.AdtTax + srvCharge;
                objNew.AdtFare = objNew.AdtFare + srvCharge;
                //SMS charge add end
                if (!string.IsNullOrEmpty(objNew.AdtFar) && objNew.AdtFar.Trim().ToUpper()=="N")
                {
                    objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());
                    objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier + "S", objNew.AdtFare, searchInputs.Trip.ToString());

                    objNew.AdtDiscount1 = 0;  //-AdtComm  
                    objNew.AdtCB = 0;
                    objNew.AdtSrvTax1 = 0;// added TO TABLE
                    objNew.AdtDiscount = 0;
                    objNew.AdtEduCess = 0;
                    objNew.AdtHighEduCess = 0;
                    objNew.AdtTF = 0;
                    objNew.AdtTds = 0;
                }
                else
                {
                    objNew.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier, fsr.AdtFare, searchInputs.Trip.ToString());
                    objNew.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], objNew.ValiDatingCarrier, fsr.AdtFare, searchInputs.Trip.ToString());

                    CommDt.Clear();
                    CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), 1, objNew.AdtRbd, objNew.AdtCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "NRM", "");
                    objNew.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                    objNew.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                    STTFTDS.Clear();
                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.AdtDiscount1, objNew.AdtBfare, objNew.AdtFSur, searchInputs.TDS);
                    objNew.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                    objNew.AdtDiscount = objNew.AdtDiscount1 - objNew.AdtSrvTax1;
                    objNew.AdtEduCess = 0;
                    objNew.AdtHighEduCess = 0;
                    objNew.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                    objNew.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                }

                //if (searchInputs.IsCorp == true)
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.AdtBfare.ToString()), decimal.Parse(objNew.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.AdtFare.ToString()));
                //        objNew.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        objNew.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }
            #endregion

            #region Child
            if (objNew.Child > 0)
            {
               // objNew.ChdOT = objNew.ChdTax - objNew.ChdFSur;
                //SMS charge add 
                objNew.ChdOT = objNew.ChdOT + srvCharge;
                objNew.ChdTax = objNew.ChdTax + srvCharge;
                objNew.ChdFare = objNew.ChdFare + srvCharge;
                //SMS charge add end
                if (!string.IsNullOrEmpty(objNew.AdtFar) && objNew.AdtFar.Trim().ToUpper()=="N")
                {
                    objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier + "S", objNew.ChdFare, objNew.Trip.ToString());
                    objNew.ChdDiscount1 = 0;  //-AdtComm  
                    objNew.ChdCB = 0;
                    objNew.ChdSrvTax1 = 0;// added TO TABLE
                    objNew.ChdDiscount = 0;
                    objNew.ChdEduCess = 0;
                    objNew.ChdHighEduCess = 0;
                    objNew.ChdTF = 0;
                    objNew.ChdTds = 0;
                }
                else
                {
                    objNew.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objNew.ValiDatingCarrier, objNew.ChdFare, objNew.Trip.ToString());

                    CommDt.Clear();
                    CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), 1, objNew.ChdRbd, objNew.ChdCabin, searchInputs.DepDate, objNew.OrgDestFrom + "-" + objNew.OrgDestTo, searchInputs.RetDate, objNew.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), objNew.FlightIdentification, objNew.OperatingCarrier, objNew.MarketingCarrier, "NRM", "");
                    objNew.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                    objNew.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                    STTFTDS.Clear();
                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, objNew.ValiDatingCarrier, objNew.ChdDiscount1, objNew.ChdBFare, objNew.ChdFSur, searchInputs.TDS);
                    objNew.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                    objNew.ChdDiscount = objNew.ChdDiscount1 - objNew.ChdSrvTax1;
                    objNew.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                    objNew.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                    objNew.ChdEduCess = 0;
                    objNew.ChdHighEduCess = 0;
                }
                //if (searchInputs.IsCorp == true)
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.ChdBFare.ToString()), decimal.Parse(objNew.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.ChdFare.ToString()));
                //        objNew.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        objNew.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }
            #endregion

            #region Infant
            if (objNew.Infant > 0)
            {
               // objNew.InfOT = objNew.InfTax - objNew.InfFSur;
                //if (searchInputs.IsCorp == true)
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objNew.ValiDatingCarrier, decimal.Parse(objNew.InfBfare.ToString()), decimal.Parse(objNew.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objNew.InfFare.ToString()));
                //        objNew.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        objNew.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }

            #endregion

            objNew.STax = (objNew.AdtSrvTax * objNew.Adult) + (objNew.ChdSrvTax * objNew.Child) + (objNew.InfSrvTax * objNew.Infant);
            objNew.TFee = (objNew.AdtTF * objNew.Adult) + (objNew.ChdTF * objNew.Child);// +(objlist.InfTF * objlist.Infant);
            objNew.TotDis = (objNew.AdtDiscount * objNew.Adult) + (objNew.ChdDiscount * objNew.Child);
            objNew.TotCB = (objNew.AdtCB * objNew.Adult) + (objNew.ChdCB * objNew.Child);
            objNew.TotTds = (objNew.AdtTds * objNew.Adult) + (objNew.ChdTds * objNew.Child);// +objFS.InfTds;
            objNew.TotMrkUp = (objNew.ADTAdminMrk * objNew.Adult) + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAdminMrk * objNew.Child) + (objNew.CHDAgentMrk * objNew.Child);
            objNew.TotMgtFee = (objNew.AdtMgtFee * objNew.Adult) + (objNew.ChdMgtFee * objNew.Child) + (objNew.InfMgtFee * objNew.Infant);
            objNew.TotalTax = objNew.TotalTax + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.TotalFare = objNew.TotalFare + objNew.TotMrkUp + objNew.STax + objNew.TFee + objNew.TotMgtFee + (srvCharge * objNew.Adult) + (srvCharge * objNew.Child);
            objNew.NetFare = (objNew.TotalFare + objNew.TotTds) - (objNew.TotDis + objNew.TotCB + (objNew.ADTAgentMrk * objNew.Adult) + (objNew.CHDAgentMrk * objNew.Child));

            objNew.OriginalTT = srvCharge;
            #endregion


            return objNew;



        }
        #endregion

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
           // int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }
                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }


    }



}
