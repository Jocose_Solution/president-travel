﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.Shared;

namespace STD.BAL
{
   


    public class YABaggage
    {

        public string Code { get; set; }
        public string Desc { get; set; }
        public decimal Amount { get; set; }
    }

    public class YAMeal
    {

        public string Code { get; set; }
        public string Desc { get; set; }
        public decimal Amount { get; set; }
    }

    public enum YASSRType
    {
    Meal,
    Baggage
    }
    public class YASSR
    {

        public string Code { get; set; }
        public string Desc { get; set; }
        public decimal Amount { get; set; }
        public YASSRType SSRType { get; set; }
        public STD.Shared.TripType TripType { get; set; }

    }
}
