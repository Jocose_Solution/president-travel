﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net.Mail;
namespace EXCEPTION_LOG
{
    public class ErrorLog
    {
        SqlConnection con; SqlCommand cmd;
      public static  string  ErrorLogPath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
        public ErrorLog()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
           
        }
        public void writeErrorLog(Exception ex, string methodname)
        {

            string ermessage = ex.Message;
            string pagename = "";
            string method = "";
            string errorSource = "err";
            int linenumber = 0;
            string path = ErrorLogPath + DateTime.Now.Date.ToString("dd-MM-yyyy") + "";
            try
            {


                try
                {
                    System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

                    pagename = trace.GetFrame((trace.FrameCount - 1)).GetFileName();
                    method = trace.GetFrame((trace.FrameCount - 1)).GetMethod().Name.ToString();
                    errorSource = trace.GetFrame((trace.FrameCount - 1)).GetFileName().ToString();
                     linenumber = trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber();
                }
                catch { }
                //---------------------save in db--------------------------------------------//
                insertLog(ermessage, pagename, method, errorSource, linenumber);
                
                GetSendMail(ermessage, pagename, method, errorSource, linenumber);
                //-----------------------end-------------------------------------------------//
               
                DirectoryInfo info = new DirectoryInfo(path);
                if (!Directory.Exists(info.FullName))
                {
                    string new_path = ""; string filename = "";
                    Directory.CreateDirectory(path);
                    filename = Path.GetFileName(methodname + ".txt");
                    new_path = Path.Combine(path, filename);
                    FileStream fs = new FileStream(new_path, FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Actual Error Message:" + ermessage);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Errror Source:" + errorSource);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Method:" + method);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error LineNo:" + linenumber);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                }
                else
                {
                    string newFilename = Path.GetFileName(methodname.ToString() + ".txt");
                    string newPth = Path.Combine(path, newFilename);
                    FileStream fs = new FileStream(newPth, FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Actual Error Message:" + ermessage);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Errror Source:" + errorSource);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Method:" + method);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error LineNo:" + linenumber);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                }

            }
            catch (Exception exx)
            {
                try
                {
                    try
                    {
                        System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

                        ermessage = ex.Message;
                        pagename = trace.GetFrame((trace.FrameCount - 1)).GetFileName();
                        method = trace.GetFrame((trace.FrameCount - 1)).GetMethod().Name.ToString();
                        errorSource = trace.GetFrame((trace.FrameCount - 1)).GetFileName().ToString();
                        linenumber = trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber();
                    }
                    catch { }
                    string newFilename = Path.GetFileName(errorSource.ToString() + ".txt");
                    string newPth = Path.Combine(path, newFilename);
                    FileStream fs = new FileStream(newPth, FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Actual Error Message:" + exx.Message);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Errror Source:" + errorSource);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Method:" + method);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error LineNo:" + linenumber);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                }
                catch { }
            }
        }
        private void insertLog(string msg, string pagename, string methodname, string errsource, int linenumber)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                cmd = new SqlCommand("SP_BUS_WRITE_ERROR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@er_message", msg.Trim());
                cmd.Parameters.AddWithValue("@er_pagename", pagename.Trim());
                cmd.Parameters.AddWithValue("@er_methodname", methodname.Trim());
                cmd.Parameters.AddWithValue("@er_errorsource", errsource.Trim());
                cmd.Parameters.AddWithValue("@er_linenumber", linenumber);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }
        public void GetSendMail(string ermessage,string pagename,string method,string errorSource,int linenumber)
        {
            MailMessage msgHtlDetails = new MailMessage();
            try
            {

                string layoutMsg = "";
                layoutMsg += "Error message :<br />message= '" + ermessage + "'<br />Page Name= '" + pagename + "'<br />Method= '" + method + "'<br />Error Source= '" + errorSource + "'<br />line No= '" + linenumber + "'";
                msgHtlDetails.Subject = "Bus Exception";
                msgHtlDetails.From = new MailAddress("info@looknbook.com".Trim());
                msgHtlDetails.To.Add("anupkumar@looknbook.com".Trim());
                msgHtlDetails.Body = layoutMsg;
                msgHtlDetails.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("shekhal.springtravels.com".Trim());
                client.Credentials = new System.Net.NetworkCredential("b2bticketing".Trim(), "america".Trim());
                client.Port = 25;
                client.Send(msgHtlDetails);
            }
            catch (Exception ex)
            {
               
            }
        }


        public static string get_Error_Det(string Err_code)
        {
            string s = Err_code; //Error_Resource.ResourceManager.GetString(Err_code);
            return s;
        }
        public static void FileHandling(string Err_Service, string Err_Code, Exception Err_Msg, string Err_Module)
        {
            try
            {
                //Error Details
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(Err_Msg, true);
                string ErrorLineNo = (trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber()).ToString();
                string Err_Source = (trace.GetFrame((trace.FrameCount - 1)).GetFileName()).ToString();

                // Specify a "currently active folder" 
               // string activeDir = @"C:\Error_Folder_\" + DateTime.Now.Date.ToString("dd-MMM-yyyy");

                string activeDir = ErrorLogPath + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                // Creating the folder
                DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
                if (!Directory.Exists(objDirectoryInfo.FullName))
                {
                    string newPath = "", newFileName = "";
                    try
                    {
                        // Create a new file name. This example generates 
                        Directory.CreateDirectory(activeDir);
                        newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                        // Combine the new file name with the path
                        newPath = Path.Combine(activeDir, newFileName);
                    }
                    catch (Exception ex)
                    {
                        string activeDir2 = ErrorLogPath + DateTime.Now.Date.ToString("dd-MMM-yyyy");
                        DirectoryInfo objDirectoryInfo2 = new DirectoryInfo(activeDir2);
                        // Create a new file name. This example generates
                        Directory.CreateDirectory(activeDir2);
                        newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                        // Combine the new file name with the path
                        newPath = Path.Combine(activeDir2, newFileName);
                    }
                    //// Create a new file name. This example generates 
                    //string newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                    //// Combine the new file name with the path
                    //string newPath = Path.Combine(activeDir, newFileName);
                    FileStream fs = new FileStream(newPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Code" + Err_Code);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Actual Error Message:" + Err_Msg.Message);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("User Friendly Message:" + get_Error_Det(Err_Code));
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Errror Source:" + Err_Source);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Module:" + Err_Module);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error LineNo:" + ErrorLineNo);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                }
                else
                {
                    string newFileName = Path.GetFileName(Err_Service.ToString() + ".txt");
                    // Combine the new file name with the path
                    string newPath = Path.Combine(activeDir, newFileName);
                    FileStream fs = new FileStream(newPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Time:" + DateTime.Now.ToString());
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Code" + Err_Code);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Actual Error Message:" + Err_Msg.Message);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("User Friendly Message:" + get_Error_Det(Err_Code));
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Errror Source:" + Err_Source);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error Module:" + Err_Module);
                    sw.Write(sw.NewLine);
                    sw.WriteLine("Error LineNo:" + ErrorLineNo);
                    sw.Write(sw.NewLine);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                    //}

                }
            }
            catch (Exception ex)
            {

            }
        }



    }
}
