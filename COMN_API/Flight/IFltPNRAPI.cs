﻿using COMN_SHARED.Flight;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMN_API.Flight
{
   public  interface IFltPNRAPI
    {
       FltPNR GetPNR(DataSet PaxDs, DataSet FltHdrDs, DataSet FltDs, string OrderId); 

    }
}
