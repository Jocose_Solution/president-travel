﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
//using System.Diagnostics;
using HotelShared;
using HotelDAL;
namespace HotelBAL
{
    public class HotelThreadCall
    {
        HotelSearch SearchQuery = new HotelSearch();
        TGHotelResponse objTGHotel = new TGHotelResponse(); GTAHotelResponse objGTAHotel = new GTAHotelResponse();  RoomXMLHotelResponse objRoomXmlHotel = new RoomXMLHotelResponse();
        HotelComposite TGHotels = new HotelComposite(); HotelComposite GTAHotels = new HotelComposite(); HotelComposite RoomXMLHotels = new HotelComposite();
        public HotelComposite HotelAvailability(HotelSearch SearchDetails)
        {
            HotelComposite obgHotelDetails = new HotelComposite();
            ArrayList vlist1 = new ArrayList(); ArrayList vlist = new ArrayList();
            try
            {
                DateTime chekout = Convert.ToDateTime(SearchDetails.CheckOutDate);
                TimeSpan tsTimeSpan = chekout.Subtract(Convert.ToDateTime(SearchDetails.CheckInDate));
                SearchDetails.NoofNight = tsTimeSpan.Days;
                string[] citylists = SearchDetails.HTLCityList.Split(',');
                if (citylists.Length > 4)
                {
                    SearchDetails.SearchCity = citylists[0];
                    SearchDetails.SearchCityCode = citylists[1];
                    SearchDetails.Country = citylists[2];
                    SearchDetails.CountryCode = citylists[3];
                    SearchDetails.SearchType = citylists[4];
                    SearchDetails.RegionId = citylists[5];
                }
                if (SearchDetails.CountryCode == "IN")
                    SearchDetails.HtlType = HotelStatus.Domestic.ToString();
                else
                    SearchDetails.HtlType = HotelStatus.International.ToString();

                SearchDetails = SetAuthentication(SearchDetails, "Search");
                SearchDetails.CurrancyRate = CurrancyConvert_USD_To_INR();
                SearchQuery = SearchDetails;
                if (SearchDetails.CountryCode == "IN")
                {
                    Thread TG = new Thread(new ThreadStart(TGAvailability));
                    TG.Start();
                    vlist.Add(TG);
                    vlist1.Add(DateTime.Now);

                    Thread GTA = new Thread(new ThreadStart(GTAAvailability));
                    GTA.Start();
                    vlist.Add(GTA);
                    vlist1.Add(DateTime.Now);

                    Thread RoomXML = new Thread(new ThreadStart(RoomXMLAvailability));
                    RoomXML.Start();
                    vlist.Add(RoomXML);
                    vlist1.Add(DateTime.Now);
                }
                else
                {
                    //obgHotelDetails = objGTAHotel.GTAHotels(SearchDetails);

                    Thread GTA = new Thread(new ThreadStart(GTAAvailability));
                    GTA.Start();
                    vlist.Add(GTA);
                    vlist1.Add(DateTime.Now);

                    Thread RoomXML = new Thread(new ThreadStart(RoomXMLAvailability));
                    RoomXML.Start();
                    vlist.Add(RoomXML);
                    vlist1.Add(DateTime.Now);
                }

                #region Wait for Thread
                int counter = 0;
                while ((counter < vlist.Count))
                {
                    Thread TH = (Thread)vlist[counter];
                    if (TH.ThreadState == ThreadState.WaitSleepJoin)
                    {
                        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
                        if ((DIFF.Seconds > 50))
                        {
                            TH.Abort();
                            counter += 1;
                        }
                    }
                    else if ((TH.ThreadState == ThreadState.Stopped))
                    {
                        counter += 1;
                    }
                }
                #endregion
                if (SearchDetails.CountryCode == "IN")
                {
                    //SearchDetails.NoofHotel = TGHotels.Hotelresults.Count;
                    if (GTAHotels.Hotelresults.Count > 0 && TGHotels.Hotelresults.Count > 0 && RoomXMLHotels.Hotelresults.Count > 0)
                    {
                        if (GTAHotels.Hotelresults[0].HtlError == null && TGHotels.Hotelresults[0].HtlError == null && RoomXMLHotels.Hotelresults[0].HtlError == null)
                        {
                            var HotelOrgLists = TGHotels.Hotelresults.Union(GTAHotels.Hotelresults).Union(RoomXMLHotels.Hotelresults).ToList();
                            var Hotellist = HotelOrgLists.GroupBy(item => item.HotelName).SelectMany(g => g.Count() > 1 ? g.Where(x => x.hotelPrice == g.Min(c => c.hotelPrice)) : g).ToList();

                            obgHotelDetails.HotelOrgList = HotelOrgLists;
                            obgHotelDetails.Hotelresults = Hotellist;
                            obgHotelDetails.HotelSearchDetail = TGHotels.HotelSearchDetail;
                        }
                        else if (GTAHotels.Hotelresults[0].HtlError == null && TGHotels.Hotelresults[0].HtlError == null && RoomXMLHotels.Hotelresults[0].HtlError != null)
                        {
                            var HotelOrgLists = TGHotels.Hotelresults.Union(GTAHotels.Hotelresults).ToList();
                            var Hotellist = HotelOrgLists.GroupBy(item => item.HotelName).SelectMany(g => g.Count() > 1 ? g.Where(x => x.hotelPrice == g.Min(c => c.hotelPrice)) : g).ToList();
                            obgHotelDetails.HotelOrgList = HotelOrgLists;
                            obgHotelDetails.Hotelresults = Hotellist;
                            obgHotelDetails.HotelSearchDetail = TGHotels.HotelSearchDetail;
                        }
                        else if (GTAHotels.Hotelresults[0].HtlError != null && TGHotels.Hotelresults[0].HtlError == null && RoomXMLHotels.Hotelresults[0].HtlError == null)
                        {
                            var HotelOrgLists = TGHotels.Hotelresults.Union(RoomXMLHotels.Hotelresults).ToList();
                            var Hotellist = HotelOrgLists.GroupBy(item => item.HotelName).SelectMany(g => g.Count() > 1 ? g.Where(x => x.hotelPrice == g.Min(c => c.hotelPrice)) : g).ToList();
                            obgHotelDetails.HotelOrgList = HotelOrgLists;
                            obgHotelDetails.Hotelresults = Hotellist;
                            obgHotelDetails.HotelSearchDetail = TGHotels.HotelSearchDetail;
                        }
                        else if (GTAHotels.Hotelresults[0].HtlError == null)
                            obgHotelDetails = GTAHotels;
                        else if (TGHotels.Hotelresults[0].HtlError == null)
                            obgHotelDetails = TGHotels;
                        else
                            obgHotelDetails = TGHotels;
                    }
                    else if (GTAHotels.Hotelresults.Count > 0)
                        obgHotelDetails = GTAHotels;
                    else if (TGHotels.Hotelresults.Count > 0)
                        obgHotelDetails = TGHotels;
                }
                else
                {
                    if (GTAHotels.Hotelresults.Count > 0 && RoomXMLHotels.Hotelresults.Count > 0)
                    {
                        if (GTAHotels.Hotelresults[0].HtlError == null && RoomXMLHotels.Hotelresults[0].HtlError == null)
                        {
                            var HotelOrgLists = GTAHotels.Hotelresults.Union(RoomXMLHotels.Hotelresults).ToList();
                            var Hotellist = HotelOrgLists.GroupBy(item => item.HotelName).SelectMany(g => g.Count() > 1 ? g.Where(x => x.hotelPrice == g.Min(c => c.hotelPrice)) : g).ToList();

                            obgHotelDetails.HotelOrgList = HotelOrgLists;
                            obgHotelDetails.Hotelresults = Hotellist;
                            obgHotelDetails.HotelSearchDetail = GTAHotels.HotelSearchDetail;
                        }
                        else if (GTAHotels.Hotelresults[0].HtlError == null)
                            obgHotelDetails = GTAHotels;
                        else if (RoomXMLHotels.Hotelresults[0].HtlError == null)
                            obgHotelDetails = RoomXMLHotels;
                        else
                            obgHotelDetails = GTAHotels;
                    }
                    else if (GTAHotels.Hotelresults.Count > 0)
                        obgHotelDetails = GTAHotels;
                    else if (TGHotels.Hotelresults.Count > 0)
                        obgHotelDetails = RoomXMLHotels;

                  // obgHotelDetails.HotelOrgList = obgHotelDetails.Hotelresults;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "HotelSearchResponse");
            }
            return obgHotelDetails;
        }

        public HotelSearch SetAuthentication(HotelSearch SearchDetails, string AuthenticationFor)
        {
            try
            {
                HotelDA objhtlDa = new HotelDA();
                SearchDetails = objhtlDa.GetCredentials(SearchDetails, "LIVE");

                if (AuthenticationFor == "Search")
                {
                    SearchDetails.MarkupDS = objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "SEL");
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "SetAuthentication");
            }
            return SearchDetails;
        }




        #region Corency Converter from Google API
        public decimal CurrancyConvert_USD_To_INR()
        {
            HotelDA objhtlDa = new HotelDA();
            try
            {
                WebClient web = new WebClient();
                string url = string.Format("http://www.google.com/finance/converter?a=1&from=USD&to=INR");
                byte[] databuffer = Encoding.ASCII.GetBytes("test=postvar&test2=another");
                HttpWebRequest _webreqquest = (HttpWebRequest)WebRequest.Create(url);
                _webreqquest.Method = "POST";
                _webreqquest.ContentType = "application/x-www-form-urlencoded";
                _webreqquest.Timeout = 11000;
                _webreqquest.ContentLength = databuffer.Length;
                Stream PostData = _webreqquest.GetRequestStream();
                PostData.Write(databuffer, 0, databuffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)_webreqquest.GetResponse();
                Stream finalanswer = WebResp.GetResponseStream();
                StreamReader _answer = new StreamReader(finalanswer);
                string[] value = Regex.Split(_answer.ReadToEnd(), "&nbsp;");
                var a = Regex.Split(value[1], "<div id=currency_converter_result>1 USD = <span class=bld>");
                decimal rate = 0;
                try
                {
                    rate = Convert.ToDecimal(Regex.Split(a[1], " INR</span>")[0].Trim());
                    int i = objhtlDa.UpdateCurrancyValue(rate);
                }
                catch (Exception ex)
                {
                    ConvertCurrancy_USD_To_INR_from_xe();
                    HotelDA.InsertHotelErrorLog(ex, a.ToString());
                }
                return rate;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "CurrancyConvert_USD_To_INR");
                decimal curval = objhtlDa.SelectCurrancyValue();
                if (curval > 0)
                    return curval;
                else
                    return ConvertCurrancy_USD_To_INR_from_xe();
            }
        }
        #endregion

        #region Corency Converter from http://www.xe.com
        public decimal ConvertCurrancy_USD_To_INR_from_xe()
        {
            HotelDA HTLST = new HotelDA();
            try
            {
                HttpWebRequest HttpWebReq = (HttpWebRequest)WebRequest.Create("http://www.xe.com/ucc/convert/?Amount=1&From=USD&To=INR");
                HttpWebReq.Timeout = 11000;
                HttpWebResponse WebResponse = (HttpWebResponse)HttpWebReq.GetResponse();
                Stream responseStream = WebResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                reader = new StreamReader(HttpWebReq.GetResponse().GetResponseStream());
                string response = reader.ReadToEnd();

                string convert__1 = response.Substring(response.IndexOf("XE Currency Converter"), response.IndexOf("View") - response.IndexOf("XE Currency Converter"));
                convert__1 = convert__1.Substring(convert__1.IndexOf("uccRes"), convert__1.IndexOf("uccResRgn") - convert__1.IndexOf("uccRes"));
                string[] str = Regex.Split(convert__1, "<td");
                string[] str1 = str[3].Split('>');
                string[] inr__2 = str1[1].Split('&');
                decimal rate = 0;
                try
                {
                    rate = Convert.ToDecimal(inr__2[0].Trim());
                    HTLST.UpdateCurrancyValue(rate);
                }
                catch (Exception ex)
                {
                    HotelDA.InsertHotelErrorLog(ex, convert__1);
                }
                return rate;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "ConvertCurrancy_USD_To_INR_from_xe");
                decimal curval = HTLST.SelectCurrancyValue();
                if (curval > 0)
                    return curval;
                else
                    return 0;
            }
        }
        #endregion

        protected void TGAvailability()
        {
            TGHotels = objTGHotel.TGHotelSearchResponse(SearchQuery);
        }
        protected void GTAAvailability()
        {
            GTAHotels = objGTAHotel.GTAHotels(SearchQuery);
        }
        protected void RoomXMLAvailability()
        {
            RoomXMLHotels = objRoomXmlHotel.RoomXMLHotel(SearchQuery);
        }
    }
}
