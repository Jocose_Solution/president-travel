﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotelShared;

namespace HotelBAL
{
    public class TGRequestXML
    {
        public HotelSearch HotelSearchRequest(HotelSearch HtlSearchQuery)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(HtlSearchQuery.TGXmlHeader);
                ReqXml.Append(HtlSearchQuery.TGHotelSeachHeader);

                if (HtlSearchQuery.HotelName == "")
                {
                    ReqXml.Append("<Address>");
                    ReqXml.Append("<CityName>" + HtlSearchQuery.SearchCity + "</CityName>");
                    ReqXml.Append("<CountryName Code='" + HtlSearchQuery.Country + "'></CountryName>");
                    ReqXml.Append("</Address>");
                    ReqXml.Append("<HotelRef  /> ");
                }
                else
                    ReqXml.Append(" <HotelRef HotelCode='" + HtlSearchQuery.HotelName + "' />");
                //Single Hotel Search
                ReqXml.Append("<StayDateRange End='" + HtlSearchQuery.CheckOutDate + "' Start='" + HtlSearchQuery.CheckInDate + "'/>");

                //  ReqXml.Append("<RateRange MinRate='500' MinRate='1000' />");<HotelRef HotelCode="the-infantry-hotel-00001065"/>
                ReqXml.Append("<RoomStayCandidates>");
                for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
                {
                    ReqXml.Append("<RoomStayCandidate><GuestCounts>");
                    ReqXml.Append("<GuestCount AgeQualifyingCode='10' Count='" + HtlSearchQuery.AdtPerRoom[i].ToString() + "'/>");
                    if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount Age='" + HtlSearchQuery.ChdAge[i, j] + "' AgeQualifyingCode='8'/>");
                        }
                    }
                    ReqXml.Append("</GuestCounts></RoomStayCandidate>");
                }
                ReqXml.Append("</RoomStayCandidates>");
                if (HtlSearchQuery.HotelName == "")
                {
                        if (HtlSearchQuery.StarRating != "0")
                            ReqXml.Append("<Award Rating='" + Convert.ToString(HtlSearchQuery.StarRating) + "'/>");
                        else
                        {
                           // if (HtlSearchQuery.Availability_0S != "true")
                            //if (HtlSearchQuery.SearchCity != "AMRITSAR")
                            //{
                            //    ReqXml.Append("<Award Rating='1'/>");
                            //    ReqXml.Append("<Award Rating='2'/>");
                            //    ReqXml.Append("<Award Rating='3'/>");
                            //    ReqXml.Append("<Award Rating='4'/>");
                            //    ReqXml.Append("<Award Rating='5'/>");
                            //}
                        }  
                }
                ReqXml.Append("<TPA_Extensions>");
               // ReqXml.Append("<Pagination enabled='true' hotelsFrom='1' hotelsTo='50'/>");
                ReqXml.Append("<Pagination enabled='false' />");
                ReqXml.Append("<HotelBasicInformation><Reviews/></HotelBasicInformation>");
                ReqXml.Append("<UserAuthentication password='" + HtlSearchQuery.TGPassword + "' propertyId='" + HtlSearchQuery.TGPropertyId + "' username='" + HtlSearchQuery.TGUsername + "'/>");
                ReqXml.Append(HtlSearchQuery.TGHotelSeachFooter);
                ReqXml.Append(HtlSearchQuery.TGXmlFooter);
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelSearchRequest");
            }
            HtlSearchQuery.TG_HotelSearchReq = ReqXml.ToString();
            return HtlSearchQuery;
        }

        public HotelBooking ProvosinalBookingRequest(HotelBooking BookingDetals)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(BookingDetals.TGXmlHeader);
                ReqXml.Append(BookingDetals.TGHotelBookingHeader.Replace("Orderids", BookingDetals.Orderid));
                ReqXml.Append("<RequestorID MessagePassword='" + BookingDetals.TGPassword + "' ID='" + BookingDetals.TGPropertyId + "' >");
                ReqXml.Append("<CompanyName Code='" + BookingDetals.TGUsername + "'></CompanyName>");
                ReqXml.Append("</RequestorID></Source></POS><UniqueID Type='' ID=''/>");
                //Single Hotel Search >
                ReqXml.Append(" <HotelReservations><HotelReservation><RoomStays><RoomStay><RoomTypes>");
                ReqXml.Append("<RoomType NumberOfUnits='" + BookingDetals.NoofRoom + "' RoomTypeCode='" + BookingDetals.RoomTypeCode + "'/></RoomTypes>");
                ReqXml.Append(" <RatePlans><RatePlan RatePlanCode='" + BookingDetals.RoomPlanCode + "' /> </RatePlans>");
                ReqXml.Append(" <GuestCounts IsPerRoom='false'> ");

                for (int i = 0; i < Convert.ToInt32(BookingDetals.NoofRoom); i++)
                {
                    ReqXml.Append("<GuestCount ResGuestRPH='" + i + "' AgeQualifyingCode='10' Count='" + BookingDetals.AdtPerRoom[i].ToString() + "' />");

                    if (Convert.ToInt32(Convert.ToInt32(BookingDetals.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(BookingDetals.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount ResGuestRPH='" + i + "' AgeQualifyingCode='8' Age='" + BookingDetals.ChdAge[i, j] + "' Count='1' />");
                        }
                    }
                }
                ReqXml.Append("</GuestCounts>");


                ReqXml.Append("<TimeSpan End='" + BookingDetals.CheckOutDate + "' Start='" + BookingDetals.CheckInDate + "'  />");
                ReqXml.Append("<Total AmountBeforeTax='" + BookingDetals.AmountBeforeTax + "' CurrencyCode='INR' >");

                ReqXml.Append("<Taxes Amount='" + BookingDetals.Taxes + "'/>");
                ReqXml.Append("</Total>");
                ReqXml.Append("<BasicPropertyInfo HotelCode='" + BookingDetals.HtlCode + "'/>");

                ReqXml.Append("</RoomStay></RoomStays><ResGuests><ResGuest><Profiles><ProfileInfo><Profile ProfileType='1'>");
                ReqXml.Append("<Customer><PersonName>");
                ReqXml.Append("<NamePrefix>" + BookingDetals.PGTitle + "</NamePrefix>");
                ReqXml.Append("<GivenName>" + BookingDetals.PGFirstName + "</GivenName>");
                ReqXml.Append("<MiddleName></MiddleName>");
                ReqXml.Append("<Surname>" + BookingDetals.PGLastName + "</Surname>");
                ReqXml.Append("</PersonName>");

                ReqXml.Append("<Telephone AreaCityCode='" + BookingDetals.PGContact.Substring(1, 3) + "' CountryAccessCode='91' Extension='0' PhoneNumber='" + BookingDetals.PGContact + "' PhoneTechType='1' />");
                ReqXml.Append("<Email>" + BookingDetals.PGEmail + "</Email>");

                ReqXml.Append("<Address>");
                ReqXml.Append("<AddressLine>" + BookingDetals.PGAddress + "</AddressLine>");
                ReqXml.Append("<CityName>" + BookingDetals.PGCity + "</CityName>");
                ReqXml.Append("<PostalCode>" + BookingDetals.PGPin + "</PostalCode>");
                ReqXml.Append("<StateProv>" + BookingDetals.PGState + "</StateProv>");
                ReqXml.Append("<CountryName>" + BookingDetals.PGCountry + "</CountryName>");
                ReqXml.Append("</Address></Customer></Profile></ProfileInfo></Profiles></ResGuest></ResGuests>");
                ReqXml.Append(BookingDetals.TGXmlFooter);
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            BookingDetals.ProBookingReq = ReqXml.ToString();
            return BookingDetals;
        }

        public HotelBooking BookingRequest(HotelBooking BookingDetals)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(BookingDetals.TGXmlHeader);
                ReqXml.Append(BookingDetals.TGHotelBookingHeader.Replace("Orderids", BookingDetals.Orderid));
                ReqXml.Append("<RequestorID MessagePassword='" + BookingDetals.TGPassword + "' ID='" + BookingDetals.TGPropertyId + "' >");
                ReqXml.Append("<CompanyName Code='" + BookingDetals.TGUsername + "'></CompanyName>");
                ReqXml.Append("</RequestorID></Source></POS><UniqueID Type='23' ID='" + BookingDetals.ProBookingID + "' />");
                ReqXml.Append("<HotelReservations><HotelReservation>");
                ReqXml.Append(BookingDetals.TGXmlFooter);
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            BookingDetals.BookingConfReq = ReqXml.ToString();
            return BookingDetals;
        }

        public HotelCancellation CancellationRequest(HotelCancellation BookingDetals)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<?xml version='1.0' encoding='utf-8' ?><soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns='http://www.opentravel.org/OTA/2003/05'> <soapenv:Header/> <soapenv:Body>");
                ReqXml.Append("<ns:OTA_CancelRQ CancelType='Cancel' Version='1.0'> <ns:POS> <ns:Source>");
                ReqXml.Append("<ns:RequestorID ID='" + BookingDetals.Can_PropertyId + "' MessagePassword='" + BookingDetals.Can_Password + "' />");
                ReqXml.Append("</ns:Source> </ns:POS><ns:UniqueID Type='14' ID='" + BookingDetals.BookingID + "' />");
                ReqXml.Append("<ns:Verification>");
                ReqXml.Append("<ns:PersonName><ns:Surname>" + BookingDetals.PGLastName + "</ns:Surname></ns:PersonName>");
                ReqXml.Append("<ns:Email>" + BookingDetals.PGEmail + "</ns:Email>");
                ReqXml.Append("</ns:Verification> <ns:TPA_Extensions>");
                ReqXml.Append("<ns:CancelDates>");

                string FromDate = "",ToDate = "";
               // string[] StartDate = BookingDetals.CheckInDate.Split('-');
               // FromDate = StartDate[2] + "-" + StartDate[1] + "-" + StartDate[0] + " " + "12:00:00.000";
                System.DateTime checkinDate = Convert.ToDateTime(BookingDetals.CheckInDate + " 12:00:00.000");

                //string[] EndDate = BookingDetals.CheckOutDate.Split('/');
                //ToDate = EndDate[2] + "/" + EndDate[1] + "/" + EndDate[0];
                //System.DateTime checkout = Convert.ToDateTime(ToDate);

               // BookingDetals.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                //checkinDate = checkinDate.AddDays(BookingDetals.NoofNight);
                //BookingDetals.CheckInDate = checkinDate.ToString("yyyy-MM-dd");
                //BookingDetals.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                switch(BookingDetals.CancellationType)
                {
                    case "ALL" :
                        ReqXml.Append("<ns:Dates>" + BookingDetals.CheckInDate + "</ns:Dates>");
                        for (int i = 1; i < BookingDetals.NoofNight; i++)
                        {
                            checkinDate = checkinDate.AddDays(1);
                            ReqXml.Append("<ns:Dates>" + checkinDate.ToString("yyyy-MM-dd") + "</ns:Dates>");
                        }
                        break;

                    case "CheckIN" :
                        ReqXml.Append("<ns:Dates>" + BookingDetals.CheckInDate + "</ns:Dates>");
                        for (int i = 0; i < BookingDetals.NightForCancel; i++)
                        {
                            checkinDate = checkinDate.AddDays(1);
                            ReqXml.Append("<ns:Dates>" + checkinDate.ToString("yyyy-MM-dd") + "</ns:Dates>");
                        }
                        break;

                    case "CheckOut" :
                        string[] EndDate = BookingDetals.CheckOutDate.Split('/');
                        ToDate = EndDate[2] + "/" + EndDate[1] + "/" + EndDate[0];
                        System.DateTime checkout = Convert.ToDateTime(ToDate);
                        checkout = checkout.AddDays(- BookingDetals.NightForCancel);
                        for (int i = 0; i < BookingDetals.NightForCancel; i++)
                        {
                            checkout = checkout.AddDays(1);
                            ReqXml.Append("<ns:Dates>" + checkout.ToString("yyyy-MM-dd") + "</ns:Dates>");
                        }
                        break;
                }
                ReqXml.Append("</ns:CancelDates>");
                ReqXml.Append("</ns:TPA_Extensions> </ns:OTA_CancelRQ> </soapenv:Body> </soapenv:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "CancellationRequest");
            }
            BookingDetals.BookingCancelReq = ReqXml.ToString();
            return BookingDetals;
        }

        public HotelSearch Promotion_HotelSearchRequest(HotelSearch HtlSearchQuery)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append(HtlSearchQuery.TGXmlHeader);
                //"<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='DEALS' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'>";
                ReqXml.Append("<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='" + HtlSearchQuery.Sorting + "' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'>");
                ReqXml.Append("<AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria><Criterion>");
                ReqXml.Append("<Address>");
                ReqXml.Append("<CityName>" + HtlSearchQuery.SearchCity + "</CityName>");
                ReqXml.Append("<CountryName Code='" + HtlSearchQuery.Country + "'></CountryName>");
                ReqXml.Append("</Address>");
                ReqXml.Append("<HotelRef  /> ");
                ReqXml.Append("<StayDateRange End='" + HtlSearchQuery.CheckOutDate + "' Start='" + HtlSearchQuery.CheckInDate + "'/>");
                //ReqXml.Append("<RateRange MinRate='1000' />");
                ReqXml.Append("<RoomStayCandidates>");
                for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
                {
                    ReqXml.Append("<RoomStayCandidate><GuestCounts>");
                    ReqXml.Append("<GuestCount AgeQualifyingCode='10' Count='" + HtlSearchQuery.AdtPerRoom[i].ToString() + "'/>");
                    if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount Age='" + HtlSearchQuery.ChdAge[i, j] + "' AgeQualifyingCode='8'/>");
                        }
                    }
                    ReqXml.Append("</GuestCounts></RoomStayCandidate>");
                }
                ReqXml.Append("</RoomStayCandidates>");
                if (HtlSearchQuery.StarRating != "0")
                    ReqXml.Append("<Award Rating='" + Convert.ToString(HtlSearchQuery.StarRating) + "'/>");
                //ReqXml.Append("<Award Rating='3' />");
                ReqXml.Append("<TPA_Extensions>");
                ReqXml.Append("<Pagination enabled='true' hotelsFrom='01' hotelsTo='05'/>");
                ReqXml.Append("<HotelBasicInformation><Reviews/></HotelBasicInformation>");
                ReqXml.Append("<UserAuthentication password='" + HtlSearchQuery.TGPassword + "' propertyId='" + HtlSearchQuery.TGPropertyId + "' username='" + HtlSearchQuery.TGUsername + "'/>");
                ReqXml.Append(HtlSearchQuery.TGHotelSeachFooter);
                ReqXml.Append(HtlSearchQuery.TGXmlFooter);
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "Promotion_HotelSearchRequest");
            }
            HtlSearchQuery.TG_HotelSearchReq = ReqXml.ToString();
            return HtlSearchQuery;
        }
    }
}
